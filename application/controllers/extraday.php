<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Extraday extends CI_Controller {

	public function __construct() {
        parent::__construct();        
        $this->load->model('accounts');
        $this->load->model('items');
        $this->load->model('accounts');
        $this->load->model('departments');
        $this->load->model('staffdepartments');
        $this->load->model('salesmen');
        $this->load->model('extradays');
        $this->load->model('ledgers');
    }

    public function index() {
        unauth_secure();
        $data['modules'] = array('inventory/addextraday');
        $data['parties'] = $this->accounts->fetchAll();
        $data['accounts'] = $this->accounts->getAllParties('');
        $data['departments'] = $this->departments->fetchAllDepartments();
        $data['staffdepartments'] = $this->staffdepartments->fetchAllDepartments();
        //$data['employee'] = $this->salesmen->fetchAll();
        $data['employee'] = $this->accounts->fetchAllEmployee();
        //$data['jobs'] = $this->jobs->fetchAllJobs('job_order');

        $this->load->view('template/header');
        $this->load->view('overtime/addextraday', $data);
        $this->load->view('template/mainnav');
        $this->load->view('template/footer', $data);
    }

    public function getMaxVrno() {
        $company_id = $_POST['company_id'];
        $result = $this->purchases->getMaxVrno('production',$company_id) + 1;
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    public function getMaxVrnoa() {
        $company_id = $_POST['company_id'];
        $result = $this->extradays->getMaxVrnoa('extradays',$company_id) + 1;
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    public function save() {

        if (isset($_POST)) {

            $stockmain = $_POST['stockmain'];
            $stockdetail = $_POST['stockdetail'];
            $ledger = $_POST['ledger'];
            $vrnoa = $_POST['vrnoa'];
            $voucher_type_hidden = $_POST['voucher_type'];

            if($_POST['voucher_type'] == 'new'){

                $result = $this->extradays->getMaxVrnoa('extradays',$stockmain['company_id']) + 1;
            }

            $result = $this->ledgers->save($ledger, $vrnoa, 'extradays',$voucher_type_hidden);
            $result = $this->extradays->save($stockmain, $stockdetail, $vrnoa, 'extradays');

            $response = array();
            if ( $result === false ) {
                $response['error'] = 'true';
            } else {
                $response = $result;
            }

            $this->output
                 ->set_content_type('application/json')
                 ->set_output(json_encode($response));
        }
    }

    public function fetch() {

        if (isset( $_POST )) {
            $vrnoa = $_POST['vrnoa'];
            $company_id = $_POST['company_id'];
            $result = $this->extradays->fetch($vrnoa, 'extradays',$company_id);
            $response = "";
            if ( $result === false ) {
                $response = 'false';
            } else {
                $response = $result;
            }
            $this->output
                 ->set_content_type('application/json')
                 ->set_output(json_encode($response));
        }
    }

    public function fetchAllEmployeeByDepAndGodown() {

        if (isset( $_POST )) {

            $crit = $_POST['crit'];
            $result = $this->accounts->fetchAllEmployeeByDepAndGodown($crit);
            $response = "";
            if ( $result === false ) {
                $response = 'false';
            } else {
                $response = $result;
            }
            $this->output
                 ->set_content_type('application/json')
                 ->set_output(json_encode($response));
        }
    }

    public function delete() {

        if (isset( $_POST )) {

            $vrnoa = $_POST['vrnoa'];
            $company_id = $_POST['company_id'];
            $result = $this->extradays->delete($vrnoa, 'extradays',$company_id);

            $response = "";
            if ( $result === false ) {
                $response = 'false';
            } else {
                $response = $result;
            }

            $this->output
                 ->set_content_type('application/json')
                 ->set_output(json_encode($response));
        }
    }
}

/* End of file production.php */
/* Location: ./application/controllers/production.php */