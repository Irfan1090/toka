<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Wall extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('companies');
        $this->load->model('payments');
        $this->load->model('items');
        $this->load->model('accounts');
        $this->load->model('sales');
        $this->load->model('purchases');
        $this->load->model('walls');
        $this->load->model('ledgers');
    }

    public function index()
    {
        // Redirect the user if not logged in
        unauth_secure();
        $data['modules'] = array('company_feed', 'general1');
        $data['wrapper_class'] = 'company_feed';
        $data['body_class'] = 'company_feed';
        $data['parties'] = $this->accounts->fetchAll();
        $data['items'] = $this->items->fetchAllItems();

        $company_id = $this->session->userdata('company_id');

        $data['feed'] = $this->walls->getFeed($this->session->userdata('company_id'), 0);
        $data['company_info'] = $this->companies->fetchCompany($company_id);
        $data['netsale'] = $this->walls->fetchNetSum($company_id);
        $data['netpurchase'] = $this->walls->fetchNetSumPurchase($company_id);
        $data['cashinhand'] = $this->walls->fetchNetCashInHand($company_id);

        $this->load->view('template/header', $data);
        $this->load->view('template/mainnav', $data);
        $this->load->view('companyfeed', $data);
        $this->load->view('template/footer');
    }

    public function getFeedMessage($feedItem)
    {
        $message = '';
        if ($feedItem['etype'] === 'purchase'):
            $message = '<strong class="feeditem_amount">' . $feedItem['namount'] . '</strong> has been paid to <strong class="feeditem_party1">' . $feedItem['name'] . '</strong>!';
        elseif ($feedItem['etype'] === 'sale'):
            $message = '<strong class="feeditem_amount">' . $feedItem['namount'] . '</strong> has been received from <strong class="feeditem_party1">' . $feedItem['name'] . '</strong>!';
        elseif ($feedItem['etype'] === 'salereturn'):
            $message = '<strong class="feeditem_amount">' . $feedItem['namount'] . '</strong> has been paid to <strong class="feeditem_party1">' . $feedItem['name'] . '</strong>!';
        elseif ($feedItem['etype'] === 'purchasereturn'):
            $message = '<strong class="feeditem_amount">' . $feedItem['namount'] . '</strong> has been received from <strong class="feeditem_party1">' . $feedItem['name'] . '</strong>!';
        elseif ($feedItem['etype'] === 'purchaseorder'):
            $message = 'Order of <strong class="feeditem_amount">' . $feedItem['namount'] . '</strong> has been received from <strong class="feeditem_party1">' . $feedItem['name'] . '</strong>!';
        elseif ($feedItem['etype'] === 'saleorder'):
            $message = 'Order of <strong class="feeditem_amount">' . $feedItem['namount'] . '</strong> has been made to <strong class="feeditem_party1">' . $feedItem['name'] . '</strong>!';
        elseif ($feedItem['etype'] === 'saleq'):
            $message = 'Quotation of <strong class="feeditem_amount">' . $feedItem['namount'] . '</strong> has been made to <strong class="feeditem_party1">' . $feedItem['name'] . '</strong>!';
        elseif (($feedItem['etype'] === 'cpv') && ($feedItem['debit'] != 0)):
            $message = '<strong class="feeditem_amount">' . $feedItem['debit'] . '</strong> has been paid to <strong class="feeditem_party1">' . $feedItem['name'] . '</strong>!';
        elseif (($feedItem['etype'] === 'cpv') && ($feedItem['credit'] != 0)):
            $message = '<strong class="feeditem_amount">' . $feedItem['credit'] . '</strong> has been received from <strong class="feeditem_party1">' . $feedItem['name'] . '</strong>!';
        elseif (($feedItem['etype'] === 'crv') && ($feedItem['credit'] != 0)):
            $message = '<strong class="feeditem_amount">' . $feedItem['credit'] . '</strong> has been received from <strong class="feeditem_party1">' . $feedItem['name'] . '</strong>!';
        elseif (($feedItem['etype'] === 'crv') && ($feedItem['debit'] != 0)):
            $message = '<strong class="feeditem_amount">' . $feedItem['debit'] . '</strong> has been paid to <strong class="feeditem_party1">' . $feedItem['name'] . '</strong>!';
        elseif (($feedItem['etype'] === 'jv') && ($feedItem['debit'] != 0)):
            $message = '<strong class="feeditem_amount">' . $feedItem['debit'] . '</strong> has been paid to <strong class="feeditem_party1">' . $feedItem['name'] . '</strong>!';
        elseif (($feedItem['etype'] === 'jv') && ($feedItem['credit'] != 0)):
            $message = '<strong class="feeditem_amount">' . $feedItem['credit'] . '</strong> has been received from <strong class="feeditem_party1">' . $feedItem['name'] . '</strong>!';
        elseif ($feedItem['etype'] === 'pd_receive'):
            $message = 'Cheque of <strong class="feeditem_amount">' . $feedItem['namount'] . '</strong> has been received from <strong class="feeditem_party1">' . $feedItem['name'] . '</strong> into <strong>' . $feedItem['party2'] . '</strong>!';
        elseif ($feedItem['etype'] === 'pd_issue'):
            $message = 'Cheque of <strong class="feeditem_amount">' . $feedItem['namount'] . '</strong> has been paid to <strong class="feeditem_party1">' . $feedItem['name'] . '</strong> from <strong>' . $feedItem['party2'] . '</strong>!';
        endif;

        return $message;
    }

    public function getVrnoaLink($feedItem)
    {
        $link = '<a href="@vrlink@">' . $feedItem['vrnoa'] . '</a>';
        $vrlink = '#';
        if ($feedItem['etype'] === 'purchase'):
            $vrlink = base_url('index.php/purchase?vrnoa=' . $feedItem['vrnoa']);
        elseif ($feedItem['etype'] === 'sale'):
            $vrlink = base_url('index.php/sale?vrnoa=' . $feedItem['vrnoa']);
        elseif ($feedItem['etype'] === 'salereturn'):
            $vrlink = base_url('index.php/salereturn?vrnoa=' . $feedItem['vrnoa']);
        elseif ($feedItem['etype'] === 'purchasereturn'):
            $vrlink = base_url('index.php/purchasereturn?vrnoa=' . $feedItem['vrnoa']);
        elseif ($feedItem['etype'] === 'purchaseorder'):
            $vrlink = base_url('index.php/purchase/order?vrnoa=' . $feedItem['vrnoa']);
        elseif ($feedItem['etype'] === 'saleorder'):
            $vrlink = base_url('index.php/sale/order?vrnoa=' . $feedItem['vrnoa']);
        elseif ($feedItem['etype'] === 'saleq'):
            $vrlink = base_url('index.php/sale/quotation?vrnoa=' . $feedItem['vrnoa']);
        elseif (($feedItem['etype'] === 'cpv') && ($feedItem['debit'] != 0)):
            $vrlink = base_url('index.php/payment?etype=cpv&vrnoa=' . $feedItem['vrnoa']);
        elseif (($feedItem['etype'] === 'cpv') && ($feedItem['credit'] != 0)):
            $vrlink = base_url('index.php/payment?etype=cpv&vrnoa=' . $feedItem['vrnoa']);
        elseif (($feedItem['etype'] === 'crv') && ($feedItem['credit'] != 0)):
            $vrlink = base_url('index.php/payment?etype=crv&vrnoa=' . $feedItem['vrnoa']);
        elseif (($feedItem['etype'] === 'crv') && ($feedItem['debit'] != 0)):
            $vrlink = base_url('index.php/payment?etype=crv&vrnoa=' . $feedItem['vrnoa']);
        elseif (($feedItem['etype'] === 'jv') && ($feedItem['debit'] != 0)):
            $vrlink = base_url('index.php/journal?etype=jv&vrnoa=' . $feedItem['vrnoa']);
        elseif (($feedItem['etype'] === 'jv') && ($feedItem['credit'] != 0)):
            $vrlink = base_url('index.php/journal?etype=jv&vrnoa=' . $feedItem['vrnoa']);
        elseif ($feedItem['etype'] === 'pd_receive'):
            $vrlink = base_url('index.php/payment/chequeReceive?vrnoa=' . $feedItem['vrnoa']);
        elseif ($feedItem['etype'] === 'pd_issue'):
            $vrlink = base_url('index.php/payment/chequeIssue?vrnoa=' . $feedItem['vrnoa']);
        endif;

        $link = str_replace('@vrlink@', $vrlink, $link);
        return $link;
    }

    public function fetchPartyBalances()
    {
        $to = $_POST['to'];
        $party_id = $_POST['party_id'];
        $company_id = $_POST['company_id'];

        $result = array();
        $result['closing'] = $this->walls->fetchPartyClosingBalance($to, $party_id, $company_id);
        $result['opening'] = $this->walls->fetchPartyOpeningBalance($to, $party_id, $company_id);
        $result['debit'] = $this->walls->fetchPartyNetDebit($to, $party_id, $company_id);
        $result['credit'] = $this->walls->fetchPartyNetCredit($to, $party_id, $company_id);

        $json = json_encode($result);
        echo $json;
    }

    public function getFeed()
    {
        $company_id = $_POST['company_id'];
        $page = $_POST['page'];

        $feed = $this->walls->getFeed($company_id, $page);
        $json = json_encode($feed);
        echo $json;
    }

    public function fetchItemStockValues()
    {
        unauth_secure();

        $to = $_POST['to'];
        $item_id = $_POST['item_id'];
        $company_id = $_POST['company_id'];

        $result = array();
        $result['closing'] = $this->walls->fetchItemClosingStock($to, $item_id, $company_id);
        $result['opening'] = $this->walls->fetchItemOpeningStock($to, $item_id, $company_id);
        $result['instock'] = $this->walls->fetchItemInStock($to, $item_id, $company_id);
        $result['outstock'] = $this->walls->fetchItemOutStock($to, $item_id, $company_id);

        $json = json_encode($result);
        echo $json;
    }

    public function dashboard()
    {

        $company = $this->session->userdata('company_id');
        $data['modules'] = array('dashboard1');

        $data['openingstocks'] = $this->sales->fetchAllSales($company,'openingstock');
        $data['purchaseOrderss'] = $this->purchases->fetchAllPurchases($company,'pur_order');
        $data['purchases'] = $this->sales->fetchAllSales($company,'purchase');
        $data['purchasesreturn'] = $this->sales->fetchAllSales($company,'purchasereturn');
        $data['quotationss'] = $this->sales->fetchAllSales($company,'quotationorder');
        $data['sales'] = $this->sales->fetchAllSales($company,'sale');
        $data['gatepasses'] = $this->sales->fetchAllGatepass($company,'order_loading');
        $data['inwardss'] = $this->sales->fetchAllGatepass($company,'inwardvoucher');
        $data['salereturns'] =$this->purchases->fetchAllPurchases($company,'salereturn');
        $data['itemconversionss'] = $this->purchases->fetchAllPurchases($company,'item_conversion');
        $data['consumptionss'] = $this->sales->fetchAllConsumption($company,'consumption');
        $data['saleorderss'] = $this->sales->fetchAllSales($company,'sale_order');
        $data['productions'] = $this->sales->fetchAllProductions($company,'production');
        $data['materialreturnss'] = $this->sales->fetchAllMaterial($company,'materialreturn');
        $data['requisitionss'] = $this->sales->fetchAllSales($company,'requisition');
        $data['paymentss'] = $this->ledgers->fetchAllLedgersPayments($company,'cpv');
        $data['receiptss'] = $this->ledgers->fetchAllLedgersPayments($company,'crv');
        $data['chqissuess'] = $this->ledgers->fetchAllLedgersPayments($company,'pd_issue');
        $data['jvss'] = $this->ledgers->fetchAllLedgersPayments($company,'jv');
        $data['openingbalancess'] = $this->ledgers->fetchAllLedgersPayments($company,'openingbalance');
        $data['chqreceiptss'] = $this->ledgers->fetchAllLedgersPayments($company,'pd_receive');
        $data['penaltys'] = $this->ledgers->fetchAllPenaltys($company,'penalty');
        $data['loans'] = $this->ledgers->fetchAllPenaltys($company,'loan');
        $data['advances'] = $this->ledgers->fetchAllPenaltys($company,'advance');
        $data['incentives'] = $this->ledgers->fetchAllPenaltys($company,'incentive');

        //net amounts
        $data['totalpurchases'] = $this->purchases->fetchTotalPurchases('purchase');
        $data['totalpurchasereturns'] = $this->purchases->fetchTotalPurchases('purchasereturn');
        $data['totalopeningstocks'] = $this->purchases->fetchTotalPurchases('openingstock');
        $data['totalquotationorders'] = $this->sales->fetchTotalSales('quotationorder');
        $data['totalsaleorders'] = $this->sales->fetchTotalSales('saleorder');
        $data['totalsales'] = $this->sales->fetchTotalSales('sale');
        $data['totalsalereturns'] = $this->sales->fetchTotalSales('salereturn');
        $data['totalcashpayments'] = $this->ledgers->fetchTotalPayments('cpv');
        $data['totalcashreceipts'] = $this->ledgers->fetchTotalPayments('crv');
        $data['totalbankpayments'] = $this->ledgers->fetchTotalPayments('pd_issue');
        $data['totalbankreceipts'] = $this->ledgers->fetchTotalPayments('pd_receive');
        $data['totaljvs'] = $this->ledgers->fetchTotalPayments('jv');
        $data['totalopeningbalances'] = $this->ledgers->fetchTotalPayments('openingbalance');
        $data['totalpenaltys'] = $this->ledgers->fetchTotalPayments('penalty');
        $data['totalloans'] = $this->ledgers->fetchTotalPayments('loan');
        $data['totaladvances'] = $this->ledgers->fetchTotalPayments('advance');
        $data['totalincentives'] = $this->ledgers->fetchTotalPayments('incentive');

        $this->load->view('template/header', $data);
        $this->load->view('template/mainnav', $data);
        $this->load->view('user/dashbordnew', $data);
        $this->load->view('template/footer',$data);
        
    }

}