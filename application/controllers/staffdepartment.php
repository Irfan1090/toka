<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Staffdepartment extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('staffdepartments');
		$this->load->model('accounts');
	}

	public function index() {
		redirect('staffdepartment/add');
	}

	public function add() {
		$data['modules'] = array('setup/addstaffdepartment');
		$data['staffdepartments'] = $this->staffdepartments->fetchAllDepartments();
		$data['salary_accounts'] = $this->accounts->fetchAll(1);
		$this->load->view('template/header');
		$this->load->view('setup/addstaffdepartment', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function getMaxDepartmentId() {
		$result = $this->staffdepartments->getMaxDepartmentId() + 1;
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function saveDepartment() {

		if (isset($_POST)) {

			$department = $_POST['department'];
			$result = $this->staffdepartments->saveDepartment( $department );

			$response = array();
			if ($result === false) {
				$response['error'] = true;
			} else {
				$response['error'] = false;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function fetchDepartment() {

		if (isset( $_POST )) {

			$deptid = $_POST['deptid'];
			$result = $this->staffdepartments->fetchDepartment($deptid);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function fetchAllDepartments() {

		$result = $this->staffdepartments->fetchAllDepartments();

		$response = array();
		if ( $result === false ) {
			$response = 'false';
		} else {			
			$response = $result;
		}

		$this->output
			 ->set_content_type('application/json')
			 ->set_output(json_encode($response));
	}
}

/* End of file department.php */
/* Location: ./application/controllers/department.php */