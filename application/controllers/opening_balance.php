<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Opening_balance extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('accounts');
        $this->load->model('salesmen');
        $this->load->model('transporters');
        $this->load->model('items');
        $this->load->model('ledgers');
        $this->load->model('departments');
        $this->load->model('sales');
        $this->load->model('purchases');
        $this->load->model('users');
        $this->load->model('levels');
        $this->load->model('orders');
    }

    public function index()
    {

        $data['modules'] = array('accounts/addopeningbalance');
        $data['l3s'] = $this->levels->fetchAllLevel3();
        $data['accounts'] = $this->accounts->fetchAll();
        $data['userone'] = $this->users->fetchAll();


        $this->load->view('template/header');
        $this->load->view('accounts/addopeningbalance', $data);
        $this->load->view('template/mainnav');
        $this->load->view('template/footer', $data);
    }

    public function add()
    {

    }

    public function getMaxId()
    {
        $company_id = $_POST['company_id'];
        $maxId = $this->ledgers->getMaxId('openingbalance', $company_id) + 1;
        $this->output->set_content_type('application/json')->set_output(json_encode($maxId));
    }

    public function save()
    {
        $saveObj = json_decode($_POST['saveObj'], true);
        $dcno = $_POST['dcno'];
        $etype = $_POST['etype'];
        $voucher_type_hidden = $_POST['voucher_type_hidden'];
        $result = $this->ledgers->save($saveObj, $dcno, $etype, $voucher_type_hidden);
        $response = array();
        if ($result === false)
        {
            $response['error'] = 'true';
        }
        else
        {
            $response['error'] = 'false';
        }
        $this->output
                 ->set_content_type('application/json')
                 ->set_output(json_encode($response));
    }

    public function fetch()
    {
        $dcno = $_POST['dcno'];
        $company_id = $_POST['company_id'];
        $result = $this->ledgers->fetch($dcno, 'openingbalance', $company_id);

        $response = "";
        if ($result === false)
        {
            $response = 'false';
        }
        else
        {
            $response = $result;
        }
        $this->output
                 ->set_content_type('application/json')
                 ->set_output(json_encode($response));
    }

    public function deleteVoucher()
    {
        $dcno = $_POST['dcno'];
        $company_id = $_POST['company_id'];
        $result = $this->ledgers->deleteVoucher($dcno, 'openingbalance', $company_id);

        $response = "";
        if ($result === false)
        {
            $response = 'false';
        }
        else
        {
            $response = 'true';
        }
        $this->output
                 ->set_content_type('application/json')
                 ->set_output(json_encode($response));
    }

    public function fetchVoucherRange()
    {
        $from = $_POST['from'];
        $to = $_POST['to'];

        $result = $this->ledgers->fetchVoucherRange($from, $to, 'openingbalance');
        $this->output
                 ->set_content_type('application/json')
                 ->set_output(json_encode($result));
    }
}
