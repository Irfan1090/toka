<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function save($sal_calc) {
		$this->db->query('truncate table setting');
		$this->db->insert('setting', array('sal_cal' => $sal_calc));
		return true;
	}

	public function get() {
		$result = $this->db->get('setting');
		if ($result->num_rows() > 0) {			
			$result = $result->row_array();
			return $result['sal_cal'];
		}
	}

    public function getPayrollSetting($type)
    {
        $result = $this->db->get('payroll_setting');
        $result = $result->row_array();

        if (count($result) > 0) {
            if($type == "penalty")
            {
                return $result['penalty_party_id_effect'];
            }
            else if($type == "loan")
            {
                return $result['loan_party_id_effect'];
            }
            else if($type == "advance")
            {
                return $result['advance_party_id_effect'];
            }
            else if($type == "incentive")
            {
                return $result['incentive_party_id_effect'];
            }
        }
    }
}

/* End of file settings.php */
/* Location: ./application/models/settings.php */