<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	/**
	* Users model to handle all the users related tasks
	*/
	class Userrights extends CI_Model
	{
		function __construct()
		{
			parent::__construct();
		}

		public function getMaxId()
		{
			$this->db->select_max('company_id');
			$query = $this->db->get('company');
			
			$result = $query->result_array();

			return ($result[0]['company_id'] === null) ? 0 : $result[0]['company_id'];
		}

		public function getAll()
		{
			$query = $this->db->get('company');
			return $query->result_array();
		}

        public function save($userId, $rights)
        {
            $rightArray = json_decode($rights, true);
            $this->db->query('delete from user_level2_rights where uid = "' . $userId . '"');

            foreach($rightArray as $row)
            {
                $this->db->insert('user_level2_rights', $row);
            }
            return true;
        }

		public function updateCompany( $company_data )
		{
			$this->db->where('company_id',$company_data['company_id']);
			$q = $this->db->get('company');

			unset($company_data['img']);

			if ( $q->num_rows() > 0 ) {
				$this->db->where('company_id',$company_data['company_id']);
				$this->db->update('company',$company_data);

				$rowCount = $this->db->affected_rows();
		
				if ( $rowCount !== 0 ) {
					$this->session->set_flashdata('update_success', 'true');
					return true;
				}
				else {
					$this->session->set_flashdata('update_success', 'false');	
					return false;
				}
			}
			else {
				$this->db->insert('company',$company_data);

				$rowCount = $this->db->affected_rows();
		
				if ( $rowCount !== 0 ) {
					$this->session->set_flashdata('insert_success', 'true');
					return true;
				}
				else {
					$this->session->set_flashdata('insert_success', 'false');	
					return false;
				}
			}
		}

		public function fetchAllUsers() {

			$result = $this->db->get('user');

			if ( $result->num_rows() > 0 ) {
				return $result->result_array();
			} else {
				return false;
			}
		}

		public function fetchAllLevel1() {

			$result = $this->db->get('level1');

			if ( $result->num_rows() > 0 ) {
				return $result->result_array();
			} else {
				return false;
			}
		}

		public function fetchLevel2($userId, $l1) {

            $where = "";
            if($l1 != "")
            {
                $where = "where level1.l1 = '" . $l1 . "' ";
            }

			// $result = $this->db->query("SELECT level1.l1, level1.name as l1name, level2.l2, level2.name as l2name
			// 							from level2 
			// 							inner join level1 on level1.l1 = level2.l1
   //                                      " . $where . " ");

            $result = $this->db->query("SELECT level1.l1, level1.name AS l1name, level2.l2, level2.name AS l2name, level3.l3, level3.name as l3name
										FROM level3
										inner join level2 on level2.l2 = level3.l2
										INNER JOIN level1 ON level1.l1 = level2.l1
										 " . $where . " ");

			if ($result->num_rows() > 0)
            {
                $res = $result->result_array();
                $data = array();
                foreach($res as $row)
                {
                    //$status = $this->db->query("select * from user_level2_rights where l2_id = '" . $row['l2'] . "' and uid = '" . $userId . "' ");
                    $status = $this->db->query("select * from user_level2_rights where l2_id = '" . $row['l3'] . "' and uid = '" . $userId . "' ");

                    $data[] = array(
                        "l1" => $row['l1'],
                        "l1name" => $row['l1name'],
                        "l2" => $row['l2'],
                        "l2name" => $row['l2name'],
                        "l3" => $row['l3'],
                        "l3name" => $row['l3name'],
                        "status" => ($status->num_rows() > 0)? '1':'0',
                        );
                }
				return $data;
			}
            else
            {
				return false;
			}
		}

		public function fetchCompany($company_id)
		{
			$company_id = $this->db->escape_str($company_id);
			$result = $this->db->get_where('company', array('company_id' => $company_id));

			return $result->result_array();
		}
	}
 ?>