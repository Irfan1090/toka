<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Itemphaserecepies extends CI_Model {

	public function __construct() {
		parent::__construct();
	}	

	public function getMaxVrnoa($etype,$company_id) {

		$result = $this->db->query("SELECT MAX(vrnoa) vrnoa FROM recipemain WHERE etype = '". $etype ."' and company_id=". $company_id ." ");
		$row = $result->row_array();
		$maxId = $row['vrnoa'];

		return $maxId;
	}

	public function getMaxVrno($etype,$company_id) {

		$result = $this->db->query("SELECT MAX(vrno) vrno FROM recipemain WHERE etype = '". $etype ."' AND company_id=". $company_id ."  AND DATE(vrdate) = DATE(NOW())");
		$row = $result->row_array();
		$maxId = $row['vrno'];

		return $maxId;
	}

	public function save( $recipemain, $recipedetail, $vrnoa, $etype )
    {
        $company_id = $recipemain['company_id'];
        //$godown_id = $recipedetail[0]['godown_id'];
		$this->db->where(array('vrnoa' => $vrnoa, 'etype' => $etype,'company_id'=>$recipemain['company_id'] ));
		$result = $this->db->get('recipemain');

		$rid = "";
		if ($result->num_rows() > 0) {
			$result = $result->row_array();
			$rid = $result['rid'];
			$this->db->where(array('vrnoa' => $vrnoa, 'etype' => $etype ,'company_id'=>$recipemain['company_id']));
			$affect = $this->db->update('recipemain', $recipemain);

			$this->db->where(array('rid' => $rid ));
			$this->db->delete('recipedetail');
		} else {
			$this->db->insert('recipemain', $recipemain);
			$rid = $this->db->insert_id();
			$affect = $this->db->affected_rows();
		}

		foreach ($recipedetail as $detail) {
			$detail['rid'] = $rid;
			$this->db->insert('recipedetail', $detail);
		}

        if ($affect === 0) {
			return false;
		} else {
			return true;
		}
	}


	public function fetch( $vrnoa, $etype,$company_id ) {

		$result = $this->db->query("SELECT m.vrnoa,m.vrno,m.vrdate,m.etype,m.itemfrom,m.itemto,phase.name AS phase,d.phase_id,d.phase_number,m.company_id,m.uid
									FROM recipemain AS m
									INNER JOIN recipedetail AS d ON m.rid = d.rid
									INNER JOIN phase ON d.phase_id = phase.id
									WHERE m.vrnoa='".$vrnoa."' AND m.etype='".$etype."' AND m.company_id='".$company_id."'");
		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function delete( $vrnoa, $etype , $company_id) {

		$this->db->where(array('etype' => $etype, 'vrnoa' => $vrnoa , 'company_id' => $company_id ));
		$result = $this->db->get('recipemain');

		if ($result->num_rows() == 0) {
			return false;
		} else {

			$result = $result->row_array();
			$rid = $result['rid'];

			$this->db->where(array('etype' => $etype, 'vrnoa' => $vrnoa ));
			$this->db->delete('recipemain');
			$this->db->where(array('rid' => $rid ));
			$this->db->delete('recipedetail');

			return true;
		}
	}
}

/* End of file salesmen.php */
/* Location: ./application/models/salesmen.php */