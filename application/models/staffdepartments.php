<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Staffdepartments extends CI_Model {

	public function __construct() {
		parent::__construct();
	}	

	public function getMaxDepartmentId() {

		$this->db->select_max('deptid');
		$result = $this->db->get('staffdepartment');

		$row = $result->row_array();
		$maxId = $row['deptid'];

		return $maxId;
	}

	public function saveDepartment( $staffdepartment ) {

		$this->db->where(array('deptid' => $staffdepartment['deptid']));
		$result = $this->db->get('staffdepartment');

		$affect = 0;
		if ($result->num_rows() > 0) {

			$this->db->where(array('deptid' => $staffdepartment['deptid'] ));
			$result = $this->db->update('staffdepartment', $staffdepartment);
			$affect = $this->db->affected_rows();
		} else {

			unset($staffdepartment['deptid']);
			$result = $this->db->insert('staffdepartment', $staffdepartment);
			$affect = $this->db->affected_rows();
		}

		if ($affect === 0) {
			return false;
		} else {
			return true;
		}
	}

	public function fetchDepartment( $deptid ) {

		$this->db->where(array('deptid' => $deptid));
		$result = $this->db->get('staffdepartment');
		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function fetchAllDepartments() {

		//$result = $this->db->get('staffdepartment');
		$dbQuery = 'SELECT deptid, staffdepartment.name, staffdepartment.uid, description, staffdepartment.pid, party.name AS party_name
					FROM staffdepartment
					LEFT JOIN party ON party.pid = staffdepartment.pid';
		$result = $this->db->query($dbQuery);
		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}
	
}

/* End of file Departments.php */
/* Location: ./application/models/Departments.php */