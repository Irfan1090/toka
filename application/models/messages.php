<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Messages extends CI_Model {

	public function getMaxId() {

		$this->db->select_max('message_id');
		$result = $this->db->get('message');
		$row = $result->row_array();
		$maxId = $row['message_id'];

		return $maxId;
	}

	public function save( $message ) {

		$this->db->where(array('message_id' => $message['message_id']));
		$result = $this->db->get('message');

		$affect = 0;
		if ($result->num_rows() > 0) {

			$this->db->where(array('message_id' => $message['message_id'] ));
			$result = $this->db->update('message', $message);
			$affect = $this->db->affected_rows();
		} else {

			unset($message['message_id']);
			$result = $this->db->insert('message', $message);
			$affect = $this->db->affected_rows();
		}

		if ($affect === 0) {
			return false;
		} else {
			return true;
		}
	}

	public function fetch( $message_id ) {

		$this->db->where(array('message_id' => $message_id));
		$result = $this->db->get('message');
		if ( $result->num_rows() > 0 ) {
			return $result->row_array();
		} else {
			return false;
		}
	}

	public function fetchAll() {

		$result = $this->db->get('message');
		return $result->result_array();
	}


	public function fetchPartyInfo($id) {

		$sql="SELECT city, country, mobile from party where pid = '$id';";

		$result=$this->db->query($sql);

		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}
}

/* End of file message.php */
/* Location: ./application/models/mesages.php */