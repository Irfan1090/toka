<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Currenceys extends CI_Model {

	public function __construct() {
		parent::__construct();
	}	

	public function getMaxCurrenceyId() {

		$this->db->select_max('id');
		$result = $this->db->get('currencey');

		$row = $result->row_array();
		$maxId = $row['id'];

		return $maxId;
	}

	public function saveCurrencey( $currencey ) {

		$this->db->where(array('id' => $currencey['id']));
		$result = $this->db->get('currencey');

		$affect = 0;
		if ($result->num_rows() > 0) {

			$this->db->where(array('id' => $currencey['id'] ));
			$result = $this->db->update('currencey', $currencey);
			$affect = $this->db->affected_rows();
		} else {

			unset($currencey['id']);
			$result = $this->db->insert('currencey', $currencey);
			$affect = $this->db->affected_rows();
		}

		if ($affect === 0) {
			return false;
		} else {
			return true;
		}
	}

	public function fetchCurrencey( $id ) {

		$this->db->where(array('id' => $id));
		$result = $this->db->get('currencey');
		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function fetchAllCurrencey() {

		$result = $this->db->get('currencey');

		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}
}

/* End of file currenceys.php */
/* Location: ./application/models/currenceys.php */