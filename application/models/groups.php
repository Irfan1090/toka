<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Groups extends CI_Model {

	public function __construct() {
		parent::__construct();
	}	

	public function getMaxGroupId() {

		$this->db->select_max('id');
		$result = $this->db->get('group');

		$row = $result->row_array();
		$maxId = $row['id'];

		return $maxId;
	}

	public function saveGroup( $group ) {

		$this->db->where(array('id' => $group['id']));
		$result = $this->db->get('group');

		$affect = 0;
		if ($result->num_rows() > 0) {

			$this->db->where(array('id' => $group['id'] ));
			$result = $this->db->update('group', $group);
			$affect = $this->db->affected_rows();
		} else {

			unset($group['id']);
			$result = $this->db->insert('group', $group);
			$affect = $this->db->affected_rows();
		}

		if ($affect === 0) {
			return false;
		} else {
			return true;
		}
	}

	public function fetchGroup( $id ) {

		$this->db->where(array('id' => $id));
		$result = $this->db->get('group');
		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function fetchAllGroups() {

		$result = $this->db->get('group');

		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}
}

/* End of file groups.php */
/* Location: ./application/models/Groups.php */