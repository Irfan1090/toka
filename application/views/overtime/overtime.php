<?php

	$desc = $this->session->userdata('desc');
	$desc = json_decode($desc);
	$desc = objectToArray($desc);

	$vouchers = $desc['vouchers'];
?>

<!-- main content -->
<div id="main_wrapper">

	<div class="page_bar">
		<div class="row">
			<div class="col-lg-3">
				<h1 class="page_title">Overtime</h1>
			</div>
			<div class="col-lg-9">
				<div class="pull-right">
					<a class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset</a>
					<a class="btn btn-default btnSave" data-insertbtn='<?php echo $vouchers['overtime']['insert']; ?>' data-updatebtn='<?php echo $vouchers['overtime']['update']; ?>' data-deletebtn='<?php echo $vouchers['overtime']['delete']; ?>' data-printbtn='<?php echo $vouchers['overtime']['print']; ?>'><i class="fa fa-save"></i> Save</a>
					<a class="btn btn-default btnDelete" data-deletetbtn='<?php echo $vouchers['overtime']['delete']; ?>'><i class="fa fa-trash-o"></i> Delete</a>
					
				</div>
			</div>
		</div>
	</div>

	<div class="page_content">
		<div class="container-fluid">

			<div class="col-md-12">
				<ul class="nav nav-tabs" id="tabs_a">
					<li class="active"><a data-toggle="tab" href="#add_overtime">Add Update Overtime</a></li>
					<li class=""><a data-toggle="tab" href="#view_all">View All</a></li>
				</ul>
				<div class="tab-content">
					<div id="add_overtime" class="tab-pane fade active in">

						<div class="row">
							<div class="col-lg-12">
								<div class="panel panel-default">
									<div class="panel-body">

										<form action="">

											<div class="row">
												<div class="col-lg-2">
													<div class="input-group">
														<span class="input-group-addon id-addon">Vr#</span>
														<input type="number" class="form-control num txtidupdate" data-txtidupdate='<?php echo $vouchers['overtime']['update']; ?>' id="txtId">
														<input type="hidden" id="txtMaxIdHidden">
														<input type="hidden" id="txtIdHidden">
														<input type="hidden" id="voucher_type_hidden">
													</div>
												</div>
												<div class="col-lg-2"></div>
												<div class="col-lg-3">
						                            <div class="input-group">
						                                <span class="input-group-addon fancy-addon">Date</span>
						                                <input class="form-control " type="date" id="cur_date" value="<?php echo date('Y-m-d'); ?>">
						                            </div>
						                        </div>
											</div>

											<div class="row">
												<div class="col-lg-4">
													<div class="input-group">
														<span class="input-group-addon txt-addon">Staff</span>
														<select class="form-control select2" id="staff_dropdown">
															<option value="" disabled="" selected="">Choose staff</option>
															<?php foreach ($staffs as $staff): ?>
																<option value="<?php echo $staff['staid']; ?>" data-did='<?php echo $staff['did']; ?>' data-dept_name='<?php echo $staff['dept_name']; ?>' data-fname='<?php echo $staff['fname']; ?>' data-name='<?php echo $staff['name']; ?>' data-type='<?php echo $staff['type']; ?>' data-shift_name='<?php echo $staff['shift_name']; ?>' data-shid='<?php echo $staff['shid']; ?>'><?php echo $staff['name']; ?></option>
															<?php endforeach ?>
														</select>
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-lg-2 hide">
													<div class="input-group">
														<span class="input-group-addon fancy-addon">Staff Id</span>
														<input type="text" class="form-control" id="txtStaffId" readonly="">
													</div>
												</div>
												<div class="col-lg-4">
													<div class="input-group">
														<span class="input-group-addon fancy-addon">Department</span>
														<input type="text" class="form-control" id="txtDept" readonly="">
													</div>
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<span class="input-group-addon fancy-addon">S/O</span>
														<input type="text" class="form-control" id="txtso" readonly="">
													</div>
												</div>
												<div class="col-lg-3">
													<div class="input-group">
														<span class="input-group-addon fancy-addon">Designation</span>
														<input type="text" class="form-control" id="txtDesignation" readonly="">
													</div>
												</div>
												<div class="col-lg-2">
													<div class="input-group">
														<span class="input-group-addon fancy-addon">Shift</span>
														<input type="text" class="form-control" id="txtShift" readonly="">
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-lg-4">
													<div class="input-group">
														<span class="input-group-addon txt-addon">Approved By</span>
														<input type="text" class="form-control" id="txtApprovedBy">
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-lg-4">
													<div class="input-group">
														<span class="input-group-addon txt-addon">Reason</span>
														<input type="text" class="form-control" id="txtReason">
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-lg-4">
													<div class="input-group">
														<span class="input-group-addon txt-addon">Remarks</span>
														<input type="text" class="form-control" id="txtRemarks">
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-lg-4">
													<div class="input-group">
														<span class="input-group-addon fancy-addon">OT Hour</span>
														<input type="text" class="form-control num" id="txtOTHour" maxlength="2">
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-lg-12">
													<div class="pull-right">
														<a class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset</a>
														<a class="btn btn-default btnSave" data-insertbtn='<?php echo $vouchers['overtime']['insert']; ?>'><i class="fa fa-save"></i> Save</a>
														<a class="btn btn-default btnDelete" data-deletetbtn='<?php echo $vouchers['overtime']['delete']; ?>'><i class="fa fa-trash-o"></i> Delete</a>
														
													</div>
												</div> 	<!-- end of col -->
											</div>	<!-- end of row -->
										</form>	<!-- end of form -->

									</div>	<!-- end of panel-body -->
								</div>	<!-- end of panel -->
							</div>  <!-- end of col -->
						</div>	<!-- end of row -->

					</div>	<!-- end of add_branch -->
					<div id="view_all" class="tab-pane fade">

						<div class="row">
							<div class="col-lg-12">
								<div class="panel panel-default">
									<div class="panel-body">
										<table class="table table-striped table-hover ar-datatable">
											<thead>
												<tr>
													<td>Sr#</td>
													<td>Name</td>
													<td>Father Name</td>
													<td>Aproved By</td>
													<td>Reason</td>
													<td>Reamrks</td>
													<td>OT Hours</td>
													<td></td>
												</tr>
											</thead>
											<tbody>
												<?php $counter = 1; foreach ($overtimes as $overtime): ?>
													<tr>
														<td><?php echo $counter++; ?></td>
														<td><?php echo $overtime['name']; ?></td>
														<td><?php echo $overtime['fname']; ?></td>
														<td><?php echo $overtime['approved_by']; ?></td>
														<td><?php echo $overtime['reason']; ?></td>
														<td><?php echo $overtime['remarks']; ?></td>
														<td><?php echo $overtime['othour']; ?></td>
														<!-- <td><a href="" class="btn btn-primary btn-edit-overtime showallupdatebtn" data-showallupdatebtn=<?php //echo $vouchers['overtime']['update']; ?> data-dcno="<?php echo $overtime['dcno']; ?>"><span class="fa fa-edit"></span></a></td> -->
														<td><a href="" class="btn btn-primary btn-edit-overtime showallupdatebtn"  data-dcno="<?php echo $overtime['dcno']; ?>"><span class="fa fa-edit"></span></a></td>
													</tr>
												<?php endforeach ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>

					</div> <!-- end of search_branch -->
				</div>
			</div>
		</div>
	</div>
</div>