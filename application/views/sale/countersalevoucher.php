<?php

$desc = $this->session->userdata('desc');
$desc = json_decode($desc);
$desc = objectToArray($desc);

$vouchers = $desc['vouchers'];
?>

<!-- main content -->
<div id="main_wrapper">
    <div id="AccountAddModel" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="AccountAddModelLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header" style="background:#2477a4;color:white;padding-bottom:20px;">
                <button type="button" class="modal-button cellRight modal-close pull-right btn-close" data-dismiss="modal"><span class="fa fa-times" style="font-size:26px; "></span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="AccountAddModelLabel">Add New Account</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="col-lg-9 col-lg-offset-1">
                            <form role="form">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <label for="exampleInputEmail1">Name</label>
                                            <input type="text" id="txtAccountName" class="form-control" placeholder="Account Name" maxlength="50" tabindex="1">
                                        </div>
                                        <div class="col-lg-6">
                                            <label>Acc Type3</label>
                                            <select class="form-control input-sm "  id="txtLevel3" tabindex="2">
                                                <option value="" disabled="" selected="">Choose Account Type</option>
                                                <?php foreach ($l3s as $l3): ?>
                                                    <option value="<?php echo $l3['l3']; ?>" data-level2="<?php echo $l3['level2_name']; ?>" data-level1="<?php echo $l3['level1_name']; ?>"><?php echo $l3['level3_name'] ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                        <div class="col-lg-12">
                                            <span><b>Type 2 &rarr; </b><span id="txtselectedLevel2"> </span></span> <span><b>Type 1 &rarr; </b><span id="txtselectedLevel1"> </span></span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>       
            </div>
            <div class="modal-footer">
                <div class="pull-right">
                    <a class="btn btn-success btnSaveM btn-sm" data-insertbtn="1"><i class="fa fa-save"></i> Save</a>
                    <a class="btn btn-warning btnResetM btn-sm"><i class="fa fa-refresh"></i> Reset</a>
                    <a class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-times"></i> Close</a>
                </div>
            </div>
        </div>
    </div>
</div>


 <!--/////////////Party Lookup /////////////////////-->
 <div id="party-lookup" class="modal fade modal-lookup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h3 id="myModalLabel">Party Lookup</h3>
            </div>

            <div class="modal-body">
                <table class="table table-striped modal-table">
                    <!-- <table class="table table-bordered table-striped modal-table"> -->
                    <thead>
                        <tr style="font-size:16px;">
                            <th>Id</th>
                            <th>Name</th>
                            <th>Mobile</th>
                            <th>Address</th>
                            <th style='width:3px;'>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($parties as $party): ?>
                            <tr>
                                <td width="14%;">
                                    <?php echo $party['account_id']; ?>
                                    <input type="hidden" name="hfModalPartyId" value="<?php echo $party['pid']; ?>">
                                </td>
                                <td><?php echo $party['name']; ?></td>
                                <td><?php echo $party['mobile']; ?></td>
                                <td><?php echo $party['address']; ?></td>
                                <td><a href="#" data-dismiss="modal" class="btn btn-primary populateAccount"><i class="fa fa-search"></i></a></td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <!-- <button class="btn btn-danger delete-modal-del">Delete</button> -->
                <button class="btn btn-primary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

 <!--/////////////ST-Token   Count Sale /////////////////////-->

    <div id="CountSaleModel" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="CountSaleModelLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header" style="background:#2477a4;color:white;padding-bottom:20px;">
                <button type="button" class="modal-button cellRight modal-close pull-right btn-close" data-dismiss="modal"><span class="fa fa-times" style="font-size:26px; "></span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="CountSaleModelLabel">ST- Token Count Sale</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="col-lg-9 col-lg-offset-1">
                            <form role="form">
                                <div class="form-group">
                                    <div class="row">
                                    
                                        <div class="col-lg-6"style="margin-top:10px;">
										  <label>   Category: </label>
                                            <div class="input-group" >
											             
                                                            <select class="form-control select2">
                                                                <option value="" disabled="" selected="">Category No:1    </option>
                                                               
                                                            </select>
                                             </div>
									      </div>
                                        <div class="col-lg-6"style="margin-top:10px;">
										  <label>   Sub Category: </label>
                                            <div class="input-group" >
											             
                                                            <select class="form-control select2">
                                                                <option value="" disabled="" selected="">Sub Category No:1    </option>
                                                               
                                                            </select>
                                             </div>
									      </div>
                                        
                                        <div class="col-lg-6"style="margin-top:20px;">
                                            <div class="input-group" >
											             <label style="margin-top:10px;margin-left:0px;">   Brand: </label>
                                                            <select class="form-control select2">
                                                                <option value="" disabled="" selected="">Brand Name    </option>
                                                               
                                                            </select>
                                             </div>
                                        </div>
                                        <div class="col-lg-6"style="margin-top:25px;">
                                            <a class="btn btn-primary  btn-sm" data-insertbtn="1"> Show</a>
                                        </div>
                                        <div class="col-lg-12"style="margin-top:20px;">
                                            <table class="table" id="purchase_table">
											<thead>
												<tr>
													<th style="font-size: 11px;">Item Name</th>
													<th style="font-size: 11px;">Uom</th>
													<th style="font-size: 11px;">Brand</th>
													<th style="font-size: 11px;">Nom</th>
													<th style="font-size: 11px;">S Rate</th>
													<th style="font-size: 11px;">Add</th>
												</tr>
											</thead>
											<tbody>

											<tr><td class="srno" style="font-size: 10px;">Steel Coil</td><td class="item_id" style="font-size: 10px;" data-item_id="5">Mayo 360 Small </td><td class="qty" style="font-size: 10px;">Steel</td><td class="srate" style="font-size: 10px;">430.00</td><td class="net" style="font-size: 10px;">430.00</td><td> <input class="invite-contact-checkbox" type="checkbox"></td></tr>
											<tr><td class="srno" style="font-size: 10px;">Steel Coil</td><td class="item_id" style="font-size: 10px;" data-item_id="5">Mayo 360 Small </td><td class="qty" style="font-size: 10px;">Steel</td><td class="srate" style="font-size: 10px;">430.00</td><td class="net" style="font-size: 10px;">430.00</td><td> <input class="invite-contact-checkbox" type="checkbox"></td></tr>
											<tr><td class="srno" style="font-size: 10px;">Steel Coil</td><td class="item_id" style="font-size: 10px;" data-item_id="5">Mayo 360 Small </td><td class="qty" style="font-size: 10px;">Steel</td><td class="srate" style="font-size: 10px;">430.00</td><td class="net" style="font-size: 10px;">430.00</td><td> <input class="invite-contact-checkbox" type="checkbox"></td></tr>
											</tbody>
										</table>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>       
            </div>
            <div class="modal-footer">
                <div class="pull-right">
                    <a class="btn btn-success btnSaveM btn-sm" data-insertbtn="1"><i class="fa fa-save"></i> Save</a>
                    <a class="btn btn-warning btnResetM btn-sm"><i class="fa fa-refresh"></i> Reset</a>
                    <a class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-times"></i> Close</a>
                </div>
            </div>
        </div>
    </div>
</div>

 <!--/////////////btn- modal /////////////////////-->

    <div id="btn-Model" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="CountSaleModelLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header" style="background:#ff9900;color:white;">
                <button type="button" class="modal-button cellRight modal-close pull-right btn-close" data-dismiss="modal"><span class="fa fa-times" style="font-size:26px; "></span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="CountSaleModelLabel">Add Qty</h4>
            </div>
            <div class="modal-body">
         <form>
		 <div class="row"style="margin-top:-30px;">
          <div class="col-lg-6">
            <label for="t_vrno" class="control-label">Qty:</label>
            <input type="text" class="num form-control" placeholder="Enter Qty" id="t_qty">
            
          </div>
          <div class="col-lg-6">
            <label for="t_vrno" class="control-label">Weight:</label>
            <input type="text" class="num form-control" placeholder="Enter Weight" id="t_qty">
            
          </div>
          <div class="col-lg-6">
            <label for="t_vrno" class="control-label">Rate:</label>
            <input type="text" class="num form-control" placeholder="Enter Rate" id="t_qty">
            
          </div>
          <div class="col-lg-6">
            <label for="t_vrno" class="control-label">Amount:</label>
            <input type="text" class="num form-control" placeholder="Enter Amount" id="t_qty">
            
          </div>
		  
		  </div>
		 
        </form>
      </div>
      <div class="modal-footer">
	      <div class="pull-right">
	        <button type="button" class="btn btn-success btn-ad_ad">Add</button>
	        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
	      </div>
      </div>

        </div>
    </div>
</div>






    <div class="page_content">
        <div class="container-fluid">
            
<!-- ////////////////////////////////////////////////////////////////////////////////////////Page Bar//////////// -->
           <div class="page_bar">
    <div class="row">
      <div class="col-md-5">
        <h1 class="page_title"><i class="fa fa-cogs"></i> Counter Sale Voucher</h1>
    </div>
    <div class="col-md-7">
        <div class="pull-right">
          <a class="btn btn-sm btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
          <a class="btn btn-sm btn-default btnSave" data-savegodownbtn="1" data-saveaccountbtn="1" data-saveitembtn="1" data-insertbtn="1" data-updatebtn="1" data-deletebtn="1" data-printbtn="1"><i class="fa fa-save"></i> Save F10</a>
          <a class="btn btn-sm btn-default btnDelete"><i class="fa fa-trash-o"></i> Delete F12</a>

          <div class="btn-group">
              <button type="button" class="btn btn-primary btn-sm btnPrint btnprintThermal"><i class="fa fa-save"></i>Print F9</button>
                         
                   </div>
                   <a href="#party-lookup" data-toggle="modal" class="btn btn-sm btn-default btnsearchparty sr-only"><i class="fa fa-search"></i>&nbsp;Account Lookup F1</a>
                   <!-- <a href="#item-lookup" data-toggle="modal" class="btn btn-sm btn-info btnsearchitem sr-only pull-right"><i class="fa fa-search"></i>&nbsp;Item Lookup F2</a> -->
               </div>

           </div>
       </div>
   </div>
	   <!-- ////////////////////////////////////////////////////////////////////////////////////////Counter Sale Voucher//////////// -->
            <div class="row">
                <div class="col-md-10">
                    <div class="tab-content">
                        <div class="tab-pane active fade in">
                           <div class="row">
                            <div class="col-lg-12">

                                <div class="panel panel-default">
                                    <div class="panel-body">


                                        <form action="">

                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <div class="input-group">
                                                        <span class="input-group-addon id-addon">Sr#</span>
                                                        <input type="number" class="form-control" id="txtVrnoa" >
                                                        <input type="hidden" id="txtMaxVrnoaHidden">
                                                        <input type="hidden" id="txtVrnoaHidden">
                                                        <input type="hidden" id="voucher_type_hidden">

                                                        <input type="hidden" name="uid" id="uid" value="<?php echo $this->session->userdata('uid'); ?>">
                                                        <input type="hidden" name="uname" id="uname" value="<?php echo $this->session->userdata('uname'); ?>">
                                                        <input type="hidden" name="cid" id="cid" value="<?php echo $this->session->userdata('company_id'); ?>">

                                                        <input type="hidden" id="purchaseid" value="<?php echo $setting_configur[0]['purchase']; ?>">
                                                        <input type="hidden" id="saleid" value="<?php echo $setting_configur[0]['sale']; ?>">
                                                        <input type="hidden" id="discountid" value="<?php echo $setting_configur[0]['discount']; ?>">
                                                        <input type="hidden" id="expenseid" value="<?php echo $setting_configur[0]['expenses']; ?>">
                                                        <input type="hidden" id="taxid" value="<?php echo $setting_configur[0]['tax']; ?>">
                                                        <input type="hidden" id="cashid" value="<?php echo $setting_configur[0]['cash']; ?>">
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="input-group">
                                                        <span class="input-group-addon">Vr#</span>
                                                        <input type="text" class="form-control " id="txtVrno" readonly='true'>
                                                        <input type="hidden" id="txtMaxVrnoHidden">
                                                        <input type="hidden" id="txtVrnoHidden">
                                                    </div>
                                                </div>
                                                                                               
                                                <div class="col-lg-3">
                                                    <div class="input-group">
                                                        <span class="input-group-addon">Date</span>
                                                        <input class="form-control ts_datepicker" type="text" id="current_date">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                
                                                    <div class="col-lg-4">
                                                        <div class="form-group">
                                                            <label>Party Name</label>
                                                            <div class="input-group">
                                                <select class="form-control  select2 party_dropdown" id="party_dropdown" style='width:320px;'>
                                                    <option value="" disabled="" selected="">Choose party</option>
                                                    <?php foreach ($parties as $party): ?>
                                                    <option value="<?php echo $party['pid']; ?>"><?php echo $party['name']; ?></option>
                                                <?php endforeach ?>
                                            </select>
                                            <a  tabindex="-1" class="input-group-addon btn btn-primary active" style="min-width:40px !important;" id="A2" data-target="#AccountAddModel" data-toggle="modal" href="#addCategory" rel="tooltip"
                                            data-placement="top" data-original-title="Add Category" data-toggle="tooltip" data-placement="bottom" title="Add New Account Quick (F3)"><i class='fa fa-plus'></i></a>
                                        </div>
                                                        </div>
                                                        
                                                        
                                                    </div>
                                                
                                                <div class="col-lg-4"style="margin-top:30px">
												   <div class="input-group">
                                                        <span class="input-group-addon id-addon"style="min-width:80px;">Customer</span>
                                                        <input type="text" class="form-control" >
                                                    </div>
                                                </div>
                                                <div class="col-lg-4"style="margin-top:30px">
												       <div class="input-group">
                                                        <span class="input-group-addon id-addon">Phone</span>
                                                        <input type="text" class="form-control  " >
                                                       </div>
                                                     </div>
                                                
                                                
                                                       
                                               </div>                                        

                                                  <div class="row" style="margin-top:10px;">
                                                     <div class="col-lg-6">
												    <div class="input-group">
                                                        <span class="input-group-addon id-addon"style="min-width:70px;">Address</span>
                                                        <input type="text" class="form-control " >
                                                    </div>
                                                   </div>
													 
                                                     <div class="col-lg-6">
												       <div class="input-group">
                                                        <span class="input-group-addon id-addon">Received By</span>
                                                        <input type="text" class="form-control  " >
                                                       </div>
                                                     </div>
                                                     
                                                  </div> 
                                                  <div class="row" style="margin-top:20px;">
                                                     <div class="col-lg-7"style="margin-top:28px;">
												       <div class="input-group">
                                                        <span class="input-group-addon id-addon">Remarks</span>
                                                        <input type="text" class="form-control  " >
                                                       </div>
                                                     </div>
                                                     <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <label>Godown</label>
                                                        <div class="input-group" >
                                                            <select class="form-control select2">
                                                                <option value="" disabled="" selected="">Choose Warehouse...   </option>
                                                               
                                                            </select>
                                                            
                                                        </div>
                                                    </div>
                                                    
                                                    
                                                </div>
                                                     
                                                  </div> 
												 
                                                    <!-- <div class="row"></div> -->

                                                   
                                                    <!-- <div class="row"></div> -->
                                                    <!-- <div class="row"></div> -->
                                                   


                                            </form> <!-- end of form -->

                                        </div>  <!-- end of panel-body -->
                                    </div>  <!-- end of panel -->
                                </div>  <!-- end of col -->
                            </div>  <!-- end of row -->
                                                <div class="row"style="margin-top:0px;">
			                                         <div class="col-lg-12">
				                                           <ul class="nav nav-pills"style="margin-left:12px;">
					                                               <li class="active"> <a href="#Main" data-toggle="tab"> Receive Item </a>
					                                               <li> <a href="#Main" data-toggle="tab"> Issue Item </a>
					                                      </ul>
				                                     </div>
			                                    </div>
			            </div>
			        </div>
			    </div>
				<div class="col-md-2"style="margin-top:10px;">
                <div class="row">
                    <label>Other information</label>
                    <div class="row l5ratesdisp disp ten-padding">
                        <label>Last 5 Purchase Rates</label>
                        <table class="lastrate_table table-striped" id="lastrate_table" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th>Vr#</th>
                                    <th>Date</th>
                                    <th>Rate</th>
                                    <th>Qty</th>
                                </tr>
                            </thead>

                            <tbody class="rate_tbody">

                            </tbody>
                        </table>
                    </div>
                    <div class="row laststockLocation_tabledisp ten-padding">
                        <label>Stock Positions</label>
                        <table class="laststockLocation_table table-striped" id="laststockLocation_table" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th>Location</th>
                                    <th>Qty</th>
                                </tr>
                            </thead>

                            <tbody class="rate_tbody">

                            <tr><td class="location numeric" data-title="location"> GENERAL CONSUMPTION</td><td class="qty numeric" data-title="qty"> -2.00</td></tr><tr><td class="location numeric" data-title="location"> NF</td><td class="qty numeric" data-title="qty"> -1097.00</td></tr><tr><td class="location numeric" data-title="location"> NF2</td><td class="qty numeric" data-title="qty"> -2.00</td></tr><tr><td class="location numeric" data-title="location"> SRW</td><td class="qty numeric" data-title="qty"> 186.00</td></tr></tbody>
                        </table>
                    </div>
                    <div class="partydisp disp">
                        <label>Account Detail </label>
                        <div id="party_p"></div>
                    </div>

                </div>

            </div>
			</div>
		<!-- ////////////////////////////////////////////////////////////////////////////////////////main//////////// -->
            <div class="row" style="margin-top:0px;">
                <div class="col-lg-12">
                    <div class="tab-content">
                        <div class="tab-pane active fade in" id="Main"> 
                           <div class="row">
						<div class="col-lg-4">
							<div class="panel panel-default" style="">
								<div class="panel-body" style="padding: 0px 0px 15px 0px;width:405px;min-height: 380px;max-height: 380px;">

									<div class="selected-item">
										<table class="table" >
											<thead>
												<tr>
													<th style="font-size: 11px;">Sr#</th>
													<th style="font-size: 11px;padding-left: 15px;padding-right: 15px;">Description</th>
													<th style="font-size: 11px;padding-right: 15px;">Qty</th>
													<th style="font-size: 11px;padding-right: 15px;">Weight</th>
													<th style="font-size: 11px;padding-right: 15px;">Rate</th>
													<th style="font-size: 11px;padding-right: 35px;">Amount</th>
													<th style="font-size: 11px;min-width:150px;"></th>
												</tr>
											</thead>
											<tbody>

											<tr><td class="srno" style="font-size: 10px;">1</td><td class="item_id" style="font-size: 10px;" data-item_id="5">Mayo 360 Small </td><td class="qty" style="font-size: 10px;">1</td><td class="srate" style="font-size: 10px;">430.00</td><td class="net" style="font-size: 10px;">430.00</td><td class="net" style="font-size: 10px;">430.00</td><td><a href="#" class="btn-del kotbtns">x</a><a  tabindex="-1" class="btn-modal kotbtns" style="min-width:20px !important;" id="AA2" data-target="#btn-Model" data-toggle="modal" href="#addCategory" rel="tooltip"data-placement="top" data-original-title="Add Category" data-toggle="tooltip" data-placement="bottom" title="">+</a><span class="hidd hide"></span> </td></tr></tbody>
										</table>
									</div>

								</div>
							</div>
						</div>
						
						<div class="col-lg-8">
							<div class="panel panel-default" >
								<div class="panel-body"style="min-height: 380px;">

									<div class="row">
										<div class="col-lg-12">
											
											<div class="input-group"style="margin-top:10px;">
												<span class="input-group-addon" style="min-width:40px;height:34px;"><span class="fa fa-search"></span></span>
												<input type="text" class="form-control" id="txtItemSearch" placeholder="Search...">
												<a  tabindex="-1" class="input-group-addon btn btn-primary active" style="min-width:40px !important;" id="AA2" data-target="#CountSaleModel" data-toggle="modal" href="#addCategory" rel="tooltip"
                                                data-placement="top" data-original-title="Add Category" data-toggle="tooltip" data-placement="bottom" title="Add New Account Quick (F3)">+</a>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-lg-12 itemst">
											<div class="items">
													<div style="background: #000000" class="item item22" data-item_id="5" data-item_code="M360" data-item_room="0.0000">
														<span class="item_desc">Mayo 360 Small Pizza</span>
														<span class="item_price">430</span>
														
													</div>
													<div style="background: #000000" class="item item22" data-item_id="6" data-item_code="M360M" data-item_room="0.0000">
														<span class="item_desc">Mayo 360 Medium Pizza</span>
														<span class="item_price">800</span>
														
													</div>
													<div style="background: #a4f28c" class="item item22" data-item_id="7" data-item_code="M360L" data-item_room="1500.0000">
														<span class="item_desc">Mayo 360 Large Pizza</span>
														<span class="item_price">1050</span>
														
													</div>
													<div style="background: #000000" class="item item22" data-item_id="15" data-item_code="CK" data-item_room="0.0000">
														<span class="item_desc">Chicken Kabab</span>
														<span class="item_price">100</span>
														
													</div>
													<div style="background: #000000" class="item item22" data-item_id="16" data-item_code="MB" data-item_room="0.0000">
														<span class="item_desc">Malai Boti Seikh</span>
														<span class="item_price">150</span>
														
													</div>
													<div style="background: #80ffff" class="item item22" data-item_id="17" data-item_code="Rt" data-item_room="0.0000">
														<span class="item_desc">Mint Raita</span>
														<span class="item_price">100</span>
														
													</div>
													
											</div>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
                                        
                   </div>  <!-- end of tab-pane -->
                  </div>  <!-- end of tab content -->
                 </div>  <!-- end of col -->
               </div>  <!-- end of row -->
							

                    <div class="row">
						
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-body">

									<div class="row">
										<div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon id-addon"style="min-width:70px;">Rec Qty</span>
                                                <input type="text" class="form-control  num" style="min-width:80px;background: #FFEDED;border:1px solid #ccc;"readonly="true" >
                                            </div>
										</div>
										<div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon id-addon">Rec Weight</span>
                                                <input type="text" class="form-control  num" style="min-width:80px;background: #FFEDED;border:1px solid #ccc;"readonly="true" >
                                            </div>
										</div>
										<div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon id-addon"style="min-width:70px;">Rec Amt</span>
                                                <input type="text" class="form-control  num" style="min-width:80px;background: #FFEDED;border:1px solid #ccc;"readonly="true" >
                                            </div>
										</div>
										<div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon id-addon"style="min-width:110px;">Other Charges</span>
                                                <input type="text" class="form-control  num" style="min-width:80px;background: #FFF;" >
                                            </div>
										</div>
										
									</div>
									
									<div class="row">
										<div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon id-addon"style="min-width:70px;">Iss Qty</span>
                                                <input type="text" class="form-control  num" style="min-width:80px;background: #FFEDED;border:1px solid #ccc;"readonly="true" >
                                            </div>
										</div>
										<div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon id-addon">Iss Weight</span>
                                                <input type="text" class="form-control  num" style="min-width:80px;background: #FFEDED;border:1px solid #ccc;"readonly="true" >
                                            </div>
										</div>
										<div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon id-addon"style="min-width:70px;">Iss Amt</span>
                                                <input type="text" class="form-control  num" style="min-width:80px;background: #FFEDED;border:1px solid #ccc;"readonly="true" >
                                            </div>
										</div>
										<div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon id-addon"style="min-width:110px;">Total Amount</span>
                                                <input type="text" class="form-control  num" style="min-width:80px;background: #FFEDED;border:1px solid #ccc;"readonly="true" >
                                            </div>
										</div>
										
									</div>
									

									


								
								</div>
							</div>
						</div>
					</div>
		
					<div class="row"style="margin-top:0px;">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-body"style="margin-top:-30px;">

									<div class="row">
										<div class="col-lg-4">
											<div class="input-group">
												<span class="input-group-addon id-addon" style="min-width:100px;">Discount%</span>
												<input type="text" class="form-control "style="background-color:#fff;">
											</div>
										</div>
										<div class="col-lg-4">
											<div class="input-group">
												<span class="input-group-addon id-addon" style="min-width:100px;">Discount</span>
												<input type="text" class="form-control " style="background-color:#fff;">
											</div>
										</div>
										<div class="col-lg-4">
											<div class="input-group">
												<span class="input-group-addon id-addon" style="min-width:100px;">Net Amount</span>
												<input type="text" class="form-control "style="background: #FFEDED;border:1px solid #ccc;"readonly="true" >
											</div>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>	
                    
                    <div class="row"style="margin-top:0px;">                                                                                    
                    <div class="col-lg-5">
                    </div>                                                                                   
                    <div class="col-lg-7">
                        <a class="btn btn-sm btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
                        <a class="btn btn-sm btn-default btnSave" data-savegodownbtn="1" data-saveaccountbtn="1" data-saveitembtn="1" data-insertbtn="1" data-updatebtn="1" data-deletebtn="1" data-printbtn="1"><i class="fa fa-save"></i> Save F10</a>
                        <a class="btn btn-sm btn-default btnDelete"><i class="fa fa-trash-o"></i> Delete F12</a>
                        <div class="btn-group">
                        <button type="button" class="btn btn-primary btn-sm btnPrint btnprintThermal"><i class="fa fa-save"></i>Print F9</button>
                          
                        </div>
                        <a href="#party-lookup" data-toggle="modal" class="btn btn-sm btn-default btnsearchparty "><i class="fa fa-search"></i>&nbsp;Account Lookup F1</a>
                        <!-- <a href="#item-lookup" data-toggle="modal" class="btn btn-sm btn-info btnsearchitem "><i class="fa fa-search"></i>&nbsp;Item Lookup F2</a> -->
                    </div>
                    
                                                                
                </div>					
												 
		</div>
	 </div>
												  
   </div>  

              
        <!-- </div> -->