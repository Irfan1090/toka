<?php

	$desc = $this->session->userdata('desc');
	$desc = json_decode($desc);
	$desc = objectToArray($desc);

	$vouchers = $desc['vouchers'];
?>

<div id="main_wrapper">

  	<div class="page_bar">
    	<div class="row">
      		<div class="col-lg-3">
        		<h1 class="page_title">Staff Attendance</h1>
      		</div>
      		<div class="col-lg-9">
      			<div class="pull-right">
					
					<a class="btn btn-default btnPrint" data-printtbtn='<?php echo $vouchers['student']['print']; ?>'><i class="fa fa-print"></i> Print</a>
					<a href='' class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset</a>
					<a href='' class="btn btn-default btnSave" data-insertbtn='<?php echo $vouchers['staff_attendance']['insert']; ?>' data-updatebtn='<?php echo $vouchers['staff_attendance']['update']; ?>' data-deletebtn='<?php echo $vouchers['staff_attendance']['delete']; ?>' data-printbtn='<?php echo $vouchers['staff_attendance']['print']; ?>'><i class="fa fa-save"></i> Save</a>
					<a href="" class="btn btn-default btnDelete" data-deletetbtn='<?php echo $vouchers['staff_attendance']['delete']; ?>'><i class="fa fa-trash-o"></i> Delete</a>
					
				</div>
      		</div>
    	</div>
  	</div>

  	<div class="page_content">
    	<div class="container-fluid">

    		<div class="row">
    			<div class="col-lg-12">

					<div class="panel panel-default">
						<div class="panel-body">

							<div class="row">
								<div class="col-lg-2">
	                                <div class="input-group">
	                                    <span class="input-group-addon id-addon">Vr#</span>
	                                    <input type="number" class="form-control num txtidupdate" data-txtidupdate='<?php echo $vouchers['staff_attendance']['update']; ?>' id="txtdcno">
	                                    <input type="hidden" id="txtMaxdcnoHidden">
	                                    <input type="hidden" id="txtdcnoHidden">
	                                    <input type="hidden" id="voucher_type_hidden">
	                                </div>
								</div>

								<div class="col-lg-3 col-lg-offset-1">
									<div class="input-group">
                                        <span class="input-group-addon txt-addon">Date</span>
                                        <input class="form-control " type="date" id="current_date" value="<?php echo date('Y-m-d'); ?>">
                                    </div>
								</div>
							</div>
							<div class="row post-container hide">
								<div class="col-lg-3">
									<div class="input-group">
                                        <span class="input-group-addon txt-addon">Start Date</span>
                                        <input class="form-control " type="date" id="from_date" value="<?php echo date('Y-m-d'); ?>">
                                    </div>
								</div>
								<div class="col-lg-3">
									<div class="input-group">
                                        <span class="input-group-addon txt-addon">End Date</span>
                                        <input class="form-control " type="date" id="to_date" value="<?php echo date('Y-m-d'); ?>">
                                    </div>
								</div>
								<div class="col-lg-2">
									<div class="pull-right">
										<a href='' class="btn btn-default btnPost" style="border: 1px solid black;">
	              							<i class="fa fa-search"></i>
	            						Post</a>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-3">
									<div class="input-group">
										<span class="input-group-addon txt-addon">Department</span>
										<select class="form-control" id="dept_dropdown">
							              	<option value="all" selected="">All</option>
							              	<?php foreach ($staffdepartments as $department): ?>
							                  	<option value="<?php echo $department['deptid']; ?>"><?php echo $department['name']; ?></option>
							              	<?php endforeach ?>
							            </select>
									</div>
								</div>

								<div class="col-lg-3">
									<div class="input-group">
										<span class="input-group-addon txt-addon">Status</span>
										<select class="form-control" id="status_dropdown">
							              	<option value="Present" selected="">Present</option>
							              	<option value="Absent">Absent</option>
							              	<option value="Paid Leave">Paid Leave</option>
							              	<option value="Unpaid Leave">Unpaid Leave</option>
							              	<option value="Rest Day">Rest Day</option>
							              	<option value="Gusted Holiday">Gusted Holiday</option>
							              	<option value="Short Leave">Short Leave</option>
							              	<option value="Outdoor">Outdoor</option>
							            </select>
									</div>
								</div>

								<div class="col-lg-3">
                                    <label for="autoattendance">
                                        <input type="checkbox" id="autoattendance" name="aat" value="autoattendance">
                                        Auto Attendance
                                    </label>
                                </div>

                                <div class="col-lg-3">
									<div class="pull-right">
										<a href='' class="btn btn-default btnSearch">
	              							<i class="fa fa-search"></i>
	            						Show</a>
									</div>
								</div>
							</div>

						</div>
					</div>

					

    			</div>	<!-- end of col-lg-12 -->
    		</div>	<!-- end of row -->

    		<div class="row">
    			<div class="col-lg-12">

					<div class="panel panel-default">
						<div class="panel-body">

							<table class="table table-striped table-hover ar-datatable" id='atnd-table'>
								<thead>
									<tr>
										<th>Sr#</th>
										<th>Department</th>
										<th>Employee Name</th>
										<th>Designation</th>
										<th>Shift</th>
										<th>In Time</th>
                                    	<th>Out Time</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody></tbody>
							</table>

						</div>
					</div>

					<datalist id='status'>						
						<option value="Present">
						<option value="Absent">
						<option value="Paid Leave">
						<option value="Unpaid Leave">
						<option value="Rest Day">
						<option value="Gusted Holiday">
						<option value="Short Leave">
						<option value="Outdoor">
					</datalist>

    			</div>
    		</div>

    		<div class="row">
				<div class="col-lg-12">
					<div class="pull-right">
						
						<a class="btn btn-default btnPrint" data-printtbtn='<?php echo $vouchers['student']['print']; ?>'><i class="fa fa-print"></i> Print</a>
						<a href='' class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset</a>
						<a href='' class="btn btn-default btnSave" data-insertbtn='<?php echo $vouchers['staff_attendance']['insert']; ?>'><i class="fa fa-save"></i> Save</a>
						<a href="" class="btn btn-default btnDelete" data-deletetbtn='<?php echo $vouchers['staff_attendance']['delete']; ?>'><i class="fa fa-trash-o"></i> Delete</a>
						
					</div>
				</div> 	<!-- end of col -->
			</div>	<!-- end of row -->

    	</div>	<!-- end of container-fluid -->
    </div>	<!-- end of page_content -->
</div>