<!-- main content -->
<div id="main_wrapper">

	<div class="page_bar">
		<div class="row">
			<div class="col-lg-3" style="border-top-width: 0px; margin-top: 23px;">
				<h1 class="page_title">Account Ledger</h1>
			</div>
            <div class="pull-right acc_ledger col-lg-9">
                <div class="pull-right">
                    <ul class="stats">
                        <li class='lime'>
                            <div class="details">
                                <span>Turn Over</span>
                                <span class="turnOver" style="font-size:14px;font-weight:bold;">0.00</span>
                            </div>
                        </li>

                        <li class='blue'>
                            <div class="details">
                                <span>Opening Balance</span>
                                <span class=" opening-bal" style="font-size:14px;font-weight:bold;">0.00</span>
                            </div>
                        </li>
                        <li class='red'>
                            <div class="details">
                                <span>Total Debit</span>
                                <span class=" net-debit" style="font-size:14px;font-weight:bold;">0.00</span>
                            </div>
                        </li>
                        <li class='brown'>
                            <div class="details">
                                <span>Total Credit</span>
                                <span class=" net-credit" style="font-size:14px;font-weight:bold;">0.00</span>
                            </div>
                        </li>
                        <li class='green'>
                            <div class="details">
                                <span>Closing Balance</span>
                                <span class=" running-total" style="font-size:14px;font-weight:bold;">0.00</span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page_content">
      <div class="container-fluid">

         <div class="row">
            <div class="col-lg-12">
               <div class="panel panel-default">
                  <div class="panel-body">
                     <div class="row">
                       <input type="hidden" name="uid" id="uid" value="<?php echo $this->session->userdata('uid'); ?>">
                       <input type="hidden" name="uname" id="uname" value="<?php echo $this->session->userdata('uname'); ?>">
                       <input type="hidden" name="cid" id="cid" value="<?php echo $this->session->userdata('company_id'); ?>">
                       <div class="col-lg-2">
                           <label>From</label>
                           <input class="form-control ts_datepicker" type="text" id="from_date">
                       </div>
                       <div class="col-lg-2">
                           <label>To</label>
                           <input class="form-control ts_datepicker" type="text" id="to_date">
                       </div>
                       <div class="col-lg-3">
                           <label>Account</label>
                           <div class="input-group" >
                              <select class="form-control select2" id="name_dropdown">
                                 <option value="" disabled="" selected="">Choose account</option>
                                 <?php foreach ($parties as $party): ?>
                                 <option value="<?php echo $party['pid']; ?>" data-accountid="<?php echo $party['account_id']; ?>"><?php echo $party['name']; ?></option>
                             <?php endforeach ?>
                         </select>
                         <a  tabindex="-1" class="input-group-addon btn btn-primary active" style="min-width:40px !important;" id="A2" data-target="#party-lookup" data-toggle="modal" href="#addCategory" rel="tooltip"
                         data-placement="top" data-original-title="Add Category" data-toggle="tooltip" data-placement="bottom" title="Search Account (F1)"><i class="fa fa-search"></i></a>
                     </div>
                 </div>
                 <div id="party-lookup" class="modal fade modal-lookup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h3 id="myModalLabel"><i class='fa fa-search'></i> Party Lookup</h3>
                            </div>

                            <div class="modal-body">
                                <table class="table table-striped modal-table">
                                    <!-- <table class="table table-bordered table-striped modal-table"> -->
                                    <thead>
                                        <tr style="font-size:16px;">
                                            <th>Id</th>
                                            <th>Name</th>
                                            <th>Mobile</th>
                                            <th>Address</th>
                                            <th style='width:3px;'>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($parties as $party): ?>
                                        <tr>
                                            <td width="14%;">
                                                <?php echo $party['account_id']; ?>
                                                <input type="hidden" name="hfModalPartyId" value="<?php echo $party['pid']; ?>">
                                            </td>
                                            <td><?php echo $party['name']; ?></td>
                                            <td><?php echo $party['mobile']; ?></td>
                                            <td><?php echo $party['address']; ?></td>
                                            <td><a href="#" data-dismiss="modal" class="btn btn-primary populateAccount"><i class="glyphicon glyphicon-ok"></i></a></td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <!-- <button class="btn btn-danger delete-modal-del">Delete</button> -->
                            <button class="btn btn-primary" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
               <label>.</label>
               <div class="pull-right">
                  <a class="btn btn-default btnSearch"><i class="fa fa-search"></i> Show F6</a>
                  <a class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
                  <div class="btn-group">
                      <button type="button" class="btn btn-primary btn-sm btnPrint3" ><i class="fa fa-save"></i> Print F9</button>
                      <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li ><a href="#" class="btnPrint3">Party Ledger</li>
                        <li ><a href="#" class="btnPrint4">Foreign</li>
                        <li><a class="btnPrint2"> Daily Cash Statement F8</a></li>
                    </ul>
                </div>



            </div>
        </div>
    </div>
</div>

</div>
</div>
</div>
<div class="row">
    <div class="col-lg-12">
       <div class="panel panel-default">
          <div class="panel-body">

							<!-- <div class="row">
                                <div class="pull-right acc_ledger">
                                    <ul class="stats">
                                        <li class='blue'>
                                            <div class="details">
                                                <span class="big opening-bal">0.00</span>
                                                <span>Opening Balance</span>
                                            </div>
                                        </li>
                                        <li class='red'>
                                            <div class="details">
                                                <span class="big net-debit">0.00</span>
                                                <span>Total Debit</span>
                                            </div>
                                        </li>
                                        <li class='green'>
                                            <div class="details">
                                                <span class="big net-credit">0.00</span>
                                                <span>Total Credit</span>
                                            </div>
                                        </li>
                                        <li class='brown'>
                                            <div class="details">
                                                <span class="big running-total">0.00</span>
                                                <span>Closing Balance</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div> -->

                            <div class="row">
                                <div id="no-more-tables">								
                                    <table  class="table table-striped table-hover ar-datatable1 rptTable saleRows col-lg-12 table-bordered table-striped table-condensed cf" id="datatable_example" style="width: 1189px; border-top-width: 0px; border-left-width: 0px; padding-left: 0px; padding-right: 0px; border-bottom-width: 0px;">
                                       <thead class="cf">
                                          <tr>
											<!-- <th style="color: #000 !important;">Sr#</th>
											<th style="color: #000 !important;">Party ID</th> -->
											<th style="width: 20px;">#</th>
                                            <th >Date</th>
                                            <th style="width:134px;" class="text-left">Voucher</th>
                                            <th style="width:267px;" class="text-left">Description</th>
                                            <th style="width:267px;" class="text-left">Remarks</th>
                                            <th style="text-align: right !impotant;" class="numeric text-right">Debit</th>
                                            <th style="text-align: right;" class="numeric text-right">Credit</th>
                                            <th style="text-align: right;" class="numeric text-right">Balance</th>
                                            <th style="width:20px;"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<input type="hidden" id="hfBaseUrl" value="<?php echo base_url(); ?>" />
