<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h3>Payables / Receiveables</h3>
			<div class="">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#payablesTab" data-toggle="tab">Payables</a></li>
					<li><a href="#receiveablesTab" data-toggle="tab">Receiveables</a></li>
				</ul>
			</div>
			<div class="tab-content">
				<div class="tab-pane active" id="payablesTab">
					<!-- <a href="#" class="btn btn-primary btnprint" style="margin-bottom: 10px;">Print</a> -->
					<table class="table table-bordered table-hover table-striped tblPayable">
						<thead>
							<tr>
								<th>#</th>
								<th>Account</th>
								<th>Address</th>
								<th>Email</th>
								<th>Mobile</th>
								<th>Phone#</th>
								<th>Balance</th>
							</tr>
						</thead>
						<tbody>
							<?php 
								$counter = 1;
								foreach ($payables as $payable): ?>
								<tr>
								  <td class="no_sort tblSerial"><?php echo $counter++; ?></td>
								  <td class="no_sort tblParty"><?php echo $payable['account']; ?></td>
								  <td class="no_sort tblAddress"><?php echo $payable['address'] ? $payable['address'] : '<span style="text-align:center; display:block;">-</span>' ; ?></td>
								  <td class="no_sort tblEmail"><?php echo $payable['email'] ? $payable['email'] : '<span style="text-align:center; display:block;">-</span>' ; ?></td>
								  <td class="no_sort tblMobile"><?php echo $payable['mobile'] ? $payable['mobile'] : '<span style="text-align:center; display:block;">-</span>' ; ?></td>
								  <td class="no_sort tblPhone"><?php echo $payable['phone_off'] ? $payable['PHONE_OFF'] : '<span style="text-align:center; display:block;">-</span>' ; ?></td>
								  <td class="no_sort tblBalance" style="text-align:right; !important"><?php echo $payable['balance']; ?></td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
				<div class="tab-pane" id="receiveablesTab">
					<!-- <a href="#" class="btn btn-primary btnprint" style="margin-bottom: 10px;">Print</a> -->
					<table class="table table-bordered table-hover table-striped tblReceiveable">
						<thead>
							<tr>
								<th>#</th>
								<th>Account</th>
								<th>Address</th>
								<th>Email</th>
								<th>Mobile</th>
								<th>Phone#</th>
								<th>Balance</th>
							</tr>
						</thead>
						<tbody>
							<?php 
								$counter = 1;
								foreach ($receiveables as $receiveable): ?>
								<tr>
								  <td class="no_sort tblSerial"><?php echo $counter++; ?></td>
								  <td class="no_sort tblParty"><?php echo $receiveable['account']; ?></td>
								  <td class="no_sort tblAddress"><?php echo $receiveable['address'] ? $receiveable['address'] : '<span style="text-align:center; display:block;">-</span>' ; ?></td>
								  <td class="no_sort tblEmail"><?php echo $receiveable['email'] ? $receiveable['email'] : '<span style="text-align:center; display:block;">-</span>' ; ?></td>
								  <td class="no_sort tblMobile"><?php echo $receiveable['mobile'] ? $receiveable['mobile'] : '<span style="text-align:center; display:block;">-</span>' ; ?></td>
								  <td class="no_sort tblPhone"><?php echo $receiveable['phone_off'] ? $receiveable['phone_off'] : '<span style="text-align:center; display:block;">-</span>' ; ?></td>
								  <td class="no_sort tblBalance" style="text-align:right; !important"><?php echo $receiveable['balance']; ?></td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="<?php //echo base_url('application/assets/js/general.js'); ?>"></script>
	<script src=" <?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
<script>
	$(document).ready(function () {
		$('.btnprint').on('click', function ( ) {
			window.open(base_url + 'application/views/reportPrints/payableReceivable.php');
		});

		$('a[href="' + window.location.hash + '"]').trigger('click');
		
		// $('.tblPayable').dataTable();
		// $('.tblReceiveable').dataTable();
	});
</script>