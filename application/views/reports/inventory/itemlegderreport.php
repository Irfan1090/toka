<script id="voucher-item-template" type="text/x-handlebars-template">
  <tr>
     <td>{{serial}}</td>
     <td>{{{vrnoa}}}</td>
     <td>{{date}}</td>
     <td>{{party_name}}</td>
     <td>{{remarks}}</td>
     <td>{{location}}</td>
     <td class="text-right" style="text-align:right !important;">{{_in}}</td>
     <td class="text-right" style="text-align:right !important;">{{_out}}</td>
     <td class="text-right balance_qty" style="text-align:right !important;">{{balance}}</td>
     <td class="text-right" style="text-align:right !important;">{{_in_weight}}</td>
     <td class="text-right" style="text-align:right !important;">{{_out_weight}}</td>
     <td class="text-right balance_weight" style="text-align:right !important;">{{balance_weight}}</td>

  </tr>
</script>
<script id="voucher-sum-template" type="text/x-handlebars-template">
  <tr class="hightlight_tr">
     <td></td>
     <td></td>
     <td></td>
     <td></td>
     <td></td>
     <td></td>
     <td class="text-right" style="text-align:right !important;">{{total_in}}</td>
     <td class="text-right" style="text-align:right !important;">{{total_out}}</td>
     <td></td>
     <td class="text-right" style="text-align:right !important;">{{total_in_weight}}</td>
     <td class="text-right" style="text-align:right !important;">{{total_out_weight}}</td>
     <td></td>
  </tr>
</script>
<!-- main content -->
<div id="main_wrapper">

    <div class="page_bar">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page_title">Item Ledger Report</h1>
            </div>
        </div>
    </div>

    <div class="page_content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-12">

                    <div class="row">
                        <div class="col-lg-12">


                            <div class="panel panel-default">
                                <div class="panel-body">

                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon">From</span>
                                                <input class="form-control ts_datepicker" type="text" id="from_date">
                                            </div>
                                            <input type="hidden" name="uid" id="uid" value="<?php echo $this->session->userdata('uid'); ?>">
                                            <input type="hidden" name="uname" id="uname" value="<?php echo $this->session->userdata('uname'); ?>">
                                            <input type="hidden" name="cid" id="cid" value="<?php echo $this->session->userdata('company_id'); ?>">
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon">To</span>
                                                <input class="form-control ts_datepicker" type="text" id="to_date">
                                            </div>
                                        </div>
                                                                               
                                        <div class="col-lg-6">
                                            <div class="pull-right">
                                                <a href='' class="btn btn-primary btn-sm btnReset">Reset F5</a>
                                                <a href='' class="btn btn-primary btn-sm btnSearch">Show Report F6</a>
                                                <a href='' class="btn btn-success btn-sm btnPrint">Print Report F9</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-5">
                                            <div class="input-group">
                                                <span class="input-group-addon txt-addon">Item</span>
                                                <select class="form-control select2" id="item_dropdown">
                                                    <option value="" disabled="" selected="">Choose item</option>
                                                    <?php foreach ($items as $item): ?>
                                                        <option value="<?php echo $item['item_id']; ?>"><?php echo $item['item_des']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">                                                
                                          <div class="input-group">
                                          <span class="input-group-addon txt-addon">Godown</span>
                                          <select class="form-control select2" id="dept_dropdown">
                                              <option value="" selected="" disabled="">Choose Warehouse</option>
                                              <?php foreach ($departments as $department): ?>
                                                  <option value="<?php echo $department['did']; ?>"><?php echo $department['name']; ?></option>
                                              <?php endforeach ?>
                                          </select>
                                          </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="input-group">
                                                <span class="input-group-addon txt-addon">Op Qty</span>
                                                <input class="form-control text" type="text" id="Opening_Qty" readonly="true">
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="input-group">
                                                <span class="input-group-addon txt-addon">Op Weight</span>
                                                <input class="form-control text" type="text" id="Opening_Weight" readonly="true">
                                            </div>
                                        </div>                                        
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <table id="datatable_example" class="table table-striped">
                                                <thead class='dthead'>
                                                    <tr>
                                                        <th style="width: 4%;">Sr#</th>
                                                        <th style="width: 15%;">Vr#</th>
                                                        <th style="width: 15%;" >Date</th>
                                                        <th style="width: 50%;" >Account Detail</th>
                                                        <th>Remarks</th>
                                                        <th>Location</th>
                                                        <th style="text-align: right;">Qty In</th>
                                                        <th style="text-align: right;">Qty Out</th>
                                                        <th style="text-align: right;">Balance</th>

                                                        <th style="text-align: right;">Weight In</th>
                                                        <th style="text-align: right;">Weight Out</th>
                                                        <th style="text-align: right;">Balance</th>

                                                    </tr>
                                                </thead>
                                                <tbody id="itemRows" class="report-rows">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>  <!-- end of panel-body -->
                            </div>  <!-- end of panel -->



                        </div>  <!-- end of col -->
                    </div>  <!-- end of row -->

                </div>  <!-- end of level 1-->
            </div>
        </div>
    </div>
</div>