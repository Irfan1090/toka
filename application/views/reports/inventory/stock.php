<script id="general-head-template" type="text/x-handlebars-template">
    <tr>
        <th class="no_sort" style="width: 50px;">Sr#</th>
        <th class="no_sort" style="width: 900px;">Description </th>
        <th class="no_sort" style="text-align:right; width: 150px;">Qty </th>
        <th class="no_sort" style="text-align:right; width: 150px;">Weight </th>
    </tr>
</script>

<script id="general-head-template-value" type="text/x-handlebars-template">
    <tr>
        <th class="no_sort" style="width: 50px;">Sr#</th>
        <th class="no_sort" style="width: 900px;">Description </th>
        <th class="no_sort" style="text-align:right; width: 150px;">Qty </th>
        <th class="no_sort" style="text-align:right; width: 150px;">Weight </th>
        <th class="no_sort" style="text-align:right; width: 150px;">Cost </th>
        <th class="no_sort" style="text-align:right; width: 150px;">Value </th>
    </tr>
</script>

<script id="general-item-template" type="text/x-handlebars-template">
  <tr>
     <td>{{SERIAL}}</td>
     <td>{{DESCRIPTION}}</td>
     <td style="text-align:right !important;">{{QTY}}</td>
     <td style="text-align:right !important;">{{WEIGHT}}</td>
  </tr>
</script>
<script id="general-item-template-value" type="text/x-handlebars-template">
  <tr>
     <td>{{SERIAL}}</td>
     <td>{{DESCRIPTION}}</td>
     <td style="text-align:right !important;">{{QTY}}</td>
     <td style="text-align:right !important;">{{WEIGHT}}</td>
     <td style="text-align:right !important;">{{PRATE}}</td>
     <td style="text-align:right !important;">{{VALUE}}</td>
  </tr>
</script>

<script id="general-item2-template" type="text/x-handlebars-template">
  <tr>
     <td>{{NAME}}</td>
     <td style="text-align:right !important;">{{QTY}}</td>
     <td style="text-align:right !important;">{{WEIGHT}}</td>
  </tr>
</script>
<script id="general-vhead-template" type="text/x-handlebars-template">
  <tr class="hightlight_tr">
     <td></td>
     <td>{{GROUP1}}</td>
     <td></td>
     <td></td>
  </tr>
</script>
<script id="general-vhead-template-value" type="text/x-handlebars-template">
  <tr class="hightlight_tr">
     <td></td>
     <td>{{GROUP1}}</td>
     <td></td>
     <td></td>
     <td></td>
     <td></td>
  </tr>
</script>

<script id="general-grouptotal-template" type="text/x-handlebars-template">
  <tr class="finalsum">
     <td></td>
     <td style="text-align:right !important;">{{TOTAL}}</td>
     <td style="text-align:right !important;">{{TOTAL_QTY}}</td>
     <td style="text-align:right !important;">{{TOTAL_WEIGHT}}</td>
  </tr>
</script>

<script id="general-grouptotal-template-value" type="text/x-handlebars-template">
  <tr class="finalsum">
     <td></td>
     <td style="text-align:right !important;">{{TOTAL}}</td>
     <td style="text-align:right !important;">{{TOTAL_QTY}}</td>
     <td style="text-align:right !important;">{{TOTAL_WEIGHT}}</td>
     <td></td>
     <td style="text-align:right !important;">{{TOTAL_VALUE}}</td>
  </tr>
</script>


<script id="general-vhead2-template" type="text/x-handlebars-template">
  <tr class="hightlight_tr">
     <td>{{DESCRIPTION}}</td>
     <td></td>
     <td></td>
  </tr>
</script>
<!-- main content -->
<div id="main_wrapper">

    <div class="page_bar">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page_title">Stock Report</h1>
            </div>
        </div>
    </div>

    <div class="page_content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-12">

                    <div class="row">
                        <div class="col-lg-12">


                            <div class="panel panel-default">
                                <div class="panel-body">

                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon">From</span>
                                                <input class="form-control ts_datepicker" type="text" id="from_date">
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon">To</span>
                                                <input class="form-control ts_datepicker" type="text" id="to_date">
                                            </div>
                                        </div>
                                        <div class="col-lg-2"></div>
                                        <div class="col-lg-6">
                                            <div class="pull-right">
                                                <a href='' class="btn btn-primary btn-sm btnSearch">Show Report F6</a>
                                                <a href='' class="btn btn-primary btn-sm btnSearchValue">Show Value</a>
                                                <a href='' class="btn btn-success btn-sm btnReset">Reset Filters F5</a>
                                                <!-- <a href='' class="btn btn-success btn-sm btnPrint">Print Report F9</a>
                                                <a href='' class="btn btn-success btn-sm btnPrintPdf">Print Pdf F8</a> -->
                                                <div class="btn-group">
                                                      <button type="button" class="btn btn-primary btn-sm btnPrint" ><i class="fa fa-save"></i>Print F9</button>
                                                      <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        <span class="caret"></span>
                                                        <span class="sr-only">Toggle Dropdown</span>
                                                      </button>
                                                      <ul class="dropdown-menu" role="menu">
                                                        <li ><a href="#" class="btnPrint"> Print F9</a></li>
                                                        <li ><a href="#" class="btnPrintPdf"> Pdf F8</a></li>
                                                        <li ><a href="#" class="btnPrintPdfWithoutHeader"> Pdf Without Header</a></li>
                                                      </ul>
                                                </div>
                                            </div>
                                            <input type="hidden" name="uid" id="uid" value="<?php echo $this->session->userdata('uid'); ?>">
                                            <input type="hidden" name="uname" id="uname" value="<?php echo $this->session->userdata('uname'); ?>">
                                            <input type="hidden" name="cid" id="cid" value="<?php echo $this->session->userdata('company_id'); ?>">
                                        </div>
                                    </div>

                                    <legend style='margin-top: 30px;'>Selection Criteria</legend>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <a href='' class="btn btn-primary btn-sm btnSelCre">Location Wise</a>
                                            <a href='' class="btn btn-default btn-sm btnSelCre">Item Wise</a>
                                        </div>
                                    </div>

                                    <div class="row hide">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label class="radio-inline">
                                                    <input type="radio" name="rbRpt" id="Radio1" value="detailed" checked="checked">
                                                    Detailed
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="rbRpt" id="Radio2" value="summary">
                                                    Summary
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <table id="datatable_example" class="table table-striped full table-bordered">
                                                <thead class='dthead'>
                                                </thead>
                                                <tbody id="saleRows" class="report-rows saleRows">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>  <!-- end of panel-body -->
                            </div>  <!-- end of panel -->



                        </div>  <!-- end of col -->
                    </div>  <!-- end of row -->

                </div>  <!-- end of level 1-->
            </div>
        </div>
    </div>
</div>