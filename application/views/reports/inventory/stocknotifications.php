<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h3>Minimum Stock Alerts</h3>			
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<table class="table table-bordered table-hover table-striped">
				<thead>
					<tr>
						<th style="width: 300px;">Product</th>
						<th style="width: 300px;">Supplier</th>
						<th>Current Stock</th>
						<th>Minimum Level</th>
						<th>Order Value</th>
						<th>@ Pur.</th>
					</tr>
				</thead>
				<tbody>
						<?php foreach ($notifications as $notif): ?>
							<tr>
								<th class="rows-th"><?php echo $notif['description']; ?></th>	
								<td style=""><?php echo $notif['supplier']; ?></td>	
								<td style="text-align: right !important;"><?php echo $notif['curr_stock']; ?></td>	
								<td style="text-align: right !important;"><?php echo $notif['min_level']; ?></td>	
								<td style="text-align: right !important;"><?php echo $notif['order_value']; ?></td>	
								<td style="text-align: right !important;"><?php echo $notif['prate']; ?></td>	
							</tr>
						<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
