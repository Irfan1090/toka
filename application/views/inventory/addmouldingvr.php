<?php
    $desc = $this->session->userdata('desc');
    $desc = json_decode($desc);
    $desc = objectToArray($desc);
    $vouchers = $desc['vouchers'];
?>
<!-- main content -->
<div id="main_wrapper">
    <div class="page_bar">
        <div class="row">
            <div class="col-md-5">
                <h1 class="page_title">Head Moulding Voucher</h1>
            </div>  
            <div class="col-md-7">
                <div class="pull-right">
                    <a class="btn btn-sm btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
                    <a class="btn btn-sm btn-default btnSave" data-saveaccountbtn='<?php echo $vouchers['account']['insert']; ?>' data-saveitembtn='<?php echo $vouchers['item']['insert']; ?>' data-insertbtn='<?php echo $vouchers['mouldingsheet']['insert']; ?>' data-updatebtn='<?php echo $vouchers['mouldingsheet']['update']; ?>' data-deletebtn='<?php echo $vouchers['mouldingsheet']['delete']; ?>' data-printbtn='<?php echo $vouchers['mouldingsheet']['print']; ?>' ><i class="fa fa-save"></i> Save F10</a>
                    <a class="btn btn-sm btn-default btnDelete"><i class="fa fa-trash-o"></i> Delete F12</a>
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary btn-sm btnPrint" ><i class="fa fa-save"></i>Print F9</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page_content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <form action="">
                                        <div class="row">
                                            <div class="col-lg-2">
                                                <div class="input-group">
                                                    <span class="input-group-addon id-addon">Sr#</span>
                                                    <input type="number" class="form-control " id="txtVrnoa" >
                                                    <input type="hidden" id="txtMaxVrnoaHidden">
                                                    <input type="hidden" id="txtVrnoaHidden">
                                                    <input type="hidden" id="voucher_type_hidden">

                                                    <input type="hidden" name="uid" id="uid" value="<?php echo $this->session->userdata('uid'); ?>">
                                                    <input type="hidden" name="uname" id="uname" value="<?php echo $this->session->userdata('uname'); ?>">
                                                    <input type="hidden" name="cid" id="cid" value="<?php echo $this->session->userdata('company_id'); ?>">

                                                    <input type="hidden" id="purchaseid" value="<?php echo $setting_configur[0]['purchase']; ?>">
                                                    <input type="hidden" id="discountid" value="<?php echo $setting_configur[0]['discount']; ?>">
                                                    <input type="hidden" id="expenseid" value="<?php echo $setting_configur[0]['expenses']; ?>">
                                                    <input type="hidden" id="taxid" value="<?php echo $setting_configur[0]['tax']; ?>">
                                                    <input type="hidden" id="cashid" value="<?php echo $setting_configur[0]['cash']; ?>">
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-lg-offset-1">
                                                <div class="input-group">
                                                    <span class="input-group-addon">Date</span>
                                                    <input class="form-control  ts_datepicker" type="text" id="current_date">
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="input-group">
                                                    <span class="input-group-addon">Vr#</span>
                                                    <input type="text" class="form-control " id="txtVrno" readonly='true'>
                                                    <input type="hidden" id="txtMaxVrnoHidden">
                                                    <input type="hidden" id="txtVrnoHidden">
                                                </div>
                                            </div>                                                
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">                                                
                                                <label>Employee Name</label>
                                                <div class="input-group" >
                                                    <select class="form-control select2" id="employee_dropdown">
                                                        <option value="" disabled="" selected="">Choose employee</option>
                                                        <?php foreach ($accountEmployee as $employee): ?>
                                                            <option value="<?php echo $employee['pid']; ?>"><?php echo $employee['name']; ?></option>
                                                        <?php endforeach ?>
                                                    </select>   
                                                    <a href="#party-lookup" data-toggle="modal" class="input-group-addon btn btn-primary active" style="min-width:40px !important;"><i class="fa fa-search"></i></a>
                                                    <!-- <a class="input-group-addon btn btn-primary active"   tabindex="-1" style="min-width:40px !important;" id="A3" data-target="#ItemAddModel" data-toggle="modal" href="#addItem" rel="tooltip"
                                                    data-placement="top" data-original-title="Add Item" data-toggle="tooltip" data-placement="bottom" title="Add New Account Quick (F3)"><i class="fa fa-search"></i></a> -->
                                                </div>
                                                                                             
                                            </div>
                                            <div class="col-lg-3">                                                
                                                <label>Warehouse</label>
                                                <select class="form-control select2" id="dept_dropdown">
                                                    <option value="" selected="" disabled="">Choose Warehouse</option>
                                                    <?php foreach ($departments as $department): ?>
                                                        <option value="<?php echo $department['did']; ?>"><?php echo $department['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>                            
                                            </div>
                                            <div class="col-lg-3">                                                
                                                <label>Received By</label>
                                                <input class='form-control ' type='text' list="receivers" id='receivers_list'>
                                                <datalist id='receivers'>
                                                    <?php foreach ($receivers as $receiver): ?>
                                                        <option value="<?php echo $receiver['received_by']; ?>">
                                                    <?php endforeach ?>
                                                </datalist>                                                
                                            </div>                                    
                                           <!--  <div class="col-lg-1">                                                
                                                <label>Inv#</label>
                                                <input type="text" class="form-control  num" id="txtInvNo">                                                
                                            </div>   -->                                   
                                            <!-- <div class="col-lg-3">                                                
                                                <label>Through</label>
                                                <select class="form-control  select2" id="transporter_dropdown">
                                                    <option value="" disabled="" selected="">Choose transporter</option>
                                                    <?php //foreach ($transporters as $transporter): ?>
                                                        <option value="<?php //echo $transporter['transporter_id']; ?>"><?php //echo $transporter['name']; ?></option>
                                                    <?php //endforeach ?>
                                                </select>                                               
                                            </div> -->
                                        </div>                                        
                                        <div class="row">                                                                        
                                            <div class="col-lg-9">                                            
                                                <label>Remarks</label>
                                                <input type="text" class="form-control " id="txtRemarks">                                                                                                                                                                             
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <ul class="nav nav-pills">
                                                    <li class="active"><a href="#itemadd" data-toggle="tab">Raw Item</a></li>
                                                    <li><a href="#itemless" data-toggle="tab">Costing</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">                        
                                                <div class="tab-content" style="margin-top: -30px !important;">
                                                    <div class="tab-pane active fade in" id="itemadd">
                                                        <div class="container-wrap">
                                                            <div class="row">
                                                                <div class="col-lg-2">
                                                                    <label for="">Item Code</label>
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon" style='min-width: 0px;'><span class="fa fa-barcode"></span></span>
                                                                        <select class="form-control select2" id="itemid_dropdownL">
                                                                            <option value="" disabled="" selected="">Item Code</option>
                                                                            <?php foreach ($items as $item): ?>
                                                                                <option value="<?php echo $item['item_id']; ?>" data-uom_item="<?php echo $item['uom']; ?>" data-prate="<?php echo $item['cost_price']; ?>" data-srate="<?php echo $item['srate']; ?>" data-grweight="<?php echo $item['grweight']; ?>" data-stqty="<?php echo $item['stqty']; ?>" data-stweight="<?php echo $item['stweight']; ?>"><?php echo $item['item_id']; ?></option>
                                                                            <?php endforeach ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4">
                                                                    <label for="" id="stqty_lbl">Item Description</label>                      
                                                                    <div class="input-group" >
                                                                        <select class="form-control select2" id="item_dropdownless">
                                                                            <option value="" disabled="" selected="">Item description</option>
                                                                            <?php foreach ($items as $item): ?>
                                                                                <option value="<?php echo $item['item_id']; ?>" data-uom_item="<?php echo $item['uom']; ?>" data-prate="<?php echo $item['cost_price']; ?>" data-srate="<?php echo $item['srate']; ?>"  data-grweight="<?php echo $item['grweight']; ?>" data-stqty="<?php echo $item['stqty']; ?>" data-stweight="<?php echo $item['stweight']; ?>" ><?php echo $item['item_des']; ?></option>
                                                                            <?php endforeach ?>
                                                                        </select>
                                                                        <a class="input-group-addon btn btn-primary active"   tabindex="-1" style="min-width:40px !important;" id="A3" data-target="#ItemAddModel" data-toggle="modal" href="#addItem" rel="tooltip"
                                                                        data-placement="top" data-original-title="Add Item" data-toggle="tooltip" data-placement="bottom" title="Add New Account Quick (F3)"><i class="fa fa-plus"></i></a>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-1">
                                                                    <label for="">Qty</label>                                                    
                                                                    <input type="text" class="form-control num" id="txtQtyL">                                                    
                                                                </div>
                                                                <div class="col-lg-1">
                                                                    <label for="">GW</label>                                                    
                                                                    <input type="text" class="form-control readonly num" id="txtGWeightL" readonly="" tabindex="-1">                                                    
                                                                </div>
                                                                <div class="col-lg-1">
                                                                    <label for="">Uom</label>                                                    
                                                                    <input type="text" class="form-control readonly num" id="txtUomL" readonly="" tabindex="-1">                                                    
                                                                </div>
                                                                <div class="col-lg-1">
                                                                    <label for="">Weight</label>                                                    
                                                                    <input type="text" class="form-control num" id="txtWeightL">                                                    
                                                                </div>
                                                                <div class="col-lg-1">
                                                                    <label for="">T.Weight</label>                                                    
                                                                    <input type="text" class="form-control num" id="txtTotWeightL">                                                    
                                                                </div>
                                                                <div class="col-lg-1" style='margin-top: 30px;'>                                                    
                                                                    <a href="" class="btn btn-primary" id="less_btnAdd"><i class='fa fa-plus'></i></a>
                                                                </div>                                                
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div id="no-more-tables">
                                                                    <table class="col-lg-12 table table-bordered table-striped table-condensed cf" id="purchase_table">
                                                                        <thead>
                                                                            <tr>
                                                                                <th class="numeric">Sr#</th>
                                                                                <th >Item Detail</th>
                                                                                <th class="numeric">Qty</th>
                                                                                <th class="numeric">Weight</th>
                                                                                <th class="numeric">Rate</th>
                                                                                <th class="numeric">Amount</th>
                                                                                <th>Action</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>

                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="itemless">
                                                        <div class="container-wrap">
                                                            <div class="row">
                                                                <div class="col-lg-1" >
                                                                    <label>Code</label>
                                                                    <select class="form-control select2" id="itemid_dropdown">
                                                                        <option value="" disabled="" selected="">Item Id</option>
                                                                        <?php foreach ($items as $item): ?>
                                                                            <option value="<?php echo $item['item_id']; ?>" data-uom_item="<?php echo $item['uom']; ?>" data-prate="<?php echo $item['cost_price']; ?>" data-grweight="<?php echo $item['grweight']; ?>"><?php echo $item['item_id']; ?></option>
                                                                        <?php endforeach ?>
                                                                    </select>
                                                                </div>
                                                                <div class="col-lg-2" >
                                                                    <label for="">Item Description</label>
                                                                    <select class="form-control select2" id="item_dropdown">
                                                                        <option value="" disabled="" selected="">Item description</option>
                                                                        <?php foreach ($items as $item): ?>
                                                                            <option value="<?php echo $item['item_id']; ?>" data-uom_item="<?php echo $item['uom']; ?>" data-prate="<?php echo $item['cost_price']; ?>" data-grweight="<?php echo $item['grweight']; ?>"><?php echo $item['item_des']; ?></option>
                                                                        <?php endforeach ?>
                                                                    </select>
                                                                </div>
                                                                <div class="col-lg-1">
                                                                    <label for="">Qty</label>                                                    
                                                                    <input type="text" class="form-control num" id="txtQty">                                                    
                                                                </div>
                                                                <div class="col-lg-1">
                                                                    <label for="">GW</label>                                                    
                                                                    <input type="text" class="form-control readonly num" id="txtGWeight" readonly="" tabindex="-1">                                                    
                                                                </div>
                                                                <div class="col-lg-1">
                                                                    <label for="">Uom</label>                                                    
                                                                    <input type="text" class="form-control readonly num" id="txtUom" readonly="" tabindex="-1">                                                    
                                                                </div>
                                                                <div class="col-lg-1">
                                                                    <label for="">Weight</label>                                                    
                                                                    <input type="text" class="form-control num" id="txtWeight">                                                    
                                                                </div>
                                                                <div class="col-lg-1">
                                                                    <label for="">Mould@</label>                                                    
                                                                    <input type="text" class="form-control num" id="txtPRate">                                                    
                                                                </div>
                                                                <div class="col-lg-1">
                                                                    <label for="">M Amount</label>                                                    
                                                                    <input type="text" class="form-control readonly num" id="txtAmount" readonly="true" tabindex="-1">                                                    
                                                                </div>
                                                                <div class="col-lg-1">
                                                                    <label for="">Dhary@</label>                                                    
                                                                    <input type="text" class="form-control num" id="txtDharyRate">
                                                                </div>
                                                                <div class="col-lg-1">
                                                                    <label for="">D Amount</label>                                                    
                                                                    <input type="text" class="form-control readonly num" id="txtDharyAmount" readonly="true" tabindex="-1">
                                                                </div>
                                                                <div class="col-lg-1" style='margin-top: 30px;'>                                                    
                                                                    <a href="" class="btn btn-primary" id="btnAdd"><i class="fa fa-plus"></i></a>
                                                                </div>                                                
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <table class="table table-striped" id="moulding_table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Sr#</th>
                                                                            <th>Item Detail</th>
                                                                            <th>Uom</th>
                                                                            <th>Qty</th>
                                                                            <th>Weight</th>
                                                                            <th>Mould@</th>
                                                                            <th>M Amount</th>
                                                                            <th>Dhary@</th>
                                                                            <th>D Amount</th>
                                                                            <th>Action</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>

                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="party-lookup" class="modal fade modal-lookup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                        <h3 id="myModalLabel"><i class='fa fa-search'></i> Party Lookup</h3>
                                                    </div>
                                                    <div class="modal-body mrgtop">
                                                        <table class="table table-striped modal-table">
                                                            <thead>
                                                                <tr style="font-size:16px;">
                                                                <th>Id</th>
                                                                <th>Name</th>
                                                                <th>Mobile</th>
                                                                <th>Address</th>
                                                                <th style='width:3px;'>Actions</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php foreach ($parties as $party): ?>
                                                                <tr>
                                                                <td width="14%;">
                                                                <?php echo $party['account_id']; ?>
                                                                <input type="hidden" name="hfModalPartyId" value="<?php echo $party['pid']; ?>">
                                                                </td>
                                                                <td><?php echo $party['name']; ?></td>
                                                                <td><?php echo $party['mobile']; ?></td>
                                                                <td><?php echo $party['address']; ?></td>
                                                                <td><a href="#" data-dismiss="modal" class="btn btn-primary populateAccount"><i class="glyphicon glyphicon-ok"></i></a></td>
                                                                </tr>
                                                                <?php endforeach ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button class="btn btn-primary pull-right" data-dismiss="modal">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="item-lookup" class="modal fade modal-lookup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                        <h3 id="myModalLabel"><i class='fa fa-search'></i> Item Lookup</h3>
                                                    </div>
                                                    <div class="modal-body">
                                                        <table class="table table-striped modal-table">
                                                            <thead>
                                                                <tr style="font-size:16px;">
                                                                <th>Id</th>
                                                                <th>Description</th>
                                                                <th>Code</th>
                                                                <th>Uom</th>
                                                                <th style='width:3px;'>Actions</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php foreach ($items as $item): ?>
                                                                <tr>
                                                                <td width="14%;">
                                                                <?php echo $item['item_id']; ?>
                                                                <input type="hidden" name="hfModalitemId" value="<?php echo $item['item_id']; ?>">
                                                                </td>
                                                                <td><?php echo $item['item_des']; ?></td>
                                                                <td><?php echo $item['item_code']; ?></td>
                                                                <td><?php echo $item['uom']; ?></td>
                                                                <td><a href="#" data-dismiss="modal" class="btn btn-primary populateItem"><i class="glyphicon glyphicon-ok"></i></a></td>
                                                                </tr>
                                                                <?php endforeach ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button class="btn btn-primary pull-right" data-dismiss="modal">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form> <!-- end of form -->
                                </div>  <!-- end of panel-body -->
                            </div>  <!-- end of panel -->
                        </div>  <!-- end of col -->
                    </div>  <!-- end of row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="row">                                    
                                        <div class="col-lg-3">   
                                             <div class="input-group">
                                                 <div class="input-group">
                                                    <span class="input-group-addon id-addon fixwidn">Total Weight</span>
                                                    <input type="text" class="form-control readonly num" id="txtTotalWeight" readonly="true" tabindex="-1">
                                                </div>
                                            </div>                                                 
                                        </div>         
                                        <div class="col-lg-3">   
                                            <div class="input-group">
                                                <span class="input-group-addon fixwid">Mould Amount</span>
                                                <input type="text" class=" form-control num"  id="txtMouldAmount" readonly="true" tabindex="-1">
                                            </div>                                                 
                                        </div>       
                                        <div class="col-lg-3">      
                                            <div class="input-group">
                                                <span class="input-group-addon fixwid">Dhary Amount</span>
                                                <input type="text" class=" form-control num"  id="txtDharyGAmount" readonly="true" tabindex="-1">
                                            </div>                                              
                                        </div>                                                                       
                                        <div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon fixwid">Expense Account*</span>
                                                <select class="form-control" id="exp_dropdown">
                                                    <option value="" disabled="" selected="">Choose</option>
                                                    <?php foreach ($accountExp as $expensess): ?>
                                                        <option value="<?php echo $expensess['pid']; ?>"><?php echo $expensess['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">   
                                        <div class="col-lg-3">   
                                            <div class="input-group">
                                                <span class="input-group-addon id-addon fixwidn">Total Qty</span>
                                                <input type="text" class="form-control readonly num" id="txtTotalQty" readonly="true" tabindex="-1">
                                            </div>                                                 
                                        </div>                                                                                
                                        <div class="col-lg-3">  
                                            <div class="input-group">
                                                <span class="input-group-addon fixwid">Mould Bonus</span>
                                                <input type="text" class=" form-control num"  id="txtMouldBonus">
                                            </div>                                                  
                                        </div>
                                        <div class="col-lg-3">   
                                            <div class="input-group">
                                                <span class="input-group-addon fixwid">Dhary Bonus</span>
                                                <input type="text" class=" form-control num"  id="txtDharyBonus">
                                            </div>                                                 
                                        </div>   
                                        <div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon fixwid">Cash Account*</span>
                                                <select class="form-control" id="cash_dropdown">
                                                    <option value="" disabled="" selected="">Choose</option>
                                                    <?php foreach ($accountCashs as $accountCash): ?>
                                                        <option value="<?php echo $accountCash['pid']; ?>"><?php echo $accountCash['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>                                             
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-3">     
                                            <div class="input-group">
                                                <span class="input-group-addon id-addon fixwidn">Net Amount</span>
                                                <input type="text" class="form-control readonly " id='txtNetAmount' readonly="" tabindex="-1">
                                            </div>                                               
                                        </div>  
                                        <div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon fixwid">Mould Deduction</span>
                                                <input type="text" class=" form-control num"  id="txtMouldDed">
                                            </div>                                                    
                                        </div> 
                                        <div class="col-lg-3">  
                                            <div class="input-group">
                                                <span class="input-group-addon fixwid">Dhary Deduction</span>
                                                <input type="text" class=" form-control num"  id="txtDharyDed">
                                            </div>
                                        </div>
                                        <div class="col-lg-3">    
                                            <div class="input-group">
                                                <span class="input-group-addon fixwid">Paid</span>
                                                <input type="text" class="form-control num" id="txtPaid">
                                            </div>                                                
                                        </div>
                                    </div>
                                    <div class="row">                                                                                    
                                        <div class="col-lg-9">
                                            <a class="btn btn-sm btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
                                            <a class="btn btn-sm btn-default btnSave" data-saveaccountbtn='<?php echo $vouchers['account']['insert']; ?>' data-saveitembtn='<?php echo $vouchers['item']['insert']; ?>' data-insertbtn='<?php echo $vouchers['mouldingsheet']['insert']; ?>' data-updatebtn='<?php echo $vouchers['mouldingsheet']['update']; ?>' data-deletebtn='<?php echo $vouchers['mouldingsheet']['delete']; ?>' data-printbtn='<?php echo $vouchers['mouldingsheet']['print']; ?>' ><i class="fa fa-save"></i> Save F10</a>
                                            <a class="btn btn-sm btn-default btnDelete"><i class="fa fa-trash-o"></i> Delete F12</a>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-primary btn-sm btnPrint" ><i class="fa fa-save"></i>Print F9</button>
                                                <!-- <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                  <span class="caret"></span>
                                                  <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li ><a href="#" class="btnPrint"> Print with header</li>
                                                    <li ><a href="#" class="btnprintwithOutHeader"> Print with Out header</li>
                                                </ul> -->
                                            </div>
                                            <a href="#party-lookup" data-toggle="modal" class="btn btn-sm btn-default btnsearchparty "><i class="fa fa-search"></i>&nbsp;Account Lookup F1</a>
                                            <a href="#item-lookup" data-toggle="modal" class="btn btn-sm btn-default btnsearchitem "><i class="fa fa-search"></i>&nbsp;Item Lookup F2</a>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon fixwid">User: </span>
                                                <select class="form-control " disabled="" id="user_dropdown" style="width:137px;">
                                                    <option value="" disabled="" selected="">...</option>
                                                    <?php foreach ($userone as $user): ?>
                                                        <option value="<?php echo $user['uid']; ?>"><?php echo $user['uname']; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>                                             
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>