<img src="<?php echo base_url('assets/img/dm.png') ?>" alt="DigitalManagerErp" id='loginpagelogo'>

<div class="login_container">
	<form id="login_form" action="" method='post'>
		<h1 class="login_heading">Enter Mobile Code</h1>
		<div class="form-group">
			<input type="text" class="form-control " id="txtUsername" value="<?php echo $this->session->userdata('uname'); ?>"  readonly="true" tabindex="-1"> 
		</div>
		<div class="form-group">
			<input type="text" class="form-control " id="txtCode" >
		</div>
		<div class="submit_section">
			<a class="btn btn-lg btn-success btn-block btnSigninCode">Enter</a>
		</div>
		<div class="errors_section">
			<?php echo $errors; ?>
		</div>
		<div class="builtby">
			<span>Powered By: <a href="http://www.digitalsofts.com" target="_blank">DigitalSofts</a></span>
		</div>
	</form>
</div>
