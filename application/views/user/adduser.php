<?php
	$desc = $this->session->userdata('desc');
	$desc = json_decode($desc);
	$desc = objectToArray($desc);
	$vouchers = $desc['vouchers'];
?>
<!-- main content -->

<div id="main_wrapper">
  	<div class="page_bar">
    	<div class="row">
      		<div class="col-md-12">
        		<h1 class="page_title"><i class="fa fa-user"></i> Add User</h1>
      		</div>
    	</div>
  	</div>
  	<div class="page_content">
    	<div class="container-fluid">

			<div class="row">
		      	<div class="col-md-12" style="margin-top:-40px;">

		        	<form action="">

						<div class="row">
							<div class="panel panel-default">
								<div class="panel-body">

									<div class="row">
										<div class="col-lg-2">
											<div class="input-group">
				                            	<span class="input-group-addon">User Id</span>
				                            	<input type="number" class="form-control num txtidupdate" id="txtId" data-txtidupdate='<?php echo $vouchers['user']['update']; ?>'>
			                                    <input type="hidden" id="txtMaxIdHidden">
			                                    <input type="hidden" id="txtIdHidden">
			                                    
			                                    <input type="hidden" name="uuid" id="uuid" value="<?php echo $this->session->userdata('uid'); ?>">
                                                <input type="hidden" name="uuname" id="uuname" value="<?php echo $this->session->userdata('uname'); ?>">
                                                <input type="hidden" name="cid" id="cid" value="<?php echo $this->session->userdata('company_id'); ?>">
					                        	<input type="hidden" id="voucher_type_hidden">
					                        </div>
										</div>										
									</div>
									<div class="row">
										<div class="col-lg-4">
				                          	<div class="input-group">
				                            	<span class="input-group-addon">User Name</span>
				                            	<input type="text" class="form-control" id="txtUsername">
					                        </div>
				                        </div>
									</div>
									<div class="row">
										<div class="col-lg-4">
				                          	<div class="input-group">
				                            	<span class="input-group-addon">Password</span>
				                            	<input type="password" class="form-control" id="txtPassowrd">
					                        </div>
				                        </div>
									</div>
									<div class="row">
										<div class="col-lg-4">
				                          	<div class="input-group">
				                            	<span class="input-group-addon">Full Name</span>
				                            	<input type="text" class="form-control" id="txtFullName">
					                        </div>
				                        </div>
									</div>
									<div class="row">
										<div class="col-lg-4">
				                          	<div class="input-group">
				                            	<span class="input-group-addon">Email</span>
				                            	<input type="text" class="form-control" id="txtEmail">
					                        </div>
				                        </div>
									</div>
									<div class="row">
										<div class="col-lg-4">
				                          	<div class="input-group">
				                            	<span class="input-group-addon">Mobile#</span>
				                            	<input type="text" class="form-control num" id="txtMobileNo">
					                        </div>
				                        </div>
									</div>
									<div class="row">
										<div class="col-lg-4">
											<div class="input-group">
												<span class="input-group-addon">Role</span>
												<select class="form-control" id="role_dropdown">
													<option value="" disabled="" selected="">Choose role</option>
													<?php foreach ($pgroups as $rolegroup): ?>										
														<option value="<?php echo $rolegroup['rgid']; ?>"><?php echo $rolegroup['name']; ?></option>
													<?php endforeach; ?>
												</select>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-4">
											<div class="input-group">
												<span class="input-group-addon">Company</span>
												<select class="form-control" id="company_dropdown">
													<option value="" disabled="" selected="">Choose Company</option>
													<?php foreach ($companies as $company): ?>										
														<option value="<?php echo $company['company_id']; ?>"><?php echo $company['company_name']; ?></option>
													<?php endforeach; ?>
												</select>
											</div>
										</div>
										<div class="col-lg-3" >
		                                    <div class="input-group">
		                                        <span class="input-group-addon">User: </span>
		                                        <select class="form-control " disabled="" id="user_dropdown">
		                                            <option value="" disabled="" selected="">...</option>
		                                            <?php foreach ($userone as $user): ?>
		                                                <option value="<?php echo $user['uid']; ?>"><?php echo $user['uname']; ?></option>
		                                            <?php endforeach; ?>
		                                        </select>
		                                    </div>
                                        </div>
									</div>

									<div class="row">
										<div class="col-lg-12">
											<div class="pull-right">
												<!-- <a href='' class="btn btn-default btnSave" data-insertbtn='<?php echo $vouchers['add_new_user']['insert']; ?>'> <i class="fa fa-save"></i> -->
												<a href='' class="btn btn-default btnSave" data-insertbtn='<?php echo $vouchers['user']['insert']; ?>' data-updatebtn='<?php echo $vouchers['user']['update']; ?>'><i class="fa fa-save"></i>
													Save F10
												</a>
												<a href='' class="btn btn-default btnReset"> <i class="fa fa-refresh"></i>
													Reset F5
												</a>
											</div>
										</div>
									</div> <!-- end of row -->

								</div>
							</div>

						</div>

		        	</form>   <!-- end of form -->

		      	</div>  <!-- end of col -->
	      	</div>

    	</div>  <!-- end of container fluid -->
  	</div>   <!-- end of page_content -->
</div>