<img src="<?php echo base_url('assets/img/dm.png') ?>" alt="digitalmanagercamp" id='loginpagelogo'>

<div class="login_container">
	<form id="login_form" action="" method='post'>
		<h1 class="login_heading">Sign In</h1>
		<div class="form-group">
			<input type="text" class="form-control " id="txtUsername" placeholder='Username'>
		</div>
		<div class="form-group">
			<input type="password" class="form-control " id="txtPassowrd" placeholder='Password'>
		</div>
		<div class="form-group">
			<input type="text" class="form-control num" id="txtMobCode" placeholder='Mobile Code' title="We won't send you any marketing material."  >
		</div>
		<!-- <div class="submit_section1"> -->
			<div class="row" > 
			<!-- <a class="btn btn-lg btn-success btn-block btnSignin"><i class="glyphicon glyphicon-user"></i>Sign In</a> -->
				<div class="col-lg-6" >
					<a class="btn btn-sm btn-default btnSignin"><i class="glyphicon glyphicon-user"></i>User Sign In</a>
				</div>
				<div class="col-lg-6" >
					<a class="btn btn-sm btn-default btnMobCode"><i class="glyphicon glyphicon-phone-alt"></i>Send Mobile Code</a>
				</div>
			</div>
		<!-- </div> -->
		
		<div class="errors_section"> 
			
		</div>
		<div class="builtby">
			<span>Powered By: <a href="http://www.digitalsofts.com" target="_blank">DigitalSofts</a></span>
		</div>
	</form>
</div>
