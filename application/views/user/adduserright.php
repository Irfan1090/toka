<?php

    $desc = $this->session->userdata('desc');
    $desc = json_decode($desc);
    $desc = objectToArray($desc);

    $vouchers = $desc['vouchers'];
?>
<!-- main content -->
<div id="main_wrapper">

    <div class="page_bar">
        <div class="row">
            <div class="col-md-4">
                <h1 class="page_title"><i class='fa fa-users'></i> User Rights Accounts</h1>
            </div>
            <div class="col-md-8">
                <div class="pull-right">
                    <a class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
                    <a class="btn btn-warning btnSave" data-insertbtn='<?php echo $vouchers['user_right']['insert']; ?>' data-updatebtn='<?php echo $vouchers['user_right']['update']; ?>'><i class="fa fa-spinner"></i> Update F10</a>
                </div>
            </div>
        </div>
    </div>
    <div class="page_content" style="margin-top:-20px;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form action="" style="margin-top:10px;">
                                <div class="row">
                                    <div class="col-lg-3">      
                                        <input type="hidden" id="voucher_type_hidden">                                          
                                        <label>User Name</label>
                                        <select class="form-control select2" id="users_dropdown">
                                            <option value="" disabled="" selected="">Choose Name</option>
                                            <?php foreach ($users as $user): ?>
                                                <option value="<?php echo $user['uid']; ?>"><?php echo $user['uname']; ?></option>
                                            <?php endforeach ?>
                                        </select>
                                        <input type="hidden" class="form-control" id="txtUsersHidden">
                                    </div>
                                     <div class="col-lg-3">                                                
                                        <label>Level1:</label>
                                        <select class="form-control select2" id="level_dropdown">
                                            <option value="">Choose Level</option>
                                            <?php foreach ($levels as $level): ?>
                                                <option value="<?php echo $level['l1']; ?>"><?php echo $level['name']; ?></option>
                                            <?php endforeach ?>
                                        </select>
                                        <input type="hidden" class="form-control" id="txtRecipeHidden">
                                    </div>
                                    <!--<div class="col-lg-1" style="margin-top:30px;">
                                        <a class="btn btn-primary"><i class="fa fa-search"></i> Show Levels F6</a>
                                    </div>-->
                                    <div class="col-lg-2 pull-right">
                                        <label for="">Auto Check</label>
                                        <label class="radio-inline">
                                            <input type="radio" name="inlineRadioOptions" id="radioCheckedAll" value="option1"> Check
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="inlineRadioOptions" id="radioUncheckedAll" value="option2"> Un Check
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <table class="table table-striped" id="levels_table">
                                            <thead>
                                                <tr>
                                                    <th class="hsr text-center" style="width:120px;">Right</th>
                                                    <th class="hitem">Level1</th>
                                                    <th class="hitem">Level2</th>
                                                    <th class="hitem">Level3</th>
                                                </tr>
                                            </thead>
                                            <tbody id="levelsRows">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </form> <!-- end of form -->
                        </div>  <!-- end of panel-body -->
                    </div>  <!-- end of panel -->
                </div>  <!-- end of col -->
            </div>  <!-- end of row -->
        </div>
    </div>
</div>