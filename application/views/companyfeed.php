<link href="<?php echo base_url('assets/css/wall.css'); ?>" rel="stylesheet" media="screen">
<script id="feeditem-template" type="text/x-handlebars-template">
    <div class="row">
        <div class="col-lg-12 feedItem">
            <div class="feed-header">
                <span class="feedUname"><span style="text-transform: uppercase; font-weight: bold;">{{etype}}</span> transaction # </span>
                {{{vrnoa}}}
            </div>
            <div class="feed-content">
                {{{message}}}
            </div>
            <div class="feed-footer">
                {{uname}}
                <span class="blueAt">@</span>
                <span class="feed-footercompany">{{company_name}}</span>
                on <span class="small feedDatetime">{{vrdate}}</span>
            </div>
        </div>
    </div>
</script>
<div class="container company-feed">
    <input type="hidden" name="cid" class="cid" value="<?php echo $this->session->userdata('company_id'); ?>">
    <div class="row">
        <!-- <h3 class="page-title">Company Feed</h3> -->
        <div class="col-lg-3 left_content" style="max-width: 259px;">
            <div class="row">
                <div class="company-info company-info-block col-lg-12">
                    <span class="company-img"><img src="<?php echo base_url('assets/img/ajaxloader.png'); ?>" alt=""></span>
                    <span class="feat-companyname"><?php echo $company_info[0]['company_name']; ?></span>
                    <span class="feat-companyaddress"><?php echo $company_info[0]['address']; ?></span>
                    <span class="contact"><?php echo $company_info[0]['contact_person']; ?></span>
                    <span class="contact-num"><?php echo $company_info[0]['contact']; ?></span>
                </div>
            </div>
        </div>
        <div class="col-lg-5">
            <div class="feedItems">
                <?php if ( !$feed ): ?>
                    <div class="row" id="nomorefeed">
                        <div class="feedItem col-lg-12">
                            <div class="feed-content">No transaction feed available!</div>
                        </div>
                    </div>
                <?php endif ?>
                <?php
                foreach ($feed as $feedItem):
                    $CI =& get_instance();
                    $vrlink = $CI->getVrnoaLink( $feedItem );
                    $message = $CI->getFeedMessage( $feedItem );
                    ?>
                    <div class="row">
                        <!-- <div class="span3"></div> -->
                        <div class="col-lg-12 feedItem">
                            <div class="feed-header">
                                <span class="feedUname"><span style="text-transform: uppercase; font-weight: bold;"><?php echo $feedItem['etype']; ?></span> transaction #</span>
                                <?php echo $vrlink; ?>
                            </div>
                            <div class="feed-content">
                                <?php echo $message; ?>
                            </div>
                            <div class="feed-footer">
                                <?php echo $feedItem['uname']; ?>
                                <span class="blueAt">@</span>
                                <span class="feed-footercompany"><?php echo $feedItem['company_name']; ?></span>
                                on <span class="small feedDatetime"><?php echo $feedItem['vrdate']; ?></span>
                            </div>
                        </div>
                        <!-- <div class="span3"></div> -->
                    </div>
                <?php endforeach ?>
            </div>
            <div class="row" id="nomorefeed">
                <div class="feedItem col-lg-12" style="background:rgb(131, 110, 133) !important;">
                    <div class="feed-content">No more transaction feeds available!</div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 feed-cashsats">
			<span class="cash-stat-block">
				<span class="stat-head">Sales</span>
				<span class="cash-stat-value"><?php echo round($netsale[0]['SALES_TOTAL'], 2); ?></span>
			</span>
			<span class="cash-stat-block">
				<span class="stat-head">Purchases</span>
				<span class="cash-stat-value"><?php echo round($netpurchase[0]['PURCHASES_TOTAL'], 2); ?></span>
			</span>
			<span class="cash-stat-block">
				<span class="stat-head">Cash in Hand</span>
				<span class="cash-stat-value"><?php echo round($cashinhand[0]['NET_CASH_IN_HAND'], 2); ?></span>
			</span>
            <div class="row-fluid balcheck-block action-block">
                <div class="col-lg-12">
                    <h3 class="text-left">Check Balance</h3>
                    <div class="input-prepend input-block-level" style="margin: 0;">
                        <span class="add-on col-lg-4" style="color:black;margin-top:4px;">Account</span>
                        <select name="drpParties" id="drpParties" class="col-lg-8 select2">
                            <option value="">Chose Account</option>
                            <?php foreach ($parties as $party): ?>
                                <option value="<?php echo $party['pid']; ?>"><?php echo $party['name']; ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <table class="balcheck-table table table_marg">
                        <thead>
                        <tr>
                            <th>Opening Balance</th>
                            <th>Total Debit</th>
                            <th>Total Credit</th>
                            <th>Closing Balance</th>
                        </tr>
                        <tr style="background-color: gainsboro;">
                            <td class="opening-bal"></td>
                            <td class="net-debit"></td>
                            <td class="net-credit"></td>
                            <td class="closing-bal"></td>
                        </tr>
                        </thead>
                    </table>

                    <p class="balcheck-para text-left"><a href="#" class="take-to-acledger">Detail &raquo;</a></p>
                </div>
            </div>
            <div class="row-fluid balcheck-block action-block">
                <div class="col-lg-12">
                    <h3 class="text-left">Check Stock</h3>
                    <div class="input-prepend input-block-level" style="margin: 0;">
                        <span class="add-on col-lg-4" style="color:black;margin-top:4px;">Product</span>
                        <select name="drpItems" id="drpItems" class="col-lg-8 select2">
                            <option value="">Chose Product</option>
                            <?php foreach ($items as $item): ?>
                                <option value="<?php echo $item['item_id']; ?>"><?php echo $item['item_des']; ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <table class="balcheck-table table table_marg">
                        <thead>
                        <tr>
                            <th>Opening Stock</th>
                            <th>In Stock</th>
                            <th>Out Stock</th>
                            <th>Closing Stock</th>
                        </tr>
                        <tr style="background-color: gainsboro;">
                            <td class="opening-stock"></td>
                            <td class="in-stock"></td>
                            <td class="out-stock"></td>
                            <td class="closing-stock"></td>
                        </tr>
                        </thead>
                    </table>

                    <p class="balcheck-para text-left"><a href="#" class="take-to-itledger">Detail &raquo;</a></p>
                </div>
            </div>
        </div>
    </div>
</div>