<?php

    $desc = $this->session->userdata('desc');
    $desc = json_decode($desc);
    $desc = objectToArray($desc);

    $vouchers = $desc['vouchers'];
?>
<!-- main content -->
<div class="wraper">
<div class="content">
    <form action="#">
    <div class="row"><!--1-->
        <div class="col-md-12 bod_bottom">
                <span class="tile">Update Item Rate</span><br>
                <span>&nbsp;</span>
        </div>
    </div><!--row 1-->
           <div class="row"><!--2-->
            <div class="col-md-2 cstm_bod">
                <!-- <div class="left_content"> -->

                    <div class="col-md-9 cstm_back">
                        <span><input type="checkbox" class="" id="updateItemCheck" /><b>Update Rate History</b></span>
                    </div>
                    <div class="row" style="margin-right: -99px !important;"><!--3-->
                        <div class="col-md-9 cstm_back">
                            <h6><b>Date Selection</b></h6>
                            <div class="input-group">
                                <span class="input-group-addon id-addon">Current</span>
                                <input type="" class="ts_datepicker form-control" value="" id="currDate" />
                            </div>
                            <br>
                            <div class="input-group">
                                <span class="input-group-addon id-addon">Effected</span>
                                <input type="" class="ts_datepicker form-control" value="" id="effectedDate" />  
                            </div>
                        </div>
                        <!-- <div class="col-md-9 cstm_back"> -->
                            <!-- <h6><b>Quality</b></h6>
                            <span><input type="radio" name="inlineRadioOptions" value="option1" id="radioCheckedAll" /> Check All</span>&nbsp;&nbsp;
                            <span><input type="radio" name="inlineRadioOptions" value="option2" id="radioUncheckedAll" /> UnCheck All</span>
                            <div class="cstm_bod">
                                <ul class="chkbob_ul">
                                    <?php foreach ($brands as $brand): ?>
                                        <li><input type="checkbox" class="checkboxes" value="<?php echo $brand['bid']; ?>" /><?php echo $brand['bname']; ?></li>
                                    <?php endforeach ?>
                                </ul>
                            </div> -->
                            <div class="col-md-9 cstm_back">
                            <label>Category</label>
                            <select class="form-control select2" multiple="true" id="category_dropdown" data-placeholder="Choose Category....">
                              <?php foreach ($categories as $category): ?>
                                <option value="<?php echo $category['catid']; ?>"><?php echo $category['name']; ?></option>
                              <?php endforeach ?>
                            </select>
                          </div>
                          <div class="col-md-9 cstm_back">
                            <label>Sub Catgeory</label>
                            <select class="form-control select2" multiple="true" id="subcategory_dropdown" data-placeholder="Choose Sub Category....">
                              <?php foreach ($subcategories as $subcategory): ?>
                                <option value="<?php echo $subcategory['subcatid']; ?>"><?php echo $subcategory['name']; ?></option>
                              <?php endforeach ?>
                            </select>
                          </div>
                          <div class="col-md-9 cstm_back">
                            <label>Brand</label>
                            <select class="form-control select2" multiple="true" id="brand_dropdown" data-placeholder="Choose Brand....">
                              <?php foreach ($brands as $brand): ?>
                                <option value="<?php echo $brand['bid']; ?>"><?php echo $brand['name']; ?></option>
                              <?php endforeach ?>
                            </select>
                          </div>
                        <!-- </div> -->
                        <div class="col-md-9 cstm_back">
                            <a href='' class="btn btn-primary btn-success btn-sm btnPrint btn_cstm btnsearch"><i class="fa fa-search"></i> Show (F6)</a><br>
                            <a href='' class="btn btn-primary btn-danger btn-sm btnReset btn_cstm"><i class="fa fa-refresh"></i> Reset (F5)</a>
                            <a href='' class="btn btn-primary btn-sm btnSave btn_cstm"><i class="fa fa-save"></i> Update (F11)</a>
                        </div>
                        <div class="col-md-9 cstm_back">
                            <h6><b>Price List History</b></h6>
                                <!-- <div class="lefttab"> -->
                                <table class="left_table11">
                                    <thead>
                                        <tr>
                                            <!-- <th>Sr#</th> -->
                                            <th style="width:5px !important; padding-left: 6px !important;padding-right: 6px !important;">RID</th>
                                            <th style="width:100px !important; padding-left: 6px !important;padding-right: 6px !important;">DATE</th>
                                            <th style="width:100px !important; padding-left: 6px !important;padding-right: 6px !important;">EDATE</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!--<?php $counter = 0; foreach ($updateItemHistory as $history): ?>-->
                                        <tr>
                                            <!-- <td><?php //echo ++$counter; ?></td> -->
                                            <!-- <td><?php echo $history['update_item_history_id']; ?></td>
                                            <td><?php echo substr($history['curr_date'],0,10); ?></td>
                                            <td><?php echo substr($history['effected_date'],0,10); ?></td> -->
                                        </tr>
                                        <!--<?php endforeach ?>-->
                                    </tbody>
                                </table>
                            <!-- </div> -->
                            <br><br>
                        </div>
                    </div><!--row 3-->
                <!--</div>left_content-->
            </div><!--col-md-3-->
            <div class="col-md-10 cstm_bod">
            <div class="row"></div>
            <div class="row">
               <div class="col-md-4 col-md-offset-8">
                   <input type="text" id="txtSearch" class="form-control" placeholder='Search'>
               </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="righttab">
                       <table class="right_table table table-striped full table-bordered"  id="itemUpdateTable">
                           <thead>
                               <tr>
                                   <th style="width:5px !important; padding-left: 6px !important;padding-right: 6px !important;">Sr#</th>
                                   <th style="width:70px !important; padding-left: 6px !important;padding-right: 6px !important;">Code</th>
                                   <th style="width:200px !important; padding-left: 6px !important;padding-right: 6px !important;">Description</th>
                                   <th style="width:60px !important; text-align:right !important; padding-left: 6px !important;padding-right: 6px !important;">GW</th>
                                   <th style="width:60px !important; text-align:right !important; padding-left: 6px !important;padding-right: 6px !important;">Cost@</th>
                                   <th style="width:60px !important; text-align:right !important; padding-left: 6px !important;padding-right: 6px !important;">Dealar@</th>
                                   <th style="width:60px !important; text-align:right !important; padding-left: 6px !important;padding-right: 6px !important;">Retail@</th>
                                   <th style="width:60px !important; text-align:right !important; padding-left: 6px !important;padding-right: 6px !important;">Deduction@</th>
                                   <th style="width:60px !important; text-align:right !important; padding-left: 6px !important;padding-right: 6px !important;">Mold@</th>
                                   <th style="width:60px !important; text-align:right !important; padding-left: 6px !important;padding-right: 6px !important;">Dhary@</th>
                                   <th style="width:60px !important; text-align:right !important; padding-left: 6px !important;padding-right: 6px !important;">Bilty@</th>
                                   <th style="width:60px !important; text-align:right !important; padding-left: 6px !important;padding-right: 6px !important;">Gari@</th>
                                   <th style="width:60px !important; text-align:right !important; padding-left: 6px !important;padding-right: 6px !important;">Self@</th>
                               </tr>
                           </thead>
                           <tbody id="itemRows">
                               
                           </tbody>
                       </table>
                    </div><!--right_table-->
                </div>
            </div>
              <br><br> 
           </div><!--col-md-9-->
           </div><!--row2-->
                </form>
               </div><!--content-->             
</div><!--wrapper-->


<style type="text/css">
 .wraper{
    padding-top:10px; 
    padding-left:85px;
    padding-right:10px;
    padding-bottom:10px;
 }   
 .content{
    margin-right:20px;
    margin-left:20px;
    margin-bottom: 20px;
 }
 .tile{
    font-size:20px;
    font-weight:bold;
 }
 .bod_bottom{
    border-bottom:1px solid #e8e8e8;
 }
 .cstm_bod{
    background-color:#E7F0EF;
    overflow:hidden;
    /* border:1px solid black; */
 }
 .left_content{
    overflow:hidden;
    background-color:white;
 }
 .chkbob_ul{
    border:2px solid #e8e8e8;
    height:175px;
    overflow-y:auto;
    overflow-x:hidden;
    padding-left:5px;
 }
 .chkbob_ul li{
    list-style:none;
 }
 .chkbob_ul li:hover{
    background-color:#9E8888;
    color:white;
 }
 .btn_cstm{
    margin:3px;
 }
 .lefttab{
    height:100px !important;
    overflow-y:auto ;
    overflow-x:hidden ;
 }
 .left_table{
    border:1px solid red;
    width:60px;
 }
 .left_table thead th{
    padding:3px;
    border:1px solid white;
    font-size:11px;
 }
 .left_table tbody td{
    padding:3px;
    border:1px solid white;
    font-size:11px;
 }
 .righttab{
    height:514px;
    /*overflow:scroll;*/
    overflow-y: auto;    /* Trigger vertical scroll    */
    overflow-x: hidden;  /* Hide the horizontal scroll */
 }
 .right_table{
   /* border:1px solid black;*/
    width:100%;
 }
 .right_table thead th{
    border:1px solid white;
    padding:3px;
 }
 /*.right_table tr:hover{
    color:white;
    background-color:#9E8888;
 }*/
 .left_table tr:hover{
    background-color:#9E8888;
    color:white;
 }
 .right_table tr th:nth-child(5){
    width:225px;
}
 .right_table tbody td{
    padding:5px;
    /*border:1px solid white;*/
 }
 .input_back{
    /*background-color:#E7F0EF !important;*/
    width:60px !important;
 }
 .cstm_back{
    background-color:#E7F0EF;
 }

</style>