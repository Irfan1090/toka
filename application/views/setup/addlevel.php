<?php

    $desc = $this->session->userdata('desc');
    $desc = json_decode($desc);
    $desc = objectToArray($desc);

    $vouchers = $desc['vouchers'];
?>
<?php
// $desc = Users::getUserInfo();
// $l1s = Levels::fetchAllLevel1();
// $l2s = Levels::fetchAllLevel2();
// $l3s = Levels::fetchAllLevel3();
// $vouchers = $desc['vouchers'];
?>
<!-- main content -->
<div id="main_wrapper">

	<div class="page_bar">
		<div class="row">
			<div class="col-md-12">
				<h1 class="page_title"><span class="badge badge-primary badge_style"><i class="fa fa-sort-amount-asc"></i></span> Levels</h1>
			</div>
		</div>
	</div>

	<div class="page_content pcontent_style">
		<div class="container-fluid">

			<div class="row">
				<div class="col-md-12">
					<ul class="nav nav-tabs" id="tabs_a">
						<li class="active"><a data-toggle="tab" href="#add_level1"><i class="fa fa-plus"></i> Add Update Level 1</a></li>
						<li class=""><a data-toggle="tab" href="#view_all_level1"><i class="fa fa-table"></i> View All</a></li>
					</ul>
					<div class="tab-content">
						<div id="add_level1" class="tab-pane fade active in">

							<div class="row">
								<div class="col-lg-12">
									<div class="panel panel-default" style="margin-top:-10px;">
										<div class="panel-body">

											<form action="">
	
												<div class="row"></div>

												<div class="row">
													<div class="col-lg-2"></div>
													<div class="col-lg-2">
														<div class="input-group">
															<span class="input-group-addon id-addon">Id</span>
															<input type="text" class="form-control input-sm" id="txtLevel1Id" disabled>
															<input type="hidden" id="txtMaxLevel1IdHidden">
															<input type="hidden" id="txtLevel1IdHidden">
															<input type="hidden" id="VoucherTypeHidden">
														</div>
													</div>
												</div>

												<div class="row">
													<div class="col-lg-2"></div>
													<div class="col-lg-3">
														<div class="input-group">
															<span class="input-group-addon txt-addon">Name</span>
															<input type="text" class="form-control input-sm" id="txtLevel1Name">
															<input type="hidden" id="txtLevel1NameHidden">
														</div>														
													</div>
												</div>

												<div class="row"></div>

												<div class="row">
													<div class="col-lg-2"></div>
													<div class="col-lg-3">
														<div class="pull-right">
															<a class="btn-sm btn btn-default btnSaveL1 btnSave" data-insertbtn='<?php echo $vouchers['level']['insert']; ?>' data-updatebtn='<?php echo $vouchers['level']['update']; ?>'><i class="fa fa-save"></i> Save Level</a>
															<a class="btn-sm btn btn-default btnResetL1 btnReset"><i class="fa fa-refresh"></i> Reset</a>
														</div>
													</div> 	<!-- end of col -->
												</div>	<!-- end of row -->
											</form>	<!-- end of form -->

										</div>	<!-- end of panel-body -->
									</div>	<!-- end of panel -->
								</div>  <!-- end of col -->
							</div>	<!-- end of row -->

						</div>	<!-- end of add_branch -->
						<div id="view_all_level1" class="tab-pane fade">

							<div class="row">
								<div class="col-lg-12">
									<div class="panel panel-default" style="margin-top:-10px;">
										<div class="panel-body">
											<table class="table table-striped table-hover ar-datatable">
												<thead>
													<tr>
														<th>#</th>
														<th>Level</th>
														<th class="text-center">Action</th>
													</tr>
												</thead>
												<tbody>
													<?php $counter = 1; foreach ($l1s as $l1): ?>
														<tr>
															<td><?php echo $counter++; ?></td>
															<td><?php echo $l1['name'] ?></td>
															<td><a href="" class="btn-sm btn btn-primary btn-edit-level1 showallupdatebtn" data-l1="<?php echo $l1['l1']; ?>"><span class="fa fa-edit"></span></a></td>
														</tr>
													<?php endforeach ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>

						</div> <!-- end of search_branch -->
					</div>
				</div>	<!-- end of level 1-->
			</div>

			<div class="row">
				<div class="col-md-12">
					<ul class="nav nav-tabs" id="tabs_a">
						<li class="active"><a data-toggle="tab" href="#add_level2"><i class="fa fa-plus"></i> Add Update Level 2</a></li>
						<li class=""><a data-toggle="tab" href="#view_all_level2"><i class="fa fa-table"></i> View All</a></li>
					</ul>
					<div class="tab-content">
						<div id="add_level2" class="tab-pane fade active in">

							<div class="row">
								<div class="col-lg-12">
									<div class="panel panel-default" style="margin-top:-10px;">
										<div class="panel-body">

											<form action="">
	
												<div class="row"></div>

												<div class="row">
													<div class="col-lg-2"></div>
													<div class="col-lg-2">
														<div class="input-group">
															<span class="input-group-addon id-addon">Id</span>
															<input type="text" class="form-control input-sm" id="txtLevel2Id" disabled>
															<input type="hidden" id="txtMaxLevel2IdHidden">
															<input type="hidden" id="txtLevel2IdHidden">
														</div>
													</div>
												</div>

												<div class="row">
													<div class="col-lg-2"></div>
													<div class="col-lg-3">
														<div class="input-group">
															<span class="input-group-addon txt-addon">Level 1:</span>
															<select class="form-control input-sm" id="level1_dropdown">
																<option value="" disabled="" selected="">Choose associated Level 1</option>
																<?php $counter = 1; foreach ($l1s as $l1): ?>
																	<option value="<?php echo $l1['l1']; ?>"><?php echo $l1['name']; ?></option>
																<?php endforeach ?>
															</select>
														</div>														
													</div>
												</div>

												<div class="row">
													<div class="col-lg-2"></div>
													<div class="col-lg-3">
														<div class="input-group">
															<span class="input-group-addon txt-addon">Name</span>
															<input type="text" class="form-control input-sm" id="txtLevel2Name">
															<input type="hidden" id="txtLevel2NameHidden">
														</div>														
													</div>
												</div>

												<div class="row"></div>

												<div class="row">
													<div class="col-lg-2"></div>
													<div class="col-lg-3">
														<div class="pull-right">
															<a class="btn-sm btn btn-default btnSaveL2 btnSave" data-insertbtn='<?php echo $vouchers['level']['insert']; ?>'><i class="fa fa-save"></i> Save Level</a>
															<a class="btn-sm btn btn-default btnResetL2 btnReset"><i class="fa fa-refresh"></i> Reset</a>
														</div>
													</div> 	<!-- end of col -->
												</div>	<!-- end of row -->
											</form>	<!-- end of form -->

										</div>	<!-- end of panel-body -->
									</div>	<!-- end of panel -->
								</div>  <!-- end of col -->
							</div>	<!-- end of row -->

						</div>	<!-- end of add_branch -->
						<div id="view_all_level2" class="tab-pane fade">

							<div class="row">
								<div class="col-lg-12">
									<div class="panel panel-default" style="margin-top:-10px;">
										<div class="panel-body">
											<table class="table table-striped table-hover ar-datatable">
												<thead>
													<tr>
														<th>#</th>
														<th>Level 2</th>
                                                        <th>Level 2 Code</th>
														<th>Level 1</th>
														<th class="text-center">Action</th>
													</tr>
												</thead>
												<tbody>
													<?php $counter = 1; foreach ($l2s as $l2): ?>
														<tr>
															<td><?php echo $counter++; ?></td>
															<td><?php echo $l2['level2_name'] ?></td>
                                                            <td><?php echo $l2['l2_code'] ?></td>
															<td><?php echo $l2['level1_name'] ?></td>
															<td><a href="" class="btn-sm btn btn-primary btn-edit-level2 showallupdatebtn" data-l2="<?php echo $l2['l2']; ?>"><span class="fa fa-edit"></span></a></td>
														</tr>
													<?php endforeach ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>

						</div> <!-- end of search_branch -->
					</div>
				</div>	<!-- end of level 2-->
			</div>

			<div class="row">
				<div class="col-md-12">
					<ul class="nav nav-tabs" id="tabs_a">
						<li class="active"><a data-toggle="tab" href="#add_level3"><i class="fa fa-plus"></i> Add Update Level 3</a></li>
						<li class=""><a data-toggle="tab" href="#view_all_level3"><i class="fa fa-table"></i> View All</a></li>
					</ul>
					<div class="tab-content">
						<div id="add_level3" class="tab-pane fade active in">

							<div class="row">
								<div class="col-lg-12">
									<div class="panel panel-default" style="margin-top:-10px;"> 
										<div class="panel-body">

											<form action="">
	
												<div class="row"></div>

												<div class="row">
													<div class="col-lg-2"></div>
													<div class="col-lg-2">
														<div class="input-group">
															<span class="input-group-addon id-addon">Id</span>
															<input type="text" class="form-control input-sm" id="txtLevel3Id" disabled>
															<input type="hidden" id="txtMaxLevel3IdHidden">
															<input type="hidden" id="txtLevel3IdHidden">
														</div>
													</div>
												</div>

												<div class="row">
													<div class="col-lg-2"></div>
													<div class="col-lg-3">
														<div class="input-group">
															<span class="input-group-addon txt-addon">Level 2:</span>
															<select class="form-control input-sm" id="level2_dropdown">
																<option value="" disabled="" selected="">Choose associated Level 2</option>
																<?php $counter = 1; foreach ($l2s as $l2): ?>
																	<option value="<?php echo $l2['l2']; ?>"><?php echo $l2['level2_name']; ?></option>
																<?php endforeach ?>
															</select>
														</div>														
													</div>
												</div>

												<div class="row">
													<div class="col-lg-2"></div>
													<div class="col-lg-3">
														<div class="input-group">
															<span class="input-group-addon txt-addon">Name</span>
															<input type="text" class="form-control input-sm" id="txtLevel3Name">
															<input type="hidden" id="txtLevel3NameHidden">
														</div>														
													</div>
												</div>

												<div class="row"></div>

												<div class="row">
													<div class="col-lg-2"></div>
													<div class="col-lg-3">
														<div class="pull-right">
															<a class="btn-sm btn btn-default btnSaveL3 btnSave" data-insertbtn='<?php echo $vouchers['level']['insert']; ?>'><i class="fa fa-save"></i> Save Level</a>
															<a class="btn-sm btn btn-default btnResetL3 btnReset" ><i class="fa fa-refresh"></i> Reset</a>
														</div>
													</div> 	<!-- end of col -->
												</div>	<!-- end of row -->
											</form>	<!-- end of form -->

										</div>	<!-- end of panel-body -->
									</div>	<!-- end of panel -->
								</div>  <!-- end of col -->
							</div>	<!-- end of row -->

						</div>	<!-- end of add_branch -->
						<div id="view_all_level3" class="tab-pane fade">

							<div class="row">
								<div class="col-lg-12">
									<div class="panel panel-default" style="margin-top:-10px;">
										<div class="panel-body">
											<table class="table table-striped table-hover ar-datatable">
												<thead>
													<tr>
														<th>Sr#</th>
														<th>Level 3</th>
                                                        <th>Level 3 Code</th>
														<th>Level 2</th>
														<th class="text-center">Action</th>
													</tr>
												</thead>
												<tbody>
													<?php $counter = 1; foreach ($l3s as $l3): ?>
														<tr>	
															<td><?php echo $counter++; ?></td>
															<td><?php echo $l3['level3_name'] ?></td>
                                                            <td><?php echo $l3['l3_code'] ?></td>
															<td><?php echo $l3['level2_name'] ?></td>
															<td><a href="" class="btn-sm btn btn-primary btn-edit-level3 showallupdatebtn" data-l3="<?php echo $l3['l3']; ?>"><span class="fa fa-edit"></span></a></td>
														</tr>
													<?php endforeach ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>

						</div> <!-- end of search_branch -->
					</div>
				</div>	<!-- end of level 2-->
			</div>
		</div>
	</div>
</div>