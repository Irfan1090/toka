<?php

	$desc = $this->session->userdata('desc');
	$desc = json_decode($desc);
	$desc = objectToArray($desc);

	$vouchers = $desc['vouchers'];
?>

<!-- main content -->
<div id="main_wrapper">

  <div class="page_bar">
    <div class="row">
      <div class="col-lg-3">
        <h1 class="page_title"><i class="fa fa-users"></i> Add Staff</h1>
      </div>
      <div class="col-lg-9">
      	<div class="pull-right">
      		<a href='' class="btn btn-default btnSave"  data-insertbtn='<?php echo $vouchers['staff']['insert']; ?>' data-updatebtn='<?php echo $vouchers['staff']['update']; ?>'><i class="fa fa-save"></i>Save</a>
	        <a href='' class="btn btn-default btnReset"><i class="fa fa-refresh"></i>Reset</a>
      	</div>
      </div>
    </div>
  </div>

  <div class="page_content" style="margin-top:-45px;">
    <div class="container-fluid">

      <div class="col-md-12">

        <form action="">

          	<ul class="nav nav-pills">
            	<li class="active"><a href="#basicInformation" data-toggle="tab">Basic Information</a></li>
            	<li><a href="#salarydeductions" data-toggle="tab">Salary &amp; Deductions</a></li>
            	<li><a href="#qualification" data-toggle="tab">Qualifications</a></li>
            	<li><a href="#experience" data-toggle="tab">Experience</a></li>
            	<li><a href="#viewall" data-toggle="tab">Staff List</a></li>
            	<li><a href="#phase" data-toggle="tab">Phase</a></li>
          	</ul>

        <div class="tab-content">
            <div class="tab-pane active" id="basicInformation">

	            <div class="row">
	                <div class="col-lg-12">
	                  	<div class="panel panel-default">
	                  		<div class="panel-body">

	                  			<div class="row">
	                  				<div class="col-lg-9">

										<div class="row"></div>
										
					                    <div class="row">

					                        <div class="col-lg-3">
					                          	<div class="input-group">
					                            	<span class="input-group-addon">Id</span>
					                            	<input type="number" class="form-control num showallupdatebtn" id="txtStaffId" data-showallupdatebtn='<?php echo $vouchers['staff']['update']; ?>'>
				                                    <input type="hidden" id="txtMaxStaffIdHidden">
				                                    <input type="hidden" id="txtStaffIdHidden">
				                                    <input type="hidden" id="txtPIdHidden">
				                                    <input type="hidden" id="voucher_type_hidden">
						                        </div>
					                        </div>

					                        <div class="col-lg-4">
					                            <div class="form-group">
						                            <div class="input-group input-group-block">
						                              <span class="switch-addon">Is active?</span>
						                              <input type="checkbox" checked="" class="bs_switch active_switch" id="gender">
						                            </div>
					                            </div>
					                        </div>
					                    </div>

					                    <div class="row">

					                    	<div class="col-lg-4">
					                            <div class="input-group">
					                                <span class="input-group-addon">Date</span>
					                                <input class="form-control" type="date" id="current_date" value="<?php echo date('Y-m-d'); ?>">
					                            </div>
					                        </div>
					                        <!-- <div class="col-lg-1"></div> -->
					                        <div class="col-lg-4">
					                            <div class="input-group">
					                                <span class="input-group-addon">Type</span>
					                                <select class="form-control" id="type_dropdown">
					                                	<option value="" disabled="" selected="">Choose Type</option>
					                                	<?php foreach ($types as $type): ?>
			                                              	<option value="<?php echo $type['type']; ?>"><?php echo $type['type']; ?></option>
			                                          	<?php endforeach ?>
					                                </select>
					                                <span class="input-group-btn">
					                                	<a href="#TypeModel" data-toggle="modal" class="btn btn-primary"><i class="fa fa-plus"></i></a>
					                                </span>
					                            </div>
					                        </div>

					                        <div class="col-lg-4">
					                            <div class="input-group">
					                                <span class="input-group-addon">Agreement</span>
					                                <select class="form-control" id="agreement_dropdown">
					                                	<option value="" disabled="" selected="">Choose Agreement</option>
					                                	<?php foreach ($agreements as $agreement): ?>
			                                              	<option value="<?php echo $agreement['agreement']; ?>"><?php echo $agreement['agreement']; ?></option>
			                                          	<?php endforeach ?>
					                                </select>
					                                <span class="input-group-btn">
					                                	<a href="#AgreementModel" data-toggle="modal" class="btn btn-primary"><i class="fa fa-plus"></i></a>
					                                </span>
					                            </div>
					                        </div>
					                    </div>

					                    <div class="row">
					                    	<div class="col-lg-4">
					                    		<div class="input-group">
					                                <span class="input-group-addon">WareHouse</span>
					                                <select class="form-control" id="dept_dropdown">
					                                	<option value="" disabled="" selected="">Choose Warehouse</option>
					                                	<?php foreach ($departments as $department): ?>
			                                              	<option value="<?php echo $department['did']; ?>"><?php echo $department['name']; ?></option>
			                                          	<?php endforeach ?>
					                                </select>
					                            </div>
					                    	</div>
					                    	<div class="col-lg-4">
					                    		<div class="input-group">
					                                <span class="input-group-addon">Department</span>
					                                <select class="form-control" id="department_dropdown">
					                                	<option value="" disabled="" selected="">Choose Department</option>
					                                	<?php foreach ($staffdepartments as $department): ?>
			                                              	<option value="<?php echo $department['deptid']; ?>"><?php echo $department['name']; ?></option>
			                                          	<?php endforeach ?>
					                                </select>
					                            </div>
					                    	</div>
					                    	<div class="col-lg-4">
												<div class="input-group">
													<span class="input-group-addon">Designation</span>
													<!-- <input type="text" class="form-control" id="txtDesignation"> -->
													<input type="text" list='desig' class="form-control" id="txtDesignation" />
	                                                <datalist id='desig'>
	                                                    <?php foreach ($desigs as $desig): ?>
	                                                        <option value="<?php echo $desig['designation']; ?>">
	                                                    <?php endforeach ?>
	                                                </datalist>

												</div>
											</div>
					                    </div>
	                  				</div>
	                  				<div class="col-lg-3">
                  						<div style="background: #F5F6F7;padding: 15px;border: 1px solid #ccc;box-shadow: 1px 1px 1px #000;">
				                            <div class="row">
				                              	<div class="col-lg-12">
				                                  	<div class="studentImageWrap">
				                                      	<img src="<?php echo base_url('assets/img/student.jpg'); ?>" alt="student" style="margin: auto;display: block;" id="staffImageDisplay">
				                                  	</div>
				                              	</div>
				                            </div>

				                            <div class="row">
				                              	<div class="col-lg-12">
				                                  	<input type="file" id="staffImage">
				                              	</div>
				                            </div>
                  						</div>
	                  				</div>
	                  			</div>

			                </div>
	                  	</div>
	                </div>
	            </div>  <!-- end of row 1 of basic information -->

              <div class="row">
                  <div class="col-lg-6">
                      <div class="panel panel-default">
                          <div class="panel-heading">General Information</div>

                          <div class="panel-body">

                              <div class="row">
                                  <div class="col-lg-12">
                                      <div class="input-group">
                                        <span class="input-group-addon">Name</span>
                                        <input type="text" class="form-control" placeholder="Staff name" id="txtName"/>
                                    </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-lg-12">
                                      <div class="input-group">
                                          <span class="input-group-addon">Father Name</span>
                                          <input type="text" class="form-control" placeholder="Father name" id="txtFatherName">
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-lg-12">
                                      <div class="input-group">
                                          <span class="input-group-addon">Gender</span>
                                          <select class="form-control" id="gender_dropdown">
                                            <option value='male'>Male</option>
                                            <option value='female'>Female</option>
                                          </select>
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-lg-12">
                                      <div class="input-group">
                                          	<span class="input-group-addon">Martial Status</span>
                                          	<select class="form-control" id="marital_dropdown">
                                            	<option value='single'>Single</option>
                                            	<option value='married'>Married</option>
                                            	<option value='divorced'>Divorced</option>
                                            	<option value='widowed'>Widowed</option>
                                          	</select>
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-lg-12">
	                                    <div class="input-group">
	                                        <span class="input-group-addon">Religion</span>
	                                        <select class="form-control" id="religion_dropdown">
	                                            <option value="" disabled="" selected="">Choose Religion</option>
	                                            <?php foreach ($religions as $religion): ?>
	                                              	<option value="<?php echo $religion['religion']; ?>"><?php echo $religion['religion']; ?></option>
	                                          	<?php endforeach ?>
	                                        </select>
	                                        <span class="input-group-btn">
	                                            <a href="#ReligionModel" data-toggle="modal" class="btn btn-primary"><i class="fa fa-plus"></i></a>
	                                        </span>
	                                    </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-lg-12">
                                      <div class="input-group">
                                          <span class="input-group-addon">CNIC</span>
                                          <input class="form-control num" type="text" placeholder="Staff national ID card no" id="txtcnic">
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-lg-12">
                                      <div class="input-group">
                                          <span class="input-group-addon">Date of Birth</span>
                                          <input class="form-control" type="date" id="birth_date" value="<?php echo date('Y-m-d'); ?>">
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-lg-12">
                                      <div class="input-group">
                                          <span class="input-group-addon">Date of Joining</span>
                                          <input class="form-control" type="date" id="joining_date" value="<?php echo date('Y-m-d'); ?>">
                                      </div>
                                  </div>
                              </div>

                          </div>
                      </div>
                  </div>    <!-- end of general information -->
                  <div class="col-lg-6">
                  		<div class="row">
                  			<div class="col-lg-12">
                  				<div class="panel panel-default">
		                        <div class="panel-heading">Contact Information</div>

		                        <div class="panel-body">
		                              <div class="row">
		                                  <div class="col-lg-12">
		                                      <div class="input-group">
		                                          <span class="input-group-addon">Address</span>
		                                          <input type="text" class="form-control" placeholder="Home address" id="txtAddress">
		                                      </div>
		                                  </div>
		                              </div>
		                              <div class="row">
		                                  <div class="col-lg-12">
		                                      <div class="input-group">
		                                          <span class="input-group-addon">Phone No</span>
		                                          <input type="text" class="form-control num" placeholder="Phone no" id="txtPhoneNo">
		                                      </div>
		                                  </div>
		                              </div>
		                              <div class="row">
		                                  <div class="col-lg-12">
		                                      <div class="input-group">
		                                          <span class="input-group-addon">Mobile No</span>
		                                          <input type="text" class="form-control num" placeholder="Mobile No" id="txtMobileNo">
		                                      </div>
		                                  </div>
		                              </div>
		                          </div>
		                      </div>
                  			</div>
                  		</div>

                  		<div class="row">
                  			<div class="col-lg-12">
			              		<div class="panel panel-default">
			              			<div class="panel-heading">Genra Salary Information</div>
			              			<div class="panel-body">
			              				<!--<div class="row">
			              					<div class="col-lg-12">
			              						<div class="input-group">
			              							<span class="input-group-addon">Bank Name</span>
						                            <select class="form-control" id="bank_dropdown">
						                                <option value="" disabled="" selected="">Choose Bank</option>
						                                <?php foreach ($banks as $bank): ?>
			                                              	<option value="<?php echo $bank['bankname']; ?>"><?php echo $bank['bankname']; ?></option>
			                                          	<?php endforeach ?>
						                            </select>
						                            <span class="input-group-btn">
						                            	<a href="#BankModel" data-toggle="modal" class="btn btn-primary"><i class="fa fa-plus"></i></a>
						                            </span>
			              						</div>
			              					</div>
			              				</div>-->

										<!--<div class="row">
			              					<div class="col-lg-12">
			              						<div class="input-group">
			              							<span class="input-group-addon">Account #</span>
			              							<input type="text" class="form-control" id="txtAccountNo">
			              						</div>
			              					</div>
			              				</div>-->

										<div class="row">
			              					<div class="col-lg-12">
			              						<div class="input-group">
			              							<span class="input-group-addon">Per Day</span>
			              							<input type="text" class="form-control" id="txtPerDay">
			              						</div>
			              					</div>
			              				</div>
			              				<div class="row">
			              					<div class="col-lg-12">
			              						<div class="input-group">
			              							<span class="input-group-addon">Per Day Production</span>
			              							<input type="text" class="form-control" id="txtPerdayproduction">
			              						</div>
			              					</div>
			              				</div>
			              				<div class="row">
			              					<div class="col-lg-12">
			              						<div class="input-group">
			              							<span class="input-group-addon">Salary</span>
			              							<input type="text" class="form-control" id="txtSalary" readonly>
			              						</div>
			              					</div>
			              				</div>
			              			</div>
			              		</div>
                  			</div>
                  		</div>
                  </div>    <!-- end of contact information -->
              </div>  <!-- end of row general information -->

              <div class="row">
      			<div class="col-lg-6">
              		<div class="panel panel-default">
              			<div class="panel-heading">Shift Information</div>
              			<div class="panel-body">
              				<div class="row">
              					<div class="col-lg-6">
              						<div class="input-group">
              							<span class="input-group-addon">Shift Group</span>
			                            <select class="form-control" id="shiftgroup_dropdown">
			                                <option value="" disabled="" selected="">Choose shift group</option>
			                                <?php foreach ($shiftGroups as $shiftGroup): ?>
                                              	<option value="<?php echo $shiftGroup['gid']; ?>" data-date="<?php echo substr($shiftGroup['date'], 0, 10); ?>"><?php echo $shiftGroup['name']; ?></option>
                                          	<?php endforeach ?>
			                            </select>
              						</div>
              					</div>

              					<div class="col-lg-6">
              						<div class="input-group">
              							<span class="input-group-addon">Shift Date</span>
              							<input class="form-control ts_datepicker" type="text" id="shiftgroup_date">
              						</div>
              					</div>
              				</div>
              			</div>
              		</div>
      			</div>
      		</div>

      		

            </div>    <!-- end of basicInformation -->

            <div class="tab-pane" id="salarydeductions">

            	<div class="row">
	              	<div class="col-lg-8">
	              		<div class="row">
	              			<div class="col-lg-12">	              				
								<div class="panel panel-default">
									<div class="panel-heading">Salary</div>
									<div class="panel-body">
										<div class="row">
											<div class="col-lg-6">
												<div class="input-group">
													<span class="input-group-addon">PS</span>
													<input type="text" class="form-control num" id='txtbs'>
												</div>
											</div>									
											<div class="col-lg-6">
												<div class="input-group">
													<span class="input-group-addon">Basic Pay</span>
													<input type="text" class="form-control num calc" id='txtbpay'>
												</div>
											</div>
										</div>

										<div class="row">
											<div class="col-lg-6">
												<div class="input-group">
													<span class="input-group-addon">Conv. Allow.</span>
													<input type="text" class="form-control num calc" id='txtconvallow'>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="input-group">
													<span class="input-group-addon">House Rent</span>
													<input type="text" class="form-control num calc" id='txthrent'>
												</div>
											</div>
										</div>

										<div class="row">
											<div class="col-lg-6">
												<div class="input-group">
													<span class="input-group-addon">Entertainment</span>
													<input type="text" class="form-control num calc" id='txtentertain'>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="input-group">
													<span class="input-group-addon">Med. Allow.</span>
													<input type="text" class="form-control num calc" id='txtmedallow'>
												</div>
											</div>
										</div>

										<div class="row">
											<div class="col-lg-6">
												<div class="input-group">
													<span class="input-group-addon">Other</span>
													<input type="text" class="form-control num calc" id='txtadhoc1'>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="input-group pull-right">
													<span class="input-group-addon other-addon">Gross Pay</span>
													<input type="text" class="form-control num" readonly="readonly" id='txttotalpay'>
												</div>
											</div>
										</div>
									</div>
								</div>	              			
	              			</div>
	              		</div>
	              		<div class="row">
			      			<div class="col-lg-5">
			              		<div class="panel panel-default">
			              			<div class="panel-heading">Allowed Leaves</div>
			              			<div class="panel-body">
			
										<div class="row">
			              					<div class="col-lg-12">
			              						<div class="input-group">
			              							<span class="input-group-addon">Paid Leaves</span>
			              							<input class="form-control num" type="text" id="txtAldLeaves" maxlength="3">
			              						</div>
			              					</div>
		              					</div>

										<div class="row">
			              					<div class="col-lg-12">
			              						<div class="input-group">
			              							<span class="input-group-addon">Unpaid Leaves</span>
			              							<input class="form-control num" type="text" id="txtAldUnpaidLeaves" maxlength="3">
			              						</div>
			              					</div>
		              					</div>

										<div class="row hide">
			              					<div class="col-lg-12">
			              						<div class="input-group">
			              							<span class="input-group-addon">Medical Leaves</span>
			              							<input class="form-control num" type="text" id="txtAldMedLeaves" maxlength="3">
			              						</div>
			              					</div>
			              				</div>
			              			</div>
			              		</div>
			      			</div>

			      			<div class="col-lg-7">
			              		<div class="panel panel-default">
			              			<div class="panel-heading">Over Time</div>
			              			<div class="panel-body">
			              				<div class="row">
			              					<div class="col-lg-7">
					                            <div class="row">
			                                        <div class="col-lg-5">
			                                            <label for="otallowed">
			                                                <input type="radio" id="otallowed" name="ot" value="otallowed">
			                                                Allowed
			                                            </label>
			                                        </div>

			                                        <div class="col-lg-7">
			                                            <label for="otnotallowed">
			                                                <input type="radio" id="otnotallowed" name="ot" value="otnotallowed" checked="checked">
			                                                Not Allowed
			                                            </label>
			                                        </div>
					                            </div>
					                        </div>

			              					<div class="col-lg-5">
			              						<div class="input-group">
			              							<span class="input-group-addon">OT Rate</span>
			              							<input class="form-control num" type="text" id="txtOTRate">
			              						</div>
			              					</div>
			              				</div>
			              			</div>
			              		</div>
			      			</div>
			      		</div>
	              	</div>

	              	<div class="col-lg-4">
	              		<div class="panel panel-default">
	              			<div class="panel-heading">Deductions</div>
	              			<div class="panel-body">	              				
	              				<div class="row">
	              					<div class="col-lg-12">
	              						<div class="input-group">
											<span class="input-group-addon cus-group-addon2">EOBI</span>
											<input type="text" class="form-control num calc2" id='txteobi'>
										</div>
	              					</div>
	              				</div>
	              				<div class="row">
	              					<div class="col-lg-12">
	              						<div class="input-group">
											<span class="input-group-addon cus-group-addon2">Social Security</span>
											<input type="text" class="form-control num calc2" id='txtsocialsecurity'>
										</div>
	              					</div>
	              				</div>
	              				<div class="row">
	              					<div class="col-lg-12">
	              						<div class="input-group">
											<span class="input-group-addon cus-group-addon2">Insurance</span>
											<input type="text" class="form-control num calc2" id='txtinsurance'>
										</div>
	              					</div>
	              				</div>

								<div class="row">
									<hr>
									<div class="col-lg-10">
										<div class="input-group">
											<span class="input-group-addon">Total Deductions</span>
											<input type="text" class="form-control" readonly="readonly" id='txttdeduc'>
										</div>
									</div>
								</div>
								<div class="row">
				              		<div class="col-lg-10">
	              						<div class="input-group">
											<span class="input-group-addon">Net Pay</span>
											<input type="text" class="form-control" readonly="readonly" id='txtnetpay'>
										</div>
	              					</div>
								</div>
	              			</div>
	              		</div>
	              	</div>
              	</div>

              	

              	
            </div>    <!-- end of salary -->

            <div class="tab-pane" id="qualification">
				<div class="row">
                  <div class="col-lg-12">
                      <div class="panel panel-default">
                          <div class="panel-body">
                            <div class="row">

                              	<div class="col-lg-4" style="">
                                	<div class="input-group">
                                  		<span class="input-group-addon">Quali.</span>
                                  		<input type="text" class="form-control" id='txtQuali'>
                                	</div>
                              	</div>

                              	<div class="col-lg-2" style="">
                                	<div class="input-group">
                                  		<span class="input-group-addon">Division</span>
                                  		<input type="text" class="form-control" id='txtDivision'>
                                	</div>
                              	</div>

	                            <div class="col-lg-2" style="">
	                                <div class="input-group" >
	                                  <span class="input-group-addon">Year</span>
	                                  <input type="text" class="form-control num" id='txtYear'>
	                                </div>
	                            </div>
                              	<div class="col-lg-4" style="">
	                                <div class="input-group">
	                                  <span class="input-group-addon">Name of Insti.</span>
	                                  <input type="text" class="form-control" id='txtInstitute'>
	                                </div>
                              	</div>
                          	</div>
                          	<div class="row">
                              	<div class="col-lg-4" style="">
	                                <div class="input-group">
	                                  <span class="input-group-addon">Major Subjects</span>
	                                  <input type="text" class="form-control" style="width:228px;" id='txtMSubjects'>
	                                </div>
                              	</div>
                              	<div class="col-lg-1">
	                                <div class="input-group">
	                                  <a href='' class="btn btn-primary" id='btnAddQuali'><i class="fa fa-plus"></i></a>
	                                </div>
                              	</div>
                          	</div>
                          	<div class="row">
                              <div class="col-lg-12">
                                  <table class="table table-striped table-hover" id='qualification-table'>
                                      <thead>
                                          <tr>
                                              <th class="thwidth3">Qualification/Job</th>
                                              <th class="thwidth4">Division</th>
                                              <th class="thwidth5">Year</th>
                                              <th class="thwidth6">Name of Institution</th>
                                              <th class="thwidth2">Major Subjects</th>
                                              <th></th>
                                          </tr>
                                      </thead>
                                      <tbody>

                                      </tbody>
                                  </table>
                              </div>
                          </div>
                          </div>
                      </div>
                  </div>
                </div>  <!-- end of admission test row 2 -->
            </div>     <!-- end of familyBackground -->

            <div class="tab-pane" id="experience">

                <div class="row">
                  <div class="col-lg-12">
                      <div class="panel panel-default">
                          <div class="panel-body">
                            <div class="row">
                              <div class="col-lg-3">
                                <div class="input-group">
                                  <span class="input-group-addon">Job Held</span>
                                  <input type="text" class="form-control" id='txtJobHeld'>
                                </div>
                              </div>
                              <div class="col-lg-3">
                                <div class="input-group">
                                  <span class="input-group-addon">From</span>
                                  <input class="form-control ts_datepicker" type="text" id='from_date'>
                                </div>
                              </div>
                              <div class="col-lg-3">
                                <div class="input-group">
                                  <span class="input-group-addon">To</span>
                                  <input class="form-control ts_datepicker" type="text" id='to_date'>
                                </div>
                              </div>
                              <div class="col-lg-2">
                                <div class="input-group">
                                  <span class="input-group-addon">Pay Draws</span>
                                  <input type="text" class="form-control num" id='txtPayDraws'>
                                </div>
                              </div>
                              <div class="col-lg-1">
                                <div class="input-group">
                                  <a href='' class="btn btn-primary" id='btnAddExp'><i class="fa fa-plus"></i></a>
                                </div>
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-lg-12">
                                  <table class="table table-striped table-hover" id='experience-table'>
                                      <thead>
                                          <tr>
                                              <th>Job Held</th>
                                              <th>From</th>
                                              <th>To</th>
                                              <th>Pay Draws</th>
                                              <th></th>
                                          </tr>
                                      </thead>
                                      <tbody></tbody>
                                  </table>
                              </div>
                          </div>
                          </div>
                      </div>
                  </div>
                </div>  <!-- end of admission test row 2 -->

            </div>    <!-- end of admissionTest -->

            <div id="viewall" class="tab-pane fade">

				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-body">
								<table class="table table-striped table-hover ar-datatable" id="staffLists">
									<thead>
										<tr>
											<th>Sr#</th>
											<th>Sid</th>
											<th>Name</th>
											<th>Designation</th>
											<th>Department</th>
											<th>Shift</th>
											<th>Salary</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<?php $counter = 1; foreach ($staffs as $staff): ?>
											<tr>
												<td><?php echo $counter++; ?></td>
												<td><?php echo $staff['staid']; ?></td>
												<td><?php echo $staff['name']; ?></td>
												<td><?php echo $staff['designation']; ?></td>
												<td><?php echo $staff['dept_name']; ?></td>
												<td><?php echo $staff['shift_name']; ?></td>
												<td><?php echo $staff['bpay']; ?></td>
												<!-- <td><a href="" class="btn btn-primary btn-edit-staff showallupdatebtn" data-showallupdatebtn=<?php echo $vouchers['staff']['update']; ?> data-staid="<?php echo $staff['staid']; ?>"><span class="fa fa-edit"></span></a></td> -->
												<td><a href="" class="btn btn-primary btn-edit-staff showallupdatebtn" data-showallupdatebtn="" data-staid="<?php echo $staff['staid']; ?>"><span class="fa fa-edit"></span></a></td>
											</tr>
										<?php endforeach ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>

			</div> <!-- end of search_branch -->

				<div class="tab-pane fade" id="phase">
	            	<div class="row-fluid">
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12">
										<div class="row">
											<div class="col-lg-3">
												<label>Item</label>
												<select class="form-control select2" id="item_dropdown">
													<option value="" disabled="" selected="">Choose Item</option>
													<?php foreach ($items as $item): ?>
														<option value="<?php echo $item['item_id']; ?>"><?php echo $item['item_des']; ?></option>
													<?php endforeach ?>
												</select>
											</div>
											<div class="col-lg-3">
												<label>Phase</label>
												<select class="form-control select2" id="phase_dropdown">
													<option value="" disabled="" selected="">Choose Phase</option>
													<?php foreach ($phases as $phase): ?>
														<option value="<?php echo $phase['id']; ?>"><?php echo $phase['name']; ?></option>
													<?php endforeach ?>
												</select>
											</div>
											<div class="col-lg-1" style="width:140px;">
												<label>Item Rate</label>
												<input type="text" class='form-control' id="txtRate">
											</div>
											<div class="col-lg-2">
												<label>Calc Method</label>
												<input type="text" list='type' class="form-control" id="txtCalc"/>
												<!-- <datalist id='type'>
													<?php //foreach ($types as $type): ?>
														<option value="<?php //echo $type['barcode']; ?>">
													<?php //endforeach ?>
												</datalist> -->
											</div>
											 <div class="col-lg-1">
                                                           
                                            <label>PerDay</label>
                                                 <input type="text" list='type' class="form-control" id="txtPerday"/>
                                            </div>

                                            <div class="col-lg-1">

                                                <label>Type</label>

                                                <select class="form-control select2" id="txttype">
                                                    <option value="" disabled="" selected="">Choose Type</option>
                                                    <option value="head" >Head</option>
                                                    <option value="sparepart">SparePart</option>
                                                </select>
                                            </div>


											<div class="col-lg-1" style='margin-top: 30px;'>                                                    
	                                            <a href="" class="btn btn-primary" id="btnAddItem"><i class="fa fa-plus"></i></a>
	                                        </div> 
										</div>
									</div>
								</div>
							</div>
						</div>
	            	</div>
					<div class="row">
						<div class="col-lg-12" style="margin-top:-20px;">
							<div class="panel panel-default">
								<div class="panel-body">
									<table class="table table-striped table-hover" id="item_table">
										<thead>
											<tr>
												<th style='background: #368EE0;'>Sr#</th>
												<th style='background: #368EE0;'>Item</th>
												<th style='background: #368EE0;'>Phase</th>
												<th style='background: #368EE0;'>Item Rate</th>
												<th style='background: #368EE0;'>Method</th>
												<th style='background: #368EE0;'>Per Day Production</th>
												<th style='background: #368EE0;'>Action</th>
											</tr>
										</thead>
										<tbody>
											<!-- <?php $counter //= 1; foreach ($items as $item): ?>
												<tr>
													<td><?php //echo $counter++; ?></td>
													<td><?php //echo $item['category_name']; ?></td>
													<td><?php //echo $item['subcategory_name']; ?></td>
													<td><?php //echo $item['brand_name']; ?></td>
													<td><?php //echo $item['item_des']; ?></td>
													<td><a href="" class="btn btn-sm btn-primary btn-edit-item" data-itemid="<?php //echo $item['item_id']; ?>"><span class="fa fa-edit"></span></a>
													<a href="" class="btn btn-sm btn-primary btn-edit-item" data-itemid="<?php //echo $item['item_id']; ?>"><span class="fa fa-edit"></span></a></td>
												</tr>
											<?php //endforeach ?> -->
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
	            </div>
          </div>

          <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                  <div class="panel-body">
                    <a href='' class="btn btn-default btnSave" data-insertbtn='<?php echo $vouchers['staff']['insert']; ?>'>
                      <i class="fa fa-save"></i>
                     Save</a>
                     <a href='' class="btn btn-default btnReset">
                      <i class="fa fa-refresh"></i>
                     Reset</a>
                  </div>
              </div>
            </div>
          </div>    <!-- end of a href='' row -->


        </form>   <!-- end of form -->

      </div>  <!-- end of col -->
    </div>  <!-- end of container fluid -->
  </div>   <!-- end of page_content -->
</div>

<div id="TypeModel" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="model-contentwrapper">
	    <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	        <h3 id="myModalLabel">Add Type</h3>
	    </div>
	    <div class="modal-body">

	      <div class="input-group">
	        <span class="input-group-addon">Type</span>
	        <input type="text" class="form-control" id="txtNewType">
	      </div>
	    </div>
	    <div class="modal-footer">
	      <div class="pull-right">
	        <a class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</a>
	        <a class="btn btn-default btnNewType"><i class="fa fa-plus"></i> Add</a>
	      </div>
	    </div>
	</div>
</div>

<div id="AgreementModel" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="model-contentwrapper">
	    <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	        <h3 id="myModalLabel">Add Agreement</h3>
	    </div>
	    <div class="modal-body">

	      <div class="input-group">
	        <span class="input-group-addon">Agreement</span>
	        <input type="text" class="form-control" id="txtNewAgreement">
	      </div>
	    </div>
	    <div class="modal-footer">
	      <div class="pull-right">
	        <a class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</a>
	        <a class="btn btn-default btnNewAgreement"><i class="fa fa-plus"></i> Add</a>
	      </div>
	    </div>
	</div>
</div>

<div id="ReligionModel" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="model-contentwrapper">
	    <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	        <h3 id="myModalLabel">Add Religion</h3>
	    </div>
	    <div class="modal-body">

	      <div class="input-group">
	        <span class="input-group-addon">Religion</span>
	        <input type="text" class="form-control" id="txtNewReligion">
	      </div>
	    </div>
	    <div class="modal-footer">
	      <div class="pull-right">
	        <a class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</a>
	        <a class="btn btn-default btnNewReligion"><i class="fa fa-plus"></i> Add</a>
	      </div>
	    </div>
	</div>
</div>

<div id="BankModel" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="model-contentwrapper">
	    <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	        <h3 id="myModalLabel">Add Bank</h3>
	    </div>
	    <div class="modal-body">

	      <div class="input-group">
	        <span class="input-group-addon">Bank</span>
	        <input type="text" class="form-control" id="txtNewBank">
	      </div>
	    </div>
	    <div class="modal-footer">
	      <div class="pull-right">
	        <a class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</a>
	        <a class="btn btn-default btnNewBank"><i class="fa fa-plus"></i> Add</a>
	      </div>
	    </div>
	</div>
</div>