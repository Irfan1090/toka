<?php
	$desc = $this->session->userdata('desc');
	$desc = json_decode($desc);
	$desc = objectToArray($desc);
	$vouchers = $desc['vouchers'];
?>

<!-- main content -->
<div id="main_wrapper">
	<div class="page_bar">
		<div class="row-fluid">
			<div class="col-md-12">
				<h1 class="page_title"><i class="fa fa-money"></i> Add Currency</h1>
			</div><!-- end of col -->
		</div><!-- row-fluid -->
	</div><!-- page-bar -->
	<div class="page_content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<ul class="nav nav-tabs" id="tabs_a">
						<li class="active"><a data-toggle="tab" href="#add_dept">Add Currency </a></li>
						<li class=""><a data-toggle="tab" href="#view_all">View All</a></li>
					</ul>
					<div class="tab-content">
						<div id="add_dept" class="tab-pane fade active in">
							<div class="row">
								<div class="col-lg-12">
									<div class="panel panel-default" style="margin-top:-10px;">
										<div class="panel-body">
											<form action="">
												<div class="form-group">
													<div class="row">
														<div class="col-lg-1" style='width:120px;'>
															<label>Currency ID</label>
															<input type="number" class="form-control num" id="txtId">
															<input type="hidden" id="txtIdHidden">
															<input type="hidden" id="txtMaxIdHidden">
															<input type="hidden" id="vouchertypehidden">
															<input type="hidden" name="uid" id="uid" value="<?php echo $this->session->userdata('uid'); ?>">
	                            <input type="hidden" name="uname" id="uname" value="<?php echo $this->session->userdata('uname'); ?>">
	                            <input type="hidden" name="cid" id="cid" value="<?php echo $this->session->userdata('company_id'); ?>">
														</div>
													</div>
												</div><!-- form-group -->
												<div class="form-group">
													<div class="row">
														<div class="col-lg-3">
															<label>Name</label>
															<input type="text" class="form-control " id="txtName">
														</div>
														<div class="col-lg-2">
															<label>Currency Symbol</label>
															<input type="text" class="form-control " id="txtCurrencySymbol">
														</div>
														<div class="col-lg-2">
															<label>Exchange Rate</label>
															<input type="text" class="form-control  num" id="txtExchangeRate">
														</div>
													</div>
												</div><!-- form-group -->
												<!-- <div class="row" >
													<div class="col-lg-3">
		                        <div class="input-group">
		                          <span class="input-group-addon">User: </span>
		                          <select class="form-control " disabled="" id="user_dropdown">
		                            <option value="" disabled="" selected="">...</option>
		                            <?php foreach ($userone as $user): ?>
		                            <option value="<?php echo $user['uid']; ?>"><?php echo $user['uname']; ?></option>
		                            <?php endforeach; ?>
		                          </select>
		                        </div>
		                      </div>
		                    </div> -->
												<!--<div class="row">
													<div class="col-lg-3">
														<div class="input-group">
															<div class="input-group-addon txt-addon">Description</div>
															<input type="text" class="form-control " id="txtDescription">
														</div>
													</div>
												</div> -->
												<div class="row">
													<div class="col-lg-12">
														<div class="pull-right">
															<a class="btn btn-sm btn-default btnSave" data-saveaccountbtn='<?php echo $vouchers['account']['insert']; ?>' data-saveitembtn='<?php echo $vouchers['item']['insert']; ?>' data-insertbtn='<?php echo $vouchers['currency']['insert']; ?>' data-updatebtn='<?php echo $vouchers['currency']['update']; ?>' data-deletebtn='<?php echo $vouchers['currency']['delete']; ?>' data-printbtn='<?php echo $vouchers['currency']['print']; ?>' ><i class="fa fa-save"></i> Save F10</a>
															<a class="btn btn-sm btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
														</div><!-- pull-right -->
													</div><!-- end of col -->
												</div><!-- end of row -->
											</form><!-- end of form -->
										</div><!-- end of panel-body -->
									</div><!-- end of panel -->
								</div><!-- end of col -->
							</div><!-- end of row -->
						</div><!-- end of add_branch -->
						<div id="view_all" class="tab-pane fade">
							<div class="row">
								<div class="col-lg-12">
									<div class="panel panel-default" style="margin-top:-10px;">
										<div class="panel-body">
											<table class="table table-striped table-hover ar-datatable">
												<thead>
													<tr>
														<th style='background: #368EE0;'>Sr#</th>
														<th style='background: #368EE0;'>Name</th>
														<th style='background: #368EE0;'>Symbol</th>
														<th style='background: #368EE0;' class="text-right">Exchange Rate</th>
														<th style='background: #368EE0;' >Action</th>
													</tr>
												</thead>
												<tbody>
													<?php $counter = 1; foreach ($currenceys as $currencey): ?>
														<tr>
															<td><?php echo $counter++; ?></td>
															<td><?php echo $currencey['name']; ?></td>
															<td><?php echo $currencey['cur_symbol']; ?></td>
															<td class="text-right"><?php echo $currencey['exchange_rate']; ?></td>
															<td><a href="" class="btn btn-sm btn-primary btn-edit-dept" data-id="<?php echo $currencey['id']; ?>"><span class="fa fa-edit"></span></a></td>
														</tr>
													<?php endforeach ?>
												</tbody>
											</table>
										</div><!-- panel-body -->
									</div><!-- end of panel -->
								</div><!-- end of col -->
							</div><!-- end of row -->
						</div><!-- end of search_branch -->
					</div><!-- tab-content -->
				</div><!-- end of col -->
			</div><!-- end of row -->
		</div><!-- container-fluid -->
	</div><!-- page-content -->
</div><!-- main-wrapper -->