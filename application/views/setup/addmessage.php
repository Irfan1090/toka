<?php
    $desc = $this->session->userdata('desc');
    $desc = json_decode($desc);
    $desc = objectToArray($desc);
    $vouchers = $desc['vouchers'];
?>
	<!-- main content -->
<div id="main_wrapper">
	<div class="page_bar">
		<div class="row-fluid">
			<div class="col-md-12">
				<h1 class="page_title"><i class="ion-android-mail"></i> Message to Dealers</h1>
			</div><!-- end of col -->
		</div><!-- row-fluid -->
	</div><!-- page-bar -->
	<div class="page_content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<ul class="nav nav-tabs" id="tabs_a">
						<li class="active"><a data-toggle="tab" href="#add_message"><i class="ion-android-mail"></i> Message</a></li>
						<li class=""><a data-toggle="tab" href="#view_all"><i class="fa fa-users"></i> View All Parties</a></li>
					</ul>
					<div class="tab-content">
						<div id="add_message" class="tab-pane fade active in">
							<div class="row">
								<div class="col-lg-12">
									<div class="panel panel-default" style="margin-top:-10px;">
										<div class="panel-body">
											<form action="">
												<div class="form-group">
													<div class="row">
														<div class="col-lg-1">
															<label>ID(Auto)</label>
															<input type="number" class="form-control num txtidupdate" id="txtId">
															<input type="hidden" id="txtIdHidden">
															<input type="hidden" id="txtMaxIdHidden">
															<input type="hidden" id="vouchertypehidden">
														</div>
														<div class="col-lg-2">
															<label>Date</label>
															<input class="form-control ts_datepicker" type="text" id="current_date">           
														</div><!-- end of col -->
													</div><!-- end of row -->
												</div><!-- form-group -->
												<div class="form-group">
													<div class="row">
														<div class="col-lg-3">
															<label>Party <i style="color:red;">*</i></label>
															<select class="form-control select2" id="party_dropdown11" >
	                              <option value="" disabled="" selected="">Choose party</option>
	                              <?php foreach ($parties as $party): ?>
	                              <option value="<?php echo $party['pid']; ?>"><?php echo $party['name']; ?></option>
	                              <?php endforeach ?>
	                          	</select>
														</div>
														<div class="col-lg-2">
															<label>City</label>
															<input type="text" class="form-control" id="txtcity">
														</div>
														<div class="col-lg-2">
															<label>Country</label>
															<input type="text" class="form-control" id="txtcountry">
														</div>
														<div class="col-lg-2">
															<label>Mobile</label>
															<input type="text" class="form-control" id="txtmobile">
														</div><!-- end of col -->
													</div><!-- end of row -->
												</div><!-- form-group -->
												<div class="form-group">
													<div class="row">
														<div class="col-lg-9">
															<label>Message</label>
															<textarea rows="4" class="form-control" id="message_text" placeholder="Text Message ........"></textarea>
														</div><!-- end of col -->
													</div><!-- end of row -->
												</div><!-- form-group -->
												<div class="form-group">
													<div class="row">
														<div class="col-lg-1 col-lg-offset-7">
															<a class="btn btn-primary btn-lg" style="margin-left: 10px;"><i class="ion-android-mail"></i> Send Message</a>
														</div><!-- end of col -->
													</div><!-- end of row -->
												</div><!-- form-group -->
												<div class="row">
													<div class="col-lg-12">
														<div class="pull-right">
															<a class="btn btn-sm btn-default btnSave" data-saveaccountbtn='<?php echo $vouchers['account']['insert']; ?>' data-saveitembtn='<?php echo $vouchers['item']['insert']; ?>' data-insertbtn='<?php echo $vouchers['sendmessage']['insert']; ?>' data-updatebtn='<?php echo $vouchers['sendmessage']['update']; ?>' data-deletebtn='<?php echo $vouchers['sendmessage']['delete']; ?>' data-printbtn='<?php echo $vouchers['sendmessage']['print']; ?>' ><i class="fa fa-save"></i> Save F10</a>
															<a class="btn btn-sm btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
														</div><!-- pull-right -->
													</div><!-- end of col -->
												</div><!-- end of row -->
											</form><!-- end of form -->
										</div><!-- end of panel-body -->
									</div><!-- end of panel -->
								</div><!-- end of col -->
							</div><!-- end of row -->
						</div><!-- end of add_branch -->
						<div id="view_all" class="tab-pane fade">
							<div class="row">
								<div class="col-lg-12">
									<div class="panel panel-default" style="margin-top: -10px;">
										<div class="panel-body">
											<table class="table table-striped table-hover ar-datatable" id="mesage_table">
												<thead>
													<tr>
														<th>Sr#</th>
														<th>Name</th>
														<th>Mobile#</th>
														<th>Address</th>
														<th>City</th>
														<th>Country</th>
														<th></th>
													</tr>
												</thead>
												<tbody>
													<?php $counter = 1; foreach ($accounts as $account): ?>
													<tr>
														<td ><?php echo $counter++; ?></td>
														<td class="partyid" style='display:none;'><?php echo $account['pid']; ?></td>
														<td class="partyname"><?php echo $account['name']; ?></td>
														<td class="partymobile"><?php echo $account['mobile']; ?></td>
														<td class="partyaddress"><?php echo $account['address']; ?></td>
														<td class="partycity"><?php echo $account['city']; ?></td>
														<td class="partycountry"><?php echo $account['country'] ?></td>
														<td><a href="" class="btn btn-sm btn-primary btn-edit-account showallupdatebtn" data-pid="<?php echo $account['pid']; ?>" data-showallupdatebtn=<?php echo $vouchers['account']['update']; ?>><span class="fa fa-edit"></span></a></td>
													</tr>
													<?php endforeach ?>
												</tbody>
											</table>
										</div><!-- panel-body -->
									</div><!-- end of panel -->
								</div><!-- end of col -->
							</div><!-- end of row -->
						</div><!-- end of view all tab -->
					</div><!-- tab-content -->
				</div><!-- end of col -->
			</div><!-- end of row -->
		</div><!-- cotainer-fluid -->
	</div><!-- page-content -->
</div><!-- main-wrapper -->