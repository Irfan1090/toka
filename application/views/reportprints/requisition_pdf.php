<!doctype html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Voucher</title>

		<style>
			 * { margin: 0; padding: 0; font-family: tahoma !important; }
			 body { font-size:14px !important; }
			 p { margin: 0 !important; /* line-height: 17px !important; */ }
			 .field { font-size:14px !important; font-weight: bold !important; display: inline-block !important; width: 10px !important; } 
			 .field1 { font-size:14px !important; font-weight: bold !important; display: inline-block !important; width: 150px !important; } 
			 .voucher-table{ border-collapse: none !important; }
			 table { width: 100% !important; border: 0.5px solid black !important; border-collapse:collapse !important; table-layout:fixed !important; margin-left:1px}
			 th {  padding: 5px !important; }
			 td { /*text-align: center !important;*/ vertical-align: top !important;  }
			 td:first-child { text-align: left !important; }
			 .voucher-table thead th {background: #ccc !important; } 
			 tfoot {border-top: 0.5px solid black !important; } 
			 .bold-td { font-weight: bold !important; border-bottom: 0px solid black !important;}
			 .nettotal { font-weight: bold !important; font-size: 14px !important; border-top: 0.5px solid black !important; }
			 .invoice-type { border-bottom: 0.5px solid black !important; }
			 .relative { position: relative !important; }
			 .signature-fields{ font-size: 14px; border: none !important; border-spacing: 20px !important; border-collapse: separate !important;} 
			 .signature-fields th {border: 0px !important; border-top: 1px solid black !important; border-spacing: 10px !important; }
			 .inv-leftblock { width: 280px !important; }
			 .text-left { text-align: left !important; }
			 .text-right { text-align: right !important; }
			 td {font-size: 14px !important; font-family: tahoma !important; line-height: 14px !important; padding: 4px !important; } 
			 .rcpt-header { width: 700px !important; margin: 0px; display: inline;  top: 0px; right: 0px; }
			 .inwords, .remBalInWords { text-transform: uppercase !important; }
			 .barcode { margin: auto !important; }
			 h3.invoice-type {font-size: 16px !important; line-height: 24px !important;}
			 .extra-detail span { background: #7F83E9 !important; color: white !important; padding: 5px !important; margin-top: 17px !important; display: block !important; margin: 5px 0px !important; font-size: 14px !important; text-transform: uppercase !important; letter-spacing: 1px !important;}
			 .nettotal { color: red !important; font-size: 14px !important;}
			 .remainingBalance { font-weight: bold !important; color: blue !important;}
			 .centered { margin: auto !important; }
			 p { position: relative !important; font-size: 14px !important; }
			 thead th { font-size: 14px !important; font-weight: bold !important; padding: 10px !important; }
			 .fieldvalue { font-size:14px !important; position: absolute !important; width: 497px !important; }

			 @media print {
			 	.noprint, .noprint * { display: none !important; }
			 }
			 .pl20 { padding-left: 20px !important;}
			 .pl40 { padding-left: 40px !important;}
				
			.barcode { float: right !important; }
			.item-row td { font-size: 14px !important; padding: 10px !important; border-top: 0.5px solid black !important;}
			.footer_company td { font-size: 8px !important; padding: 10px !important; border-top: 0.5px solid black !important;}

			
			h3.invoice-type { border: none !important; margin: 0px !important; position: relative !important; top: 34px !important; }
			tfoot tr td { font-size: 14px !important; padding: 10px !important;  }
			.nettotal, .subtotal, .vrqty,.vrweight { font-size: 14px !important; font-weight: bold !important;}
			.footer {
	            position: absolute;
	            color: red;
	            bottom: 0;
        	}
        	.break{
		        display: block;
		        clear: both;
		        page-break-after: always;
		        border :1px solid red
		      }
		      .page-breaker {
		      display: block;
		      page-break-after: always;
		      border :1px solid red
		      }

        	@page{  
        		.signature-fields{
			    	position:fixed;
			    	bottom:0;
		    }   }
		</style>
	</head>
	<body>
		<div class="container-fluid" style="">
			<div class="row-fluid">
				<div class="span12 centered">
					<div class="row-fluid">
						<div class="span12"><img class="rcpt-header" src="<?php echo $header_img;?>" alt=""></div>
					</div>

					
					<div class="row-fluid">
						<table class="voucher-table" style="border:none !important;">
							<tbody>
								<tr>
									<td  style=" width: 10px; font-weight:bold !important; font-size:20px !important;">Noted By:</td>
									<td style=" width: 50px; "> <?php echo $vrdetail[0]['noted_by'];?> </td>
									<td  style=" width: 10px; border-left:1px solid !important; border-top:1px solid !important;  font-weight:bold !important; font-size:16px !important;">Req#</td>
									<td style=" width: 10px; border-right:1px solid !important; border-top:1px solid !important;"> <?php echo $vrdetail[0]['vrnoa'];?> </td>
								</tr>
								<tr>
									<td  style=" width: 10px; font-weight:bold !important; font-size:16px !important;">Remarks:</td>
									<td style=" width: 50px; "> <?php echo $vrdetail[0]['remarks'];?> </td>
									<td  style=" width: 10px; border-left:1px solid !important; border-bottom:1px solid !important;font-weight:bold !important; font-size:16px !important; ">Date:</td>
									<td style=" width: 10px; border-right:1px solid !important; border-bottom:1px solid !important; "> <?php echo date('d-M-y', strtotime($vrdetail[0]['vrdate']));;?> </td>
									
								</tr>
							</tbody>
						</table>
					</div>

					<div class="block pull-right" style="width:280px !important; float: right; display:inline !important;">
						<h3 class="invoice-type text-right" style="border:none !important; margin: 0px !important; position: relative; top: 12px !important; font-size:20px !important; "><?php echo $title;?></h3>
					</div>

					<br>
					<br>
					<br>
					
					<div class="row-fluid">
						<table class="voucher-table">
							<thead>
								<tr>
									<th style=" width: 10px; ">Sr#</th>
									<th style=" width: 100px; text-align:left; ">Description</th>
									<th style=" width: 15px; " class='text-right'>Qty</th>
									<th style=" width: 15px; " class='text-right'>Rate</th>
									<!-- <th style=" width: 30px; text-align: left; ">Last Vendor</th> -->
								</tr>
							</thead>

							<tbody>
								
								<?php 
									$serial = 1;
									$netQty = 0;
									$netRate=0;


									foreach ($vrdetail as $row):
										$netQty += abs($row['qty']);
										$netRate  += abs($row['bundle']);
								?>
									<tr  class="item-row">
									   <td class='text-left'><?php echo $serial++;?></td>
									   <td class='text-left'><?php echo $row['item_name'];?></td>
									   <td class='text-right'><?php echo number_format(abs($row['qty']),0);?></td>
									   <td class='text-right'><?php echo number_format(abs($row['bundle']),0);?></td>
                                      <!--  <td class='text-left'><?php //echo $row['last_vendor'];?></td> -->
									</tr>
									
								<?php endforeach?>
							</tbody>
							<tfoot>
								<tr class="foot-comments">
									<td class="subtotal bold-td text-right" style="text-align: right !important;" colspan="2">Subtotal:</td>
									<td class="subtotal bold-td text-right"><?php echo number_format($netQty,0);?></td>
									<td class="subtotal bold-td text-right"><?php echo number_format($netRate,0);?></td>
									<!-- <td></td> -->
								</tr>
							</tfoot>
						</table>
					</div>
					<div class="row-fluid">
						<div class="span12 add-on-detail1" style="margin-top: 10px;">
							<!-- <p class="" style="text-transform1: uppercase;">
								<strong>In words: </strong> <span class="inwords"></span><?php echo $amtInWords;?>ONLY <br>
								<br>
									<p><span class="field1">Previous Balance:</span><span class="fieldvalue inv-vrnoa"><?php echo number_format($previousBalance,0);?></span></p>
									<p><span class="field1">This Invoice:</span><span class="fieldvalue inv-date"><?php echo number_format($vrdetail[0]['namount'],0);?></span></p>
									<p><span class="field1">Current Balance:</span><span class="fieldvalue cust-name"><?php echo number_format($vrdetail[0]['namount']+$previousBalance,2) ;?></span></p>
							</p> -->
						</div>
					</div>
					<!-- End row-fluid -->
					<br>
					<br>
					<div class="row-fluid">
						<div class="span12">
							<table class="signature-fields">
								<thead>
									<tr>
										<th>Approved By</th>
										<th>Accountant</th>
										<th>Received By</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
					<div class="row-fluid">
						<p>
							<span class="footer_company">User:<?php echo $user;?></span><br>
							<!-- <span class="footer_company">Sofware By: www.digitalsofts.com, Mob:03218661765</span> -->
						</p>
					</div>
				</div>
			</div>
		</div>
	</body>
	</html><!doctype html>
	