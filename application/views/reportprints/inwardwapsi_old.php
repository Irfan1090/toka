<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
    
<style type="text/css">
  body{
}
.container{
  width:350px;
  margin:0 auto;
}
.header{
  float:left;
}
.header h1{
  text-align:center;
}
.header p{
  width:175px;
  float:left;
  text-align:right;
}
.table1{
  float:left;
}
td{
  width:86px;
}
.row-head{
  border-top: 1px solid #000;
  border-bottom: 1px solid #000;
    float: left;
    width: 350px;
}
.row-content{
  float:left;
}
.footer{
  margin-top:10px;
  float:left;
}
.footer h3{
  text-align:right;
}
body, h1, h2, h3, h4, h5, h6, p, span,table,tbody,tr,td,th,tfoot, input, title {
    font-family: Jameel Noori Nastaleeq, Nafees Web Naskh, Arial, Urdu Naskh Asiatype, Tahoma, Unicode MS !important;
  }body, h1, h2, h3, h4, h5, h6, p, span,table,tbody,tr,td,th,tfoot, input, title {
    font-family: Jameel Noori Nastaleeq, Nafees Web Naskh, Arial, Urdu Naskh Asiatype, Tahoma, Unicode MS !important;
  }
</style>
</head>

<body>
<div class="container">
  <div class="header">
    <h1>ان ورڈ واپسی</h1>
    <p><span class="inv_data"></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:واؤچر نمبر</p>
    <p><span class="date_data"></span>&nbsp;&nbsp;&nbsp;&nbsp;:تاریخ</p>
    <p style="width:100%; text-align:right;">:پارٹی<span  class="party_data" style="text-decoration:underline; width:88%; float:left; "></span></p>
    <p style="width:100%;">:تفصیل<span class="remarks_data" style="text-decoration:underline;text-align:right;float:left; width:85%;"></span></p>
  </div>
  <div class="table1">
    <table >
      <thead>
        <tr class="row-head">
          <td>وزن</td>
          <td>تعداد</td>
          <td>گودام</td>
          <td>آئٹم تفصیل</td>
        </tr>
      </thead>
      <tbody class="myrows">
        
      </tbody>
      <tfoot>
        <tr class="row-head">
          <td style="width:200px; text-align:center;">کل ٹوٹل:<span class="total-qty"></span></td>
          <td></td>
          <td></td>
        </tr>
      </tfoot>
    </table>
  </div>
  <div class="footer">
  <h3>ٹرانسپورٹر</h3>
  <p>Prepared By:&nbsp;&nbsp;&nbsp;<span>_____________________________</span></p>
  <p>Received By:&nbsp;&nbsp;&nbsp;<span>_____________________________</span></p>
  <p>Approved By:&nbsp;&nbsp;<span>_____________________________</span></p>
  </div>
</div>
<script type="text/javascript" src="../../../assets/js/jquery.min.js"></script>
    <script src="../../../assets/js/handlebars.js"></script>
    <script type="text/javascript">
    $(function(){
      
      var opener = window.opener;
      
          
            var item_des = "";
            var qty = "";
            var weight = "";
            var godown = "";
            var rate = "";
            var amount = "";
           opener.$('#salereturn_table').find('tbody tr').each(function(index, elem)
           {
              // item_code += $.trim($(elem).find('td.item_desc').data('item_code')) + "<br>";

              item_des += $.trim($(elem).find('td.item_desc').data('uitem_des')) + "<br>";
              godown += opener.$('#dept_dropdown').find('option:selected').text() + "<br>";
              qty += $.trim($(elem).find('td.qty').text()) + "<br>";
              weight += $.trim($(elem).find('td.weight').text()) + "<br>";
           });

            $('.myrows').empty();
            var htmls = "<tr class='row-content'>";
            
            htmls += "<td>" +  weight + "</td>";
            htmls += "<td>" +  qty + "</td>";
            htmls += "<td class='text-right'>" + godown + "</td>";
            htmls += "<td class='text-right'>" + item_des + "</td>";
            htmls += "</tr>";
            $(".myrows").append(htmls);
            var htmls = "";
            htmls += "<tr class='tbody'>";
            htmls += "</tr>";
            $(".myrows").append(htmls);


        var party_data = opener.$('#party_dropdown').find('option:selected').data('uname');//+ ' ' +opener.$('#party_dropdown').find('option:selected').data('uaddress');

      // $('.shadowhead').html('Recipe Costing');
      $('.inv_data').text(opener.$('#txtVrnoa').val());
      $('.date_data').text((opener.$('#current_date').val()).substr(0,10));
      $('.party_data').text(party_data);
      $('.noted_by').html(opener.$('#txtNotedBy').val());
      $('.remarks_data').html(opener.$('#txtRemarks').val());
      $('.total-qty').html(opener.$('#txtTotalQty').val());
      /*$('.total-amount').html(opener.$('#txtTotalAmount').val());
      $('.cbalance').html(opener.$('#party_dropdown').find('option:selected').data('pbalance'));
      $('.tbalance').html(opener.$('#txtNetAmount').val());*/

      });
    </script>
</body>
</html>
