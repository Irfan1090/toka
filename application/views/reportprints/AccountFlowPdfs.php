<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Voucher</title>

    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/css/bootstrap-responsive.min.css">
    <link rel="stylesheet" href="../../../assets/bootstrap/css/bootstrap.min.css">

    <style>
        * { margin: 0; padding: 0; font-family: tahoma; }
        body { font-size:8px; }
        p { margin: 0; /* line-height: 17px; */ }
        .field {font-weight: bold; display: inline-block; width: 150px; }
        .voucher-table{ border-collapse: collapse;border:none !important; }
        table { width: 100%; border: 1px solid black; border-collapse:collapse; table-layout:fixed;margin-top: 8%;}
        th { border: 1px solid black; padding: 5px; }
        td { /*text-align: center;*/ vertical-align: top; /*padding: 5px 10px;*/ border-left: 1px solid black;}
        td:first-child { text-align: left; }
        .voucher-table thead th {background: white; }
        tfoot {border-top: 1px solid black; }
        .bold-td { font-weight: bold; border-bottom: 1px solid black;}
        .nettotal { font-weight: bold; font-size: 11px !important; border-top: 1px solid black; }
        .invoice-type { border-bottom: 1px solid black; }
        .relative { position: relative; }
        .signature-fields{ border: none; border-spacing: 20px; border-collapse: separate;}
        .signature-fields th {border: 0px; border-top: 1px solid black; border-spacing: 10px; }
        .inv-leftblock { width: 280px; }
        .text-left { text-align: left !important; }
        .text-right { text-align: right !important; }
        td {font-size: 15px !important; font-family: tahoma; line-height: 13px !important; padding: 4px;  }
        .rcpt-header { width: 450px; margin: auto; display: block; }
        .inwords, .remBalInWords { text-transform: uppercase; }
        .barcode { margin: auto; }
        h3.invoice-type {font-size: 40px; line-height: 35px;}
        .extra-detail span { background: #7F83E9; color: white; padding: 5px; margin-top: 17px; display: block; margin: 5px 0px; font-size: 10px; text-transform: uppercase; letter-spacing: 1px;}
        .nettotal { color: red; font-size: 12px;}
        .remainingBalance { font-weight: bold; color: blue;}
        .centered { margin: auto; }
        p { position: relative; font-size: 16px; }
        thead th { font-size: 18px !important; font-weight: bold; }
        .fieldvalue.cust-name {position: absolute; width: 497px; }
        @media print {
            .noprint, .noprint * { display: none; }
        }
        .pl20 { padding-left: 20px !important;}
        .pl40 { padding-left: 40px !important;}
        .barcode { float: right; }
        .item-row td { font-size: 20px; line-height:24px;  padding: 10px; border:none !important;}
        .rcpt-header { width: 205px !important; margin: 0px; display: inline; position: absolute; top: 0px; right: 0px; }
        h3.invoice-type { border: none !important; margin: 0px !important; position: relative; top: 0px;left:350px; }
        tfoot tr td { font-size: 20px; padding: 5px; }
        .nettotal, .subtotal, .vrqty { font-size: 50px !important; font-weight: normal !important;}
        tr{ page-break-inside: avoid;}
    </style>
</head>
<body>
<div class="container-fluid" style="">
    <div class="row-fluid">
        <div class="span12 centered">
            <div class="row-fluid">
                <div class="span12" >
                    <h3 class="invoice-type"><?php echo $title; ?></h3><br>
                </div>
            </div><br>
            <div class="row-fluid relative">
                <div class="span12">
                    <h3 style="text-align:center;font-size:19px;" >Account Code<?php echo $accountCode; ?> <span><?php echo $account; ?></span></h3><br>
                    <p style="text-align:center;font-size:19px;"><span class="fieldvalue inv-date"><?php echo $date_between; ?></span></p>
                </div>
            </div>
            <div class="row-fluid">
                <table class="voucher-table" style="margin-top: 10px;">
                    <thead>
                    <tr>
                        <th class='text-left' style="width: 50px;border:none !important;border-top:1px solid !important;border-bottom:1px solid !important; ">Voucher</th>
                        <th class='text-left' style=" width: 80px;border:none !important;border-top:1px solid !important;border-bottom:1px solid !important; ">Voucher Date</th>
                        <th class='text-left' style=" width: 0px;border:none !important;border-top:1px solid !important;border-bottom:1px solid !important; "></th>
                        <th class='text-left' style=" width: 150px;border:none !important;border-top:1px solid !important;border-bottom:1px solid !important; ">Acount Name</th>
                        <th class='text-right' style=" width: 60px;border:none !important;border-top:1px solid !important;border-bottom:1px solid !important; ">Receipt</th>
                        <th class='text-right' style=" width: 60px;border:none !important;border-top:1px solid !important;border-bottom:1px solid !important; ">Payment</th>
                        <th class='text-right' style=" width: 60px;border:none !important;border-top:1px solid !important;border-bottom:1px solid !important; ">Balance</th>
                        <th class='text-right' style=" width: 15px;border:none !important;border-top:1px solid !important;border-bottom:1px solid !important; "></th>
                        <th class='' style=" width: 150px;border:none !important;border-top:1px solid !important;border-bottom:1px solid !important; ">Narration</th>
                    </tr>
                    </thead>
                    <tbody style="border:none !important;">
                    <tr class="item-row">
                        <td  class='text-left'><b>Opening Balance</b></td>
                        <td  class="text-right"></td>
                        <td  class="text-right"></td>
                        <td  class="text-right"></td>
                        <td  class="text-right"></td>
                        <td  class="text-right"></td>
                        <td  class="text-right"><?php echo ($previousBalance!=0?number_format($previousBalance,2):'-');?></td>
                        <td class="text-right"><?php echo ($previousBalance<0?"Cr":"Dr");?></td>
                        <td  class=""></td>
                    </tr>
                    <?php
                    $serial = 1;
                    $Total_Debit = 0.00;
                    $Total_Credit = 0.00;
                    $Total_Balance = 0.00;
                    $Total_Balance = $previousBalance;
                    foreach ($pledger as $row):

                        $Total_Debit += $row['DEBIT'];
                        $Total_Credit += $row['CREDIT'];
                        $Total_Balance -= (float)$row['DEBIT'];
                        $Total_Balance += (float)$row['CREDIT'];
                        ?>
                        <tr class="item-row">
                            <td  class='text-left' style="font-size: 16px;"><?php echo $row['VRNOA'].'-'.substr($row['ETYPE'],0,8);?></td>
                            <td  class="text-left"><?php echo date_format (new DateTime($row['VRDATE']), 'd-M-Y'); ?></td>
                            <td  class="text-left"></td>
                            <td  class="text-left"><?php echo $row['acc_name']; ?></td>
                            <td  class="text-right"><?php if($row['CREDIT']==0) {echo '-';} else { echo number_format($row['CREDIT'],0);} ?></td>
                            <td  class="text-right"><?php if($row['DEBIT']==0) {echo '-';} else { echo number_format($row['DEBIT'],0);}  ?></td>
                            <td  class="text-right"><?php if($Total_Balance==0) {echo '-';} else { echo number_format($Total_Balance,0);} ?></td>
                            <td  class="text-right"><?php echo ($Total_Balance<0?"Cr":"Dr"); ?></td>
                            <td  class="text-center" style="text-align:center !important"><?php if ($row['ETYPE']=="cpv" || $row['ETYPE']=="crv" || $row['ETYPE']=="csrv" ||$row['ETYPE']=="cspv" ||$row['ETYPE']=="foreigncashpayment" ||$row['ETYPE']=="foreigncashreceipt"){ echo $row['REMARKS'];} ELSE {echo $row['DESCRIPTION'];} ?></td>
                        </tr>
                    <?php   endforeach ?>
                    <?php  // } ?>
                    </tbody>
                    <tfoot>
                    <tr class="foot-comments">
                        <td class="vrqty bold-td text-right" style="border:none !important;border-top:1px solid !important;border-bottom:1px solid !important;"><?php //echo $Total_Debit; ?></td>
                        <td class="bold-td text-right" colspan="3" style="border:none !important;border-top:1px solid !important;border-bottom:1px solid !important;">Grand Total</td>
                        <td class="bold-td text-right" style="border:none !important;border-top:1px solid !important;border-bottom:1px solid !important;"><?php echo $Total_Credit; ?></td>
                        <td class="bold-td text-right" style="border:none !important;border-top:1px solid !important;border-bottom:1px solid !important;"><?php echo $Total_Debit; ?></td>
                        <td class="bold-td text-right" style="border:none !important;border-top:1px solid !important;border-bottom:1px solid !important;"><?php echo $Total_Credit - $Total_Debit; ?></td>
                        <td class="bold-td" style="border:none !important;border-top:1px solid !important;border-bottom:1px solid !important;"><?php echo (($Total_Credit - $Total_Debit)<0?"Cr":"Dr"); ?></td>
                        <td class="bold-td" style="border:none !important;border-top:1px solid !important;border-bottom:1px solid !important;"></td>
                    </tr>
                    </tfoot>
                    <?php   //} ?>
                </table>
            </div>
            <br>
            <br>
            <div class="row-fluid">
                <div class="span12">
                    <table class="signature-fields">
                        <thead>
                        <tr>
                            <th>Approved By</th>
                            <th style="border-top:none !important;"></th>
                            <th>Accountant</th>
                            <th style="border-top:none !important;"></th>
                            <th>Received By</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>	