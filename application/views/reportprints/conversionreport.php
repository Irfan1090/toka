<!doctype html>
<html>

	<head>
		<link rel="stylesheet" href="../../../assets/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="../../../assets/css/bootstrap-responsive.min.css">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet'>
		<style type='text/css'>
			/*.table thead tr th{
				border: none !important;
			}
*/
			.bodr-botm{
				border-bottom: 1px solid black;
				width: 100%;
			}
			.bodr-botm1{
				width:170px;border-bottom:1px solid;display:block;
			}
			.bodr-botm2{
				width:190px;
				margin-left: 30px;
				border-bottom:1px solid;display:block;
			}
			.bodr-botm3{
				width:200px;
				border-bottom:1px solid;display:block;
			}
			.bodr-botm4{
				display: block;
				width:200px;
				margin-left: 50px;
				border-bottom:1px solid;display:block;
			}
			.wid_td{
				width: 11% !important;
			}
			.new1{
				border-bottom: 0px !important;
				width: 20%;
			}
			thead tr th{
				border-bottom: 1px solid #ddd !important;
				border-top: 1px solid #ddd !important;
			}
			tfoot tr td{
				border-bottom: 1px solid #ddd !important;
				border-top: 1px solid #ddd !important;
			}
			.width_100px{
				width: 100px;
			}
			.headr{
				background: black;
				color: white;
			}
			.newone{
				padding-left: 0px !important;
				padding-right: 0px !important;
				margin-top: -20px !important;
			}
			.main{
				margin-top:40px;
				/*border:1px solid black;*/
				height: 11.69in;
			}
			.forpr{
				width: 200px;
				border-bottom: 1px solid black;
				display: block;
			}
			@media print {
				.wid_td{
					width: 20% !important;
				}
				.newone{
				padding-left: 0px !important;
				padding-right: 0px !important;
				margin-top: -20px !important;
				}
				.headr{
				background: black;
				color: white;
				}
			}

		</style>
	</head>

	<body>
		<div class="container main">
			<!-- <div class="row">
				<div class="col-xs-12 newone">
					<h3 class='text-center headr'>Conversion Voucher</h3>
				</div>
			</div> -->
			<div class="row">
				<div class="col-xs-12">
					<h3 class='text-center' ><span style="border-bottom:1px solid black;">Conversion Voucher</span></h3>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12">
					<table>
						<tr class='botm-margn'>
							<td class='wid_td'><b>Vr#</b></td>
							<td><span class='bodr-botm1'></span></td>
							<td colspan="2"></td>
							<td class='pull-right wid_td'><b>Date:</b></td>
							<td><span class='bodr-botm2' style='margin-left:60px;'></span></td>
						</tr>
						<br>
						<tr class='botm-margn'>
							<td class='wid_td'><b>Received By:</b></td>
							<td colspan="5" class='bodr-botm recieved_data'></td>
						</tr>
						<tr class='botm-margn'>
							<td class='wid_td'><b>Remarks:</b></td>
							<td colspan="5" class='bodr-botm remarks_data'></td>
						</tr>
						<div></div>
						<tr class='botm-margn'>
							<td class='wid_td'><b>Item:</b></td>
							<td colspan="5" class='bodr-botm items1'></td>
						</tr>
						<tr class='botm-margn'>
							<td class='wid_td'><b>Godown</b></td>
							<td><span class='bodr-botm3'></span></td>
							<td class='pull-right wid_td' style='margin-top:15px;'><b>Qty:</b></td>
							<td><span class='bodr-botm4' style='margin-top:15px;'></span></td>
							<td class='pull-right wid_td'><b>Weight:</b></td>
							<td><span class='bodr-botm4 weight_data' style='margin-top:15px;'></span></td>
						</tr>
					</table>
				</div>
			</div>
			<br>
			
			<div class="row">
				<div class="col-xs-12">
					
							<table class="table table-bordered">
								<thead class="myhead">
									
								</thead>
								<tbody class="myrows">
									

								</tbody>
								<tfoot>
									<tr>
										<td class='text-right' colspan="3"><b>Sub Total</b></td>
										<td class='text-right'><b><span class="total-qty"></span></b></td>
										<td class='text-right'><b><span class="total-weight"></span></b></td>
									</tr>
								</tfoot>
							</table>
							
				</div><!-- end of col -->
			</div><!-- row-fluid -->
			<div class="row">
				<div class="col-xs-6">
					<span class='forpr'><b>Received By:</b></span>
				</div>
				<div class="col-xs-3 col-xs-offset-3">
					<span class='forpr'><b>Approved By:</b></span>
				</div>
			</div>

		</div><!--container-fluid -->
		<script type="text/javascript" src="../../../assets/js/jquery.min.js"></script>
		<script src="../../../assets/js/handlebars.js"></script>
		<script type="text/javascript">
		$(function(){
			
			
			var opener = window.opener;
			
				
			var etype = opener.$('.page_title').text();
			
	        var po = "";
	        var item_des = "";
	        var godown = "";
	        var qty = "";
	        var weight = "";
	        opener.$('#purchase_table').find('thead tr').each(function(index, elem)
	         {
              item_des += $.trim($(elem).find('th.hitem').text()) + "<br>";
              godown += $.trim($(elem).find('th.hgdown').text()) + "<br>";
              qty += $.trim($(elem).find('th.hqty').text()) + "<br>";
              weight += $.trim($(elem).find('th.hweight').text()) + "<br>";
              
	         });

	          var html = "<tr>";
	          
	          html += "<th style='width:90px;' class='text-left'>Sr#</th>";
	          html += "<th class='text-left'>" +  item_des + "</th>";
	          html += "<th style='width: 110px;' class='text-right'>Godown</th>";
	          html += "<th style='width:120px;' class='text-right'>"+ qty + "</th>";
	          html += "<th style='width:120px;' class='text-right'>"+weight+"</th>";
	          html += "</tr>";
								
	          $(".myhead").append(html);
	          var item_des = "";
	          var qty = "";
	          var godown = "";
	          var weight = '';
	          var netweight = 0;
	          var srno = "";
	          var pid = "";
	        opener.$('#purchase_table').find('tbody tr').each(function(index, elem)
	         {
    		  netweight += parseFloat($(elem).find('td.item_desc').data('item_weight'));
              srno += $.trim($(elem).find('td.srno').text()) + "<br>";
              item_des += $.trim($(elem).find('td.item_desc').text()) + "<br>";
              godown += $.trim($(elem).find('td.godown_desc').text()) + "<br>";
              qty += $.trim($(elem).find('td.qty').text()) + "<br>";
              weight += $(elem).find('td.weight').text() + "<br>";
             // alert(weight);
	         });

	          var htmls = "<tr>";
	          
	          htmls += "<td>" +  srno + "</td>";
	          htmls += "<td>" +  item_des + "</td>";
	          htmls += "<td>" +  godown + "</td>";
	          htmls += "<td class='text-right'>" + qty + "</td>";
	          htmls += "<td class='text-right'>" + weight + "</td>";
	          htmls += "</tr>";
	          $(".myrows").append(htmls);
		    

			
			$('.shadowhead').html('Recipe Costing');
			$('.bodr-botm1').html(opener.$('#txtVrnoa').val());
			$('.bodr-botm2').html((opener.$('#current_date').val()).substr(0,10));
			$('.recieved_data').html(opener.$('#receivers_list').val());
			$('.remarks_data').html(opener.$('#txtRemarks').val());
			$('.items1').html(opener.$('#item_dropdownItemC').find('option:selected').text());
			$('.bodr-botm3').html(opener.$('#dept_dropdownitemc').find('option:selected').text());
			$('.bodr-botm4').html(opener.$('#txtQtyItemC').val());
			$('.weight_data').html(opener.$('#txtWeightItemC').val());
			$('.total-qty').html(opener.$('.txtTotalQty').text());
			$('.total-weight').html(opener.$('.txtTotalWeight').text());

			window.print();

			
		});
	</script>
	</body>
</html>
