<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Voucher</title>

	<link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="../../assets/css/bootstrap-responsive.min.css">
	<link rel="stylesheet" href="../../../assets/bootstrap/css/bootstrap.min.css">

	<style>
		* { margin: 0; padding: 0; font-family: tahoma; }
		body { font-size:12px; }
	p { margin: 0; /* line-height: 17px; */ }
	.field {font-weight: bold; display: inline-block; width: 150px; } 
	.field1 {font-weight: bold; display: inline-block; width: 150px; }
	.block p{ padding-top: 10px; }
	.voucher-table{ border-collapse: none; }
	table { width: 100%; border: 2px solid black; border-collapse:collapse; table-layout:fixed; margin-left:1px}
	th {  padding: 5px; }
	td { /*text-align: center;*/ vertical-align: top;  }
	td:first-child { text-align: left; }
	.voucher-table thead th {background: #ccc; } 
	tfoot {border-top: 1px solid black; } 
	.bold-td { font-weight: bold; border-bottom: 0px solid black;}
	.nettotal { font-weight: bold; font-size: 11px !important; border-top: 1px solid black; }
	.invoice-type { border-bottom: 1px solid black; }
	.relative { position: relative; }
	.signature-fields{ border: none; border-spacing: 20px; border-collapse: separate;} 
	.signature-fields th {border: 0px; border-top: 1px solid black; border-spacing: 10px; }
	.inv-leftblock { width: 280px; }
	.text-left { text-align: left !important; }
	.text-right { text-align: right !important; }
	td {font-size: 10px; font-family: tahoma; line-height: 14px; padding: 4px; } 
	.rcpt-header { width: 550px; margin: auto; display: block; }
	.inwords, .remBalInWords { text-transform: uppercase; }
	.barcode { margin: auto; }
	h3.invoice-type {font-size: 20px; line-height: 24px;}
	.extra-detail span { background: #7F83E9; color: white; padding: 5px; margin-top: 17px; display: block; margin: 5px 0px; font-size: 10px; text-transform: uppercase; letter-spacing: 1px;}
	.nettotal { color: red; font-size: 12px;}
	.remainingBalance { font-weight: bold; color: blue;}
	.centered { margin: auto; }
	p { position: relative; font-size: 16px; }
	thead th { font-size: 13px; font-weight: bold; padding: 10px; }
	.fieldvalue.cust-name {position: absolute; width: 497px; }
	.fieldvalue.cust-mobile {position: absolute; width: 416px; margin-top: 20px; }
	.fieldvalue.inv-date{margin-left: 25px;}
	.fieldvalue {border-bottom:1pt solid;  }
	@media print {
		.noprint, .noprint * { display: none; }
	}
	.pl20 { padding-left: 20px !important;}
	.pl40 { padding-left: 40px !important;}

	.barcode { float: right; }
	.item-row td { font-size: 15px; padding: 10px; border-top: 1px solid black;}

	.rcpt-header { width: 305px !important; margin: 0px; display: inline; position: absolute; top: 0px; right: 0px; }
	h3.invoice-type { border: none !important; margin: 0px !important; position: relative; top: 34px; }
	tfoot tr td { font-size: 13px; padding: 10px;  }
	.nettotal, .subtotal, .vrqty,.vrweight { font-size: 14px !important; font-weight: bold !important;}
</style>
</head>
<body>
	<div class="container-fluid">
		<div class="row-fluid">
			<div class="span12 centered">
				<h1 style="width:170px; margin: 0px 300px; text-align: center;"><?php echo $title;?></h1>

					<!-- <div class="row-fluid top-img-head" style="display:none; padding-top: 15px; padding-bottom: 15px;">
						<div class="span12">
							<img src="#" class="top-head" alt="">
						</div>
					</div> -->
					<div class="row-fluid relative">
						<div class="span12">
							<div class="block pull-left inv-leftblock" style="width:250px !important; display:inline !important;">
								<p style="float: left;"><span class="field">Vr#</span><span class="fieldvalue inv-vrnoa"><?php echo $vrdetail[0]['vrnoa'];?></span></p>									
								<p style="overflow: auto; padding-left: 200px;"><b>Date:</b><span class="fieldvalue inv-date"><?php echo date('d-M-y', strtotime($vrdetail[0]['vrdate']));?></span></p>
								<p><span class="field">Stock Location:</span><span class="fieldvalue"><?php echo $vrdetail[0]['dept_name'];?></span></p>
								<p><span class="field">Remarks:</span><span class="fieldvalue cust-mobile"><?php echo $vrdetail[0]['remarks'];?></span></p>
								<!-- <p><span class="field">Receipt By</span><span class="fieldvalue rcptBy">[Receipt By]</span></p> -->
							</div>
							<div class="block pull-right" style="width:280px !important; float: right; display:inline !important;">
								<div class="span12" style="display:none;"><img style="float:right; width:280px !important;" class="rcpt-header logo-img" src="<?php echo $header_img;?>" alt=""></div>

							</div>
						</div>
					</div>
					<br>
					<br>
					<br>
					
					<div class="row-fluid">
						<table class="voucher-table">
							<thead>
								<tr>
									<th style=" width: 10px; ">Sr#</th>
									<th style=" width: 70px; text-align:left; ">Description</th>
									<th style=" width: 20px; ">Uom</th>
									<th style=" width: 15px; ">Qty</th>
									<th style=" width: 18px; ">Weight</th>
									<th style=" width: 25px; ">Mould@</th>
									<th style=" width: 25px; ">M Amount</th>
									<th style=" width: 25px; ">Dhary@</th>
									<th style=" width: 25px; ">D Amount</th>
								</tr>
							</thead>

							<tbody>
								
								<?php 
								$serial = 1;
								$netQty = 0;
								$netAmount=0;
								$netAmountDhary=0;
								$netWeight=0;
								foreach ($vrdetail as $row):
                                    if ($row['qty']>0)
                                    {$netQty += abs($row['qty']);
								$netAmount += $row['s_amount'];
								$netAmountDhary += $row['s_damount'];
								$netWeight += abs($row['weight']);

								?>

								<tr  class="item-row">
									<td class='text-left' style='font-size:13px;'><?php echo $serial++;?></td>
									<td class='text-left' style='font-size:13px;'><?php echo $row['item_name'];?></td>
									<td class='text-right' style='font-size:13px;'><?php echo $row['uom'];?></td>
									<td class='text-right' style='font-size:13px;'><?php echo number_format(abs($row['qty']),2);?></td>
									<td class='text-right' style='font-size:13px;'><?php echo number_format(abs($row['weight']),2);?></td>
									<td class='text-right' style='font-size:13px;'><?php echo number_format(($row['s_rate']),2);?></td>
									<td class="text-right" style='font-size:13px;'><?php echo number_format(($row['s_amount']),2);?></td>
									<td class='text-right' style='font-size:13px;'><?php echo number_format(($row['s_discount']),2);?></td>
									<td class="text-right" style='font-size:13px;'><?php echo number_format(($row['s_damount']),2);?></td>
								</tr>

							<?php } endforeach?>
						</tbody>
						<tfoot>
							<tr class="foot-comments">
								<td class="bold-td text-right" colspan="3">Subtotal:</td>
								<td class="vrqty bold-td text-right"><?php echo number_format($netQty,2);?></td>
								<td class="vrweight bold-td text-right"><?php echo number_format($netWeight,2);?></td>
								<td class="vrrate bold-td text-right" colspan="4"></td>
							</tr>
								<!-- <tr>
									<td class="bold-td text-right" >Mould=></td>
									<td class="bold-td text-right" >Amount:</td>
									<td class="subtotal bold-td text-right"><?php echo number_format($netAmount,2);?></td>
									<td class="bold-td text-right discount-td">Bonus:</td>
									<td class="vrweight bold-td text-right"><?php echo number_format(($vrdetail[0]['discp']),2);?></td>
									<td class="bold-td text-right discount-td " >Deduction:</td>
									<td class="vrweight bold-td text-right"><?php echo number_format(($vrdetail[0]['expense']),2);?></td>
									<td class="bold-td text-right discount-td ">Total:</td>
									<td class="vrweight bold-td text-right"> <?php echo number_format($vrdetail[0]['expense']+$vrdetail[0]['discp']+$netAmount,2);?></td>
								</tr>
								<tr>
									<td class="vrrate bold-td text-right">Dhary=></td>
									<td class="vrrate bold-td text-right">Amount:</td>
									<td class="subtotal bold-td text-right"><?php echo number_format($netAmountDhary,2);?></td>
									<td class="bold-td text-right discount-td">Bonus:</td>
									<td class="vrweight bold-td text-right"><?php echo number_format(($vrdetail[0]['taxpercent']),2);?></td>
									<td class="bold-td text-right discount-td">Deduction:</td>
									<td class="vrweight bold-td text-right"><?php echo number_format(($vrdetail[0]['exppercent']),2);?></td>
									<td class="bold-td text-right discount-td">Total:</td>
									<td class="vrweight bold-td text-right"><?php echo number_format($vrdetail[0]['exppercent']+$vrdetail[0]['taxpercent']+$netAmountDhary,2);?></td>
								</tr> -->
								<!-- <tr>
									<td class="bold-td text-right discount-td " colspan="4">Cash Paid:</td>
									<td class="vrweight bold-td text-right"><?php echo number_format(($vrdetail[0]['paid']),2);?></td>
									<td class="bold-td text-right discount-td"></td>
									<td class="bold-td text-right discount-td " colspan="2">NetAmount:</td>
									<td class="vrweight bold-td text-right"><?php echo number_format(($vrdetail[0]['namount']),2);?></td>
								</tr> -->
							</tfoot>
						</table>
					</div>
					<div style="margin: 50px 0px 0px 50px;"> 
						<table style="width: 30%; float: left; margin-right: 20px;">
							<thead>
								<tr>
								<th>Employee Moulding:</th>
								<th><?php echo $vrdetail[0]['party_name'];?></th>
									<th>Mould:</th>
									<th><?php echo number_format($netAmount,2);?></th>
								</tr>
								<tr>
									<th>Bonus:</th>
									<th><?php echo number_format(($vrdetail[0]['discp']),2);?></th>
								</tr>
								<tr style="border-bottom: 1px solid;">
									<th>Deduction:</th>
									<th><?php echo number_format(($vrdetail[0]['expense']),2);?></th>
								</tr>
								<tr>
									<th>Mould Paid:</th>
									<th><?php echo $vrdetail[0]['paid']?></th>
								</tr>
								<tr>
									<th>Total:</th>
									<th><?php echo number_format($vrdetail[0]['expense']+$vrdetail[0]['discp']+$vrdetail[0]['paid']+$netAmount,2);?></th>
								</tr>
							</thead>
						</table>
						<table style="width: 30%;">
							<thead>
								<tr>
								<th>Employee Dhary:</th>
								<th><?php echo $vrdetail[0]['employee_id'];?></th>
									<th>Dhary:</th>
									<th><?php echo number_format($netAmountDhary,2);?></th>
								</tr>
								<tr>
									<th>Bonus:</th>
									<th><?php echo number_format(($vrdetail[0]['taxpercent']),2);?></th>
								</tr>
								<tr style="border-bottom: 1px solid;">
									<th>Deduction:</th>
									<th><?php echo number_format(($vrdetail[0]['exppercent']),2);?></th>
								</tr>
								<tr>
									<th>Dharry Paid:</th>
									<th><?php echo $vrdetail[0]['dharrypaid']?></th>
								</tr>
								<tr>
									<th>Total:</th>
									<th><?php echo number_format($vrdetail[0]['exppercent']+$vrdetail[0]['taxpercent']+$vrdetail[0]['dharrypaid']+$netAmountDhary,2);?></th>
								</tr>
							</thead>
						</table>
					</div>
					<div class="row-fluid">
						<div class="span12 add-on-detail1" style="margin-top: 10px;">
							<p class="" style="text-transform1: uppercase;">
								<!-- <strong>In words: </strong> <span class="inwords"></span><?php echo $amtInWords;?>ONLY <br> -->
								<br>
								<?php if ( $pre_bal_print==1  ){?>
								<p><span class="field1">Net Amount:</span><span class="fieldvalue"><?php echo number_format($vrdetail[0]['namount'],0);?></span></p>
								<!-- <p><span class="field1">Previous Balance:</span><span class="fieldvalue inv-vrnoa"><?php number_format($previousBalance,0);?></span></p> -->
								<p><span class="field1">Cash Paid:</span><span class="fieldvalue inv-vrnoa"><?php echo number_format(($vrdetail[0]['paid']),2);?></span></p>
								<!-- <p><span class="field1">Current Balance:</span><span class="fieldvalue cust-name"><?php echo number_format($vrdetail[0]['namount']+$previousBalance,2) ;?></span></p> -->
								<?php };?>
							</p>
						</div>
					</div>
					<!-- End row-fluid -->
					<br>
					<br>
					<div class="row-fluid">
						<div class="span12">
							<table class="signature-fields">
								<thead>
									<tr>
										<th>Approved By</th>
										<th>Accountant</th>
										<th>Received By</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
					<!-- <div class="row-fluid">
						<p>
							<span class="loggedin_name">User:<?php echo $user;?></span><br>
							<span class="website">Sofware By: www.digitalsofts.com, Mob:03218661765</span>
						</p>
					</div> -->
				</div>
			</div>
		</div>
	</body>
	</html>