<!DOCTYPE html>
<html>
	<head>
		<title>Sale Invoice</title>
		<link rel="stylesheet" href="../../../assets/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="../../../assets/css/bootstrap-responsive.min.css">
		<style>
			@page{margin:0px auto;width: 380px;}
        @media print {
          body{ width: 380px;margin: 0 auto; }
          .numeric{font-weight: normal;}
         }
			@media screen {
    	.main {margin: 8px 0px 0px 31px !important;}
			}
			@media print {
    	.main { margin: 0px 0px 0px 0px !important;}
			}
		 * { margin: 0; padding: 0; font-family: tahoma; }
			body { font-size:20px; }
			th{font-size: 12px; border:none !important;}
			td{border:none !important;}
			table{border:none !important;}
			.token_header{margin:40px 0px 0px 0px !important;}
			.report_title{font-size:18px; font-weight: bold;margin:15px 0px 0px 15px;}
			.fromDate .token_no{font-weight: normal;}
			.footer{  margin: 4px 0px 0px -19px !important; }
			.mid_header{border-bottom:1px solid;}
			.table>tbody>tr>td{padding: 0px;}
			.received{border-top:1px solid !important;}
		</style>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-4 main" style="width: 420px;">
					<div class="row">
						<div class="col-lg-12">
							<p class="text-center report_title"><span style="text-decoration:underline;">Sale Invoice</span></p>
						</div>
					</div><br>
					<div class="row">
						<div class="col-lg-12">
							<table class="table">
								<tr>
									<td><b>Sr#</b></td>
									<td class="sr_no" style="width:70px"></td>
									<td style="width:140px;"><b>Date :</b></td>
									<td class="current_Date"></td>
								</tr>
								<tr>
									<td><b>Account :</b></td>
									<td class="party_drop" colspan="3" style="border-bottom:1px solid !important;"></td>
								</tr>
								<tr>
									<td style="width: 140px;"><b>Customer :</b></td>
									<td colspan="3" style="border-bottom:1px solid !important;"></td>
								</tr>
								<tr>
									<td><b>Location :</b></td>
									<td colspan="3" style="border-bottom:1px solid !important;"></td>
								</tr>
							</table>
						</div><!-- end of col -->
					</div><!-- end of row -->
					<div class="row">
						<div class="col-lg-12">
							<table class="table" id="add">
								<thead style="border-top:1px solid #A7ABA4;border-bottom:1px solid #A7ABA4;">
									<tr>
										<th>Item</th>
										<th>Qty</th>
										<th>Weight</th>
										<th style="width:50px;">Rate</th>
										<th>Amount</th>
									</tr>
									
								</thead>
								<tbody>
									
									
								</tbody>
							</table>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<h6 style="text-align:center;"><b>This Invoice</b></h6>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<table class="table">
								<tr> 
									<td> <strong style="border-bottom:1px solid;padding: 4px 144px 0px 0px;">Received By :</strong></td>
									<td></td>
									<td class="applicant_name"></td>
								</tr>
								<tr>
									<td><strong style="border-bottom:1px solid;padding: 4px 144px 0px 0px;">Accountant :</strong></td>
									<td></td>
									<td class="applicantion_subject"></td>
								</tr>
								<tr>
									<td><strong style="border-bottom:1px solid;padding: 4px 144px 0px 0px;">Approved By :</strong></td>
									<td></td>
									<td class="applicantion_Type"></td>
								</tr>
							</table>
						</div><!-- end of col -->
					</div><!-- end of row -->
					<div class='row footer'>
						<div class="col-lg-12">
							<div class="foter_style">
								<p class="text-center"><strong>Thank You For Shopping Here</strong> </p>
								<p class="text-center" style="margin-top:-10px;"><strong>Ph# : </strong> 041-8543827,8714167 
							</div>
						</div>
					</div>
				</div><!--end of col-->
			</div><!-- end of row -->
		</div><!-- container-fluid -->

		<script type="text/javascript" src="../../../assets/js/jquery.min.js"></script>
		<script src="../../../assets/js/handlebars.js"></script>
		<script type="text/javascript">
			$(function(){
				var opener = window.opener;
				var srNo = opener.$('#txtVrnoa').val();
				var currentDate = opener.$('#current_date').val();
				var received_by = opener.$('#receivers_list').val();
				var remrks = opener.$('#txtRemarks').val();
				var party = opener.$('#party_dropdown').find('option:selected').text();
				var appType = opener.$('#type_dropdown').find('option:selected').text();

				var descriptionOfGoods = "";
				var qty = '';
				var weight = '';
				var rate = '';
				var amount = '';

				var html = '';
				var qty = 0;
				var weight = 0;
				var amount = 0;
				html += "<tr><td><b>Add</b></td><td></td><td></td><td></td><td></td></tr>";
				//html += "<tr><td><b><br></td><td></td><td></td><td></td><td></td></tr>";
				opener.$('#purchase_table').find('tbody tr').each(function(index, elem)
		        {
		            html += "<tr><td>"+$.trim($(elem).find('td.item_desc').text()) + "</td>";
		            html += "<td>"+$.trim($(elem).find('td.qty').text()) + "</td>";
		            html += "<td>"+$.trim($(elem).find('td.weight').text()) + "</td>";
		            html += "<td>"+$.trim($(elem).find('td.rate').text()) + "</td>";
		            html += "<td>"+$.trim($(elem).find('td.amount').text()) + "</td></tr>";
		            qty += parseFloat($.trim($(elem).find('td.qty').text()));
		            weight += parseFloat($.trim($(elem).find('td.weight').text()));
		            amount += parseFloat($.trim($(elem).find('td.amount').text()));
		        });
		        html += "<tr><td></td><td><b>"+ qty.toFixed(2) +"</b></td><td><b>"+ weight.toFixed(2) +"</b></td><td></td><td><b>"+ amount.toFixed(2) +"</b></td></tr>";
		        $("#add tbody").append(html);

		        var qty1 = 0;
				var weight1 = 0;
				var amount1 = 0;
				var html1 = '';
				html1 += "<tr><td><br></td><td></td><td></td><td></td><td></td></tr>";
				html1 += "<tr><td><b>Less</b></td><td></td><td></td><td></td><td></td></tr>";
				opener.$('#purchase_table_less').find('tbody tr').each(function(index, elem)
		        {	
		            html1 += "<tr><td>"+$.trim($(elem).find('td.item_desc').text()) + "</td>";
		            html1 += "<td>"+$.trim($(elem).find('td.qty').text()) + "</td>";
		            html1 += "<td>"+$.trim($(elem).find('td.weight').text()) + "</td>";
		            html1 += "<td>"+$.trim($(elem).find('td.rate').text()) + "</td>";
		            html1 += "<td>"+$.trim($(elem).find('td.amount').text()) + "</td></tr>";
		            qty1 += parseFloat($.trim($(elem).find('td.qty').text()));
		            weight1 += parseFloat($.trim($(elem).find('td.weight').text()));
		            amount1 += parseFloat($.trim($(elem).find('td.amount').text()));
		        });
		        html1 += "<tr><td></td><td><b>"+ qty1.toFixed(2) +"</b></td><td><b>"+ weight1.toFixed(2) +"</b></td><td></td><td><b>"+ amount1.toFixed(2) +"</b></td></tr>";
		        html1 += "<tr style='border-top:1px solid;border-bottom:1px solid;'><td><b>Sub Total</b></td><td><b>"+ (qty+qty1).toFixed(2) +"</b></td><td><b>"+ (weight+weight1).toFixed(2) +"</b></td><td></td><td><b>"+ (amount-amount1).toFixed(2) +"</b></td></tr>";
		        $("#add tbody").append(html1);


				$('.sr_no').html(srNo);
				$('.current_Date').html(currentDate);
				$('.rec_by').html(received_by);
				$('.remarks').html(remrks);
				$('.party_drop').html(party);
				$('.applicantion_Type').html(appType);
			});
		</script>
	</body>
</html>