<!doctype html>
<html>

	<head>
		 <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
	    <link rel="stylesheet" href="../../assets/css/bootstrap-responsive.min.css">
	    <link rel="stylesheet" href="../../../assets/bootstrap/css/bootstrap.min.css">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet'>
		<style type='text/css'>
		.table{
			border: 0px !important;
		}
		.table thead tr th{
			border-bottom: 1px solid black !important;
			border-top: 1px solid black !important;
			/*font-weight: normal !important;*/
		}
		.table tbody tr td{
			border-top: 0px !important;
		}
		.table tfoot tr td{
			border-bottom: 1px solid black !important;
			border-top: 1px solid black !important;
		}
		/*.tbody { height: 320px; overflow: fixed;} */
		.linebrk{
			border-bottom: 1px solid black;
			display: block;
			margin-left:100px; 
			margin-top: 0px;
		}
		.linebrk1{
			border-bottom: 1px solid black;
			display: block;
			margin-left:100px; 
			margin-top: -20px;
		}
		.transpoter_name{
			border-bottom: 1px solid black;
			display: block;
			margin-left:100px; 
			margin-top: -20px;
		}
		#mytable > thead > tr > th{
			border-left: 0px !important;
			border-right: 0px !important;
			padding: 5px !important;
		}
		#mytable > tbody > tr > td{
			border-left: 0px !important;
			border-right: 0px !important;
			padding: 3px !important;
		}
		</style>
	</head>

	<body>
		<div class="container" style="margin-top:30px;">
			<div class="row">
				<div class="col-xs-12">
					
					<div class="row">
						<div class="col-xs-12">
							<span>
								<h3 class="text-center" style="font-weight:bold;font-family: 'Open Sans', sans-serif;"><b>Order Parts Detail</b></h3>
							</span>
						</div><!-- end of col -->
					</div><!-- end of row -->
					<div class="row">
						<div class="col-lg-12">
							
						</div>
					</div>
					<div class="row">
						<div class="col-xs-7"><br><br>
							<span><b>Party:</b></span>
							<span class='linebrk1'>.</span>
						</div>
						<div class="col-xs-5">
							<div class="pull-right">
								<span style="font-weight:bold;font-family: 'Open Sans', sans-serif;" class=''><b>Dated : </b><span class='pull-right dates' style='width:150px;text-align:right;'></span></span><br>
								<span style="font-weight:bold;font-family: 'Open Sans', sans-serif;" class=''><b>Order# :</b> <span class='pull-right orders' style='width:150px;text-align:right;'></span></span><br>
								<span style="font-weight:bold;font-family: 'Open Sans', sans-serif;" class=''><b>Vr# :</b> <span class='pull-right vrno' style='width:150px;text-align:right;'></span></span><br>
								
							</div>
						</div>
					</div><!-- end of row-fluid -->
				
					<div class="row" style="margin-top:10px;">
						<div class="col-lg-12">
							<span><span><b>Remarks:</b></span><br>
							<span class='linebrk'></span></span>
						</div>
					</div>
					<div class="row" style="margin-top:10px;">
						<div class="col-lg-12">
							<span><span><b>Transpoter:</b></span><br>
							<span class='transpoter_name'></span></span>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-lg-12">
							<table class="table table-bordered" id="mytable">
								<thead style="background:#E8E8E8;" class='myhead'>
									<tr style="font-family: 'Open Sans', sans-serif;">
										<th>Sr#</th> 
										<th style="width:130px;" class="text-left">Item Code</th>
										<th style="width:350px;" class="text-left">Item Description</th>
										<th style="width: 150px;" class="text-left">Location</th>
										<th style="width:100px;" class="text-right">Qty</th>
										<th style="width:100px;" class="text-right">Weight</th>
									</tr>
								</thead>
								<tbody class='tbody myrows'>
									<!-- <tr>
										<td colspan="5"><b>Parts</b></td>
									</tr>
									
									<tr>
										<td colspan="5"><b>Spare Parts</b></td>
									</tr>
									
									<tr class=''></tr> -->
								</tbody>
								<tfoot>
									<!-- <tr>
										<td colspan="3"><b>Grand Total:</b></td>
										<td class="text-right qty"></td>
										<td class="text-right weight"></td>
									</tr> -->
								</tfoot>
							</table>
						</div>
					</div>	
				</div><!-- end of col -->
			</div><!-- row-fluid -->
			<!-- <div class="row">
				<div class="col-xs-7">
					<div class="pull-right text-right">
						<span class='text-right'>Previouse Balance:</span><br>
						<span class='text-right'>This Invoice:</span><br>
						<span class='text-right'>Current Balance:</span>
					</div>
				</div>
				<div class="col-xs-5">
					<span>00000</span><br>
					<span>00000</span><br>
					<span>00000</span>
					
				</div>
			</div> -->
			<br><br><br><br><br>
			<div class="row-fluid">
				<div class="col-xs-3" style="border-top:1px solid black;text-align:center;">
					<span>Prepared By:</span>
				</div>
				<div class="col-xs-3 col-xs-offset-2" style="border-top:1px solid black;text-align:center;">
					<span>Checked By:</span>
				</div>
				<div class="col-xs-3 col-xs-offset-1" style="border-top:1px solid black;text-align:center;">
					<span>Approved By:</span>
				</div>
			</div>


		</div><!--container-fluid -->
		<script type="text/javascript" src="../../../assets/js/jquery.min.js"></script>
		<script src="../../../assets/js/handlebars.js"></script>
		<script type="text/javascript">
		$(function(){
			
			var opener = window.opener;
			
	       
	        var html = '';
	        var totaqty = 0;
	        var counter = 1;
	        var i = 0 ;
	        var j = 0 ;
	       
	        if( opener.$('#purchase_table').find('tbody tr').length != 0 ){
	        	html += "<tr><td colspan='6'><b>Parts</b></td></tr>";
		       	opener.$('#purchase_table').find('tbody tr').each(function(index, elem)
		        {	
		        	if( $.trim($(elem).find('td.type').text()) == 'add' || $.trim($(elem).find('td.type').text()) == 'parts' ){
		        		
		        		html += "<tr><td>" + counter + "</td>";
		        		html += "<td>" + $.trim($(elem).find('td.item_desc').data('item_id')) + "</td>";
		            	html += "<td>" + $.trim($(elem).find('td.item_desc').text()) + "</td>";
		            	html += "<td>" + opener.$('#dept_dropdown').find('option:selected').text() + "</td>";
		            	html += "<td class='text-right'>" + $.trim($(elem).find('td.qty').text()) + "</td>";
		            	html += "<td class='text-right'>" + $.trim($(elem).find('td.weight').text()) + "</td></tr>";
		            	j++;
		            	counter++;
		       		}
		       		else{
		       			i++;
		       		}
		        });
	        }
	        
	        
	        if( j != 0 ){
	        	$("#mytable tbody").append(html);
	        }
	        

	        var htmls = '';

	        if( i != 0){

	        	htmls += "<tr><td colspan='6'><b>Spare Parts</b></td></tr>";
		       	opener.$('#purchase_table').find('tbody tr').each(function(index, elem)
		        {
		        	if( $.trim($(elem).find('td.type').text()) == 'spare parts' ){
		        		htmls += "<tr><td>" + counter + "</td>";
			            htmls += "<td>" + $.trim($(elem).find('td.item_desc').data('item_id')) + "</td>";
			            htmls += "<td>" + $.trim($(elem).find('td.item_desc').text()) + "</td>";
			            htmls += "<td>" + opener.$('#dept_dropdown').find('option:selected').text() + "</td>";
			            htmls += "<td class='text-right'>" + $.trim($(elem).find('td.qty').text()) + "</td>";
			            htmls += "<td class='text-right'>" + $.trim($(elem).find('td.weight').text()) + "</td></tr>";
		        	}
		        });

	        }

	        $("#mytable tbody").append(htmls);
	        var htmls1 = '';
	        if(opener.$('#purchase_table_less').find('tbody tr').length != 0 ){
	        	 htmls1 += "<tr><td colspan='6'><b>Less</b></td></tr>";
		       	opener.$('#purchase_table_less').find('tbody tr').each(function(index, elem)
		        {
		        		htmls1 += "<tr><td>" + counter + "</td>";
			            htmls1 += "<td>" + $.trim($(elem).find('td.item_desc').data('item_id')) + "</td>";
			            htmls1 += "<td>" + $.trim($(elem).find('td.item_desc').text()) + "</td>";
			            htmls1 += "<td>" + opener.$('#dept_dropdown').find('option:selected').text() + "</td>";
			            htmls1 += "<td class='text-right'>" + $.trim($(elem).find('td.qty').text()) + "</td>";
			            htmls1 += "<td class='text-right'>" + $.trim($(elem).find('td.weight').text()) + "</td></tr>";
		        	
		        });
		       
		        $("#mytable tbody").append(htmls1);
	        }
	       
	       

	        // $('.qty').html(opener.$('#txtTotalQty').val());
	        // $('.weight').html(opener.$('#txtTotalWeight').val());
	        $('.linebrk1').html(opener.$('#party_dropdown').find('option:selected').text());
	        $('.linebrk').html(opener.$('#txtRemarks').val());
	        $('.transpoter_name').html(opener.$('#transporter_dropdown').find('option:selected').text());

	        $('.orders').html(opener.$('#txtOrderNo').val());
	        $('.vrno').html(opener.$('#txtVrno').val());
	        $('.dates').html(opener.$('#current_date').val());

			});
		</script>
	</body>
</html>
