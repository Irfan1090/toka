<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
    
<style type="text/css">
  body{
}
.container{
  width:390px;
  height: auto;
  font-size: 22px;
  margin:0 auto;
}
.header{
  float:left;
}
.header h1{
  text-align:center;
}
.header p{
  width:175px;
  float:left;
  text-align:right;
}
.table1{
  float:left;
}
td{
  width:86px;
}
.row-head{
  border-top: 1px solid #000;
  border-bottom: 1px solid #000;
    float: left;
    width: 400px;
}
.row-content{
  float:left;
}
.footer{
  margin-top:10px;
  float:left;
}
.footer h3{
  text-align:right;
}
body, h1, h2, h3, h4, h5, h6, p, span,table,tbody,tr,td,th,tfoot, input, title {
    font-family: Jameel Noori Nastaleeq, Nafees Web Naskh, Arial, Urdu Naskh Asiatype, Tahoma, Unicode MS !important;
  }body, h1, h2, h3, h4, h5, h6, p, span,table,tbody,tr,td,th,tfoot, input, title {
    font-family: Jameel Noori Nastaleeq, Nafees Web Naskh, Arial, Urdu Naskh Asiatype, Tahoma, Unicode MS !important;
  }
</style>
</head>

<body>
<div class="container">
  <div class="header">
    <h1>پرچیز ریٹرن</h1>
    <p><span class="inv_data"></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:واؤچر نمبر</p>
    <p><span class="date_data"></span>&nbsp;&nbsp;&nbsp;&nbsp;:تاریخ</p>
    <p style="width:100%; text-align:right;">:پارٹی<span  class="party_data" style="text-decoration:underline; width:88%; float:left; "></span></p>
    <p style="width:100%;">:تفصیل<span class="remarks_data" style="text-decoration:underline;text-align:right;float:left; width:85%;"></span></p>
  </div>
  <div class="table1">
    <table>
      <thead>
        <tr class="row-head" style=" text-align: center;">
          <td><b>وزن</b></td>
          <td><b>تعداد</b></td>
          <td><b>گودام</b></td>
          <td style="width: 224px;"><b>آئٹم تفصیل</b></td>
        </tr>
      </thead>
      <tbody class="myrows">
        
      </tbody>
      <tfoot>
        <tr class="row-head">
          <td class="total-wgt" style="font-weight: bold;"></td>
          <td class="total-qty" style="font-weight: bold;">2</td>
          <td></td>
          <td style="font-weight: bold;">:کل ٹوٹل</td>
        </tr>
        <tr class="row-head" style="border: none;">
          <td class="total-amount" style="font-weight: bold;"></td>
          <td></td>
          <td style="font-weight: bold;">:کل اماؤنٹ</td>
          <td></td>
        </tr>
      </tfoot>
    </table>
  </div>
  <div class="footer">
  <h3>ٹرانسپورٹر</h3>
  <p>Prepared By:&nbsp;&nbsp;&nbsp;<span>_____________________________</span></p>
  <p>Received By:&nbsp;&nbsp;&nbsp;<span>_____________________________</span></p>
  <p>Approved By:&nbsp;&nbsp;<span>_____________________________</span></p>
  </div>
</div>
<script type="text/javascript" src="../../../assets/js/jquery.min.js"></script>
    <script src="../../../assets/js/handlebars.js"></script>
    <script type="text/javascript">
    $(function(){
      
      var opener = window.opener;
      
          
            var item_des = "";
            var qty = "";
            var weight = "";
            var godown = "";
            var rate = "";
            var amount = "";
           opener.$('#purchasereturn_table').find('tbody tr').each(function(index, elem)
           {
              // item_code += $.trim($(elem).find('td.item_desc').data('item_code')) + "<br>";
              item_des += $.trim($(elem).find('td.item_desc').data('uitem_des')) + "<br>";
              // godown += opener.$('#dept_dropdown').find('option:selected').text() + "<br>";
              godown += $.trim($(elem).find('td.godown').text()) + "<br>";

              qty += $.trim($(elem).find('td.qty').text()) + "<br>";
              weight += $.trim($(elem).find('td.weight').text()) + "<br>";
           });

            $('.myrows').empty();
            var htmls = "<tr class='row-content' style='text-align: center;'>";
            
            htmls += "<td>" +  weight + "</td>";
            htmls += "<td>" +  qty + "</td>";
            htmls += "<td>" + godown + "</td>";
            htmls += "<td style='width: 200px;'>" + item_des + "</td>";
            htmls += "</tr>";
            $(".myrows").append(htmls);


        var party_data = opener.$('#party_dropdown').find('option:selected').data('uname');
        
      // $('.shadowhead').html('Recipe Costing');
      $('.inv_data').text(opener.$('#txtVrnoa').val());
      $('.date_data').text((opener.$('#current_date').val()).substr(0,10));
      $('.party_data').text(party_data);
      $('.noted_by').html(opener.$('#txtNotedBy').val());
      $('.remarks_data').text(opener.$('#txtRemarks').val());
      $('.total-qty').html(opener.$('#txtTotalQty').val());
      $('.total-wgt').html(opener.$('#txtTotalWeight').val());
      $('.total-amount').html(opener.$('#txtTotalAmount').val());
      window.print();
      // $('.cbalance').html(opener.$('#party_dropdown11').find('option:selected').data('pbalance'));
      // $('.tbalance').html(opener.$('#txtNetAmount').val());

      });
    </script>
</body>
</html>
