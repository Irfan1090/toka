<!doctype html>
<html>

	<head>
		<link rel="stylesheet" href="../../../assets/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="../../../assets/css/bootstrap-responsive.min.css">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet'>
		<style type='text/css'>
		/*.table{
			border: 0px !important;
		}
		.table thead tr th{
			border-bottom: 1px solid black !important;
			border-top: 1px solid black !important;
			/*font-weight: normal !important;*/
		}
		/* .table tbody tr td{
			border-top: 0px !important;
		} */
		.table tfoot tr td{
			border-bottom: 1px solid black !important;
			border-top: 1px solid black !important;
		}
		/*.tbody { height: 320px; overflow: fixed;} */
		.linebrk{
			border-bottom: 1px solid black;
			display: block;
			widows: 100%;
			margin-left:100px; 
			margin-top: -10px;
		}
		.linebrk2{
			border-bottom: 1px solid black;
			display: block;
			width: 15%;
			margin-left:100px; 
			margin-top: -20px;
		}
		.linebrk1{
			border-bottom: 1px solid black;
			display: block;
			width: 26%;
			margin-left:100px; 
			margin-top: -20px;
		}
		</style>
	</head>

	<body>
		<div class="container" style="margin-top:30px;">
			<div class="row">
				<div class="col-xs-12">
					
					<div class="row">
						<div class="col-xs-12">
							<span>
								<h3 class="text-center" style="font-weight:bold;font-family: 'Open Sans', sans-serif;"><b>Extra Day Voucher</b></h3>
							</span>
						</div><!-- end of col -->
					</div><!-- end of row -->
					<div class="row">
						<div class="col-xs-7"><br><br>
							<span><b>Vr No:</b></span>
							<span class='linebrk1 vrno'>.</span>
						</div>
					</div><!-- end of row-fluid -->
					<div class="row" style="margin-top:10px;">
						<div class="col-lg-12">
							<span><span><b>Date:</b></span><br>
							<span class='linebrk2 dates'></span></span>
						</div>
					</div>
					<div class="row" style="margin-top:10px;">
						<div class="col-lg-12">
							<span><span><b>Remarks:</b></span><br>
							<span class='linebrk ramark'></span></span>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-lg-12">
							<table class="table table-bordered" id="mytable">
								<thead style="background:#E8E8E8;">
									<tr style="font-family: 'Open Sans', sans-serif;"> 
										<th style="width:60px;" class="text-center">Sr#</th>
										<th style="width:350px;" class="text-left">Employee Name</th>
										<th style="width: 150px;" class="text-left">Department</th>
										<th style="width:150px;" class="text-left">Godown</th>
										<th style="width:140px;" class="text-right">Extra Days</th>
										<th style="width:80px;" class="text-right">Rate</th>
										<th style="width:100px;" class="text-right">Amount</th>
									</tr>
								</thead>
								<tbody class='tbody myrows'>
									
								</tbody>
								<tfoot>
									<tr>
										<td colspan="4" class='text-right' style="color:red;"><b>Total:</b></td>
										<td class="text-right extrdys"></td>
										<td class="text-right">######</td>
										<td class="text-right totamt"></td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>	
				</div><!-- end of col -->
			</div><!-- row-fluid -->
			<br><br><br><br><br>


		</div><!--container-fluid -->
		<script type="text/javascript" src="../../../assets/js/jquery.min.js"></script>
		<script src="../../../assets/js/handlebars.js"></script>
		<script type="text/javascript">
		$(function(){
			
			var opener = window.opener;

	        var htmls = '';
	        var html = '';
	        var totaqty = 0;
	       	opener.$('#purchase_table').find('tbody tr').each(function(index, elem)
	        {
	        	htmls += "<tr><td class='text-center'>" + $.trim($(elem).find('td.srno').text()) + "</td>";
	        	htmls += "<td>" + $.trim($(elem).find('td.employee').text()) + "</td>";
	            // htmls += "<td>" + $.trim($(elem).find('td.employee').data('emp_id')) + "</td>";
	            htmls += "<td>" + opener.$('#dept_dropdown').find('option:selected').text() + "</td>";
	            htmls += "<td>" + opener.$('#godown_dropdown').find('option:selected').text() + "</td>";
	            htmls += "<td class='text-right'>" + $.trim($(elem).find('td.extradays').text()) + "</td>";
	            htmls += "<td class='text-right'>" + $.trim($(elem).find('td.rate').text()) + "</td>";
	            htmls += "<td class='text-right'>" + $.trim($(elem).find('td.amount').text()) + "</td></tr>";

	        });

	        $("#mytable tbody").append(htmls);

	        $('.totamt').html(opener.$('#txtTotAmount').text());
	        $('.extrdys').html(opener.$('#txtTotextradays').text());
	        $('.ramark').html(opener.$('#txtRemarks').val());
	        $('.vrno').html(opener.$('#txtVrnoa').val());
	        $('.dates').html(opener.$('#current_date').val());

			});
		</script>
	</body>
</html>
