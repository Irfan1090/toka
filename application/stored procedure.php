EXISTING
Name = spw_HeadMouldingLess
Create New Parameter = godown
Procedure = 
insert into stockdetail(stid,godown_id,item_id,qty,rate,amount,type)
SELECT m.stid,godown,i.item_id,-(ifnull(sum(d.qty *(rcpd.qty/ IFNULL(rcp.fqty,1))),0)) AS qty,IF(ifnull

(lp.rate,0)=0,i.cost_price,lp.rate) as lprate ,ifnull(sum(d.qty *(rcpd.qty/ IFNULL(rcp.fqty,1))),0)* IF

(ifnull(lp.rate,0)=0,i.cost_price,lp.rate) as amount,'less'
FROM stockmain AS m
INNER JOIN stockdetail AS d ON m.stid = d.stid
INNER JOIN recipemain rcp ON rcp.item_id=d.item_id
INNER JOIN recipedetail rcpd ON rcpd.rid=rcp.rid
INNER JOIN item i ON i.item_id = rcpd.item_id
LEFT JOIN (
SELECT d.item_id,(d.rate-(d.discount*d.rate/100)) AS rate
FROM stockdetail d
WHERE d.qty>0 AND d.stid=(
SELECT stid
FROM stockdetail
WHERE item_id=d.item_id
ORDER BY stid DESC
LIMIT 1) and d.stid in(select stid from stockmain where etype='purchase' and 

company_id=company_id and vrdate <(select vrdate from stockmain where etype='headmoulding' 
and vrnoa=vrnoa and company_id=company_id order by vrdate desc limit 1 ))
GROUP BY d.item_id
ORDER BY d.item_id) AS lp ON lp.item_id=i.item_id
where m.etype='headmoulding' and m.vrnoa=vrnoa and m.company_id=company_id
group by m.stid,i.item_id



CREATE NEW
Name = spw_MouldingLess
Parameters = vrnoa, company_id, godown
Procedure = 
insert into stockdetail(stid,godown_id,item_id,qty,rate,amount,type)
SELECT m.stid,godown,i.item_id,-(ifnull(sum(d.qty *(rcpd.qty/ IFNULL(rcp.fqty,1))),0)) AS qty,IF(ifnull

(lp.rate,0)=0,i.cost_price,lp.rate) as lprate ,ifnull(sum(d.qty *(rcpd.qty/ IFNULL(rcp.fqty,1))),0)* IF

(ifnull(lp.rate,0)=0,i.cost_price,lp.rate) as amount,'less'
FROM stockmain AS m
INNER JOIN stockdetail AS d ON m.stid = d.stid
INNER JOIN recipemain rcp ON rcp.item_id=d.item_id
INNER JOIN recipedetail rcpd ON rcpd.rid=rcp.rid
INNER JOIN item i ON i.item_id = rcpd.item_id
LEFT JOIN (
SELECT d.item_id,(d.rate-(d.discount*d.rate/100)) AS rate
FROM stockdetail d
WHERE d.qty>0 AND d.stid=(
SELECT stid
FROM stockdetail
WHERE item_id=d.item_id
ORDER BY stid DESC
LIMIT 1) and d.stid in(select stid from stockmain where etype='purchase' and 

company_id=company_id and vrdate <(select vrdate from stockmain where etype='moulding' 
and vrnoa=vrnoa and company_id=company_id order by vrdate desc limit 1 ))
GROUP BY d.item_id
ORDER BY d.item_id) AS lp ON lp.item_id=i.item_id
where m.etype='moulding' and m.vrnoa=vrnoa and m.company_id=company_id
group by m.stid,i.item_id