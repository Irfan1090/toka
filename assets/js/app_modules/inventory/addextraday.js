var Purchase = function() {

var saveItem = function( item ) {
		$.ajax({
			url : base_url + 'index.php/item/save',
			type : 'POST',
			data : item,
			// processData : false,
			// contentType : false,
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'true') {
					alert('An internal error occured while saving voucher. Please try again.');
				} else {
					alert('Item saved successfully.');
					$('#ItemAddModel').modal('hide');
					fetchItems();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var saveAccount = function( accountObj ) {
		$.ajax({
			url : base_url + 'index.php/account/save',
			type : 'POST',
			data : { 'accountDetail' : accountObj },
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'false') {
					alert('An internal error occured while saving account. Please try again.');
				} else {
					alert('Account saved successfully.');
					$('#AccountAddModel').modal('hide');
					fetchAccount();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

var fetchAccount = function() {

		$.ajax({
			url : base_url + 'index.php/account/fetchAll',
			type : 'POST',
			data : { 'active' : 1 },
			dataType : 'JSON',
			success : function(data) {
				if (data === 'false') {
					alert('No data found');
				} else {
					populateDataAccount(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	var fetchItems = function() {
		$.ajax({
			url : base_url + 'index.php/item/fetchAll',
			type : 'POST',
			data : { 'active' : 1 },
			dataType : 'JSON',
			success : function(data) {
				if (data === 'false') {
					alert('No data found');
				} else {
					populateDataItem(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateDataAccount = function(data) {
		$("#party_dropdown").empty();
		
		$.each(data, function(index, elem){
			var opt="<option value='"+elem.party_id+"' >" +  elem.name + "</option>";
			$(opt).appendTo('#party_dropdown');
		});
	}
	var populateDataItem = function(data) {
		$("#itemid_dropdown").empty();
		$("#item_dropdown").empty();

		$.each(data, function(index, elem){
			var opt="<option value='"+elem.item_id+"' data-prate= '"+ elem.cost_price +"' data-uom_item= '"+ elem.uom +"' data-grweight= '"+ elem.grweight +"' >" +  elem.item_des + "</option>";
			 // var = "<option value='" + $item['item_id'] + "' data-uom_item="<?php echo $item['uom']; ?>" data-prate="<?php echo $item['cost_price']; ?>" data-grweight="<?php echo $item['grweight']; ?>"><?php echo $item['item_des']; ?></option>";
			$(opt).appendTo('#item_dropdown');
			var opt1="<option value='"+elem.item_id+"' data-prate= '"+ elem.cost_price +"' data-uom_item= '"+ elem.uom +"' data-grweight= '"+ elem.grweight +"' >" +  elem.item_id + "</option>";
			 // var = "<option value='" + $item['item_id'] + "' data-uom_item="<?php echo $item['uom']; ?>" data-prate="<?php echo $item['cost_price']; ?>" data-grweight="<?php echo $item['grweight']; ?>"><?php echo $item['item_des']; ?></option>";
			$(opt1).appendTo('#itemid_dropdown');

		});
	}
	var getSaveObjectAccount = function() {

		var obj = {
			pid : '20000',
			active : '1',
			name : $.trim($('#txtAccountName').val()),
			level3 : $.trim($('#txtLevel3').val()),
			dcno : $('#txtVrnoa').val(),
			etype : 'purchase',
			uid : $.trim($('#uid').val()),
			company_id : $.trim($('#cid').val()),
		};

		return obj;
	}
var getSaveObjectItem = function() {
		
		var itemObj = {
			item_id : 20000,
			active : '1',
			open_date : $.trim($('#current_date').val()),
			catid : $('#category_dropdown').val(),
			subcatid : $.trim($('#subcategory_dropdown').val()),
			bid : $.trim($('#brand_dropdown').val()),
			barcode : $.trim($('#txtBarcode').val()),
			description : $.trim($('#txtItemName').val()),
			item_des : $.trim($('#txtItemName').val()),
			cost_price : $.trim($('#txtPurPrice').val()),
			srate : $.trim($('#txtSalePrice').val()),
			uid : $.trim($('#uid').val()),
			company_id : $.trim($('#cid').val()),
		};
		return itemObj;
	}

	var validateSaveItem = function() {

		var errorFlag = false;
		// var _barcode = $('#txtBarcode').val();
		var _desc = $.trim($('#txtItemName').val());
		var cat = $.trim($('#category_dropdown').val());
		var subcat = $('#subcategory_dropdown').val();
		var brand = $.trim($('#brand_dropdown').val());

		// remove the error class first
		$('.inputerror').removeClass('inputerror');
		if ( _desc === '' || _desc === null ) {
			$('#txtItemName').addClass('inputerror');
			errorFlag = true;
		}
		if ( !cat ) {
			$('#category_dropdown').addClass('inputerror');
			errorFlag = true;
		}
		if ( !subcat ) {
			$('#subcategory_dropdown').addClass('inputerror');
			errorFlag = true;
		}
		if ( !brand ) {
			$('#brand_dropdown').addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}
	var validateSaveAccount = function() {

		var errorFlag = false;
		var partyEl = $('#txtAccountName');
		var deptEl = $('#txtLevel3');

		// remove the error class first
		$('.inputerror').removeClass('inputerror');

		if ( !partyEl.val() ) {
			$('#txtAccountName').addClass('inputerror');
			errorFlag = true;
		}
		if ( !deptEl.val() ) {
			deptEl.addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}
	var save = function(extradays) {
		// alert(extradays.stockmain['company_id']);
		$.ajax({
			url : base_url + 'index.php/extraday/save',
			type : 'POST',
			data : { 'stockmain' : extradays.stockmain, 'stockdetail' : extradays.stockdetail, 'ledger' : extradays.pledger, 'vrnoa' : extradays.vrnoa, 'voucher_type' : $('#voucher_type_hidden').val() },
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'true') {
					alert('An internal error occured while saving voucher. Please try again.');
				} else {
					var printConfirmation = confirm('Voucher saved!\nWould you like to print the invoice as well?');
					if (printConfirmation === true) {
						Print_Voucher(1);
					} else {
						general.reloadWindow();
					}
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
var Print_Voucher = function(hd) {
		if ( $('.btnSave').data('printbtn')==0 ){
				alert('Sorry! you have not print rights..........');
		}else{
			var etype=  'extradays';
			var vrnoa = $('#txtVrnoa').val();
			var company_id = $('#cid').val();
			var user = $('#uname').val();
			// var hd = $('#hd').val();
			var pre_bal_print =  '0';
			
			var url = base_url + 'index.php/doc/print_pro_voucher/' + etype + '/' + vrnoa + '/' + company_id + '/' + '-1' + '/' + user + '/' + pre_bal_print+ '/' + hd;
			// var url = base_url + 'index.php/doc/CashVocuherPrintPdf/' + etype + '/' + dcno   + '/' + companyid + '/' + '-1' + '/' + user;
			window.open(url);
		}

	}
	var fetchThroughPO = function(poNo) {

		$.ajax({

			url : base_url + 'index.php/purchaseorder/fetch',
			type : 'POST',
			data : { 'vrnoa' : poNo },
			dataType : 'JSON',
			success : function(data) {

				$('#purchase_table').find('tbody tr').remove();
				if (data === 'false') {
					alert('No data found.');
				} else {
					populatePOData(data);
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populatePOData = function(data) {

		$.each(data, function(index, elem) {
			appendToTable('1', elem.item_name, elem.item_id, elem.godown_id,elem.dept_name,elem.uom, elem.item_qty, elem.weight );
			calculateNetQty(elem.item_qty,elem.weight);
		});
	}

	var fetch = function(vrnoa) {

		$.ajax({

			url : base_url + 'index.php/extraday/fetch',
			type : 'POST',
			data : { 'vrnoa' : vrnoa ,'company_id': $('#cid').val() },
			dataType : 'JSON',
			success : function(data) {
				resetFields();
				$('#purchase_table').find('tbody tr').remove();
				if (data === 'false') {
					alert('No data found.');
				} else {
					populateData(data);
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var resetFields = function(){

		$('#current_date').datepicker('update', new Date());
		$('#txtTotAmount').text('');
		$('#txtTotextradays').text('');
		$('#txtRemarks').val('');
		$('#dept_dropdown').select2('val','');
		$('#godown_dropdown').select2('val','');
		$('#employee_dropdown').select2('val','');
		$('#expense_dropdown').select2('val','');
	}

	var populateData = function(data) {
		$('#voucher_type_hidden').val('edit');

		$('#txtVrnoHidden').val(data[0]['vrnoa']);
		$('#txtVrnoa').val(data[0]['vrnoa']);
		$('#txtVrnoaHidden').val(data[0]['vrnoa']);
		$('#current_date').datepicker('update', data[0]['vrdate'].substr(0, 10));
		$('#dept_dropdown').select2('val',data[0]['department_id']);
		$('#godown_dropdown').select2('val',data[0]['godown_id']);
		// $('#employee_dropdown').select2('val',data[0]['employee_id']);
		$('#expense_dropdown').select2('val',data[0]['expense_account']);
		$('#txtRemarks').val(data[0]['remarks']);

		$.each(data, function(index, elem) {
			appendToTable('1', elem.emp_name, elem.employee_id, elem.extra_days, elem.rate, elem.amount);
			calculateNetAmount(elem.extra_days,elem.amount);
		});
	}

	// gets the max id of the voucher
	var getMaxVrno = function() {

		$.ajax({

			url : base_url + 'index.php/production/getMaxVrno',
			type : 'POST',
			data : {'company_id':$('#cid').val()},
			dataType : 'JSON',
			success : function(data) {

				$('#txtVrno').val(data);
				$('#txtMaxVrnoHidden').val(data);
				$('#txtVrnoHidden').val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var getMaxVrnoa = function() {

		$.ajax({

			url : base_url + 'index.php/extraday/getMaxVrnoa',
			type : 'POST',
			data : {'company_id':$('#cid').val()},
			dataType : 'JSON',
			success : function(data) {

				$('#txtVrnoa').val(data);
				$('#txtMaxVrnoaHidden').val(data);
				$('#txtVrnoaHidden').val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var validateSingleProductAdd = function() {


		var errorFlag = false;
		var emp_id = $('#employee1_dropdown').val();
		var extradays = $('#txtExtraDays').val();
		var rate = $('#txtRate').val();
		var amount = $('#txtAmount').val();

		// remove the error class first
		$('#employee1_dropdown').removeClass('inputerror');
		$('#txtExtraDays').removeClass('inputerror');
		$('#txtRate').removeClass('inputerror');
		$('#txtAmount').removeClass('inputerror');

		if ( emp_id === '' || emp_id === null ) {
			$('#employee1_dropdown').addClass('inputerror');
			errorFlag = true;
		}

		if ( extradays === '' || extradays === null ) {
			$('#txtExtraDays').addClass('inputerror');
			errorFlag = true;
		}

		if ( rate === '' || rate === null ) {
			$('#txtRate').addClass('inputerror');
			errorFlag = true;
		}

		if ( amount === '' || amount === null ) {
			$('#txtAmount').addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var appendToTable = function(srno, employee, emp_id, extradays, rate,amount) {

		var srno = $('#purchase_table tbody tr').length + 1;

		var row = 	"<tr>" +
						"<td class='srno'> "+ srno +"</td>" +
				 		"<td class='employee' data-emp_id='"+ emp_id +"'> "+ employee +"</td>" +
				 		"<td class='extradays text-right'> "+ extradays +"</td>" +
				 		"<td class='rate text-right'> "+ rate +"</td>" +
				 		"<td class='amount text-right'> "+ amount +"</td>" +
					 	"<td class='text-center'><a href='' class='btn btn-primary btnRowEdit'><span class='fa fa-edit'></span></a> <a href='' class='btn btn-primary btnRowRemove'><span class='fa fa-trash-o'></span></a> </td>" +
				 	"</tr>";
		$(row).appendTo('#purchase_table');
	}

	var getSaveObject = function() {

		var stockmain = {};
		var stockdetail = [];
		var ledgers = [];

		stockmain.vrnoa 		= $('#txtVrnoaHidden').val();
		stockmain.department_id = $('#dept_dropdown').val();
		stockmain.godown_id 	= $('#godown_dropdown').val();
		//stockmain.employee_id 	= $('#employee_dropdown').val();
		stockmain.vrdate 		= $('#current_date').val();
		stockmain.expense_account = $('#expense_dropdown').val();
		
		stockmain.remarks 		= $('#txtRemarks').val();
		stockmain.etype 		= 'extradays';
		stockmain.uid 			= $('#uid').val();
		stockmain.company_id	= $('#cid').val();

		$('#purchase_table').find('tbody tr').each(function( index, elem ) {
			var od = {};

			od.extradays_did = '';
			od.employee_id 	 = $.trim($(elem).find('td.employee').data('emp_id'));
			//od.godown_id 	 = $.trim($(elem).find('td.dept').data('dept_id'));
			od.extra_days 	 = $.trim($(elem).find('td.extradays').text());
			od.rate 		 = $.trim($(elem).find('td.rate').text());
			od.amount 		 = $.trim($(elem).find('td.amount').text());
			stockdetail.push(od);

			var pledger = {};
			pledger.pledid = '';
			pledger.pid = $.trim($(elem).find('td.employee').data('emp_id'));
			pledger.description = $('#expense_dropdown').find('option:selected').text();
			pledger.remarks =  '-';
			pledger.date = $('#current_date').val();
			pledger.credit = $.trim($(elem).find('td.amount').text());
			pledger.debit = 0;
			pledger.dcno = $('#txtVrnoaHidden').val();
			pledger.etype = 'extradays';
			pledger.pid_key = $('#expense_dropdown').val();
			pledger.uid = $('#uid').val();
			pledger.company_id = $('#cid').val();	
			pledger.isFinal = 0;
			ledgers.push(pledger);
		});

		var pledger = {};
		pledger.pledid = '';
		pledger.pid = $('#expense_dropdown').val();
		pledger.description = $('#expense_dropdown').find('option:selected').text();
		pledger.remarks =  '-';
		pledger.date = $('#current_date').val();
		pledger.credit = 0;
		pledger.debit = $.trim($('#txtTotAmount').text());
		pledger.dcno = $('#txtVrnoaHidden').val();
		pledger.etype = 'extradays';
		pledger.pid_key = $('#expense_dropdown').val();
		pledger.uid = $('#uid').val();
		pledger.company_id = $('#cid').val();	
		pledger.isFinal = 0;
		ledgers.push(pledger);
		
		var data = {};
		data.stockmain = stockmain;
		data.stockdetail = stockdetail;
		data.pledger = ledgers;
		data.vrnoa = $('#txtVrnoaHidden').val();

		return data;
	}

	var deleteVoucher = function(vrnoa) {
		
		$.ajax({
			url : base_url + 'index.php/extraday/delete',
			type : 'POST',
			data : { 'vrnoa' : vrnoa,'company_id': $('#cid').val() },
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No data found');
				} else {
					alert('Voucher deleted successfully');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}


	var calculateNetAmount = function(extradays,amount) {

		var _amount = ($('#txtTotAmount').text() == "") ? 0 : $('#txtTotAmount').text();
		var _extradays = ($('#txtTotextradays').text() == "") ? 0 : $('#txtTotextradays').text();
		//var _weight = ($('#txtGWeight').val() == "") ? 0 : $('#txtGWeight').val();

		var tempAmount = parseFloat(_amount) + parseFloat(amount);
		$('#txtTotAmount').text(parseFloat(tempAmount).toFixed(2));

		var tempextradays = parseFloat(_extradays) + parseFloat(extradays);
		$('#txtTotextradays').text(parseFloat(tempextradays).toFixed(2));
		
	}

	var calculateUpperTotal = function() {

		var job_cost = ($('#job_dropdown').val() == '') ? 0 : $('#job_dropdown').find('option:selected').data('job_cost');
		var qty = ($('#txtSQty').val() == '') ? 0 : $('#txtSQty').val();
		$('#txtCost').val(job_cost);
		var net = parseFloat(job_cost) * parseFloat(qty);
		$('#txtAmpunt').val(net);
	}

	var calculateUpperSum = function() {

		var _extradays = getNumVal($('#txtExtraDays'));
		var _rate	=getNumVal($('#txtRate'));
		
		var _tempAmnt = parseFloat(_extradays) * parseFloat(_rate);			
		
		$('#txtAmount').val(_tempAmnt.toFixed(2));
	}
	var getNumVal = function(el){
		return isNaN(parseFloat(el.val())) ? 0 : parseFloat(el.val());
	}

	var fetchAllEmployeeByDepAndGodown = function(crit){

		$.ajax({

			url : base_url + 'index.php/extraday/fetchAllEmployeeByDepAndGodown',
			type : 'POST',
			data : {crit:crit},
			dataType : 'JSON',
			success : function(data) {

				if(data){

					$("#employee1_dropdown").empty();
					var options = '<option value="" disabled="" selected="">Choose Employee</option>';
			
					$.each(data, function(index, elem){
						options += "<option value='"+elem.pid+"' data-per_day_rate='"+elem.per_day_rate+"'>" +  elem.name + "</option>";
					});

					$(options).appendTo('#employee1_dropdown');	
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	return {

		init : function() {
			$('#voucher_type_hidden').val('new');
			this.bindUI();
		},

		bindUI : function() {
			var self = this;

			$('#txtLevel3').on('change', function() {
				
				var level3 = $('#txtLevel3').val();
				$('#txtselectedLevel1').text('');
				$('#txtselectedLevel2').text('');
				if (level3 !== "" && level3 !== null) {
					// alert('enter' + $(this).find('option:selected').data('level2') );	
					$('#txtselectedLevel2').text(' ' + $(this).find('option:selected').data('level2'));
					$('#txtselectedLevel1').text(' ' + $(this).find('option:selected').data('level1'));
				}
			});

			$('#dept_dropdown, #godown_dropdown').on('change', function() {
				
				var deptId = $('#dept_dropdown').val();
				var gId = $('#godown_dropdown').val();
				
				var crit = '';
				if(deptId){

					crit = ' and deptid='+deptId;
				}

				if(gId){

					crit += ' and did='+gId;
				}

				fetchAllEmployeeByDepAndGodown(crit);
			});

			// $('#txtLevel3').select2();
			$('.btnSaveM').on('click',function(e){
				if ( $('.btnSave').data('saveaccountbtn')==0 ){
					alert('Sorry! you have not save accounts rights..........');
				}else{
					e.preventDefault();
					self.initSaveAccount();
				}
			});
			/*$('.btnPrint').on('click',  function(e) {
				e.preventDefault();
				Print_Voucher(1);
			});*/
			$('.btnPrint').on('click', function(ev) {
                 window.open(base_url + 'application/views/reportprints/extradayprint.php', "Token", 'width=1345, height=650');
            });
			$('.btnprintwithOutHeader').on('click', function(e) {
				e.preventDefault();
				Print_Voucher(0);
			});
			$('.btnResetM').on('click',function(){
				
				$('#txtAccountName').val('');
				$('#txtselectedLevel2').text('');
				$('#txtselectedLevel1').text('');
				$('#txtLevel3').select2('val','');
			});
			$('#AccountAddModel').on('shown.bs.modal',function(e){
				$('#txtAccountName').focus();
			});
			shortcut.add("F3", function() {
    			$('#AccountAddModel').modal('show');
			});

			$('#txtExtraDays').on('input',function(){
				calculateUpperSum();
			});
			$('#txtRate').on('input',function(){
				calculateUpperSum();
			});

			$('.btnSaveMItem').on('click',function(e){
				if ( $('.btnSave').data('saveitembtn')==0 ){
					alert('Sorry! you have not save item rights..........');
				}else{
					e.preventDefault();
					self.initSaveItem();
				}
			});

			$('.btnSave').on('click',  function(e) {
				if ($('#voucher_type_hidden').val()=='edit' && $('.btnSave').data('updatebtn')==0 ){
					alert('Sorry! you have not update rights..........');
				}else if($('#voucher_type_hidden').val()=='new' && $('.btnSave').data('insertbtn')==0){
					alert('Sorry! you have not insert rights..........');
				}else{
					e.preventDefault();
					self.initSave();
				}
			});

			$('.btnReset').on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});

			$('.btnDelete').on('click', function(e){
				e.preventDefault();
				var vrnoa = $('#txtVrnoa').val();
				if (vrnoa !== '') {
					deleteVoucher(vrnoa);
				}
			});

			$('#job_dropdown').on('change', function(){
				calculateUpperTotal();
			});
			$('#txtSQty').on('input', function() {
				calculateUpperTotal();
			});
			shortcut.add("F10", function() {
    			self.initSave();
			});
			shortcut.add("F2", function() {
				$('a[href="#item-lookup"]').trigger('click');
			});
			shortcut.add("F9", function() {
				Print_Voucher();
			});
			shortcut.add("F6", function() {
    			$('#txtVrnoa').focus();
    			// alert('focus');
			});
			shortcut.add("F5", function() {
    			self.resetVoucher();
			});

			shortcut.add("F12", function() {
    			var vrnoa = $('#txtVrnoa').val();
				if (vrnoa !== '') {
					deleteVoucher(vrnoa);
				}
			});


			/////////////////////////////////////////////////////////////////
			/// setting calculations for the single product
			/////////////////////////////////////////////////////////////////

			$('#itemid_dropdown').on('change', function() {
				var item_id = $(this).val();
				var uom = $(this).find('option:selected').data('uom');
				$('#item_dropdown').select2('val', item_id);
				$('#txtUOM').val(uom);
				var stqty = $(this).find('option:selected').data('stqty');
                var stweight = $(this).find('option:selected').data('stweight');
                $('#stqty_lbl').text('Item,     Qty:' + stqty + ', Weight ' + stweight);
                
			});
			$('#item_dropdown').on('change', function() {
				var item_id = $(this).val();
				var uom = $(this).find('option:selected').data('uom');
				$('#itemid_dropdown').select2('val', item_id);
				$('#txtUOM').val(uom);
				var stqty = $(this).find('option:selected').data('stqty');
                var stweight = $(this).find('option:selected').data('stweight');
                $('#stqty_lbl').text('Item,     Qty:' + stqty + ', Weight ' + stweight);
			});

			$('#employee1_dropdown').on('change', function(e){

				var perDayRate = $(this).find('option:selected').data('per_day_rate');
				$('#txtRate').val(perDayRate);
			});

			$('#btnAdd').on('click', function(e) {
				e.preventDefault();

				var error = validateSingleProductAdd();
				if (!error) {

					var employee 	= $('#employee1_dropdown').find('option:selected').text();
					var emp_id  	= $('#employee1_dropdown').val();
					//var dept 		= $('#dept_dropdown').find('option:selected').text();
					//var dept_id 	= $('#dept_dropdown').val();
					var extradays 	= $('#txtExtraDays').val();
					var rate 		= $('#txtRate').val();
					var amount 		= $('#txtAmount').val();
					

					// reset the values of the annoying fields
					$('#employee1_dropdown').select2('val', '');
					//$('#item_dropdown').select2('val', '');
					$('#txtExtraDays').val('');
					$('#txtRate').val('');
					//$('#dept_dropdown').select2('val', '');
					$('#txtAmount').val('');
					//$('#stqty_lbl').text('Item');

					appendToTable('1', employee, emp_id, extradays, rate,amount);
					calculateNetAmount(extradays,amount);
				} else {
					alert('Correct the errors!');
				}

			});

			// when btnRowRemove is clicked
			$('#purchase_table').on('click', '.btnRowRemove', function(e) {
				e.preventDefault();
				var amount = $.trim($(this).closest('tr').find('td.amount').text());
				var extradays = $.trim($(this).closest('tr').find('td.extradays').text());
				calculateNetAmount("-"+extradays,"-"+amount);
				$(this).closest('tr').remove();
			});
			$('#purchase_table').on('click', '.btnRowEdit', function(e) {
				e.preventDefault();

				// getting values of the cruel row
				var emp_id = $.trim($(this).closest('tr').find('td.employee').data('emp_id'));
				var extradays = $.trim($(this).closest('tr').find('td.extradays').text());
				var rate = $.trim($(this).closest('tr').find('td.rate').text());
				var amount = $.trim($(this).closest('tr').find('td.amount').text());
				//var uom = $.trim($(this).closest('tr').find('td.uom').text());
				

				//$('#itemid_dropdown').select2('val', item_id);
				$('#employee1_dropdown').select2('val', emp_id);
				$('#txtExtraDays').val(extradays);
				//$('#dept_dropdown').select2('val', dept_id);
				$('#txtRate').val(rate);
				$('#txtAmount').val(amount);
				
				calculateNetAmount("-"+extradays,"-"+amount);

				// now we have get all the value of the row that is being deleted. so remove that cruel row
				$(this).closest('tr').remove();	// yahoo removed
			});
		
			$('#txtVrnoa').on('change', function() {
				fetch($(this).val());
			});
			$('#txtVrnoa').on('keypress', function(e) {

				if (e.keyCode === 13) {
					e.preventDefault();
					var vrnoa = $('#txtVrnoa').val();
					if (vrnoa !== '') {
						fetch(vrnoa);
					}
				}
			});

			$('#txtPoNo').on('keypress', function(e) {
				if (e.keyCode === 13) {
					e.preventDefault();
					var poNo = $('#txtPoNo').val();
					if (poNo !== '') {
						fetchThroughPO(poNo);
					}
				}
			});

			getMaxVrnoa();
		},

		// prepares the data to save it into the database
		initSave : function() {

			var saveObj = getSaveObject();

			var rowsCount = $('#purchase_table').find('tbody tr').length;

			if (rowsCount > 0 ) {
				save(saveObj);
			} else {
				alert('No data found to save!');
			}
		},
		fetchRequestedVr : function () {

		var vrnoa = general.getQueryStringVal('vrnoa');
		vrnoa = parseInt( vrnoa );
		$('#txtVrnoa').val(vrnoa);
		$('#txtVrnoaHidden').val(vrnoa);
		if ( !isNaN(vrnoa) ) {
			fetch(vrnoa);
		}else{
			getMaxVrno();
			getMaxVrnoa();
		}
	},
			initSaveAccount : function() {

			var saveObjAccount = getSaveObjectAccount();
			var error = validateSaveAccount();

			if (!error) {
					saveAccount(saveObjAccount);
			} else {
				alert('Correct the errors...');
			}
		},
		initSaveItem : function() {

			var saveObjItem = getSaveObjectItem();
			var error = validateSaveItem();

			if (!error) {
					saveItem(saveObjItem);
			} else {
				alert('Correct the errors...');
			}
		},

				bindModalPartyGrid : function() {

			
				            var dontSort = [];
				            $('#party-lookup table thead th').each(function () {
				                if ($(this).hasClass('no_sort')) {
				                    dontSort.push({ "bSortable": false });
				                } else {
				                    dontSort.push(null);
				                }
				            });
				            purchase.pdTable = $('#party-lookup table').dataTable({
				                // "sDom": "<'row-fluid table_top_bar'<'span12'>'<'to_hide_phone'>'f'<'>r>t<'row-fluid'>",
				                "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
				                "aaSorting": [[0, "asc"]],
				                "bPaginate": true,
				                "sPaginationType": "full_numbers",
				                "bJQueryUI": false,
				                "aoColumns": dontSort

				            });
				            $.extend($.fn.dataTableExt.oStdClasses, {
				                "s`": "dataTables_wrapper form-inline"
				            });
},

bindModalItemGrid : function() {

			
				            var dontSort = [];
				            $('#item-lookup table thead th').each(function () {
				                if ($(this).hasClass('no_sort')) {
				                    dontSort.push({ "bSortable": false });
				                } else {
				                    dontSort.push(null);
				                }
				            });
				            purchase.pdTable = $('#item-lookup table').dataTable({
				                // "sDom": "<'row-fluid table_top_bar'<'span12'>'<'to_hide_phone'>'f'<'>r>t<'row-fluid'>",
				                "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
				                "aaSorting": [[0, "asc"]],
				                "bPaginate": true,
				                "sPaginationType": "full_numbers",
				                "bJQueryUI": false,
				                "aoColumns": dontSort

				            });
				            $.extend($.fn.dataTableExt.oStdClasses, {
				                "s`": "dataTables_wrapper form-inline"
				            });
},

		// instead of reseting values reload the page because its cruel to write to much code to simply do that
		resetVoucher : function() {
			general.reloadWindow();
		}
	}

};

var purchase = new Purchase();
purchase.init();