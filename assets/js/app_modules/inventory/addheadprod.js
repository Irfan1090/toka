var headprod = function(){


	var getmaxvrnoa = function(){
			$.ajax({
				url : base_url + 'index.php/headprod/getmaxvrnoa',
				type : 'POST',
				data : {'company_id' : $('#cid').val()},
				dataType : 'JSON',
				success : function(data){
					$('#txtVrnoa').val(data);
					$('#txtMaxVrnoaHidden').val(data);
					$('#txtVrnoaHidden').val(data);
				}, error : function(xhr, status, error) {
					console.log(xhr.responseText);
				}
			});
	}

	var validateSingleProductAdd = function() {


		var errorFlag = false;

		var item_id = $('#hfItemId').val();
		var item_qty = $('#txtSQty').val();
		var godown = $('#dept_dropdown').find('option:selected').val();

		// remove the error class first
		$('#txtItemId').removeClass('inputerror');
		$('#item_dropdown').removeClass('inputerror');
		$('#txtSQty').removeClass('inputerror');
		$('#dept_dropdown').removeClass('inputerror');

		if ( item_id === '' || item_id === null ) {
			$('#txtItemId').addClass('inputerror').focus();
			errorFlag = true;
		}

		if ( item_qty === '' || item_qty === null ) {
			$('#txtSQty').addClass('inputerror').focus();
			errorFlag = true;
		}

		if ( godown === '' || godown === null ) {
			$('#dept_dropdown').addClass('inputerror').focus();
			errorFlag = true;
		}

		return errorFlag;
	}
    var getNumVal = function(el){
        return isNaN(parseFloat(el.val())) ? 0 : parseFloat(el.val());
    }

    var calculateLowerTotal = function(qty) {
        var _qty = getNumVal($('#txtTotalQty'));
        var tempQty = parseFloat(_qty) + parseFloat(qty);
        $('#txtTotalQty').val(tempQty);
    }

	var appendToTable = function(srno, item_id, item_code, item_desc, item_qty){

		var srno = $('#item_table tbody tr').length + 1;

		var row = "<tr>" +
                "<td class='srno'> " + srno + "</td>" +
                "<td class='item' data-item_id='" + item_id + "'> " + item_code + "</td>" +
                "<td class='item_desc'> " + item_desc + "</td>" +
                "<td class='qty' id='item_id_"+item_id+"'> " + item_qty + "</td>" +
                "<td><a href='' class='btn btn-primary btnRowEdit'><span class='fa fa-edit'></span></a> <a href='' class='btn btn-primary btnRowRemove'><span class='fa fa-trash-o'></span></a> </td>" +
                "</tr>";
            $(row).appendTo('#item_table tbody');

	}

	var appendToStaff = function(srno, itemIds, item_des, pid, name, rate, qty, phaseId,perday){

		var srno = '';
		if( phaseId == 1 ){
        	
        	srno = $('#staff_tableF tbody tr').length + 1;
        }else if( phaseId == 11 ){
        	
        	srno = $('#staff_tableM7 tbody tr').length + 1;
        }else if( phaseId == 12 ){
        	
        	srno = $('#staff_tableM8 tbody tr').length + 1;
        }

		//var qty = $('#item_table tbody tr').find('#item_id_'+itemIds+'').text();
		var amount = rate*qty;

		var row = "<tr>" +
                "<td class='srno'> " + srno + "</td>" +
                "<td class='item_des' data-item_id='"+itemIds+"'> " + item_des + "</td>" +
                "<td class='name' data-pid=' " + pid + " '> " + name + "</td>" +
                "<td class='qty'> " + qty + "</td>" +
                "<td class='rate'> " + rate + "</td>" +
                "<td class='amount'> " + amount + "</td>" +
            	"<td class='perday hide'> " + perday + "</td>" +
                "<td> <a href='' class='btn btn-primary btnRowRemove'><span class='fa fa-trash-o'></span></a> </td>" +
                "</tr>";
            // $(row).appendTo('#staff_table');
			console.log(perday);
            if( phaseId == 1 ){
            	$(row).appendTo('#staff_tableF');
            }else if( phaseId == 2 ){
            	$(row).appendTo('#staff_tableC');
            }else if( phaseId == 3 ){
            	$(row).appendTo('#staff_tableG');
            }else if( phaseId == 4 ){
            	$(row).appendTo('#staff_tableW');
            }else if( phaseId == 5 ){
            	$(row).appendTo('#staff_tableM1');
            }else if( phaseId == 6 ){
            	$(row).appendTo('#staff_tableM2');
            }else if( phaseId == 7 ){
            	$(row).appendTo('#staff_tableM3');
            }else if( phaseId == 8 ){
            	$(row).appendTo('#staff_tableM4');
            }else if( phaseId == 9 ){
            	$(row).appendTo('#staff_tableM5');
            }else if( phaseId == 10 ){
            	$(row).appendTo('#staff_tableM6');
            }else if( phaseId == 11 ){
            	$(row).appendTo('#staff_tableM7');
            }else if( phaseId == 12 ){
            	$(row).appendTo('#staff_tableM8');
            }else{
            	alert('no data found');
            }
			
	}

	var calculateTotalAmount = function(){

		var amount = 0;
		var fitting=0;
        var helping1=0;
        var helping2=0;

		$('#staff_tableF').find('tbody td.amount').each(function(index, elem){
			amount += parseFloat($(elem).text());
			fitting +=parseFloat($(elem).text());
		});
		$('#staff_tableM7').find('tbody td.amount').each(function(index, elem){
			amount += parseFloat($(elem).text());
            helping1 +=parseFloat($(elem).text());
		});
		$('#staff_tableM8').find('tbody td.amount').each(function(index, elem){
			amount += parseFloat($(elem).text());
            helping2 +=parseFloat($(elem).text());
		});
        $('#txtFittingAmnt').val(fitting);
        $('#txtHelping1Amnt').val(helping1);
        $('#txtHelping2Amnt').val(helping2);
		$('#txtTotAmnt').val(amount);
	}

	var lessTotalAmount = function(amount){
		
		var tempAmount = 0;
		var totAmount = $('#txtTotAmnt').val();
		tempAmount = parseFloat(totAmount) - parseFloat(amount);
        $('#txtTotAmnt').val(tempAmount);
	}

	var getSaveObject = function() {

		var stockmain = {};
		var stockdetail = [];
		var ledger = [];
		

		stockmain.vrnoa = $('#txtVrnoaHidden').val();
		stockmain.remarks = $('#txtRemarks').val();
		stockmain.etype = 'head_production';
		stockmain.uid = $('#uid').val();
		stockmain.company_id = $('#cid').val();
        stockmain.vrdate = $('#current_date').val();
        stockmain.party_id = $.trim($('#party_dropdown').find('option:selected').val());
		stockmain.type=$('#voucher_type_hidden').val();
		$('#item_table').find('tbody tr').each(function( index, elem ) {
			var sd = {};

			sd.stdid = '';
            sd.type = 'add';
			sd.item_id = $.trim($(elem).find('td.item').data('item_id'));
			sd.godown_id = $.trim($('#dept_dropdown').find('option:selected').val());
			sd.qty = $.trim($(elem).find('td.qty').text());
			sd.perday=$.trim($(elem).find('td.perday').text());

            stockdetail.push(sd);
		});

		$('#staff_tableF').find('tbody tr').each(function( index, elem ) {
			

			var sd = {};

			sd.stdid = '';
			sd.type = 'add_fitting';
			sd.item_id = $.trim($(elem).find('td.item_des').data('item_id'));
			sd.godown_id = $.trim($('#dept_dropdown').find('option:selected').val());
			sd.emp_id = $.trim($(elem).find('td.name').data('pid'));
			sd.qty = $.trim($(elem).find('td.qty').text());
			sd.rate = $.trim($(elem).find('td.rate').text());
			sd.amount = $.trim($(elem).find('td.amount').text());
			sd.perday=$.trim($(elem).find('td.perday').text());
            var perday=$.trim($(elem).find('td.perday').text());
            var qty =$.trim($(elem).find('td.qty').text());
            sd.produced=qty/perday;
			stockdetail.push(sd);

			var sd = {};
			sd.stdid = '';
			sd.type = 'less_fitting';
			sd.item_id = $.trim($(elem).find('td.item_des').data('item_id'));
			sd.godown_id = $.trim($('#dept_dropdown').find('option:selected').val());
			sd.emp_id = $.trim($(elem).find('td.name').data('pid'));
			sd.qty = '-'+$.trim($(elem).find('td.qty').text());
			sd.rate = $.trim($(elem).find('td.rate').text());
			sd.amount = '-'+$.trim($(elem).find('td.amount').text());
            sd.perday=$.trim($(elem).find('td.perday').text());
            var perday=$.trim($(elem).find('td.perday').text());
            var qty =$.trim($(elem).find('td.qty').text());
            sd.produced=qty/perday;
			stockdetail.push(sd);


			var pledger = {};

			pledger.pid = $.trim($(elem).find('td.name').data('pid'));
			pledger.description = $.trim($(elem).find('td.item_des').text() + $(elem).find('td.qty').text() + '@' + $(elem).find('td.rate').text());
			pledger.remarks =  $('#txtRemarks').val();
			pledger.credit = $.trim($(elem).find('td.amount').text());
			pledger.debit = 0;
			pledger.date =  $.trim($('#current_date').val());
			pledger.etype =  'head_production';
			pledger.dcno =  $.trim($('#txtVrnoa').val());
			pledger.pid_key =  $.trim($(elem).find('td.name').data('pid'));

			ledger.push(pledger);
		});

		$('#staff_tableM7').find('tbody tr').each(function( index, elem ) {
			

			var sd = {};

			sd.stdid = '';
			sd.type = 'add_helping1';
			sd.item_id = $.trim($(elem).find('td.item_des').data('item_id'));
			sd.godown_id = $.trim($('#dept_dropdown').find('option:selected').val());
			sd.emp_id = $.trim($(elem).find('td.name').data('pid'));
			sd.qty = $.trim($(elem).find('td.qty').text());
			sd.rate = $.trim($(elem).find('td.rate').text());
			sd.amount = $.trim($(elem).find('td.amount').text());
            sd.perday=$.trim($(elem).find('td.perday').text());
            var perday=$.trim($(elem).find('td.perday').text());
            var qty =$.trim($(elem).find('td.qty').text());
            sd.produced=qty/perday;
			stockdetail.push(sd);

			var sd = {};
			sd.stdid = '';
			sd.type = 'less_helping1';
			sd.item_id = $.trim($(elem).find('td.item_des').data('item_id'));
			sd.godown_id = $.trim($('#dept_dropdown').find('option:selected').val());
			sd.emp_id = $.trim($(elem).find('td.name').data('pid'));
			sd.qty = '-'+$.trim($(elem).find('td.qty').text());
			sd.rate = $.trim($(elem).find('td.rate').text());
			sd.amount = '-'+$.trim($(elem).find('td.amount').text());
            sd.perday=$.trim($(elem).find('td.perday').text());
            var perday=$.trim($(elem).find('td.perday').text());
            var qty =$.trim($(elem).find('td.qty').text());
            sd.produced=qty/perday;
			stockdetail.push(sd);


			var pledger = {};

			pledger.pid = $.trim($(elem).find('td.name').data('pid'));
			pledger.description = $.trim($(elem).find('td.item_des').text() + $(elem).find('td.qty').text() + '@' + $(elem).find('td.rate').text());
			pledger.remarks =  $('#txtRemarks').val();
			pledger.credit = $.trim($(elem).find('td.amount').text());
			pledger.debit = 0;
			pledger.date =  $.trim($('#current_date').val());
			pledger.etype =  'head_production';
			pledger.dcno =  $.trim($('#txtVrnoa').val());
			pledger.pid_key =  $.trim($(elem).find('td.name').data('pid'));
			ledger.push(pledger);
		});

		$('#staff_tableM8').find('tbody tr').each(function( index, elem ) {
			

			var sd = {};

			sd.stdid = '';
			sd.type = 'add_helping2';
			sd.item_id = $.trim($(elem).find('td.item_des').data('item_id'));
			sd.godown_id = $.trim($('#dept_dropdown').find('option:selected').val());
			sd.emp_id = $.trim($(elem).find('td.name').data('pid'));
			sd.qty = $.trim($(elem).find('td.qty').text());
			sd.rate = $.trim($(elem).find('td.rate').text());
			sd.amount = $.trim($(elem).find('td.amount').text());
            sd.perday=$.trim($(elem).find('td.perday').text());
            var perday=$.trim($(elem).find('td.perday').text());
            var qty =$.trim($(elem).find('td.qty').text());
            sd.produced=qty/perday;
			stockdetail.push(sd);

			var sd = {};
			sd.stdid = '';
			sd.type = 'less_helping2';
			sd.item_id = $.trim($(elem).find('td.item_des').data('item_id'));
			sd.godown_id = $.trim($('#dept_dropdown').find('option:selected').val());
			sd.emp_id = $.trim($(elem).find('td.name').data('pid'));
			sd.qty = '-'+$.trim($(elem).find('td.qty').text());
			sd.rate = $.trim($(elem).find('td.rate').text());
			sd.amount = '-'+$.trim($(elem).find('td.amount').text());
            sd.perday=$.trim($(elem).find('td.perday').text());
            var perday=$.trim($(elem).find('td.perday').text());
            var qty =$.trim($(elem).find('td.qty').text());
            sd.produced=qty/perday;
			stockdetail.push(sd);


			var pledger = {};

			pledger.pid = $.trim($(elem).find('td.name').data('pid'));
			pledger.description = $.trim($(elem).find('td.item_des').text() + $(elem).find('td.qty').text() + '@' + $(elem).find('td.rate').text());
			pledger.remarks =  $('#txtRemarks').val();
			pledger.credit = $.trim($(elem).find('td.amount').text());
			pledger.debit = 0;
			pledger.date =  $.trim($('#current_date').val());
			pledger.etype =  'head_production';
			pledger.dcno =  $.trim($('#txtVrnoa').val());
			pledger.pid_key =  $.trim($(elem).find('td.name').data('pid'));
			ledger.push(pledger);
		});

		var acledger = {};
		acledger.pid = $.trim($('#party_dropdown').find('option:selected').val());
		acledger.description = 'Expence';
		acledger.remarks =  $('#txtRemarks').val();
		acledger.credit = 0;
		acledger.debit = $.trim($('#txtTotAmnt').val());
		acledger.date =  $.trim($('#current_date').val());
		acledger.etype =  'head_production';
		acledger.dcno =  $.trim($('#txtVrnoa').val());
		acledger.pid_key =  $.trim($('#party_dropdown').find('option:selected').val());
		ledger.push(acledger);
		var data = {};
		data.stockmain = stockmain;
		data.stockdetail = stockdetail;
		data.ledger = ledger;
		data.vrnoa = $('#txtVrnoaHidden').val();

		return data;
	}

    shortcut.add("F10", function() {
        $('.btnSave').trigger('click');
    });
    
    shortcut.add("F12", function() {
        $('.btnDelete').trigger('click');
    });
    shortcut.add("F5", function() {
        self.resetVoucher();
    });
    shortcut.add("F6", function() {
        $('#txtVrnoa').focus();
        // alert('focus');
    });
    shortcut.add("F9", function() {
        Print_Voucher(1,'lg','');
    });
	var save = function(production) {
		// alert(production.stockmain['company_id']);
		$.ajax({
			url : base_url + 'index.php/headprod/save',
			type : 'POST',
			data : { 'stockmain' : production.stockmain, 'stockdetail' : JSON.stringify(production.stockdetail), 'ledger' : production.ledger, 'vrnoa' : production.stockmain['vrnoa'], 'etype' : production.stockmain['etype'] },
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'true') {
					alert('An internal error occured while saving voucher. Please try again.');
				} else {
					var printConfirmation = confirm('Voucher saved!\nWould you like to print the invoice as well?');
					if (printConfirmation === true) {
						Print_Voucher(1);
					} else {
						general.reloadWindow();
					}
					// alert('Voucher Saved successfully...');
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var fetch = function(vrnoa) {

		$.ajax({

			url : base_url + 'index.php/headprod/fetch',
			type : 'POST',
			data : { 'vrnoa' : vrnoa ,'company_id': $('#cid').val() },
			dataType : 'JSON',
			success : function(data) {

				$('.st').find('tbody tr').remove();
				$('#dept_dropdown').select2('val', '');
				$('#txtRemarks').val('');

				if (data === 'false') {
					alert('No data found.');
				} else {
					populateData(data);
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
    var Print_Voucher = function(hd) {
		if ( $('.btnSave').data('printbtn')==0 ){
				alert('Sorry! you have not print rights..........');
		}else{
			var etype=  'head_production';
			var vrnoa = $('#txtVrnoa').val();
			var company_id = $('#cid').val();
			var user = $('#uname').val();
			// var hd = $('#hd').val();
			var pre_bal_print =  '0';
			
			var url = base_url + 'index.php/doc/print_pro_voucher/' + etype + '/' + vrnoa + '/' + company_id + '/' + '-1' + '/' + user + '/' + pre_bal_print+ '/' + hd;
			// var url = base_url + 'index.php/doc/CashVocuherPrintPdf/' + etype + '/' + dcno   + '/' + companyid + '/' + '-1' + '/' + user;
			window.open(url);
		}

	}
	var populateData = function(data) {

		$('#voucher_type_hidden').val('edit');
		$('#txtVrnoHidden').val(data[0]['vrnoa']);
		$('#txtVrnoa').val(data[0]['vrnoa']);
		$('#txtVrnoaHidden').val(data[0]['vrnoa']);
		$('#current_date').datepicker('update', data[0]['vrdate'].substr(0, 10));
		$('#receivers_list').val(data[0]['received_by']);
		$('#txtRemarks').val(data[0]['remarks']);
		$('#dept_dropdown').select2('val', data[0]['godown_id']);
        $('#party_dropdown').select2('val', data[0]['party_id']);
        console.log(data);
		$.each(data, function(index, elem) {

			if(elem.type == 'add'){
				appendToTable('1', elem.item_id, elem.item_code, elem.item_name, elem.s_qty);
                calculateLowerTotal(elem.s_qty);
			}else if(elem.type == 'add_fitting'){
				appendToStaff('1', elem.item_id, elem.item_name, elem.emp_id, elem.name, elem.s_rate, elem.s_qty, 1,elem.perday);
			}else if(elem.type == 'add_helping1'){

				appendToStaff('1', elem.item_id, elem.item_name, elem.emp_id, elem.name, elem.s_rate, elem.s_qty, 11,elem.perday);
			}else if(elem.type == 'add_helping2'){

				appendToStaff('1', elem.item_id, elem.item_name, elem.emp_id, elem.name, elem.s_rate, elem.s_qty, 12,elem.perday);
			}
			calculateTotalAmount();
            });
	}

	var fetch_present_staff = function(itemIds, itemData, deptId, phaseId){

		$.ajax({
			url : base_url + 'index.php/headprod/fetch_present_staff',
			type : 'POST',
			data : { 'item_ids' : itemIds, item_data: itemData, 'dept_id' : deptId,  'phase_id': phaseId, 'date': $('#current_date').val()},
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No data found');
				} else {
					$.each(data, function(index, elem) {
                        appendToStaff('1', elem.item_id, elem.item_des, elem.pid, elem.name, elem.rate, elem.qty, phaseId,elem.perday);
					});
					calculateTotalAmount();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

		// checks for the empty fields
	var validateSave = function() {

		var errorFlag = false;
		var partyEl = $('#party_dropdown');
		
		$('.inputerror').removeClass('inputerror');

		if ( !partyEl.val() ) {
			
			$('#party_dropdown').addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var deleteVoucher = function(vrnoa) {
		
		$.ajax({
			url : base_url + 'index.php/headprod/delete',
			type : 'POST',
			data : { 'vrnoa' : vrnoa, 'company_id': $('#cid').val() },
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {

					alert('No data found');
				} else {
					
					alert('Voucher deleted successfully');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var clearItemData = function (){
		
		$("#hfItemId").val("");
		$("#hfItemSize").val("");
		$("#hfItemBid").val("");
		$("#hfItemUom").val("");
		$("#hfItemPrate").val("");
		$("#hfItemGrWeight").val("");
		$("#hfItemStQty").val("");
		$("#hfItemStWeight").val("");
		$("#hfItemLength").val("");
		$("#hfItemCatId").val("");
		$("#hfItemSubCatId").val("");
		$("#hfItemDesc").val("");
		$("#hfItemShortCode").val("");

        //$("#txtItemId").val("");
    }



		return {

		init : function() {
			$('#voucher_type_hidden').val('new');
            $('#cid').val(1)
			this.bindUI();
		},

		bindUI : function() {
			var self = this;

			$('#dept_dropdown').focus().css('border', '1px solid #368EE0');

			$('.btn').focus(function(){
				$(this).css('border', '2px solid black');
			}
			);
			$('.btn').blur(function(){
				$(this).css('border', '');
				}
			);

			getmaxvrnoa();

			$('#btnAddItem').on('click', function(e){
				e.preventDefault();

				var error = validateSingleProductAdd();
				if (!error) {

					var item_desc = $('#itemDesc').val();
					var uitem_des = $('#hfItemUname').val();
					var uom_item = $('#hfItemUom').val();
					var item_code = $('#hfItemShortCode').val();
					var item_id = $('#hfItemId').val();
					var item_qty = $('#txtSQty').val();

					$('#txtItemId').val('');
					$('#itemDesc').val('');
					$('#txtSQty').val('');

					appendToTable('1', item_id, item_code, item_desc, item_qty);
					calculateLowerTotal(item_qty);
				} else {
					alert('Correct the errors!');
				}

			});

			$('.st').on('click', '.btnRowRemove', function(e) {
				e.preventDefault();
				
				var amountLess = $.trim($(this).closest('tr').find('td.amount').text());
                var qty = $.trim($(this).closest('tr').find('td.qty').text());
				lessTotalAmount(amountLess);
				$(this).closest('tr').remove();
				calculateLowerTotal(-qty);


			});

			$('#item_table').on('click', '.btnRowEdit', function(e) {
				e.preventDefault();

				// getting values of the cruel row
				var item_id = $.trim($(this).closest('tr').find('td.item').data('item_id'));
				var qty = $.trim($(this).closest('tr').find('td.qty').text());
                var item_code = $.trim($(this).closest('tr').find('td.item_id').data('item_code'));

				$.ajax({
					type: "POST",
					url: base_url + 'index.php/item/getiteminfobyid',
					data: {
						item_id: item_id
					}
				}).done(function (result) {

					$("#imgItemLoader").hide();
					var item = $.parseJSON(result);

					if (item != false)
					{
						$("#hfItemId").val(item[0].item_id);
						$("#hfItemSize").val(item[0].size);
						$("#hfItemBid").val(item[0].bid);
						$("#hfItemUom").val(item[0].uom);
						$("#hfItemPrate").val(item[0].cost_price);
						$("#hfItemGrWeight").val(item[0].grweight);
						$("#hfItemStQty").val(item[0].stqty);
						$("#hfItemStWeight").val(item[0].stweight);
						$("#hfItemLength").val(item[0].length);
						$("#hfItemCatId").val(item[0].catid);
						$("#hfItemSubCatId").val(item[0].subcatid);
						$("#hfItemDesc").val(item[0].item_des);
						$("#hfItemShortCode").val(item[0].short_code);
						$("#hfItemUname").val(item[0].uname);

						$("#txtItemId").val(item[0].item_code);
						$('#itemDesc').val(item[0].item_des);
						$('#txtUom').val(item[0].uom);


                        $("#txtItemId").val(item_code);
						$('#txtSQty').val(qty);
						$('#stqty_lbl').text('Item,     Uom:' + item[0].uom);
                        // now we have get all the value of the row that is being deleted. so remove that cruel row
                    }
                });
				// now we have get all the value of the row that is being deleted. so remove that cruel row
				$(this).closest('tr').remove();
                calculateLowerTotal(-qty);
			});


			$('.btnSave').on('click',  function(e) {
				
				e.preventDefault();
				self.initSave();
			});

			/*$('.btnDelete').on('click', function(e){
				e.preventDefault();
				var vrnoa = $('#txtVrnoa').val();
				if (vrnoa !== '') {
					if (confirm('Are you sure to delete this voucher?'))
					deleteVoucher(vrnoa);
				}
			});*/
			
			$('.btnDelete').on('click', function(e){
				e.preventDefault();
				var vrnoa = $('#txtVrnoa').val();
				if (vrnoa !== '') {
					if (confirm('Are you sure to delete this voucher?'))
					deleteVoucher(vrnoa);
				}
			});
				$('.btnPrint').on('click',  function(e) {
				e.preventDefault();
				Print_Voucher(1);
			});
			$('.btnPrints').on('click',  function(e) {
				e.preventDefault();
				Print_Vouchers(0);
			});
			$('.btnprintwithOutHeader').on('click', function(e) {
				e.preventDefault();
				Print_Voucher(0);
			});

			$('#btnAddF, #btnAddC, #btnAddG, #btnAddW, #btnAddM1, #btnAddM2, #btnAddM3, #btnAddM4, #btnAddM5, #btnAddM6, #btnAddM7, #btnAddM8').on('click', function(e){

				e.preventDefault();

				var itemArray =new Array();
				var itemData = [];

				$('#item_table').find('tbody tr').each(function( index, elem ) {
					var itemId = $.trim($(elem).find('td.item').data('item_id'));
					var qty = $.trim($(elem).find('td.qty').text());
					itemArray.push(itemId);

					var item = {
						item_id: itemId,
						qty: qty
					}

					itemData.push(item);
				});

				// console.log(itemArray);
				var deptId = $.trim($(this).parent().parent().find('#dept_dropdown').val());
				var phaseId = $.trim($(this).parent().parent().find('#dept_dropdown').find('option:selected').data('phaseid'));
				
				fetch_present_staff(itemArray, itemData, deptId, phaseId);
			});

			var countItem = 0;
			$('input[id="txtItemId"]').autoComplete({

				minChars: 1,
				cache: false,
				menuClass: '',
				source: function(search, response)
				{
					try { xhr.abort(); } catch(e){}
					$('#txtItemId').removeClass('inputerror');
					$("#imgItemLoader").hide();
					if(search != "")
					{
						xhr = $.ajax({
							url: base_url + 'index.php/item/searchitem',
							type: 'POST',
							data: {

								search: search
							},
							dataType: 'JSON',
							beforeSend: function (data) {

								$(".loader").hide();
								$("#imgItemLoader").show();
								countItem = 0;
							},
							success: function (data) {

								if(data == '')
								{
									$('#txtItemId').addClass('inputerror');
									clearItemData();
									$('#itemDesc').val('');
									$('#txtQty').val('');
									$('#txtPRate').val('');
									$('#txtBundle').val('');
									$('#txtGBundle').val('');
									$('#txtWeight').val('');
									$('#txtAmount').val('');
									$('#txtGWeight').val('');
									$('#txtDiscp').val('');
									$('#txtDiscount1_tbl').val('');
								}
								else
								{
									$('#txtItemId').removeClass('inputerror');
									response(data);
									$("#imgItemLoader").hide();
								}
							}
						});
					}
					else
					{
						clearItemData();
					}
				},
				renderItem: function (item, search)
				{
					var sea = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
					var re = new RegExp("(" + sea.split(' ').join('|') + ")", "gi");

					var selected = "";
					if((search.toLowerCase() == (item.item_des).toLowerCase() && countItem == 0) || (search.toLowerCase() != (item.item_des).toLowerCase() && countItem == 0))
					{
						selected = "selected";
					}
					countItem++;
					clearItemData();

					return '<div class="autocomplete-suggestion ' + selected + '" data-val="' + search + '" data-item_id="' + item.item_id + '" data-size="' + item.pack + '" data-bid="' + item.bid +
					'" data-uom_item="'+ item.uom + '" data-srate="' + item.srate + '" data-grweight="' + item.grweight + '" data-stqty="' + item.stqty +
					'" data-stweight="' + item.stweight + '" data-length="' + item.length  + '" data-catid="' + item.catid +
					'" data-subcatid="' + item.subcatid + '" data-desc="' + item.item_des + '" data-cost_price="' + item.cost_price + '" data-code="' + item.item_code + '" data-self="' + item.self_freight_rate + '" data-bilty="' + item.bilty_freight_rate + 
					'" data-gari="' + item.gari_freight_rate + '">' + item.item_des.replace(re, "<b>$1</b>") + '</div>';
				},
				onSelect: function(e, term, item)
				{

					$("#imgItemLoader").hide();
					$("#hfItemId").val(item.data('item_id'));
					$("#hfItemSize").val(item.data('size'));
					$("#hfItemBid").val(item.data('bid'));
					$("#hfItemUom").val(item.data('uom_item'));
					$("#hfItemPrate").val(item.data('cost_price'));
					$("#hfItemGrWeight").val(item.data('grweight'));
					$("#hfItemStQty").val(item.data('stqty'));
					$("#hfItemStWeight").val(item.data('stweight'));
					$("#hfItemLength").val(item.data('length'));
					$("#hfItemCatId").val(item.data('catid'));
					$("#hfItemSubCatId").val(item.data('subcatid'));
					$("#hfItemDesc").val(item.data('desc'));
					$("#hfItemShortCode").val(item.data('code'));
					$("#hfItemUname").val(item.data('uname'));


					var freightStatus = $('input[name=inlineRadioOptions]:checked').val();

					if(freightStatus == 'self'){
						$('#txtFRate').val(item.data('self'));	
					}else if(freightStatus == 'bilty'){
						$('#txtFRate').val(item.data('bilty'));
					}else if(freightStatus == 'gari'){
						$('#txtFRate').val(item.data('gari'));
					}

					$("#txtItemId").val(item.data('code'));

					var itemId = item.data('item_id');
					var itemDesc = item.data('desc');
					var pRate = item.data('cost_price');
					var grWeight = item.data('grweight');
					var uomItem = item.data('uom_item');
					var stQty = item.data('stqty');
					var stWeight = item.data('stweight');
					var size = item.data('size');
					var brandId = item.data('bid');

					$('#stqty_lbl').text('Item,     Uom:' + uomItem);
					$('#itemDesc').val(itemDesc);
					$('#txtPRate').val(parseFloat(pRate).toFixed(2));
					$('#txtGrWeight').val(parseFloat(grWeight).toFixed(2));
					$('#txtUOM').val(uomItem);
					$('#txtGWeight').trigger('input');	
				}
			});

			$('.btnReset').on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});

			$('#txtVrnoa').on('change', function() {
				fetch($(this).val());
			});

			$('#itemid_dropdown').on('change', function() {
				var item_id = $(this).val();
				$('#item_dropdown').select2('val', item_id);
			});

			$('#item_dropdown').on('change', function() {
				var item_id = $(this).val();
				$('#itemid_dropdown').select2('val', item_id);
			});
		},

		initSave : function() {

			var error = validateSave();

			if (!error) {
				var saveObj = getSaveObject();
				var rowsCount = $('#item_table').find('tbody tr').length;

				if (rowsCount > 0 ) {
					save(saveObj);
				} else {
					alert('No data found to save!');
				}
			} else {
				alert('Correct the errors...');
			}
		},
		resetVoucher : function() {
			general.reloadWindow();
		}
	}

};
var headprod = new headprod();
headprod.init();