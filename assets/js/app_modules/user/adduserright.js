var Brand = function() {

	var save = function( rights ) {

		$.ajax({
			url : base_url + 'index.php/userright/save',
			type : 'POST',
			data : { 'user_id' : $('#users_dropdown').select2('val'), 'rights' : JSON.stringify(rights) },
			dataType : 'JSON',
			beforeSend: function(data) {

			},
			success : function(data) {

				if (data == "duplicate") {
					alert('Brand name already saved!');
				} else {					
					if (data.error === 'false') {
						alert('An internal error occured while saving voucher. Please try again.');
					} else {
						alert('User rights saved successfully.');
						general.reloadWindow();
					}
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var getSaveObject = function()
    {
        var rights = [];
        $('#levels_table').find('tbody tr').find('input[type="checkbox"]:checked').each(function( index, elem ) {
            var right = {};
            right.uid = $('#users_dropdown').select2('val');
            right.l2_id = $(elem).val();
            right.status = '1';

            rights.push(right);
        });

        return rights;
	}

    var fetchLevel2 = function(id) {

        $.ajax({
            url : base_url + 'index.php/userright/fetchlevel2',
            type : 'POST',
            data : { 'user_id' : $('#users_dropdown').select2('val'), 'l1' : id },
            dataType : 'JSON',
            success : function(data) {

                if (data === 'false') {
                    alert('No data found');
                } else {
                    populateData(data);
                }
            }, error : function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }

	var populateData = function(data) {
		$('#voucher_type_hidden').val('edit');		
		//$('#voucher_type_hidden').val('edit');
        $('#levelsRows tr').remove();
        $.each(data, function(index, elem) {
            var row = "<tr>" +
                        //"<td class='text-center'><input type='checkbox' class='checkboxStatus' value='" + elem.l2 + "' " + ((elem.status == "1")?"checked":"") + ">" +
                        "<td class='text-center'><input type='checkbox' class='checkboxStatus' value='" + elem.l3 + "' " + ((elem.status == "1")?"checked":"") + ">" +
                        "<td>" + elem.l1name + "</td>" +
                        "<td>" + elem.l2name + "</td>" +
                        "<td>" + elem.l3name + "</td>" +
                        "</tr>";
            $(row).appendTo('#levelsRows');
        });
	}

	// gets the maxid of the voucher
	var getMaxId = function() {

		$.ajax({
			url : base_url + 'index.php/item/getMaxBrandId',
			type : 'POST',
			dataType : 'JSON',
			success : function(data) {

				$('#txtId').val(data);
				$('#txtIdHidden').val(data);
				$('#txtMaxIdHidden').val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// checks for the empty fields
	var validateSave = function() {

		var errorFlag = false;

		var userId = $.trim($('#users_dropdown').select2('val'));

		// remove the error class first
		$('#users_dropdown').removeClass('inputerror');

		if (userId == '') {
            $('#users_dropdown').addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}


	return {

		init : function() {
			$('#voucher_type_hidden').val('new');
			this.bindUI();
		},

		bindUI : function() {

			var self = this;
			$('#voucher_type_hidden').val('new');		

			shortcut.add("F10", function() {
    			$('.btnSave').trigger('click');
			});
			shortcut.add("F6", function() {
    			$('#txtId').focus();
			});
			shortcut.add("F5", function() {
    			self.resetVoucher();
			});
			
			$('#txtId').on('change', function() {
				fetch($(this).val());
			});

			$('.btnSave').on('click', function(e) {
				if ($('#voucher_type_hidden').val()=='edit' && $('.btnSave').data('updatebtn')==0 ){
					alert('Sorry! you have not update rights..........');
				}else if($('#voucher_type_hidden').val()=='new' && $('.btnSave').data('insertbtn')==0){
					alert('Sorry! you have not insert rights..........');
				}else{
					e.preventDefault();
					self.initSave();
				}
			});

			$('.btnReset').on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});

			$('#txtId').on('keypress', function(e) {
				if (e.keyCode === 13) {
					if ( $('#txtId').val().trim() !== "" ) {
						var bid = $.trim($('#txtId').val());
						fetch(bid);
					}
				}
			});

			$('table').on('click', '.btn-edit-brand', function(e) {
				e.preventDefault();
				fetch($(this).data('bid'));
				$('a[href="#add_brand"]').trigger('click');
			});

            $('#users_dropdown').on('change', function() {
                var val = $(this).val();
                if(val != "")
                {
                    $('input[type="radio"]').prop('checked', false);
                    $('#level_dropdown').select2('val', '');
                    $('#levelsRows tr').remove();
                    fetchLevel2('');
                }
            });

            $('#level_dropdown').on('change', function() {
                var val = $(this).val();
                if($('#users_dropdown').select2('val') != null)
                {
                    fetchLevel2(val);
                }
            });

            $('#radioCheckedAll').on('click', function(e) {
                $('input[type="checkbox"]').prop('checked', true);
            });

            $('#radioUncheckedAll').on('click', function(e) {
                $('input[type="checkbox"]').prop('checked', false);
            });

			getMaxId();
		},

		initSave : function() {

			var saveObj = getSaveObject();
			var error = validateSave();

			if ( !error ) {
				save( saveObj );
			} else {
				alert('Correct the errors!');
			}
		},

		resetVoucher : function() {
			general.reloadWindow();
		}
	};
};

var brand = new Brand();
brand.init();