 chequeissuevoucher = function() {


	var fetch = function(dcno , etype) {
		$.ajax({

			url : base_url + 'index.php/payment/fetchChequeVoucher',
			type : 'POST',
			data : { 'dcno' : dcno, 'etype' : etype, 'company_id': $('#cid').val() },
			dataType : 'JSON',
			success : function(data) {

				if (data.length === 0) {
					alert('No data found.');
					chequeissuevoucher.resetVoucher();
				} else {
					$(data).each(function(index, elem){
							populateData(elem);
					});

					
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateData = function(data) {

		
		$('#txtIdHidden').val(data.dcno);
		$('#txtId').val(data.dcno);
		$('#current_date').val( ( data.vrdate ) ? ( data.vrdate.substr(0,10) ) : '' );
		$('#party_dropdown').select2('val', data.party_id);
		$('#tobank_dropdown').val(data.bank_name);
		$('#txtChequeNo').val(data.cheque_no);
		$('#cheque_date').val( ( data.cheque_date ) ? ( data.cheque_date.substr(0,10) ) : '' );
		$('#txtSlipNo').val(data.slip_no);
		$('#status_dropdown').val(data.status);
		$('#txtAmount').val(data.amount);
		$('#txtNote').val(data.note);
		$('#txtRemarks').val(data.remarks);
		$('#party_id_cr').select2('val', data.party_id_cr);
		$('#mature_date').val( ( data.mature_date ) ? ( data.mature_date.substr(0,10) ) : '' );
		$('input[name=post][value=' + data.post + ']').prop('checked', 'true');
		$('#UserNameTextBox').val(data.User_Name);
		$('#voucher_type_hidden').val('edit');
		$('#user_dropdown').val(data.uid);
	}

	// gets the max id of the voucher
	var getMaxId = function( etype ) {

		$.ajax({

			url : base_url + 'index.php/account/getMaxChequeId',
			type : 'POST',
			dataType : 'JSON',
			data : {'etype': etype ,'company_id':$('#cid').val()},
			success : function(data) {

				$('#txtId').val(data);
				$('#txtMaxIdHidden').val(data);
				$('#txtIdHidden').val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var getSaveObject = function() {

		var pdcheque = {};
		var _etype = ($('#pd_receive').is(':checked') === true) ? 'pd_receive' : 'pd_issue';
		pdcheque.dcno = $('#txtIdHidden').val();
		pdcheque.vrdate = $('#current_date').val();
		pdcheque.party_id = $('#party_dropdown').val();
		pdcheque.bank_name = $('#tobank_dropdown').val();
		pdcheque.cheque_no = $('#txtChequeNo').val();
		pdcheque.cheque_date = $('#cheque_date').val();
		pdcheque.slip_no = $('#txtSlipNo').val();
		pdcheque.status = $('#status_dropdown').val();
		pdcheque.amount = $('#txtAmount').val();
		pdcheque.note = $('#txtNote').val();
		pdcheque.remarks = $('#txtRemarks').val();
		pdcheque.party_id_cr = $('#party_id_cr').val();
		pdcheque.mature_date = $('#mature_date').val();
		pdcheque.post = $('input[name=post]:checked').val();
		pdcheque.etype = _etype;
		pdcheque.uid = $('#uid').val();
		pdcheque.company_id = $('#cid').val();
		// pdcheque.voucher_type_hidden = $('#voucher_type_hidden').val();

		if (pdcheque.post === 'unpost') {
			saveUnpost( pdcheque );
		} else {
			saveLedgerPost( pdcheque , _etype );
		}
	}

	var Print_Voucher = function( ) {
		if ( $('.btnSave').data('printbtn')==0 ){
				alert('Sorry! you have not save rights..........');
		}else{
			var etype= 'pd_issue';
			var dcno = $('#txtId').val();
			var companyid = '1';
			var user = $('#uname').val();
			// alert('etype  ' +  etype  +' dcno '+ user );
			var url = base_url + 'index.php/doc/pdf_singlecheque/' + etype + '/' + dcno  + '/' + companyid + '/' + '-1' + '/' + user;
			window.open(url);
		}
	}


	var saveLedgerPost = function ( pd_cheque , _etype ) {

		var pledger = [];
		var party1Pledger = {};
		var party2Pledger = {};

		party1Pledger.pledid = '';
		party1Pledger.dcno = $('#txtIdHidden').val();
		party1Pledger.date = $('#current_date').val();
		party1Pledger.pid = $('#party_dropdown').val();
		party1Pledger.description = $('#txtRemarks').val();
		party1Pledger.debit = (_etype === 'pd_issue') ? $('#txtAmount').val() : 0;
		party1Pledger.credit = (_etype === 'pd_receive') ? $('#txtAmount').val() : 0;
		party1Pledger.pid_key = $('#party_id_cr').val();
		party1Pledger.etype = _etype;
		party1Pledger.uid = $('#uid').val();
		party1Pledger.company_id = $('#cid').val();
		pledger.push(party1Pledger);

		party2Pledger.pledid = '';
		party2Pledger.dcno = $('#txtIdHidden').val();
		party2Pledger.date = $('#current_date').val();
		party2Pledger.pid = $('#party_id_cr').val();
		party2Pledger.description = $('#txtRemarks').val();
		party2Pledger.debit = (_etype === 'pd_receive') ? $('#txtAmount').val() : 0;
		party2Pledger.credit = (_etype === 'pd_issue') ? $('#txtAmount').val() : 0;
		party2Pledger.etype = _etype;
		party2Pledger.pid_key = $('#party_dropdown').val();
		party2Pledger.uid = $('#uid').val();
		party2Pledger.company_id = $('#cid').val();
		pledger.push(party2Pledger);
		
		$.ajax({

			url: base_url + 'index.php/payment/savePostPdCheque',
			type: 'POST',
			data: { pd_cheque : pd_cheque, pledger : pledger , voucher_type_hidden: $('#voucher_type_hidden').val()},

			success : function(data){ 
				// alert('Cheque saved successfully.');
				// general.reloadWindow();
				var printConfirmation = confirm('Voucher saved!\nWould you like to print the invoice as well?');
				if (printConfirmation === true) {
					Print_Voucher();
				} else {
					general.reloadWindow();
				}
			},
			error : function (error){
				 alert("Error : " + error);
			}
		});

	}

	var saveUnpost = function ( pd_cheque ) {
		// alert('pdcheque ' + pd_cheque.company_id);
		$.ajax({
			url: base_url + 'index.php/payment/saveUnpostPdCheque',
			type: 'POST',
			data: pd_cheque,			
			success : function(data){
				var printConfirmation = confirm('Voucher saved!\nWould you like to print the invoice as well?');
				if (printConfirmation === true) {
					Print_Voucher();
				} else {
					general.reloadWindow();
				}
				// alert('Cheque saved successfully.');
				// general.reloadWindow();
			},
			error : function (error){
				 alert("Error : " + error);
			}
		});
	}

	var saveAccount = function( accountObj ) {
		$.ajax({
			url : base_url + 'index.php/account/save',
			type : 'POST',
			data : { 'accountDetail' : accountObj },
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'false') {
					alert('An internal error occured while saving account. Please try again.');
				} else {
					alert('Account saved successfully.');
					$('#AccountAddModel').modal('hide');
					fetchAccount();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	
	var getSaveObjectAccount = function() {

		var obj = {
			pid : '20000',
			active : '1',
			name : $.trim($('#txtAccountName').val()),
			level3 : $.trim($('#txtLevel3').val()),
			dcno : $('#txtId').val(),
			etype : ($('#pd_receive').is(':checked') === true) ? 'pd_receive' : 'pd_issue',
			uid : $.trim($('#uid').val()),
			company_id : $.trim($('#cid').val()),
		};

		return obj;
	}

var validateSaveAccount = function() {

		var errorFlag = false;
		var partyEl = $('#txtAccountName');
		var deptEl = $('#txtLevel3');

		// remove the error class first
		$('.inputerror').removeClass('inputerror');

		if ( !partyEl.val() ) {
			$('#txtAccountName').addClass('inputerror');
			errorFlag = true;
		}
		if ( !deptEl.val() ) {
			deptEl.addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var fetchAccount = function() {

		$.ajax({
			url : base_url + 'index.php/account/fetchAll',
			type : 'POST',
			data : { 'active' : 1 },
			dataType : 'JSON',
			success : function(data) {
				if (data === 'false') {
					alert('No data found');
				} else {
					populateDataAccount(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	var populateDataAccount = function(data) {
		$("#party_dropdown").empty();
		$("#party_id_cr").empty();

		$.each(data, function(index, elem){
			var opt="<option value='"+elem.pid+"' >" +  elem.name + "</option>";
			$(opt).appendTo('#party_id_cr');
			
			// var opt1="<option value='"+elem.pid+"' >" +  elem.pid + "</option>";
			$(opt).appendTo('#party_dropdown');
		});
	}
	
	// checks for the empty fields
	var validateSave = function() {

		var partyIdEl = $('#party_dropdown');
		var amtEl = $('#txtAmount');
		var bankNameEl = $('#tobank_dropdown');
		var depositEl = $('#party_id_cr');

		var errorFlag = false;

		if ( !partyIdEl.val() ) {
			partyIdEl.addClass('inputerror');
			errorFlag = true;
		}

		if ( !amtEl.val() ) {
			amtEl.addClass('inputerror');
			errorFlag = true;
		}

		// if ( !bankNameEl.val() ) {
		// 	bankNameEl.addClass('inputerror');
		// 	errorFlag = true;
		// }

		if ( !depositEl.val() ) {
			depositEl.addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var deleteVoucher = function(dcno , etype) {

		$.ajax({
			url : base_url + 'index.php/payment/removeChequeVoucher',
			type : 'POST',
			data 	: 	{ 'dcno' : dcno, 'etype' : etype ,'company_id':$('#cid').val() },
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No data found');
				} else {
					alert('Voucher deleted successfully');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var fetchAllCheques = function (startDate, endDate, type) {

        if (typeof dTable != 'undefined') {
            dTable.fnDestroy();
            $('#VoucherRows').empty();
        }		

		$.ajax({
                url: base_url + "index.php/report/getAllCheques",                
                data: { from : startDate, to : endDate, type : type },
                type: 'POST',
                dataType: 'JSON',
                beforeSend: function () {
                    console.log(this.data);
                 },
                complete: function () { },
                success: function (data) {

                    if (data.length !== 0) {

                        var htmls = '';
                        var grandTotal = 0;

                        var handler = $('#cheque-template').html();
                        var template = Handlebars.compile(handler);

                        $(data).each(function(index, elem){

                            elem.VRDATE = ( elem.VRDATE ) ? elem.VRDATE.substr(0,10) : 'Not Available';
                            elem.CHEQUE_DATE = ( elem.CHEQUE_DATE ) ? elem.CHEQUE_DATE.substr(0,10) : 'Not Available';

                            grandTotal += parseFloat(elem.AMOUNT) ? parseFloat(elem.AMOUNT) : 0;

                            var html = template(elem);
                            htmls += html;

                        });

                        $('.grand-total').html( grandTotal );
                        $('#VoucherRows').append(htmls);
					}

                    bindGrid();
                },

                error: function (result) {
                    alert("Error:" + result);
                }
            });		

	}

	var bindGrid = function() {
        var dontSort = [];
        $('#datatable_Cheques thead th').each(function () {
            if ($(this).hasClass('no_sort')) {
                dontSort.push({ "bSortable": false });
            } else {
                dontSort.push(null);
            }
        });
        dTable = $('#datatable_Cheques').dataTable({
            // Uncomment, if prolems with datatable.
            // "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
            "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone'<'row-fluid'<'span8' f>>>'<'pag_top' p> T>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
            "aaSorting": [[0, "asc"]],
            "bPaginate": true,
            "sPaginationType": "full_numbers",
            "bJQueryUI": false,
            "aoColumns": dontSort,
			"bSort": false,
			"iDisplayLength" : 100,
            "oTableTools": {
                "sSwfPath": "js/copy_cvs_xls_pdf.swf",
                "aButtons": [{ "sExtends": "print", "sButtonText": "Print Report", "sMessage" : "Inventory Report" }]
            }
        });
        $.extend($.fn.dataTableExt.oStdClasses, {
            "s`": "dataTables_wrapper form-inline"
        });
    }

	return {

		init : function() {
			this.bindUI();
			$('#party_dropdown').select2('open');
			$('#voucher_type_hidden').val('new');
			chequeissuevoucher.bindModalPartyGrid();
			chequeissuevoucher.fetchRequestedVr();
		},

		

		bindUI : function() {
			var self = this;

			$('#txtLevel3').on('change', function() {
				
				var level3 = $('#txtLevel3').val();
				$('#txtselectedLevel1').text('');
				$('#txtselectedLevel2').text('');
				if (level3 !== "" && level3 !== null) {
					// alert('enter' + $(this).find('option:selected').data('level2') );	
					$('#txtselectedLevel2').text(' ' + $(this).find('option:selected').data('level2'));
					$('#txtselectedLevel1').text(' ' + $(this).find('option:selected').data('level1'));
				}
			});
			// $('#txtLevel3').select2();
			$('.btnSaveM').on('click',function(e){
				if ( $('.btnSave').data('saveaccountbtn')==0 ){
					alert('Sorry! you have not save accounts rights..........');
				}else{
					e.preventDefault();
					self.initSaveAccount();
				}
			});
			$('.btnResetM').on('click',function(){
				
				$('#txtAccountName').val('');
				$('#txtselectedLevel2').text('');
				$('#txtselectedLevel1').text('');
				$('#txtLevel3').select2('val','');
			});
			$('#AccountAddModel').on('shown.bs.modal',function(e){
				$('#txtAccountName').focus();
			});
			shortcut.add("F3", function() {
    			$('#AccountAddModel').modal('show');
			});

			$('.modal-lookup .populateAccount').on('click', function(){
				// alert('dfsfsdf');
				var party_id = $(this).closest('tr').find('input[name=hfModalPartyId]').val();
				$("#party_dropdown").select2("val", party_id); //set the value
			});
			shortcut.add("F10", function() {
    			self.initSave();
			});
			shortcut.add("F1", function() {
				$('a[href="#party-lookup"]').trigger('click');
			});
			shortcut.add("F9", function() {
				Print_Voucher();
			});
			shortcut.add("F6", function() {
    			$('#txtId').focus();
    			// alert('focus');
			});
			shortcut.add("F5", function() {
    			self.resetVoucher();
			});

			shortcut.add("F12", function() {
    			$('.btnDelete').trigger('click');
			});
			$('.btnSave').on('click',  function(e) {
				if ($('#voucher_type_hidden').val()=='edit' && $('.btnSave').data('updatebtn')==0 ){
					alert('Sorry! you have not update rights..........');
				}else if($('#voucher_type_hidden').val()=='new' && $('.btnSave').data('insertbtn')==0){
					alert('Sorry! you have not insert rights..........');
				}else{
					e.preventDefault();
					self.initSave();
				}
			});

			$('.btnPrint').on('click',  function(e) {
				e.preventDefault();
				Print_Voucher();
			});


			$('.btnReset').on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});

			$('.btnDelete').on('click', function(e){
				if ($('#voucher_type_hidden').val()=='edit' && $('.btnSave').data('deletebtn')==0 ){
					alert('Sorry! you have not delete rights..........');
				}else{
					e.preventDefault();
					var dcno = $('#txtId').val();
					var etype =  ($('#pd_receive').is(':checked') === true) ? 'pd_receive' : 'pd_issue';
					if (dcno !== '') {
						if (confirm('Are you sure to delete this voucher?'))
						deleteVoucher(dcno , etype);
					}
				}
			});

			$('#txtId').on('change', function(e) {
				// get the based on the id entered by the user
				
				if ( $('#txtId').val().trim() !== "" ) {
					e.preventDefault();
					var dcno = $.trim($('#txtId').val());
					var etype =  ($('#pd_receive').is(':checked') === true) ? 'pd_receive' : 'pd_issue';
					fetch(dcno , etype);
				}
			});

			$('#txtId').on('keypress', function(e) {

				if (e.keyCode === 13) {
					var dcno = $('#txtId').val();
					if (dcno !== '') {
						var etype =  ($('#pd_receive').is(':checked') === true) ? 'pd_receive' : 'pd_issue';
						fetch(dcno , etype);
					}
				}
			});

			$('#pd_receive').on('click', function() {
				self.resetVoucher();
				var check = $(this).is(':checked');
				if (check) {
					getMaxId('pd_receive');
					$('#to').text('Received To');
					$('#from').text('Received From');
				}
			});
			
			$('#pd_issue').on('click', function() {
				self.resetVoucher();
				var check = $(this).is(':checked');
				if (check) {
					getMaxId('pd_issue');
					$('#to').text('Issued To');
					$('#from').text('Issued From');
				}
			});

			// $('#tobank_dropdown').select2();
			$('#status_dropdown').select2();
			$('body').on('click','#datatable_Cheques tbody #btn-edit-sale',function(e){
				e.preventDefault();
				// $('.bs-example-modal-lg').modal('show'); 
				var td 						= $(this).closest('tr').find('td');
				var dcno 					= $.trim(td.eq(0).text());
				$('#txtId').val(dcno);
				$('#txtIdHidden').val(dcno);
				$("a[href='#addupdateCash']").trigger('click');
				fetch(dcno);
			});
			
			$('.btnSearch').on('click', function(e) {
				e.preventDefault();

				var from = $('#from_date').val();
				var to = $('#to_date').val();
				var etype = ($('#pd_receive').is(':checked') === true) ? 'pd_receive' : 'pd_issue';

				fetchAllCheques(from, to, etype );
			});


		},
		initSaveAccount : function() {

			var saveObjAccount = getSaveObjectAccount();
			var error = validateSaveAccount();

			if (!error) {
					saveAccount(saveObjAccount);
			} else {
				alert('Correct the errors...');
			}
		},
		fetchRequestedVr : function () {
			var vrnoa = general.getQueryStringVal('vrnoa');
			var etype = general.getQueryStringVal('etype');
			
			vrnoa = parseInt( vrnoa );

			if ( !isNaN(vrnoa) && etype !== '' ) {
				if (etype=='pd_receive'){
					$('#pd_receive').prop('checked', true);	
				}else{
					$('#pd_issue').prop('checked', true);	
				}
				fetch(vrnoa, etype);
			}else{
				getMaxId(($('#pd_issue').is(':checked') === true) ? 'pd_issue' : 'pd_receive');
			}
		},

		// prepares the data to save it into the database
		initSave : function() {

			var error = validateSave();
			
			if (!error) {
				getSaveObject();
			} else {
				alert('Correct the errors...');
			}
		},
bindModalPartyGrid : function() {

			
				            var dontSort = [];
				            $('#party-lookup table thead th').each(function () {
				                if ($(this).hasClass('no_sort')) {
				                    dontSort.push({ "bSortable": false });
				                } else {
				                    dontSort.push(null);
				                }
				            });
				            chequeissuevoucher.pdTable = $('#party-lookup table').dataTable({
				                // "sDom": "<'row-fluid table_top_bar'<'span12'>'<'to_hide_phone'>'f'<'>r>t<'row-fluid'>",
				                "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
				                "aaSorting": [[0, "asc"]],
				                "bPaginate": true,
				                "sPaginationType": "full_numbers",
				                "bJQueryUI": false,
				                "aoColumns": dontSort

				            });
				            $.extend($.fn.dataTableExt.oStdClasses, {
				                "s`": "dataTables_wrapper form-inline"
				            });
},
		// instead of reseting values reload the page because its cruel to write to much code to simply do that
		resetVoucher : function() {

			$('#current_date').datepicker('update', new Date());
			$('#party_dropdown').select2('val', '');
			$('#tobank_dropdown').val('');
			$('#txtChequeNo').val('');
			$('#cheque_date').datepicker('update', new Date());
			$('#txtSlipNo').val('');
			$('#status_dropdown').select2('val','pending');
			$('#txtAmount').val('');
			$('#txtNote').val('');
			$('#txtRemarks').val('');
			$('#party_id_cr').select2('val', '');
			$('#mature_date').datepicker('update', new Date());
			$('input[name=post][value="unpost"]').prop('checked', 'true');
			$('#UserNameTextBox').val('');
			$('#user_dropdown').val('');
			$('#voucher_type_hidden').val('new');

			$('#datatable_Cheques').find('tbody tr').remove();
			// $('#party_dropdown').select2('open');
		}
	}

};

var chequeissuevoucher = new chequeissuevoucher();
chequeissuevoucher.init();