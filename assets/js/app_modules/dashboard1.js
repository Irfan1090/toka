/**
 * dashboard handler
 * @type {Object}
 */


var dashboard = {

	/**
	 * Colors for jqBarGraph
	 * @type {Array}
	 */
	colors: ['#3498db', '#16a085', '#e74c3c', '#d35400', '#2c3e50', '#c0392b', '#2ecc71', '#2980b9'],

	init : function (){

		dashboard.bindUI();
		if ($('#user_rights').val()==1){
			dashboard.populate();
		}
		dashboard.populate();


		$('.purchases-sum').text($('.purchases-sum-val').text());
		$('.sales-sum').text($('.sales-sum-val').text());
		$('.saleOrder-sum').text($('.saleOrder-sum-val').text());
		$('.sum_payments').text($('.sum_payments-val').text());
		$('.sum_receipts').text($('.sum_receipts-val').text());

		$('.pd_issue-sum').text($('.pd_issue-sum-val').text());
		$('.pd_receive-sum').text($('.pd_receive-sum-val').text());
		$('.bpv-sum').text($('.bpv-sum-val').text());
		$('.brv-sum').text($('.brv-sum-val').text());
		$('.expenses-sum').text($('.expenses-sum-val').text());
	},

	bindUI : function () {


		$('#drpCompanyId').on('change', function (){
		    $('.cid').val( $(this).val() );
		    dashboard.populate();
		});
		// alert('dashboard');
		$('#btnshow').on('click',function(){

			var from = $('#from_date').val();
			var to = $('#to_date').val();
			dashboard.append_newdash(from,to);
		});

		$('.barcode-value').on('keypress', function( e ){
			if ( e.keyCode == '13' ) {

				//if ( $.trim($(this).val()) !== '') {

					// 9-SALE QUOTATION
					var barcode = $(this).val();
					var parts = barcode.split('-');
					if ( !isNaN( parseInt(parts[0]) ) && (parts.length === 2) ) {

						var vrnoa = parts[0];
						var type = parts[1];

						if ($.trim(type.toLowerCase()) === 'sale quotation') {
							window.location.href = base_url + 'index.php/sale/order?vrnoa=' + vrnoa;
						} else if ($.trim(type.toLowerCase()) === 'sale') {
							window.location.href = base_url + 'index.php/sale?vrnoa=' + vrnoa;
						} else if ($.trim(type.toLowerCase()) === 'sale return') {
							window.location.href = base_url + 'index.php/salereturn?vrnoa=' + vrnoa;
						} else if ($.trim(type.toLowerCase()) === 'purchase') {
							window.location.href = base_url + 'index.php/purchase?vrnoa=' + vrnoa;
						} else if ($.trim(type.toLowerCase()) === 'purchase import') {
							window.location.href = base_url + 'index.php/purchase/import?vrnoa=' + vrnoa;
						} else if ($.trim(type.toLowerCase()) === 'purchase quotation') {
							window.location.href = base_url + 'index.php/purchase/order?vrnoa=' + vrnoa;
						} else if ($.trim(type.toLowerCase()) === 'purchase return') {
							window.location.href = base_url + 'index.php/purchasereturn?vrnoa=' + vrnoa;
						} else if ($.trim(type.toLowerCase()) === 'navigation') {
							window.location.href = base_url + 'index.php/stocknavigation?vrnoa=' + vrnoa;
						} else if ($.trim(type.toLowerCase()) === 'cash payment') {
							window.location.href = base_url + 'index.php/payment?vrnoa=' + vrnoa + '&etype=cpv';
						} else if ($.trim(type.toLowerCase()) === 'cash receipt') {
							window.location.href = base_url + 'index.php/payment?vrnoa=' + vrnoa + '&etype=crv';
						} else if ($.trim(type.toLowerCase()) === 'journal') {
							window.location.href = base_url + 'index.php/journal?vrnoa=' + vrnoa;
						} else if ($.trim(type.toLowerCase()) === 'assembling') {
							window.location.href = base_url + 'index.php/item/assdeass?vrnoa=' + vrnoa;
						} else {
							//alert("Invalid barcode");
						}

					} else {
						//alert('Error! Invalid barcode entered.');
					}
				//} else {

					// alert('Please enter a barcode first');

				// }
			}
		});

		$("#sales-period").on('change', function () {
			dashboard.fetchStatData($(this).find('option:selected').val(), 'sale', 'purchase');
			
		});

		$("#purchases-period").on('change', function () {
			dashboard.fetchStatData($(this).find('option:selected').val(), 'purchase', 'purchase');
			// alert('purchase call');
		});

		$("#crvs-period").on('change', function () {
			dashboard.fetchStatData($(this).find('option:selected').val(), 'crv', 'payment');
		});

		$("#cpvs-period").on('change', function () {
			dashboard.fetchStatData($(this).find('option:selected').val(), 'cpv', 'payment');
		});

		$('#sales-view').on('change', function (index, elem) {
			if ( $(this).find('option:selected').val() === 'chart' ) {
				var period = $("#sales-period option:selected").val();
				dashboard.showGraph('sale', period);
			} else {
				var period = $("#sales-period option:selected").val();
				dashboard.fetchStatData(period, 'sale', 'sale');
			}
		});

		$('#purchases-view').on('change', function (index, elem) {
			if ( $(this).find('option:selected').val() === 'chart' ) {
				var period = $("#purchases-period option:selected").val();
				dashboard.showGraph('purchase', period);
			} else {
				var period = $("#purchases-period option:selected").val();
				dashboard.fetchStatData(period, 'purchase', 'purchase');
			}
		});

		$('#btnGraph1').on('click',function(){
			$('.purchaseorder_tbl').hide();
			$('.tabs_purchase').show();	
		});
		$('#btnTable1').on('click',function(){
			$('.tabs_purchase').hide();
			$('.purchaseorder_tbl').show();
		});
		$('#btnGraph2').on('click',function(){
			$('.purchasetrans_tbl').hide();
			$('.tabs_purchaseret').show();	
		});
		$('#btnTable2').on('click',function(){
			$('.tabs_purchaseret').hide();
			$('.purchasetrans_tbl').show();
		});
		$('#btnGraph3').on('click',function(){
			$('.purchasereturntrans_tbl').hide();
			$('.tabs_purchasereturn').show();	
		});
		$('#btnTable3').on('click',function(){
			$('.tabs_purchasereturn').hide();
			$('.purchasereturntrans_tbl').show();
		});
		$('#btnGraph4').on('click',function(){
			$('.openingstocktrans_tbl').hide();
			$('.tabs_openeingstock').show();	
		});
		$('#btnTable4').on('click',function(){
			$('.tabs_openeingstock').hide();
			$('.openingstocktrans_tbl').show();
		});
		$('#btnGraph5').on('click',function(){
			$('.quotationtrans_tbl').hide();
			$('.tabs_quotation').show();	
		});
		$('#btnTable5').on('click',function(){
			$('.tabs_quotation').hide();
			$('.quotationtrans_tbl').show();
		});
		$('#btnGraph6').on('click',function(){
			$('.saleordertrans_tbl').hide();
			$('.tabs_saleorder').show();	
		});
		$('#btnTable6').on('click',function(){
			$('.tabs_saleorder').hide();
			$('.saleordertrans_tbl').show();
		});
		$('#btnGraph7').on('click',function(){
			$('.ogptrans_tbl').hide();
			$('.tabs_ogp').show();	
		});
		$('#btnTable7').on('click',function(){
			$('.tabs_ogp').hide();
			$('.ogptrans_tbl').show();
		});
		$('#btnGraph8').on('click',function(){
			$('.saletrans_tbl').hide();
			$('.tabs_sales').show();	
		});
		$('#btnTable8').on('click',function(){
			$('.tabs_sales').hide();
			$('.saletrans_tbl').show();
		});
		$('#btnGraph9').on('click',function(){
			$('.salereturn_tbl').hide();
			$('.tabs_salesreturn').show();	
		});
		$('#btnTable9').on('click',function(){
			$('.tabs_salesreturn').hide();
			$('.salereturn_tbl').show();
		});
		$('#btnGraph10').on('click',function(){
			$('.requisition_tbl').hide();
			$('.tabs_requsition').show();	
		});
		$('#btnTable10').on('click',function(){
			$('.tabs_requsition').hide();
			$('.requisition_tbl').show();
		});
		$('#btnGraph11').on('click',function(){
			$('.igp_tbl').hide();
			$('.tabs_inward').show();	
		});
		$('#btnTable11').on('click',function(){
			$('.tabs_inward').hide();
			$('.igp_tbl').show();
		});
		$('#btnGraph12').on('click',function(){
			$('.consumption_tbl').hide();
			$('.tabs_consumption').show();	
		});
		$('#btnTable12').on('click',function(){
			$('.tabs_consumption').hide();
			$('.consumption_tbl').show();
		});
		$('#btnGraph13').on('click',function(){
			$('.materialreturn_tbl').hide();
			$('.tabs_material').show();	
		});
		$('#btnTable13').on('click',function(){
			$('.tabs_material').hide();
			$('.materialreturn_tbl').show();
		});
		$('#btnGraph14').on('click',function(){
			$('.itemconversion_tbl').hide();
			$('.tabs_itemcon').show();	
		});
		$('#btnTable14').on('click',function(){
			$('.tabs_itemcon').hide();
			$('.itemconversion_tbl').show();
		});
		$('#btnGraph15').on('click',function(){
			$('.production_tbl').hide();
			$('.tabs_product').show();	
		});
		$('#btnTable15').on('click',function(){
			$('.tabs_product').hide();
			$('.production_tbl').show();
		});
		$('#btnGraph16').on('click',function(){
			$('.payment_tbl').hide();
			$('.tabs_payment').show();	
		});
		$('#btnTable16').on('click',function(){
			$('.tabs_payment').hide();
			$('.payment_tbl').show();
		});
		$('#btnGraph17').on('click',function(){
			$('.receipt_tbl').hide();
			$('.tabs_receipt').show();	
		});
		$('#btnTable17').on('click',function(){
			$('.tabs_receipt').hide();
			$('.receipt_tbl').show();
		});
		$('#btnGraph18').on('click',function(){
			$('.chequeissue_tbl').hide();
			$('.tabs_chqisue').show();	
		});
		$('#btnTable18').on('click',function(){
			$('.tabs_chqisue').hide();
			$('.chequeissue_tbl').show();
		});
		$('#btnGraph19').on('click',function(){
			$('.chequereceipt_tbl').hide();
			$('.tabs_chqrecip').show();	
		});
		$('#btnTable19').on('click',function(){
			$('.tabs_chqrecip').hide();
			$('.chequereceipt_tbl').show();
		});
		$('#btnGraph20').on('click',function(){
			$('.jv_tbl').hide();
			$('.tabs_jv').show();	
		});
		$('#btnTable20').on('click',function(){
			$('.tabs_jv').hide();
			$('.jv_tbl').show();
		});
		$('#btnGraph21').on('click',function(){
			$('.opening_tbl').hide();
			$('.tabs_opbalan').show();	
		});
		$('#btnTable21').on('click',function(){
			$('.tabs_opbalan').hide();
			$('.opening_tbl').show();
		});
		$('#btnGraph22').on('click',function(){
			$('.penalty_tbl').hide();
			$('.tabs_penalty').show();	
		});
		$('#btnTable22').on('click',function(){
			$('.tabs_penalty').hide();
			$('.penalty_tbl').show();
		});
		$('#btnGraph23').on('click',function(){
			$('.loan_tbl').hide();
			$('.tabs_loan').show();	
		});
		$('#btnTable23').on('click',function(){
			$('.tabs_loan').hide();
			$('.loan_tbl').show();
		});
		$('#btnGraph24').on('click',function(){
			$('.advance_tbl').hide();
			$('.tabs_advance').show();	
		});
		$('#btnTable24').on('click',function(){
			$('.tabs_advance').hide();
			$('.advance_tbl').show();
		});
		$('#btnGraph25').on('click',function(){
			$('.incentive_tbl').hide();
			$('.tabs_incentive').show();	
		});
		$('#btnTable25').on('click',function(){
			$('.tabs_incentive').hide();
			$('.incentive_tbl').show();
		});

		$('#cpvs-view').on('change', function (index, elem) {
			if ( $(this).find('option:selected').val() === 'chart' ) {
				var period = $("#cpvs-period option:selected").val();
				dashboard.showGraph('cpv', period);
			} else {
				var period = $("#cpvs-period option:selected").val();
				dashboard.fetchStatData(period, 'cpv', 'payment');
			}
		});

		$('#crvs-view').on('change', function (index, elem) {
			if ( $(this).find('option:selected').val() === 'chart' ) {
				var period = $("#crvs-period option:selected").val();
				dashboard.showGraph('crv', period);
			} else {
				var period = $("#crvs-period option:selected").val();
				dashboard.fetchStatData(period, 'crv', 'payment');
			}
		});

	},

	populate : function () {

		// dashboard.showNetSales();
		// dashboard.showNetCash();
		// dashboard.showNetExpense();
		// dashboard.showNetPurchases();
		// dashboard.showNetPReturns();
		// dashboard.showNetSReturns();
		// dashboard.showNetCheques('pd_issue');
		// dashboard.showNetCheques('pd_receive');

		// dashboard.formatCurrency();

		// dashboard.fetchStatData('DAILY', 'sale', 'purchase');
		// dashboard.fetchStatData('DAILY', 'purchase', 'purchase');
		// dashboard.fetchStatData('DAILY', 'crv', 'payment');
		// dashboard.fetchStatData('DAILY', 'cpv', 'payment');

		// dashboard.fetchTopTenDaysCheques('pd_issue');
		// dashboard.fetchTopTenDaysCheques('pd_receive');

		$('.nav .dashboard').addClass('active');
	},
	populatetransactions : function (data) {
		/////////////////// purchase module
		//purchase order
		$.each(data.purchaseorder, function(index, elem) {
			var hreflink = '';
			hreflink = dashboard.makeHref(elem.etype,elem.vrnoa);
			elem.vrnoa = elem.vrnoa + '-' + dashboard.abbrivateEtypes(elem.etype);
			dashboard.appendToTabletransactions(elem.vrnoa, elem.date_time, elem.user_name, elem.party_name,elem.discount,elem.tax, elem.namount, 'purchaseorder_tbody',hreflink);
		});
		//purchase
		$.each(data.purchase, function(index, elem) {
			var hreflink = '';
			hreflink = dashboard.makeHref(elem.etype,elem.vrnoa);
			elem.vrnoa = elem.vrnoa + '-' + dashboard.abbrivateEtypes(elem.etype);
			dashboard.appendToTabletransactions(elem.vrnoa, elem.date_time, elem.user_name, elem.party_name,elem.discount,elem.tax, elem.namount, 'purchase_tbody',hreflink);
		});
		//purchase return
		$.each(data.purchasereturn, function(index, elem) {
			var hreflink = '';
			hreflink = dashboard.makeHref(elem.etype,elem.vrnoa);
			elem.vrnoa = elem.vrnoa + '-' + dashboard.abbrivateEtypes(elem.etype);
			dashboard.appendToTabletransactions(elem.vrnoa, elem.date_time, elem.user_name, elem.party_name,elem.discount,elem.tax, elem.namount, 'purchasereturn_tbody',hreflink);
		});
		//opening stock
		$.each(data.openingstock, function(index, elem) {
			var hreflink = '';
			hreflink = dashboard.makeHref(elem.etype,elem.vrnoa);
			elem.vrnoa = elem.vrnoa + '-' + dashboard.abbrivateEtypes(elem.etype);
			dashboard.appendToTabletransactions(elem.vrnoa, elem.date_time, elem.user_name, elem.party_name,elem.discount,elem.tax, elem.namount, 'openingstock_tbody',hreflink);
		});

		/////////////////// Sale module
		//quotation order
		$.each(data.quotationorder, function(index, elem) {
			var hreflink = '';
			hreflink = dashboard.makeHref(elem.etype,elem.vrnoa);
			elem.vrnoa = elem.vrnoa + '-' + dashboard.abbrivateEtypes(elem.etype);
			dashboard.appendToTabletransactions(elem.vrnoa, elem.date_time, elem.user_name, elem.party_name,elem.discount,elem.tax, elem.namount, 'quotation_tbody',hreflink);
		});

		//sale order
		$.each(data.saleorder, function(index, elem) {
			var hreflink = '';
			hreflink = dashboard.makeHref(elem.etype,elem.vrnoa);
			elem.vrnoa = elem.vrnoa + '-' + dashboard.abbrivateEtypes(elem.etype);
			dashboard.appendToTabletransactions(elem.vrnoa, elem.date_time, elem.user_name, elem.party_name,elem.discount,elem.tax, elem.namount, 'saleorder_tbody',hreflink);
		});

		//outwardvoucher
		$.each(data.outwardvoucher, function(index, elem) {
			var hreflink = '';
			hreflink = dashboard.makeHref(elem.etype,elem.vrnoa);
			elem.vrnoa = elem.vrnoa + '-' + dashboard.abbrivateEtypes(elem.etype);
			dashboard.appendToTabletransactions(elem.vrnoa, elem.date_time, elem.user_name, elem.party_name,elem.discount,elem.tax, elem.namount, 'ogp_tbody',hreflink);
		});

		//sale
		$.each(data.sale, function(index, elem) {
			var hreflink = '';
			hreflink = dashboard.makeHref(elem.etype,elem.vrnoa);
			elem.vrnoa = elem.vrnoa + '-' + dashboard.abbrivateEtypes(elem.etype);
			dashboard.appendToTabletransactions(elem.vrnoa, elem.date_time, elem.user_name, elem.party_name,elem.discount,elem.tax, elem.namount, 'sale_tbody',hreflink);
		});

		//sale return
		$.each(data.salereturn, function(index, elem) {
			var hreflink = '';
			hreflink = dashboard.makeHref(elem.etype,elem.vrnoa);
			elem.vrnoa = elem.vrnoa + '-' + dashboard.abbrivateEtypes(elem.etype);
			dashboard.appendToTabletransactions(elem.vrnoa, elem.date_time, elem.user_name, elem.party_name,elem.discount,elem.tax, elem.namount, 'salereturn_tbody',hreflink);
		});

		/////////////////// Inventory module
		//requisition
		$.each(data.requisition, function(index, elem) {
			var hreflink = '';
			hreflink = dashboard.makeHref(elem.etype,elem.vrnoa);
			elem.vrnoa = elem.vrnoa + '-' + dashboard.abbrivateEtypes(elem.etype);
			dashboard.appendToTabletransactions(elem.vrnoa, elem.date_time, elem.user_name, elem.party_name,elem.discount,elem.tax, elem.namount, 'requisition_tbody',hreflink);
		});

		//inwardvoucher
		$.each(data.inwardvoucher, function(index, elem) {
			var hreflink = '';
			hreflink = dashboard.makeHref(elem.etype,elem.vrnoa);
			elem.vrnoa = elem.vrnoa + '-' + dashboard.abbrivateEtypes(elem.etype);
			dashboard.appendToTabletransactions(elem.vrnoa, elem.date_time, elem.user_name, elem.party_name,elem.discount,elem.tax, elem.namount, 'igp_tbody',hreflink);
		});

		//consumption
		$.each(data.consumption, function(index, elem) {
			var hreflink = '';
			hreflink = dashboard.makeHref(elem.etype,elem.vrnoa);
			elem.vrnoa = elem.vrnoa + '-' + dashboard.abbrivateEtypes(elem.etype);
			dashboard.appendToTabletransactions(elem.vrnoa, elem.date_time, elem.user_name, elem.party_name,elem.discount,elem.tax, elem.namount, 'consumption_tbody',hreflink);
		});

		//materialreturn
		$.each(data.materialreturn, function(index, elem) {
			var hreflink = '';
			hreflink = dashboard.makeHref(elem.etype,elem.vrnoa);
			elem.vrnoa = elem.vrnoa + '-' + dashboard.abbrivateEtypes(elem.etype);
			dashboard.appendToTabletransactions(elem.vrnoa, elem.date_time, elem.user_name, elem.party_name,elem.discount,elem.tax, elem.namount, 'materialreturn_tbody',hreflink);
		});

		//item_conversion
		$.each(data.item_conversion, function(index, elem) {
			var hreflink = '';
			hreflink = dashboard.makeHref(elem.etype,elem.vrnoa);
			elem.vrnoa = elem.vrnoa + '-' + dashboard.abbrivateEtypes(elem.etype);
			dashboard.appendToTabletransactions(elem.vrnoa, elem.date_time, elem.user_name, elem.party_name,elem.discount,elem.tax, elem.namount, 'itemconversion_tbody',hreflink);
		});

		/////////////////// Production module
		//production
		$.each(data.production, function(index, elem) {
			var hreflink = '';
			hreflink = dashboard.makeHref(elem.etype,elem.vrnoa);
			elem.vrnoa = elem.vrnoa + '-' + dashboard.abbrivateEtypes(elem.etype);
			dashboard.appendToTabletransactions(elem.vrnoa, elem.date_time, elem.user_name, elem.party_name,elem.discount,elem.tax, elem.namount, 'production_tbody',hreflink);
		});

		/////////////////// Accounts module
		//payments
		$.each(data.payments, function(index, elem) {
			var hreflink = '';
			hreflink = dashboard.makeHref(elem.etype,elem.vrnoa);
			elem.vrnoa = elem.vrnoa + '-' + dashboard.abbrivateEtypes(elem.etype);
			dashboard.appendToTabletransactions(elem.vrnoa, elem.date_time, elem.user_name, elem.party_name,elem.invoice,elem.debit, elem.credit, 'payment_tbody',hreflink);
		});

		//receipts
		$.each(data.receipts, function(index, elem) {
			var hreflink = '';
			hreflink = dashboard.makeHref(elem.etype,elem.vrnoa);
			elem.vrnoa = elem.vrnoa + '-' + dashboard.abbrivateEtypes(elem.etype);
			dashboard.appendToTabletransactions(elem.vrnoa, elem.date_time, elem.user_name, elem.party_name,elem.invoice,elem.debit, elem.credit, 'receipt_tbody',hreflink);
		});

		//chequeissue
		$.each(data.chequeissue, function(index, elem) {
			var hreflink = '';
			hreflink = dashboard.makeHref(elem.etype,elem.vrnoa);
			elem.vrnoa = elem.vrnoa + '-' + dashboard.abbrivateEtypes(elem.etype);
			dashboard.appendToTabletransactions(elem.vrnoa, elem.date_time, elem.user_name, elem.party_name,elem.invoice,elem.debit, elem.credit, 'chequeissue_tbody',hreflink);
		});

		//chequereceipt
		$.each(data.chequereceipt, function(index, elem) {
			var hreflink = '';
			hreflink = dashboard.makeHref(elem.etype,elem.vrnoa);
			elem.vrnoa = elem.vrnoa + '-' + dashboard.abbrivateEtypes(elem.etype);
			dashboard.appendToTabletransactions(elem.vrnoa, elem.date_time, elem.user_name, elem.party_name,elem.invoice,elem.debit, elem.credit, 'chequereceipt_tbody',hreflink);
		});

		//jv
		$.each(data.jv, function(index, elem) {
			var hreflink = '';
			hreflink = dashboard.makeHref(elem.etype,elem.vrnoa);
			elem.vrnoa = elem.vrnoa + '-' + dashboard.abbrivateEtypes(elem.etype);
			dashboard.appendToTabletransactions(elem.vrnoa, elem.date_time, elem.user_name, elem.party_name,elem.invoice,elem.debit, elem.credit, 'jv_tbody',hreflink);
		});

		//openingbalance
		$.each(data.openingbalance, function(index, elem) {
			var hreflink = '';
			hreflink = dashboard.makeHref(elem.etype,elem.vrnoa);
			elem.vrnoa = elem.vrnoa + '-' + dashboard.abbrivateEtypes(elem.etype);
			dashboard.appendToTabletransactions(elem.vrnoa, elem.date_time, elem.user_name, elem.party_name,elem.invoice,elem.debit, elem.credit, 'opening_tbody',hreflink);
		});

		/////////////////// Payroll module
		//penalty
		$.each(data.penalty, function(index, elem) {
			var hreflink = '';
			hreflink = dashboard.makeHref(elem.etype,elem.vrnoa);
			elem.vrnoa = elem.vrnoa + '-' + dashboard.abbrivateEtypes(elem.etype);
			dashboard.appendToTabletransactions(elem.vrnoa, elem.date_time, elem.user_name, elem.party_name,'-','-',elem.debit, 'penalty_tbody',hreflink);
		});

		//advance
		$.each(data.advance, function(index, elem) {
			var hreflink = '';
			hreflink = dashboard.makeHref(elem.etype,elem.vrnoa);
			elem.vrnoa = elem.vrnoa + '-' + dashboard.abbrivateEtypes(elem.etype);
			dashboard.appendToTabletransactions(elem.vrnoa, elem.date_time, elem.user_name, elem.party_name,'-','-',elem.debit, 'advance_tbody',hreflink);
		});

		//loan
		$.each(data.loan, function(index, elem) {
			var hreflink = '';
			hreflink = dashboard.makeHref(elem.etype,elem.vrnoa);
			elem.vrnoa = elem.vrnoa + '-' + dashboard.abbrivateEtypes(elem.etype);
			dashboard.appendToTabletransactions(elem.vrnoa, elem.date_time, elem.user_name, elem.party_name,'-','-',elem.debit,'loan_tbody',hreflink);
		});

		//incentive
		$.each(data.incentive, function(index, elem) {
			var hreflink = '';
			hreflink = dashboard.makeHref(elem.etype,elem.vrnoa);
			elem.vrnoa = elem.vrnoa + '-' + dashboard.abbrivateEtypes(elem.etype);
			dashboard.appendToTabletransactions(elem.vrnoa, elem.date_time, elem.user_name, elem.party_name,'-','-', elem.debit, 'incentive_tbody',hreflink);
		});
	},
	appendToTabletransactions : function(vrnoa, vrdate, username, partyname,discount, tax, amount, tbl_name,hreflink) {
	var row = 	"<tr>" +
					"<td class='vrnoa numeric text-left' data-title='VR#' ><a href="+hreflink+">"+ vrnoa +"</a></td>" +
			 		"<td class='vrdate' data-title='Date'> "+ vrdate +"</td>" +
			 		"<td class='username' data-title='User Name'> "+ username +"</td>" +
			 		"<td class='partyname' data-title='Party Name'> "+ partyname +"</td>" +
			 		"<td class='discount numeric text-right' data-title='Discount'>  "+ discount +"</td>" +
			 		"<td class='tax numeric text-right' data-title='Tax'>  "+ tax +"</td>" +
				 	"<td class='amount numeric text-right' data-title='Amount' > "+ amount + " </td>" +
			 	"</tr>";
	$(row).appendTo('.'+tbl_name);
	},
  	abbrivateEtypes : function( etype ){

	    if( etype == 'pur_order' ){
	      etype = 'PO';
	    }else if ( etype == 'purchase' ){
	      etype = 'PI';
	    }else if ( etype == 'purchasereturn' ){
	      etype = 'PR';
	    }else if ( etype == 'quotationorder' ){
	      etype = 'QV';
	    }else if ( etype == 'saleorder' ){
	      etype = 'SO';
	    }else if ( etype == 'outwardvoucher' ){
	      etype = 'OGP';
	    }else if ( etype == 'sale' ){
	      etype = 'SI';
	    }else if ( etype == 'salereturn' ){
	      etype = 'SR';
	    }else if ( etype == 'outwardvoucher' ){
	      etype = 'OGP';
	    }else if ( etype == 'requisition' ){
	      etype = 'RV';
	    }else if ( etype == 'inwardvoucher' ){
	      etype = 'IGP';
	    }else if ( etype == 'consumption' ){
	      etype = 'CNS';
	    }else if ( etype == 'materialreturn' ){
	      etype = 'MR';
	    }else if ( etype == 'navigation' ){
	      etype = 'NV';
	    }else if ( etype == 'production' ){
	      etype = 'PR';
	    }else if ( etype == 'cpv' ){
	      etype = 'CPV';
	    }else if ( etype == 'crv' ){
	      etype = 'CRV';
	    }else if ( etype == 'pd_issue' ){
	      etype = 'CHI';
	    }else if ( etype == 'pd_receive' ){
	      etype = 'CHR';
	    }else if ( etype == 'jv' ){
	      etype = 'JV';
	    }else if ( etype == 'openingbalance' ){
	      etype = 'OB';
	    }else if ( etype == 'openingstock' ){
	      etype = 'OS';
	    }else if ( etype == 'item_conversion' ){
	      etype = 'ITC';
	    } else if ( etype == 'penalty' ){
	      etype = 'PNL';
	    } else if ( etype == 'loan' ){
	      etype = 'LN';
	    } else if ( etype == 'advance' ){
	      etype = 'ADV';
	    } else if ( etype == 'incentive' ){
	      etype = 'INC';
	    }
	    
	    return etype;
  	},
  	makeHref : function(etype , vrnoa){
  
	  voucher_type = '';

	  if ( etype.toLowerCase() == 'sale' ) {
	      voucher_type = base_url + 'index.php/saleorder/sale_invoice?vrnoa=' + vrnoa;
	  } else if ( etype.toLowerCase() == 'saleorder' ) {
	  	  voucher_type = base_url + 'index.php/saleorder?vrnoa=' + vrnoa;
  	  }else if ( etype.toLowerCase() == 'jv' ) {
	      voucher_type = base_url + 'index.php/jv?vrnoa=' + vrnoa;
	  } else if ( etype.toLowerCase() == 'pur_order' ) {
	  	  voucher_type = base_url + 'index.php/purchaseorder?vrnoa=' + vrnoa;
	  } else if ( etype.toLowerCase() == 'openingstock' ) {
	  	  voucher_type = base_url + 'index.php/openingstock?vrnoa=' + vrnoa;
	  } else if ( etype.toLowerCase() == 'quotationorder' ) {
	      voucher_type = base_url + 'index.php/quotationorder?vrnoa=' + vrnoa;
	  } else if ( ( etype.toLowerCase() == 'cpv' ) || ( etype.toLowerCase() == 'crv' ) ) {
	      voucher_type = base_url + 'index.php/payment?vrnoa=' + vrnoa + '&etype=' + etype.toLowerCase();
	  } else if ( ( etype.toLowerCase() == 'pd_issue' ) ) {
	      voucher_type = base_url + 'index.php/payment/chequeissue?vrnoa=' + vrnoa;
	  } else if ( ( etype.toLowerCase() == 'pd_receive' ) ) {
	      voucher_type = base_url + 'index.php/payment/chequereceive?vrnoa=' + vrnoa;
	  } else if ( etype.toLowerCase() == 'purchase' ) {
	      voucher_type = base_url + 'index.php/purchase?vrnoa=' + vrnoa ;
	  } else if ( etype.toLowerCase() == 'saleorder' ) {
	      voucher_type = base_url + 'index.php/saleorder?vrnoa=' + vrnoa;
	  } else if ( etype.toLowerCase() == 'salereturn' ) {
	      voucher_type = base_url + 'index.php/salereturn?vrnoa=' + vrnoa;
	  } else if ( etype.toLowerCase() == 'purchasereturn' ) {
	      voucher_type = base_url + 'index.php/purchasereturn?vrnoa=' + vrnoa;
	  } else if ( etype.toLowerCase() == 'pur_import' ) {
	      voucher_type = base_url + 'index.php/purchase/import?vrnoa=' + vrnoa;
	  } else if ( etype.toLowerCase() == 'assembling' ) {
	      voucher_type = base_url + 'index.php/item/assdeass?vrnoa=' + vrnoa;
	  } else if ( etype.toLowerCase() == 'navigation' ) {
	      voucher_type = base_url + 'index.php/stocknavigation?vrnoa=' + vrnoa;
	  } else if ( etype.toLowerCase() == 'production' ) {
	      voucher_type = base_url + 'index.php/production?vrnoa=' + vrnoa;
	  } else if ( etype.toLowerCase() == 'consumption' ) {
	      voucher_type = base_url + 'index.php/consumption?vrnoa=' + vrnoa;
	  } else if ( etype.toLowerCase() == 'materialreturn' ) {
	      voucher_type = base_url + 'index.php/materialreturn?vrnoa=' + vrnoa;
	  } else if ( etype.toLowerCase() == 'moulding' ) {
	      voucher_type = base_url + 'index.php/moulding?vrnoa=' + vrnoa;
	  } else if ( etype.toLowerCase() == 'order_loading' ) {
	      voucher_type = base_url + 'index.php/saleorder/partsloading?vrnoa=' + vrnoa;
	  } else if ( etype.toLowerCase() == 'outwardvoucher' ) {
	  	  voucher_type = base_url + 'index.php/outwardvoucher?vrnoa=' + vrnoa;
	  } else if ( etype.toLowerCase() == 'inwardvoucher' ) {
	  	  voucher_type = base_url + 'index.php/inwardvoucher?vrnoa=' + vrnoa;
	  } else if ( etype.toLowerCase() == 'requisition' ) {
	  	  voucher_type = base_url + 'index.php/requisition?vrnoa=' + vrnoa;
	  } else if ( etype.toLowerCase() == 'item_conversion' ) {
	      voucher_type = base_url + 'index.php/purchase/itemconversion?vrnoa=' + vrnoa;
	  } else if ( etype.toLowerCase() == 'openingbalance' ) {
	      voucher_type = base_url + 'index.php/openingbalance?vrnoa=' + vrnoa;
	  } else if ( etype.toLowerCase() == 'penalty' ) {
	      voucher_type = base_url + 'index.php/charge/penalty?vrnoa=' + vrnoa;
	  } else if ( etype.toLowerCase() == 'loan' ) {
	      voucher_type = base_url + 'index.php/loan?vrnoa=' + vrnoa;
	  } else if ( etype.toLowerCase() == 'advance' ) {
	      voucher_type = base_url + 'index.php/payment/advance?vrnoa=' + vrnoa;
	  } else if ( etype.toLowerCase() == 'incentive' ) {
	      voucher_type = base_url + 'index.php/payment/incentive?vrnoa=' + vrnoa;
	  }
	  else {
	      voucher_type = vrnoa + '-' + etype;
	  }

	  return voucher_type;

	},
	fetchTopTenDaysCheques : function ( etype ){

		$('.'+etype + 'Rows').empty();

		$.ajax({
			url: base_url + 'index.php/payment/fetchtoptencheques',
			type: 'POST',
			dataType: 'JSON',
			data: { etype : etype, company_id : $('.cid').val() },

			success : function ( data ) {

				var rows = $('.'+etype + 'Rows');
				var htmls = '';

				if (data.length !== 0) {
					var handler = $('#cheque-row').html();
					var template = Handlebars.compile(handler);

					$(data).each(function( index, elem ){

						elem.VRDATE = ( elem.VRDATE ) ? elem.VRDATE.substr(0,10) : '-';
						elem.MATURE_DATE = ( elem.MATURE_DATE ) ? elem.MATURE_DATE.substr(0,10) : '-';
						elem.AMOUNT = parseInt(elem.AMOUNT);

						var html = template(elem);
						htmls += html;
					});

					rows.append(htmls);
				}
				else {
					rows.append('<tr><td colspan="7">No cheques found!</td></tr>')
				}
			}
		});
	},

	/**
	 * fetchStatData => fetch sale purchase stats
	 * @param  {string} period weekly, monthly or yearly
	 * @param {string} urlChunk to form the url like index.php/[urlChunk]/fetchChartData
	 */
	fetchStatData : function ( period, etype, urlChunk ) {
		// alert('period' + period + 'etype' + etype + ' url ' + urlChunk);
		$.ajax({
			url: base_url + 'index.php/'+ urlChunk + '/fetchchartdata',
			type: 'POST',
			dataType: 'JSON',
			data: { period: period, etype : etype, company_id : $('.cid').val() },

			beforeSend: function(){ },

			success : function(data){

				// #sales-table and #purchases-table
				var table = $("#" + etype + "s-table");
				var graph = $("#" + etype + "s-graph");

				var tbody = $("#" + etype + "s-table tbody");
				var thead = $("#" + etype + "s-table thead");

				tbody.html('');
				thead.html('');

				if (data.length === 0) {
					tbody.append('<tr><td colspan="3">No record found.</td></tr>')
					$("#total-" + etype + "s-amount").html(0);
				}
				else{
					var rows = '';
					var headHandle;
					var bodyHandle;

					var dailySum = 0;
					var monthlyWeeklySum = 0;
					var monthlySum = 0;
					var yearlySum = 0;

					if ( period.toLowerCase() === 'daily' ) {
						headHandle = $("#sp-daily-head-template");
						bodyHandle = $("#sp-daily-template");
					}
					else if(period.toLowerCase() === 'weekly' ) {
						headHandle = $("#sp-weekly-monthly-head-template");
						bodyHandle = $("#sp-weekly-monthly-template");
					}
					else if( period.toLowerCase() === 'monthly' ) {
						headHandle = $("#sp-monthly-head-template");
						bodyHandle = $("#sp-monthly-template");
					}

					else if( period.toLowerCase() === 'yearly') {
						headHandle = $("#sp-yearly-head-template");
						bodyHandle = $("#sp-yearly-template");
					}

					/******************* Genearate and attach the head *******************/
					
					var headSource   = headHandle.html();
					var headTemplate = Handlebars.compile(headSource);
					var headHtml = headTemplate({});

					thead.html(headHtml);
				
					

					/*************** Generate and attach the body ***********************/

					var bodySource = bodyHandle.html();
					var bodyTemplate = Handlebars.compile(bodySource);

					$(data).each(function(index, elem){

						var dailyAmount = parseInt(elem.NAMOUNT);
						var weeklyAmount = dashboard.getWeekSum(elem);
						var monthlyAmount = dashboard.getMonthSum(elem);
						var yearlyAmount = parseInt(elem.TotalAmount);

						dailySum += isNaN(dailyAmount) ? 0 : dailyAmount;
						monthlyWeeklySum += weeklyAmount;
						monthlySum += monthlyAmount;
						yearlySum += isNaN(yearlyAmount) ? 0 : yearlyAmount;

						// Exists in Daily Data rows
						elem.NAMOUNT = parseInt(elem.NAMOUNT);

						// Exists in Weekly Data Rows
						elem.Monday = parseInt(elem.Monday);
						elem.Tuesday = parseInt(elem.Tuesday);
						elem.Wednesday = parseInt(elem.Wednesday);
						elem.Thursday = parseInt(elem.Thursday);
						elem.Friday = parseInt(elem.Friday);
						elem.Saturday = parseInt(elem.Saturday);
						elem.Sunday = parseInt(elem.Sunday);

						// Exists in Monthly Data Rows
						elem.Jan = parseInt(elem.Jan);
						elem.Feb = parseInt(elem.Feb);
						elem.Mar = parseInt(elem.Mar);
						elem.Apr = parseInt(elem.Apr);
						elem.May = parseInt(elem.May);
						elem.Jun = parseInt(elem.Jun);
						elem.Jul = parseInt(elem.Jul);
						elem.Aug = parseInt(elem.Aug);
						elem.Sep = parseInt(elem.Sep);
						elem.Oct = parseInt(elem.Oct);
						elem.Nov = parseInt(elem.Nov);
						elem.Dec = parseInt(elem.Dec);

						// Exists in Yearly Data
						elem.TotalAmount = parseInt(elem.TotalAmount);

						rows += bodyTemplate(elem); // generate html and add that to rows string, so to improve efficiency
					});

					tbody.append(rows);

					var sums = [];

					sums.push(dailySum);
					sums.push(monthlyWeeklySum);
					sums.push(yearlySum);
					sums.push(monthlySum);

					// Fetch the non-zero of the sums.
					var finalSum = sums.filter(function(x) { return x; }).pop();

					$("#total-" + etype + "s-amount").html(parseInt(finalSum));

					/******************************************************************/

				}
				graph.hide();
				table.show();
				$("#" + etype + "s-view").val('tabular');
			},

			error : function ( error ){
				//alert("Error: " + error);
				//console.log(error);

			}
		});
	},

	getWeekSum : function (week) {

		var sum = 0;

		sum =
				( isNaN(parseInt(week.Monday)) ? 0 : parseInt(week.Monday) ) +
				( isNaN(parseInt(week.Tuesday)) ? 0 : parseInt(week.Tuesday) ) +
				( isNaN(parseInt(week.Wednesday)) ? 0 : parseInt(week.Wednesday ) ) +
				( isNaN(parseInt(week.Thursday)) ? 0 : parseInt(week.Thursday) ) +
				( isNaN(parseInt(week.Friday)) ? 0 : parseInt(week.Friday) ) +
				( isNaN(parseInt(week.Saturday)) ? 0 : parseInt(week.Saturday ) )+
				( isNaN(parseInt(week.Sunday)) ? 0 : parseInt(week.Sunday) );

		return sum;
	},
	getMonthSum : function (week) {

		var sum = 0;

		sum =
				( isNaN(parseInt(week.Jan)) ? 0 : parseInt(week.Jan) ) +
				( isNaN(parseInt(week.Feb)) ? 0 : parseInt(week.Feb) ) +
				( isNaN(parseInt(week.Mar)) ? 0 : parseInt(week.Mar ) ) +
				( isNaN(parseInt(week.Apr)) ? 0 : parseInt(week.Apr) ) +
				( isNaN(parseInt(week.May)) ? 0 : parseInt(week.May) ) +
				( isNaN(parseInt(week.Jun)) ? 0 : parseInt(week.Jun ) )+
				( isNaN(parseInt(week.Jul)) ? 0 : parseInt(week.Jul) )+
				( isNaN(parseInt(week.Aug)) ? 0 : parseInt(week.Aug) ) +
				( isNaN(parseInt(week.Sep)) ? 0 : parseInt(week.Sep ) ) +
				( isNaN(parseInt(week.Oct)) ? 0 : parseInt(week.Oct) ) +
				( isNaN(parseInt(week.Nov)) ? 0 : parseInt(week.Nov) ) +
				( isNaN(parseInt(week.Dec)) ? 0 : parseInt(week.Dec ) );

		return sum;
	},

	formatCurrency : function () {
        $(".totalPurchases, .totalSales, .totalPurRet, .totalSaleRet").text(function (idx, text) {
            var formattedNum = accounting.formatMoney(text, options);
            var parts = formattedNum.split('.');
            return parts[0];
        });
    },


	showNetSales : function (){


		$.ajax({

			url: base_url + 'index.php/purchase/fetchnetsum',
			type: 'POST',
			dataType: 'JSON',
			data : { company_id : $('.cid').val() ,etype : 'sale'},

			beforeSend: function(){ },
			success : function(data){
				if (data.length === 0) {
					$('.sales-sum').html(0);
					$('.sales-sumc').html(0);
				}
				else{

					$('.sales-sum').html(parseInt(data[0].SALE_TOTAL));
					$('.sales-sumc').html(parseInt(data[0].SALE_TOTAL));
				}
			},

			error : function ( error ){
				//alert("Error: " + error);
				//console.log(error);

			}
		});
	},
	append_newdash : function (from,to){


		$.ajax({

			url: base_url + 'index.php/purchase/fetchalltransactions',
			type: 'POST',
			dataType: 'JSON',
			data : { company_id : $('#cid').val() ,from : from,to:to},

			beforeSend: function(){ },
			success : function(data){
				if (data.length === 0) {
				}
				else{
					$('.purchaseorder_tbody').empty();
					$('.purchase_tbody').empty();
					$('.purchasereturn_tbody').empty();
					$('.openingstock_tbody').empty();
					$('.quotation_tbody').empty();
					$('.saleorder_tbody').empty();
					$('.ogp_tbody').empty();
					$('.sale_tbody').empty();
					$('.salereturn_tbody').empty();
					$('.requisition_tbody').empty();
					$('.igp_tbody').empty();
					$('.consumption_tbody').empty();
					$('.materialreturn_tbody').empty();
					$('.itemconversion_tbody').empty();
					$('.production_tbody').empty();
					$('.payment_tbody').empty();
					$('.receipt_tbody').empty();
					$('.chequeissue_tbody').empty();
					$('.chequereceipt_tbody').empty();
					$('.jv_tbody').empty();
					$('.opening_tbody').empty();
					$('.jv_tbody').empty();
					$('.penalty_tbody').empty();
					$('.loan_tbody').empty();
					$('.advance_tbody').empty();
					$('.incentive_tbody').empty();
					dashboard.populatetransactions(data);
					$('.purchases-sum-voucher').text(parseFloat(data['totalofalltransaction']['totalpurchases']).toFixed(0));
					$('.purchases-sum-return').text(parseFloat(data['totalofalltransaction']['totalpurchasereturns']).toFixed(0));
					$('.openingstock-sum').text(parseFloat(data['totalofalltransaction']['totalopeningstocks']).toFixed(0));
					$('.quotationorder-sum').text(parseFloat(data['totalofalltransaction']['totalquotation']).toFixed(0));
					$('.saleorder-sum').text(parseFloat(data['totalofalltransaction']['totalsaleorder']).toFixed(0));
					$('.totalsale-sum').text(parseFloat(data['totalofalltransaction']['totalsale']).toFixed(0));
					$('.salereturn-sum').text(parseFloat(data['totalofalltransaction']['totalsalereturn']).toFixed(0));
					$('.sum_cashpayments').text(parseFloat(data['totalofalltransaction']['totalpayment']).toFixed(0));
					$('.sum_cashreceipts').text(parseFloat(data['totalofalltransaction']['totalreceipt']).toFixed(0));
					$('.bpv-sum-voucher').text(parseFloat(data['totalofalltransaction']['totalbankpayment']).toFixed(0));
					$('.brv-sum-voucher').text(parseFloat(data['totalofalltransaction']['totalbankreceipt']).toFixed(0));
					$('.jv-sum').text(parseFloat(data['totalofalltransaction']['totaljv']).toFixed(0));
					$('.ob-sum').text(parseFloat(data['totalofalltransaction']['totalop']).toFixed(0));
					$('.penalty-sum').text(parseFloat(data['totalofalltransaction']['totalpenalty']).toFixed(0));
					$('.loan-sum').text(parseFloat(data['totalofalltransaction']['totalloan']).toFixed(0));
					$('.advance-sum').text(parseFloat(data['totalofalltransaction']['totaladvance']).toFixed(0));
					$('.incentive-sum').text(parseFloat(data['totalofalltransaction']['totalincentive']).toFixed(0));
					$('.requisition-sum').text('0');
					$('.inward-sum').text('0');
					$('.consumption-sum').text('0');
					$('.materialreturn-sum').text('0');
					$('.itemconversion-sum').text('0');
					$('.production-sum').text('0');
				}
			},

			error : function ( error ){
				//alert("Error: " + error);
				//console.log(error);

			}
		});
	},


	showNetCheques : function ( etype ){

		$.ajax({

			url: base_url + 'index.php/payment/fetchnetchequesum',
			type: 'POST',
			dataType: 'JSON',
			data : { etype : etype, company_id : $('.cid').val() },

			success : function ( data ){

				if (data.length !== 0) {
					$('.' + etype + '-sum').html(parseInt(data[0]['NETTOTAL']));
				} else {
					$(etype + '-sum').html(0);
				}

			},

			error : function (){

			}
		});

	},

	showNetCash : function (){

		$.ajax({

			url: base_url + 'index.php/cash/fetchnetsum',
			type: 'POST',
			dataType: 'JSON',
			data : {company_id : $('.cid').val()},

			beforeSend: function(){ },
			success : function(data){

				if (data.length === 0) {
					$('.cash-sum').html(0);
				}
				else{
					$('.cash-sum').html(parseInt(data[0].CASH_TOTAL));
				}
			},

			error : function ( error ){
				//alert("Error: " + error);
				//console.log(error);

			}
		});
	},

	showNetExpense : function (){

		$.ajax({

			url: base_url + 'index.php/cash/fetchnetexpense',
			type: 'POST',
			dataType: 'JSON',
			data : {company_id : $('.cid').val()},

			beforeSend: function(){ },
			success : function(data){

				if (data.length === 0) {
					$('.expenses-sum').html(0);
				}
				else{
					$('.expenses-sum').html(parseInt(data[0].EXP_TOTAL));
				}
			},

			error : function ( error ){
				//alert("Error: " + error);
				//console.log(error);

			}
		});
	},


	showNetPurchases : function (){

		$.ajax({

			url: base_url + 'index.php/purchase/fetchnetsum',
			type: 'POST',
			dataType: 'JSON',
			data : { company_id : $('.cid').val() , etype : 'purchase'},

			beforeSend: function(){ },
			success : function(data){

				if (data.length === 0) {
					$('.purchases-sum').html(0);
				}
				else{
					
					$('.purchases-sum').html(parseInt(data[0].PURCHASE_TOTAL));
				}
			},

			error : function ( error ){
				//alert("Error: " + error);
				//console.log(error);

			}
		});
	},

	showNetPReturns : function () {

		$.ajax({

			url: base_url + 'index.php/purchase/fetchnetsum',
			type: 'POST',
			dataType: 'JSON',
			data : { company_id : $('.cid').val() , etype:'purchasereturn' },

			beforeSend: function(){ },
			success : function(data){

				if (data.length === 0) {
					$('.preturns-sum').html(0);
				}
				else{
					$('.preturns-sum').html(parseInt(data[0].PRETURNS_TOTAL));
				}
			},

			error : function ( error ){
				//alert("Error: " + error);
				//console.log(error);

			}
		});
	},

	showGraph : function ( etype, period ) {

		var table = $("#"+etype+"s-table");

		var tbody = table.find('tbody');
		var trs = tbody.find('tr');

		var graph = $("#"+etype+"s-graph");

		var graphData = [];

		period = period.toLowerCase();
		graph.empty();

		if ( period === 'daily' ) {
			graphData = dashboard.fetchDailyData(trs);
		} else if( period === 'weekly' ) {
			graphData = dashboard.fetchWeeklyData(trs);
		} else if( period === 'monthly' ) {
			graphData = dashboard.fetchMonthlyData(trs);
		} else if( period === 'yearly' ) {
			graphData = dashboard.fetchYearlyData(trs);
		}

		table.hide();
		graph.show();

		graph.jqBarGraph({
			data: graphData
		});

	},

	fetchDailyData : function ( trs ) {

		var dailyData = [];
		var tempArray = [];

		$( trs ).each(function (index, elem) {

			tempArray = [];

			tempArray.push( parseInt( $(elem).find('.amount').text() ) );
			tempArray.push( $(elem).find('.account').text() );
			tempArray.push( dashboard.getRandomColor() )

			dailyData.push(tempArray);

		});

		return dailyData;
	},

	fetchWeeklyData : function ( trs ) {

		var weeklyData = [];
		var tempArray = [];
		var days = ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'];

		$( trs ).each (function (index, tr) {

			var tds = $(tr).find('td');

			$(tds).each( function (tdIndex, td) {

				tempArray = [];

				tempArray.push( parseInt( $(td).text() ) );
				tempArray.push(days[tdIndex]);
				tempArray.push( dashboard.getRandomColor() );

				weeklyData.push(tempArray);
			});
		});

		return weeklyData;
	},

	fetchMonthlyData : function ( trs ) {

		var weekSum =  0;
		var monthData = [];
		var tempArray = [];

		$( trs ).each( function ( index, elem ) {

			weekSum = 0;
			tempArray = [];

			var tds = $(elem).find('td');
			$( tds ).each(function ( index, td ) {
				weekSum += parseInt( $(td).text() );
			});

			// to generate tempArray = [WEEK-1, 1208, #CCC]
			tempArray.push( weekSum );
			tempArray.push( "WEEK-" + ( index + 1 ) );
			tempArray.push( dashboard.getRandomColor() );

			monthData.push( tempArray );
		});

		return monthData;
	},

	fetchYearlyData : function ( trs ) {

		var month = 0;
		var amount = 0;
		var yearData = [];
		var tempArray = [];

		$( trs ).each( function (index, elem) {

			tempArray = [];

			month = $(elem).find('.tdMonth').text();
			amount = parseInt( $(elem).find('.tdMonthAmount').text() );

			tempArray.push(amount);
			tempArray.push(month)
			tempArray.push( dashboard.getRandomColor() )

			yearData.push(tempArray);
		});

		return yearData;
	},

	getRandomColor : function () {
		var randIndex = dashboard.getRandomNumber(0,7);
		return dashboard.colors[randIndex];
	},

	getRandomNumber : function (minimum, maximum){
		var randomnumber = Math.floor(Math.random() * (maximum - minimum + 1)) + minimum;
		return randomnumber;
	},

	showNetSReturns : function (){

		$.ajax({

			url: base_url + 'index.php/purchase/fetchnetsum',
			type: 'POST',
			dataType: 'JSON',
			data : { company_id : $('.cid').val(), etype:'salereturn' },

			beforeSend: function(){ },
			success : function(data){
				if (data.length === 0) {
					$('.sreturns-sum').html(0);
				}
				else{
					$('.sreturns-sum').html(parseInt(data[0].SRETURNS_TOTAL));
				}
			},

			error : function ( error ){
				//alert("Error: " + error);
				//console.log(error);

			}
		});
	}


};

$(document).ready(function(){
	dashboard.init();
});