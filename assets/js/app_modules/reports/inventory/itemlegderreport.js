 itemledger = function() {

 	var fetchReport = function (from, to, item_id, godown_id) {
        
        if (typeof dTable != 'undefined') {
            dTable.fnDestroy();
            $('#itemRows').empty();
        }
        
		$.ajax({
                url: base_url + "index.php/item/fetchItemLedgerReport",
                data: { 'from' : from, 'to' : to, 'item_id' : item_id ,'godown_id' : godown_id, 'company_id': $('#cid').val() },
                type: 'POST',
                dataType: 'JSON',
                beforeSend: function () {
                    console.log(this.data);
                 },
                complete: function () { },
                success: function (result) {

                    if (result.length !== 0) {

                        var totalIn = 0;
                        var totalOut = 0;
                        var Rtotal = 0;

                        var totalInWeight = 0;
                        var totalOutWeight = 0;
                        var RtotalWeight = 0;
                        Rtotal=parseFloat($('#Opening_Qty').val());
                        RtotalWeight=parseFloat($('#Opening_Weight').val());

                        if (result.length != 0) {

                            var itemRows = $("#itemRows");

                            $.each(result, function (index, elem) {
                                var obj = { };
                                obj.serial = itemRows.find('tr').length+1;
                                        if ( elem.etype.toLowerCase() == 'sale' ) {
                                            obj.vrnoa = '<a href="' + base_url + 'index.php/saleorder/Sale_Invoice?vrnoa=' + elem.vrnoa + '">' + elem.vrnoa + '-' + elem.etype + '</a>';
                                        } else if ( elem.etype.toLowerCase() == 'jv' ) {
                                            obj.vrnoa = '<a href="' + base_url + 'index.php/journal?vrnoa=' + elem.vrnoa + '">' + elem.vrnoa + '-' + elem.etype + '</a>';
                                        } else if ( ( elem.etype.toLowerCase() == 'cpv' ) || ( elem.etype.toLowerCase() == 'crv' ) ) {
                                            obj.vrnoa = '<a href="' + base_url + 'index.php/payment?vrnoa=' + elem.vrnoa + '&etype=' + elem.etype.toLowerCase() + '">' + elem.vrnoa + '-' + elem.etype + '</a>';
                                        } else if ( ( elem.etype.toLowerCase() == 'pd_issue' ) ) {
                                            obj.vrnoa = '<a href="' + base_url + 'index.php/payment/chequeIssue?vrnoa=' + elem.vrnoa + '">' + elem.vrnoa + '-' + elem.etype + '</a>';
                                        } else if ( ( elem.etype.toLowerCase() == 'pd_receive' ) ) {
                                            obj.vrnoa = '<a href="' + base_url + 'index.php/payment/chequeReceive?vrnoa=' + elem.vrnoa + '">' + elem.vrnoa + '-' + elem.etype + '</a>';
                                        } else if ( elem.etype.toLowerCase() == 'purchase' ) {
                                            obj.vrnoa = '<a href="' + base_url + 'index.php/purchase?vrnoa=' + elem.vrnoa + '">' + elem.vrnoa + '-' + elem.etype + '</a>';
                                        } else if ( elem.etype.toLowerCase() == 'salereturn' ) {
                                            obj.vrnoa = '<a href="' + base_url + 'index.php/salereturn?vrnoa=' + elem.vrnoa + '">' + elem.vrnoa + '-' + elem.etype + '</a>';
                                        } else if ( elem.etype.toLowerCase() == 'purchasereturn' ) {
                                            obj.vrnoa = '<a href="' + base_url + 'index.php/purchasereturn?vrnoa=' + elem.vrnoa + '">' + elem.vrnoa + '-' + elem.etype + '</a>';
                                        } else if ( elem.etype.toLowerCase() == 'pur_import' ) {
                                            obj.vrnoa = '<a href="' + base_url + 'index.php/purchase/import?vrnoa=' + elem.vrnoa + '">' + elem.vrnoa + '-' + elem.etype + '</a>';
                                        } else if ( elem.etype.toLowerCase() == 'assembling' ) {
                                            obj.vrnoa = '<a href="' + base_url + 'index.php/item/assdeass?vrnoa=' + elem.vrnoa + '">' + elem.vrnoa + '-' + elem.etype + '</a>';
                                        } else if ( elem.etype.toLowerCase() == 'navigation' ) {
                                            obj.vrnoa = '<a href="' + base_url + 'index.php/stocknavigation?vrnoa=' + elem.vrnoa + '">' + elem.vrnoa + '-' + elem.etype + '</a>';
                                        } else if ( elem.etype.toLowerCase() == 'production' ) {
                                            obj.vrnoa = '<a href="' + base_url + 'index.php/production?vrnoa=' + elem.vrnoa + '">' + elem.vrnoa + '-' + elem.etype + '</a>';
                                        } else if ( elem.etype.toLowerCase() == 'consumption' ) {
                                            obj.vrnoa = '<a href="' + base_url + 'index.php/consumption?vrnoa=' + elem.vrnoa + '">' + elem.vrnoa + '-' + elem.etype + '</a>';
                                        } else if ( elem.etype.toLowerCase() == 'materialreturn' ) {
                                            obj.vrnoa = '<a href="' + base_url + 'index.php/materialreturn?vrnoa=' + elem.vrnoa + '">' + elem.vrnoa + '-' + elem.etype + '</a>';
                                        } else if ( elem.etype.toLowerCase() == 'moulding' ) {
                                            obj.vrnoa = '<a href="' + base_url + 'index.php/moulding?vrnoa=' + elem.vrnoa + '">' + elem.vrnoa + '-' + elem.etype + '</a>';
                                        } else if ( elem.etype.toLowerCase() == 'order_loading' ) {
                                            obj.vrnoa = '<a href="' + base_url + 'index.php/saleorder/partsloading?vrnoa=' + elem.vrnoa + '">' + elem.vrnoa + '-' + elem.etype + '</a>';
                                        }
                                        else {
                                            obj.vrnoa = elem.vrnoa + '-' + elem.etype;
                                        }
                                        // alert(elem.vrnoa);
                                // obj.vrnoa =  elem.etype +'-'+ elem.vrnoa ;
                                obj.remarks = (elem.remarks) ? elem.remarks : "-";
                                obj.description = (elem.description) ? elem.description : "-";
                                obj.party_name = (elem.party_name) ? elem.party_name : "-";
                                obj.location = (elem.name) ? elem.name : "-";
                                obj.date = (elem.date) ? elem.date.substring(0,10) : "-";
                                Rtotal += parseFloat(elem.qty);
                                RtotalWeight += parseFloat(elem.weight);
                                if (elem.qty > 0) {                                    
                                    obj._in = elem.qty;
                                    obj._out = '-';
                                    totalIn += parseFloat(elem.qty);
                                } else {                                    
                                    obj._out = Math.abs(elem.qty);
                                    obj._in = '-';
                                    totalOut += parseFloat(elem.qty);
                                }
                                
                                obj.balance = (Rtotal!=0?parseFloat(Rtotal).toFixed(2):'-');
                                if (elem.weight > 0) {                                    
                                    obj._in_weight = elem.weight;
                                    obj._out_weight = '-';
                                    totalInWeight += parseFloat(elem.weight);
                                } else {                                    
                                    obj._out_weight = Math.abs(elem.weight);
                                    obj._in_weight = '-';
                                    totalOutWeight += parseFloat(elem.weight);
                                }
                                obj.balance_weight = (RtotalWeight!=0? parseFloat(RtotalWeight).toFixed(2):'-');

                                // Add the item of the new voucher
                                var source   = $("#voucher-item-template").html();
                                var template = Handlebars.compile(source);
                                var html = template(obj);

                                itemRows.append(html);                               
                                

                                if (index === (result.length -1)) {
                                    // add the last one's sum

                                    var source   = $("#voucher-sum-template").html();
                                    var template = Handlebars.compile(source);
                                    var html = template({total_in : totalIn.toFixed(2), total_out : Math.abs(totalOut.toFixed(2)) , total_in_weight:totalInWeight.toFixed(2) , total_out_weight : Math.abs(totalOutWeight.toFixed(2)) });

                                    itemRows.append(html);
                                };

                            });
                        }

                        bindGrid();
                    }
                }, error: function (result) {
                    alert("Error:" + result);
                }
            });

	}
    var validateSearch = function() {

        var errorFlag = false;
        var from_date = $('#from_date').val();
        var to_date = $('#to_date').val();
        var item_id = $('#item_dropdown').val();
        var godown_id = $('#dept_dropdown').val();
        

        // remove the error class first
        $('.inputerror').removeClass('inputerror');
        $('#to_date').removeClass('inputerror');     

        if ( from_date === '' || from_date === null ) {
            $('#from_date').addClass('inputerror');
            errorFlag = true;
        }
        if ( to_date === '' || to_date === null ) {
            $('#to_date').addClass('inputerror');
            errorFlag = true;
        }
        if ( item_id === '' || item_id === null ) {
            $('#item_dropdown').addClass('inputerror');
            errorFlag = true;
        }
        if ( godown_id === '' || godown_id === null ) {
            $('#dept_dropdown').addClass('inputerror');
            errorFlag = true;
        }
        if (from_date > to_date ){
            $('#from_date').addClass('inputerror');
            alert('Starting date must Be less than ending date.........')
            errorFlag = true;   
        }
        return errorFlag;
    }
    var Print_Voucher = function( ) {
        var error = validateSearch();
        if (!error) {
        var from = $('#from_date').val();
        var to = $('#to_date').val();
        var item_id = $('#item_dropdown').val();
        var godown_id = $('#dept_dropdown').val();
        var godown = $('#dept_dropdown').find('option:selected').text();
        var Opening_Qty = $('#Opening_Qty').val();
        var Opening_Weight = $('#Opening_Weight').val();
        var company_id = $('#cid').val();
        var user = $('#uname').val();
        var balance_qty = $('#itemRows tr').children('td.balance_qty').last().text();
        var balance_weight = $('#itemRows tr').children('td.balance_weight').last().text();
        from= from.replace('/','-');
        from= from.replace('/','-');
        to= to.replace('/','-');
        to= to.replace('/','-');

        // alert('etype  ' +  etype  +' dcno '+ dcno );
        var url = base_url + 'index.php/doc/Item_Ledger_Pdf/' + from + '/' + to + '/' + item_id  + '/' + company_id + '/' + '-1' + '/' + user+ '/' + godown_id+ '/' + godown+ '/' + Opening_Qty+ '/' + Opening_Weight+ '/' + balance_qty+ '/' +balance_weight;
        window.open(url);
         }
    }


	var bindGrid = function() {
        var dontSort = [];
        $('#datatable_example thead th').each(function () {
            if ($(this).hasClass('no_sort')) {
                dontSort.push({ "bSortable": false });
            } else {
                dontSort.push(null);
            }
        });
        dTable = $('#datatable_example').dataTable({
            // Uncomment, if prolems with datatable.
            // "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
            "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone'<'row-fluid'<'span8' f>>>'<'pag_top' p> T>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
            "aaSorting": [[0, "asc"]],
            "bPaginate": true,
            "sPaginationType": "full_numbers",
            "bJQueryUI": false,
            "aoColumns": dontSort,
			"bSort": false,
			"iDisplayLength" : 100,
            "oTableTools": {
                "sSwfPath": "js/copy_cvs_xls_pdf.swf",
                "aButtons": [{ "sExtends": "print", "sButtonText": "Print Report", "sMessage" : "Inventory Report" }]
            }
        });
        $.extend($.fn.dataTableExt.oStdClasses, {
            "s`": "dataTables_wrapper form-inline"
        });
    }

	return {

		init : function() {
			this.bindUI();
		},

		bindUI : function() {
			var self = this;
            $('#from_date').val('2007/11/01');

			$('.btnSearch').on('click', function(e) {
			e.preventDefault();
            var error = validateSearch();
            if (!error) {
				var from = $('#from_date').val();
				var to = $('#to_date').val();
                var item_id = $('#item_dropdown').val();
                var godown_id = $('#dept_dropdown').val();
                itemledger.fetchItemOpeningStock(from, item_id , godown_id);
				fetchReport(from, to, item_id, godown_id);
            }
			});

            $('.btnReset').on('click', function(e) {
                e.preventDefault();
                self.resetVoucher();
            });
            $('.btnPrint').on('click', function(e) {
                e.preventDefault();
                Print_Voucher();
            });

            shortcut.add("F9", function() {
                 Print_Voucher();
            });
            
            shortcut.add("F6", function() {
              
                var error = validateSearch();
            if (!error) {
                var from = $('#from_date').val();
                var to = $('#to_date').val();
                var item_id = $('#item_dropdown').val();
                var godown_id = $('#dept_dropdown').val();
                itemledger.fetchItemOpeningStock(from, item_id , godown_id);
                fetchReport(from, to, item_id , godown_id);
            }
            });
            shortcut.add("F5", function() {
                self.resetVoucher();
            });

		},

        fetchItemOpeningStock : function ( startDate, item_id , godown_id) {

        $.ajax({
            url: base_url + 'index.php/item/fetchItemOpeningStock',
            type: 'POST',
            dataType: 'JSON',
            data: { 'to': startDate, 'item_id' : item_id, 'godown_id' : godown_id ,'company_id' : $('#cid').val() },
            
            beforeSend: function(){ },
                
            success : function(data){
                $('#Opening_Qty').val(data[0]['OPENING_QTY']);
                $('#Opening_Weight').val(data[0]['OPENING_WEIGHT']);
            },

            error : function ( error ){
                alert("Error showing opening stock: " + JSON.parse(error));
            }
        });
    },


		// instead of reseting values reload the page because its cruel to write to much code to simply do that
		resetVoucher : function() {
			general.reloadWindow();
		}
	}

};

var itemledger = new itemledger();
itemledger.init();