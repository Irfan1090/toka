 Purchase = function() {

 	var fetchVouchers = function (from, to, what, company_id, rpt) {

        $('.grand-total').html(0.00);

        if (typeof purchase.dTable != 'undefined') {
            purchase.dTable.fnDestroy();
            $('#saleRows').empty();
        }

		$.ajax({
                url: base_url + "index.php/report/fetchStockReport",
                data: { 'from' : from, 'to' : to, 'what' : what, 'company_id': company_id },
                type: 'POST',
                dataType: 'JSON',
                beforeSend: function () {
                    console.log(this.data);
                 },
                complete: function () { },
                success: function (result) {

                    if (result.length !== 0) {
                        if(rpt==0){
                            var th = $('#general-head-template').html();
                        }else{
                            var th = $('#general-head-template-value').html();
                        }
                        
                        var template = Handlebars.compile( th );
                        var html = template({});
                        $('.dthead').html( html );

                        

                            // $("#datatable_example_wrapper").fadeIn();

                            // $(".cols_options").show();

                            var prevVoucher = "";
                            var prevVoucher22 = "";
                            var QTY_SUB = 0;
                            var WEIGHT_SUB = 0;
                            var VALUE_SUB = 0;

                            var QTY_NET = 0;
                            var WEIGHT_NET = 0;
                            var VALUE_NET = 0;

                            if (result.length != 0) {

                                var saleRows = $("#saleRows");

                                $.each(result, function (index, elem) {

                                    //debugger

                                    var obj = { };
                                    obj.SERIAL =index+1; //saleRows.find('tr').length+1;
                                    obj.NAME = (elem.NAME) ? elem.NAME : "-";
                                    obj.DESCRIPTION = (elem.DESCRIPTION) ? elem.DESCRIPTION : "-";
                                    obj.QTY = (elem.QTY) ? elem.QTY : "-";
                                    obj.WEIGHT = (elem.WEIGHT) ? elem.WEIGHT : "-";
                                    obj.PRATE = (elem.PRATE) ? parseFloat(elem.PRATE).toFixed(2) : "-";
                                    obj.VALUE =parseFloat(obj.PRATE * obj.QTY).toFixed(2);

                                    if (what=='item'){
                                        prevVoucher22= (obj.DESCRIPTION) ? obj.DESCRIPTION : "-";
                                        obj.DESCRIPTION=(elem.NAME) ? elem.NAME : "-";
                                    }else{
                                        prevVoucher22= (elem.NAME) ? elem.NAME : "-";
                                        obj.DESCRIPTION=(elem.DESCRIPTION) ? elem.DESCRIPTION : "-";
                                    }
                                    

                                    if (prevVoucher != prevVoucher22 ) {

                                        if (index !== 0) {
                                            // Create the heading for this new voucher.
                                            if(rpt==0){
                                                var source   = $("#general-grouptotal-template").html();
                                            }else{
                                                var source   = $("#general-grouptotal-template-value").html();
                                            }   
                                            
                                            var template = Handlebars.compile(source);
                                            var html = template({TOTAL:'Sub Total', 'TOTAL_QTY':QTY_SUB.toFixed(2), 'TOTAL_WEIGHT':WEIGHT_SUB.toFixed(2), 'TOTAL_VALUE':VALUE_SUB.toFixed(2)});

                                            saleRows.append(html);
                                        }
                                        QTY_SUB=0
                                        WEIGHT_SUB=0
                                        VALUE_SUB=0
                                        // Reset the previous voucher to current voucher.
                                       

                                        // Add the item of the new voucher
                                        if(rpt==0){
                                            var source   = $("#general-vhead-template").html();
                                        }else{
                                            var source   = $("#general-vhead-template-value").html();
                                        }

                                        var template = Handlebars.compile(source);
                                        var html = template({GROUP1: prevVoucher22});
                                        saleRows.append(html);
                                         prevVoucher = prevVoucher22;
                                    }

                                    
                                    QTY_SUB +=parseFloat((elem.QTY) ? elem.QTY : 0);
                                    WEIGHT_SUB +=parseFloat((elem.WEIGHT) ? elem.WEIGHT : 0);
                                    VALUE_SUB +=parseFloat(obj.VALUE);
                                    VALUE_NET +=parseFloat(obj.VALUE);
                                    
                                    QTY_NET +=parseFloat((elem.QTY) ? elem.QTY : 0);
                                    WEIGHT_NET +=parseFloat((elem.WEIGHT) ? elem.WEIGHT : 0);
                                    
                                    if(rpt==0){
                                        var source   = $("#general-item-template").html();
                                    }else{
                                        var source   = $("#general-item-template-value").html();
                                    }
                                    var template = Handlebars.compile(source);
                                    var html = template(obj);
                                    saleRows.append(html);
                                    if (index === (result.length -1)) {

                                        if(rpt==0){
                                            var source   = $("#general-grouptotal-template").html();
                                        }else{
                                            var source   = $("#general-grouptotal-template-value").html();
                                        }
                                        var template = Handlebars.compile(source);
                                        var html = template({TOTAL:'Sub Total', 'TOTAL_QTY':QTY_SUB.toFixed(2), 'TOTAL_WEIGHT':WEIGHT_SUB.toFixed(2), 'TOTAL_VALUE':VALUE_SUB.toFixed(2)});

                                        saleRows.append(html);

                                        // Create the heading for this new voucher.
                                        if(rpt==0){
                                            var source   = $("#general-grouptotal-template").html();
                                        }else{
                                            var source   = $("#general-grouptotal-template-value").html();
                                        }
                                        var template = Handlebars.compile(source);
                                        var html = template({TOTAL:'Grand Total', 'TOTAL_QTY':QTY_NET.toFixed(2), 'TOTAL_WEIGHT':WEIGHT_NET.toFixed(2), 'TOTAL_VALUE':VALUE_NET.toFixed(2)});

                                        

                                        saleRows.append(html);
                                    }

                                });
                            }
                       
                        }

					

                    bindGrid();
                },

                error: function (result) {
                    alert("Error:" + result);
                }
            });

	}

    var validateSearch = function() {

        var errorFlag = false;
        var from_date = $('#from_date').val();
        var to_date = $('#to_date').val();
        

        // remove the error class first
        $('#from_date').removeClass('inputerror');
        $('#to_date').removeClass('inputerror');     

        if ( from_date === '' || from_date === null ) {
            $('#from_date').addClass('inputerror');
            errorFlag = true;
        }
        if ( to_date === '' || to_date === null ) {
            $('#to_date').addClass('inputerror');
            errorFlag = true;
        }
        if (from_date > to_date ){
            $('#from_date').addClass('inputerror');
            alert('Starting date must Be less than ending date.........')
            errorFlag = true;   
        }
        return errorFlag;
    }


	var bindGrid = function() {
        var dontSort = [];
        $('#datatable_example thead th').each(function () {
            if ($(this).hasClass('no_sort')) {
                dontSort.push({ "bSortable": false });
            } else {
                dontSort.push(null);
            }
        });
        purchase.dTable = $('#datatable_example').dataTable({
            // Uncomment, if prolems with datatable.
            // "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
            "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone'<'row-fluid'<'span8' f>>>'<'pag_top' p> T>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
            "aaSorting": [[0, "asc"]],
            "bPaginate": true,
            "sPaginationType": "full_numbers",
            "bJQueryUI": false,
            "aoColumns": dontSort,
			"bSort": false,
			"iDisplayLength" : 100,
            "oTableTools": {
                "sSwfPath": "js/copy_cvs_xls_pdf.swf",
                "aButtons": [{ "sExtends": "print", "sButtonText": "Print Report", "sMessage" : "Inventory Report" }]
            }
        });
        $.extend($.fn.dataTableExt.oStdClasses, {
            "s`": "dataTables_wrapper form-inline"
        });
    }

    var Print_Voucher = function(hd) {
        var error = validateSearch();
        if (!error) {
        var from = $('#from_date').val();
        var to = $('#to_date').val();
        from= from.replace('/','-');
        from= from.replace('/','-');
        to= to.replace('/','-');
        to= to.replace('/','-');

        var what = getCurrentView();
        var company_id = $('#cid').val();
        var user = $('#uname').val();

        // alert('etype  ' +  etype  +' dcno '+ dcno );
        var url = base_url + 'index.php/doc/Stock_Pdf/' + from + '/' + to  + '/' + what  + '/' + company_id + '/' + '-1' + '/' + user + '/' + hd;
        window.open(url);
    }
    }


    var getCurrentView = function() {
        var what = $('.btnSelCre.btn-primary').text().split('Wise')[0].trim().toLowerCase();
        return what;
    }

	return {

		init : function() {
			this.bindUI();
		},

		bindUI : function() {
			var self = this;
            $('#from_date').val('2007/11/01');
			$('.btnSearch').on('click', function(e) {				
                e.preventDefault();
				var error = validateSearch();
                if (!error) {
                var from = $('#from_date').val();
				var to = $('#to_date').val();
                var company_id = $('#cid').val();
                var what = getCurrentView();				
				fetchVouchers(from, to, what,company_id, 0);
            }
			});
            $('.btnSearchValue').on('click', function(e) {               
                e.preventDefault();
                var error = validateSearch();
                if (!error) {
                var from = $('#from_date').val();
                var to = $('#to_date').val();
                var company_id = $('#cid').val();
                var what = getCurrentView();
                fetchVouchers(from, to, what,company_id,1);
            }
            });

            $('.btnReset').on('click', function(e) {
                e.preventDefault();
                self.resetVoucher();
            });
            $('.btnPrint').on('click', function(e) {
                e.preventDefault();

                purchase.showAllRows();
                window.open(base_url + 'application/views/reportprints/vouchers_reports.php', "Stock Report", 'width=1000, height=842');
            });
            $('.btnPrintPdf').on('click', function(e) {
                e.preventDefault();
                Print_Voucher(1);
            });
            $('.btnPrintPdfWithoutHeader').on('click', function(e) {
                e.preventDefault();
                Print_Voucher(0);
            });

            $('.btnSelCre').on('click', function(e) {
                e.preventDefault();

                $(this).addClass('btn-primary');
                $(this).siblings('.btnSelCre').removeClass('btn-primary');
            });

            shortcut.add("F9", function() {
                $('.btnPrint').trigger('click');
            });
             shortcut.add("F8", function() {
                Print_Voucher(1);
            });
            shortcut.add("F6", function() {
              
                var error = validateSearch();
                if (!error) {
                var from = $('#from_date').val();
                var to = $('#to_date').val();
                var company_id = $('#cid').val();
                var what = getCurrentView();                
                fetchVouchers(from, to, what,company_id,0);
            }
            });
            shortcut.add("F5", function() {
                self.resetVoucher();
            });

		},
        showAllRows : function () {

            var oSettings = purchase.dTable.fnSettings();
            oSettings._iDisplayLength = 50000;

            purchase.dTable.fnDraw();
        },
		// instead of reseting values reload the page because its cruel to write to much code to simply do that
		resetVoucher : function() {
			general.reloadWindow();
		}
	}

};

var purchase = new Purchase();
purchase.init();