var syncdatabase = function () {

	var sync_db = function () {
		$.ajax({

			url : base_url + 'index.php/syncdatabase/syncdb',
			type : 'POST',
			data : {'company_id': $('#cid').val()},
			dataType : 'JSON',
			success : function(data) {
				alert(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	
return {

		init: function  () {
			this.bindUI();
		},

		bindUI: function () {
			$('.btnLocalToServer').on('click',function () {
				sync_db();
			});
		},

	}
};
var Syncdatabase = new syncdatabase();
Syncdatabase.init(); 