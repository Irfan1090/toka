var StaffAttendance = function() {

	var settings = {

		txtdcno : $('#txtdcno'),
		txtMaxdcnoHidden : $('#txtMaxdcnoHidden'),
		txtdcnoHidden : $('#txtdcnoHidden'),

		dept_dropdown : $('#dept_dropdown'),
		status_dropdown : $('#status_dropdown'),

		counter : 1,

		// buttons
		btnSearch : $('.btnSearch'),
		btnReset : $('.btnReset'),
		btnSave : $('.btnSave'),
		btnDelete : $('.btnDelete')
	};

	var getMaxId = function() {

		$.ajax({

			url : base_url + 'index.php/attendance/getMaxStaffAtndId',
			type : 'POST',
			dataType : 'JSON',
			success : function(data) {

				$(settings.txtdcno).val(data);
				$(settings.txtMaxdcnoHidden).val(data);
				$(settings.txtdcnoHidden).val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// checks for the empty fields
	var validateSearch = function() {

		var errorFlag = false;
		var dept_dropdown = $(settings.dept_dropdown).val();

		// remove the error class first
		$(settings.dept_dropdown).removeClass('inputerror');

		if ( dept_dropdown === '' || dept_dropdown === null ) {
			$(settings.dept_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var search = function(did) {

		$.ajax({
			url : base_url + 'index.php/staff/fetchStaffReportByStatus',
			type : 'POST',
			data : { 'did' : did, 'status' : '1' },
			async : false,
			dataType : 'JSON',
			success : function(data) {

				//$('#atnd-table').find('tbody tr :not(.dataTables_empty)').remove();
				settings.counter = 1;
				if (typeof $('#atnd-table').dataTable() != 'undefined') {
		            $('#atnd-table').dataTable().fnDestroy();
		            $('#atnd-table tbody').empty();
		        }

				if (data === 'false') {
					alert('No record found.');
				} else {
					populateData(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}

		});
	}

	var populateData = function(data) {

		// removes all rows
		//$('#atnd-table').find('tbody tr :not(.dataTables_empty)').remove();
		var status = $(settings.status_dropdown).val();
		var count = 0;

		$.each(data, function(index, elem) {

			var attendanceId = 'attendance'+(++count);
			$('#atnd-table').dataTable().fnAddData( [
				settings.counter++,
				"<span class='dept_name' data-did='"+ elem.did +"'>"+ elem.dept_name +"</span>",
				"<span class='name' data-staid='"+ elem.staid +"'>"+ elem.name +"</span>",
				"<span class='designation'>"+ elem.type +"</span>",
				"<span class='shift_name' data-shid='"+ elem.shid +"'>"+ elem.shift_name +"</span>",
				"<span class='tin'>"+(elem.tin).substring(11)+"</span>",
				"<span class='tout'>"+(elem.tout).substring(11)+"</span>",
				//"<input type='text' class='tableInputCell atnd-status' list='status' value='"+ status +"'/>" 
				"<select class='atnd-status' id='"+attendanceId+"'>"+
				"<option value='Present' >Present</option>"+
				"<option value='Absent' >Absent</option>"+
				"<option value='Paid Leave' >Paid Leave</option>"+
				"<option value='Unpaid Leave' >Unpaid Leave</option>"+
				"<option value='Rest Day' >Rest Day</option>"+
				"<option value='Gusted Holiday' >Gusted Holiday</option>"+
				"<option value='Short Leave' >Short Leave</option>"+
				"<option value='Outdoor' >Outdoor</option>"+
				"</select>",
				]
			);

			$('#attendance'+count).val(status);
		});

		var oSettings1 = $('#atnd-table').dataTable().fnSettings();
    	oSettings1._iDisplayLength = 100;
    	$('#atnd-table').dataTable().fnDraw();
	}

	var save = function( atndcs, dcno ) {

		$.ajax({
			url : base_url + 'index.php/attendance/saveStaff',
			type : 'POST',
			data : { 'atndcs' : JSON.stringify(atndcs), 'dcno' : dcno, 'voucher_type_hidden' : $('#voucher_type_hidden').val() },
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'true') {
					alert('An internal error occured while saving voucher. Please try again.');
				} else {
					alert('Attendance saved successfully.');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var post = function( atndcVouchers ) {

		var obj = { 'vouchers' : atndcVouchers };
		$.ajax({
			url : base_url + 'index.php/attendance/postStaff',
			type : 'POST',
			data : { 'postData' : JSON.stringify( obj ) },
			dataType : 'JSON',
			success : function(data) {

				if (data.length === 0) {
					alert('Attendance saved successfully.');
					general.reloadWindow();
				} else {
					showErrorMessage(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var showErrorMessage = function(data) {

		var date = "";
		var did = "";
		var message = "Following voucher were already saved...\n";
		$.each(data, function(index, elem) {

			if (date !== elem.date || did !== elem.did) {
				
				message += elem.date + '        =>        ' + elem.dept_name + '\n';

				date = elem.date;	
				did = elem.did;
			}
		});

		alert(message);
		general.reloadWindow();
	}

	var isVoucherAlreadySaved = function(date, dids, dcno) {

		var response = false;
		$.ajax({

			url : base_url + 'index.php/attendance/isVoucherAlreadySaved',
			data : {'date': date, 'dids': dids, 'dcno' : dcno},
			async : false,
			type : 'POST',
			dataType : 'JSON',
			success : function(data) {

				response = data;
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});

		return response;
	}

	var getSaveObject = function() {

		var atndcs = [];

		var d = new Date();
		var t = d.toTimeString().substr(0, 8);
		var _postdate = d.getFullYear() + "-" + (d.getMonth()+1) + "-" + d.getDay() + " " + t;

		var _dcno = $(settings.txtdcnoHidden).val();
		var _did = $(settings.dept_dropdown).val();
		var _date = $('#current_date').val();

		$('#atnd-table').find('tbody tr').each(function(index, elem) {

			var _did = $.trim($(this).closest('tr').find('span.dept_name').data('did'));
			var _staid = $.trim($(this).closest('tr').find('span.name').data('staid'));
			var _shid = $.trim($(this).closest('tr').find('span.shift_name').data('shid'));
			var _tin = $.trim($(this).closest('tr').find('span.tin').text());
			var _tout = $.trim($(this).closest('tr').find('span.tout').text());
			var _status = $.trim($(this).closest('tr').find('select.atnd-status').val());
			//var _status = $.trim($(this).closest('tr').find('input.atnd-status').val());

			var atnd = {

				dcno : _dcno,
				did : _did,
				staid : _staid,
				shid : _shid,
				tin : _tin,
				tout : _tout,
				status : _status,
				postdate : _postdate,
				date : _date,
				etype : 'vr_atnd'
			}

			atndcs.push(atnd);
		});

		return atndcs;
	}

	var getSavePostObject = function() {
		// alert('dd');
		var atndcVouchers = [];
		var atndcs = [];
		var _from = $('#from_date').val();
		var _to = $('#to_date').val();
		var _dates = dateRange(_from, _to);

		var _dcno = $(settings.txtdcnoHidden).val();
		var _did = $(settings.dept_dropdown).val();

		var d = new Date();
		var t = d.toTimeString().substr(0, 8);
		var _postdate = d.getFullYear() + "-" + (d.getMonth()+1) + "-" + d.getDay() + " " + t;

		$('#atnd-table').find('tbody tr').each(function(index, elem) {

			var _did = $.trim($(this).closest('tr').find('span.dept_name').data('did'));
			var _staid = $.trim($(this).closest('tr').find('span.name').data('staid'));
			var _shid = $.trim($(this).closest('tr').find('span.shift_name').data('shid'));
			var _tin = $.trim($(this).closest('tr').find('span.tin').text());
			var _tout = $.trim($(this).closest('tr').find('span.tout').text());
			var _status = $.trim($(this).closest('tr').find('select.atnd-status').val());
			//var _status = $.trim($(this).closest('tr').find('input.atnd-status').val());
			//alert(_did);
			// console.log(_did);
			var atnd = {};
			atnd.dcno = _dcno;
			atnd.did = _did;
			atnd.staid = _staid;
			atnd.shid = _shid;
			atnd.tin = _tin;
			atnd.tout = _tout;
			atnd.status = _status;
			atnd.postdate = _postdate;
			atnd.date = '';
			atnd.etype = 'vr_atnd';

			atndcs.push(atnd);
		});

		// loop through each date and make entery on each single date
		$.each(_dates, function(index, elemdate) {
			console.log(elemdate);

			var atndVoucher = [];

			$.each(atndcs, function(index, elem) {
				var t = $.extend({}, elem);
				t.date = elemdate;
				atndVoucher.push(t);
			});
			atndcVouchers.push(atndVoucher);
		});

		return atndcVouchers;
	}

	var fetch = function(dcno) {

		$.ajax({
			url : base_url + 'index.php/attendance/fetchStaff',
			type : 'POST',
			data : { 'dcno' : dcno },
			dataType : 'JSON',
			success : function(data) {

				settings.counter = 1;
				//$('#atnd-table').find('tbody tr :not(.dataTables_empty)').remove();

				if (typeof $('#atnd-table').dataTable() != 'undefined') {
		            $('#atnd-table').dataTable().fnDestroy();
		            $('#atnd-table tbody').empty();
		        }
				
				if (data === 'false') {

					alert('No data found');
					general.reloadWindow();
				} else {
					populateVchrData(data);
					$('.btnSave').attr('disabled', false);
					general.setUpdatePrivillage();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateVchrData = function(data) {

		$('#voucher_type_hidden').val('edit');		
		$(settings.txtdcno).val(data[0]['dcno']);
		$(settings.txtdcnoHidden).val(data[0]['dcno']);
	
		var count = 0;
		$('#current_date').val( data[0]['date'].substring(0, 10));

		$.each(data, function(index, elem) {

			var attendanceId = 'attendance'+(++count);

			var tin = elem.tin;
			var tout = elem.tout;


			$('#atnd-table').dataTable().fnAddData( [
				settings.counter++,
				"<span class='dept_name' data-did='"+ elem.did +"'>"+ (elem.dept_name) ? elem.dept_name : '-' +"</span>",
				"<span class='name' data-staid='"+ elem.staid +"'>"+ elem.staff_name +"</span>",
				"<span class='designation'>"+ elem.type +"</span>",
				"<span class='shift_name' data-shid='"+ elem.shid +"'>"+ elem.shift_name +"</span>",
				"<span class='tin'>"+ tin +"</span>",
				"<span class='tout'>"+ tout +"</span>",
				//"<input type='text' class='tableInputCell atnd-status' list='status' value='"+ elem.status +"'/>" 
				"<select class='atnd-status' id='"+attendanceId+"' data-atid='" + elem.atid + "'>"+
				"<option value='present' >Present</option>"+
				"<option value='absent' >Absent</option>"+
				"<option value='paid leave' >Paid Leave</option>"+
				"<option value='unpaid leave' >Unpaid Leave</option>"+
				"<option value='rest day' >Rest Day</option>"+
				"<option value='gusted holiday' >Gusted Holiday</option>"+
				"<option value='short heave' >Short Leave</option>"+
				"<option value='outdoor' >Outdoor</option>"+
				"</select>"
				]
			);

			$('#attendance'+count).val((elem.status).toLowerCase());
		});

		var oSettings1 = $('#atnd-table').dataTable().fnSettings();
    	oSettings1._iDisplayLength = 100;
    	$('#atnd-table').dataTable().fnDraw();
	}

	var getdids = function() {

		var dids = [];
		$('#atnd-table tbody tr span.dept_name').each(function(index, elem){
			var did = $(this).data('did');
			if($.inArray(did, dids) == -1){
				dids.push(did);
			};
		});

		return dids;
	}

	var isDepartmentAlreadyAdded = function(did) {

		var error = false;

		$('#atnd-table tbody tr span.dept_name').each(function(index, elem){
			var _did = $(this).data('did');
			if (_did == did) {
				error = true;
			}
		});

		return error;
	}

	var dateRange = function(from, to) {

		var dates = general.getDateRange(from, to);

		var _dates = [];
		$.each(dates, function(index, elem) {

			var d = elem.getDate();
			var y = elem.getFullYear();
			var m = elem.getMonth() + 1;

			var _date = y + '-' + m + '-' + d;
			_dates.push(_date);
		});

		return _dates;
	}

	var print = function() {
		window.open(base_url + 'application/views/print/attendancevoucher.php', 'Attendance Voucher', 'width=720, height=850');
	}

	var deleteVoucher = function(dcno) {

		$.ajax({
			url : base_url + 'index.php/attendance/deleteAttendance',
			type : 'POST',
			data : { 'dcno' : dcno, 'etype' : 'vr_atnd' },
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No data found');
				} else {
					alert('Voucher deleted successfully');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	return {

		init : function() {

			this.bindUI();
		},

		bindUI : function() {

			var self = this;
			$('#voucher_type_hidden').val('new');		

			$(settings.btnSave).on('click', function(e) {
				e.preventDefault();
				//self.initSave();
				if ($('#voucher_type_hidden').val()=='edit' && $('.btnSave').data('updatebtn')==0 ){
					alert('Sorry! you have not update rights..........');
				}else if($('#voucher_type_hidden').val()=='new' && $('.btnSave').data('insertbtn')==0){
					alert('Sorry! you have not insert rights..........');
				}else{
					e.preventDefault();
					staffAttendance.showAllRows();
					self.initSave();
				}
			});
			$(settings.btnSearch).on('click', function(e) {
				e.preventDefault();
				self.initSearch();
			});
			$(settings.btnReset).on('click', function(e) {
				//e.preventDefault();
				//self.resetFrom();
                window.location.reload();
			});

			$(settings.btnDelete).on('click', function(e) {
				e.preventDefault();

				if ($('#voucher_type_hidden').val()=='edit' && $('.btnSave').data('deletebtn')==0 ){
					alert('Sorry! you have not delete rights..........');
				}else{

					// alert($('#voucher_type_hidden').val() +' - '+ $('.btnSave').data('deletebtn') );
					if ( $(settings.txtdcno).val().trim() !== "" ) {
						var dcno = $.trim($(settings.txtdcno).val());
						if (confirm('Are you sure to delete this voucher?'))
						deleteVoucher(dcno);
					}
				}

				// if ( $(settings.txtdcno).val().trim() !== "" ) {
				// 	var dcno = $.trim($(settings.txtdcno).val());
				// 	if (confirm('Are you sure to delete this voucher?'))
				// 	deleteVoucher(dcno);
				// }
			});

			$(settings.txtdcno).on('keypress', function(e) {

				// check if enter key is pressed
				if (e.keyCode === 13) {
					e.preventDefault();
					// get the based on the id entered by the user
					if ( $(settings.txtdcno).val().trim() !== "" ) {

						var dcno = $.trim($(settings.txtdcno).val());
						fetch(dcno);
					}
				}
			});

			$('#txtdcno').on('change', function() {
				fetch($(this).val());
			});

			// $('#txtStaffId').on('change', function(e) {
			// 		e.preventDefault();
			// 		if ( $('#txtStaffId').val().trim() !== "" ) {
			// 			var dcno = $.trim($('#txtStaffId').val());
			// 			fetch(dcno);
			// 		}
			// 	}
			// });



			$('.btnPost').on('click', function(e) {
				e.preventDefault();
				self.initPost();
			});

			$('#autoattendance').on('click', function() {
				$('.post-container').toggleClass('hide');
			});

			$('.btnPrint').on('click', function(e) {
				e.preventDefault();

				if ( $('.btnSave').data('printbtn')==0 ){
					
					alert('Sorry! you have not print rights..........');
				}else{

					print();
				}
			});

			$('#txtStaffId').on('change', function() {
				fetch($(this).val());
			});

			getMaxId();
		},

		initPost : function() {

			var rowsCount = $('#atnd-table').find("tbody tr :not(.dataTables_empty)").length;
			if (rowsCount > 0 ) {

				var from = $('#from_date').val();
				var to = $('#to_date').val();
				if (from > to) {
					alert('Starting date can\'t be less than ending date!');
				} else {
					var saveObj = getSavePostObject();

					post(saveObj);
				}
			} else {
				alert('No data found to save.');
			}
		},

		initSave : function() {

			var rowsCount = $('#atnd-table').find("tbody tr :not(.dataTables_empty)").length;
			if (rowsCount > 0 ) {

				var saveObj = getSaveObject();
				var dcno = $(settings.txtdcnoHidden).val();

				var date = $('#current_date').val();

				// get all the department id from table
				var dids = getdids();

				// this check is made to check that if we are updating the voucher or not if both max val and hidden
				// val are equal its mean that we are saving a new voucher else wise updating voucher
				// and if we are updaing the voucher then the dcno is filled and send to the query along with the dids (department ids)
				// to check that whether the new added departments attendance is already saved or not
				var _dcno = "";
				if ($('#txtMaxdcnoHidden').val() != $('#txtdcnoHidden').val()) {
					_dcno = dcno;
				}
				// checks if voucher is already saved or not
				var isSaved = isVoucherAlreadySaved(date, dids, _dcno);
				$('#atnd-table').find('.duplicate').removeClass('duplicate');

				if (isSaved == false) {
					save( saveObj, dcno );
				} else {
					$('#atnd-table tbody tr span.dept_name').each(function(index, elem) {
						var did = $(elem).data('did');
						if ($.inArray(did, isSaved)) {
							$(elem).closest('tr').find('td').addClass('duplicate');
						} else {
							$(elem).closest('tr').find('td').removeClass('duplicate');
						}
					});

					alert('Attendnce on that date is already saved.');
				}
			} else {
				alert('No data found to save.');
			}
		},

		initSearch : function() {

			var did = $(settings.dept_dropdown).val();
			var error = isDepartmentAlreadyAdded(did);

			if (!error) {
				error = validateSearch();
				if (!error) {
					search(did);
				} else {
					alert('Correct the errors...');
				}
			} else {
				alert('Department already added!');
			}
		},

		resetFrom : function() {

			$('.inputerror').removeClass('inputerror');
			$('#current_date').val( new Date());
			$(settings.dept_dropdown).val('');

			settings.counter = 1;
			// removes all rows
			$('#atnd-table').dataTable().fnClearTable();;

			getMaxId();
			general.setPrivillages();
		},
		showAllRows : function () {

        	var oSettings = $('#atnd-table').dataTable().fnSettings();
        	oSettings._iDisplayLength = 50000;
        	$('#atnd-table').dataTable().fnDraw();
        }
	}

};

var staffAttendance = new StaffAttendance();
staffAttendance.init();